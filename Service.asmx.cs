﻿/*
 * Início do desenvolvimento: 29/06/2016
 * Desenvolvido por: Marco Sá
 * Descrição: Criação de diversos serviços para o sistema Novo GTT que visa a utilização dos metodos
 *            através de uma aplicação c# e também por aplicação JAVA para mobile
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Data;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using System.Drawing;
using wsngtt.Classes;

namespace wsngtt
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://ficor.com.br/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSNGTT : System.Web.Services.WebService
    {

        [WebMethod]
        ///<summary>
        /// Este método fara a validação do usuário no sistema permitindo ou não o acesso dele ao mesmo com base em seu usuário e senha.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string Login(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui a classe de usuários
            Classes.clsUsuario clsUsur = new Classes.clsUsuario();
            Classes.clsAtividades clsAtividades = new Classes.clsAtividades();

            //aqui o objeto do usuário
            Objetos.objUsuario objUsur = new Objetos.objUsuario();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";


            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarLoginXml(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + resultado + "</descerro></ErroRetorno>";
                    return xmlRetorno;
                }

                XmlNodeList email_matricula = doc.GetElementsByTagName("user_matricula_mail");
                XmlNodeList senha = doc.GetElementsByTagName("user_senha");

                if (email_matricula.ToString().IndexOf("@") >= 0)
                {
                    objUsur.Usur_email = email_matricula[0].InnerText.Trim();
                }
                else
                {
                    objUsur.Usur_matricula = email_matricula[0].InnerText.Trim();
                }
                objUsur.Usur_senha = senha[0].InnerText.Trim();

                DataSet dsUser = new DataSet();

                //aqui crio a primeira linha do xml de retorno que é comum tanto em caso de sucesso como de falha
                xmlRetorno = "<?xml version='1.0'?><usuario>";

                if (objUsur.Usur_email != null)
                {
                    dsUser = clsUsur.Login(objUsur.Usur_email, objUsur.Usur_senha);
                }
                else
                {
                    dsUser = clsUsur.Login(objUsur.Usur_matricula, objUsur.Usur_senha);
                }

                if (dsUser.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsUser.Tables[0].Rows[0]["usur_cd_usur"].ToString().Trim() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<usur_nome>" + dsUser.Tables[0].Rows[0]["usur_nome"].ToString().Trim() + "</usur_nome>";
                    xmlRetorno = xmlRetorno + "<perf_cod_perf>" + dsUser.Tables[0].Rows[0]["perf_cod_perfil"].ToString().Trim() + "</perf_cod_perf>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empresa>" + dsUser.Tables[0].Rows[0]["emen_cod_empr"].ToString().Trim() + "</emen_cod_empresa>";
                    xmlRetorno = xmlRetorno + "<emen_carregamento_emen>" + dsUser.Tables[0].Rows[0]["emen_carregamento_emen"].ToString() + "</emen_carregamento_emen>";


                    DataSet dsAtiv = new DataSet();

                    dsAtiv = clsAtividades.SelecionarTodasPermissoesUsuario(Convert.ToInt32(dsUser.Tables[0].Rows[0]["usur_cd_usur"].ToString().Trim()));
                    for (int i = 0; i <= dsAtiv.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<atividades>";
                        xmlRetorno = xmlRetorno + "<ativ_cod_ativ>" + dsAtiv.Tables[0].Rows[i]["ativ_cod_ativ"].ToString().Trim() + "</ativ_cod_ativ>";
                        xmlRetorno = xmlRetorno + "<ativ_nm_ativ>" + dsAtiv.Tables[0].Rows[i]["ativ_nm_ativ"].ToString().Trim() + "</ativ_nm_ativ>";
                        xmlRetorno = xmlRetorno + "</atividades>";
                    }


                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</usuario>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";
                return xmlRetorno;
            }

        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do usuário para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarUsuario(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsUsuario clsUsur = new Classes.clsUsuario();

            //aqui o objeto da empresa
            Objetos.objUsuario objusur = new Objetos.objUsuario();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarCadastroAlteracaoUsuarioXML(docValidar);
                /*if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><descerro><status>Erro</status><descerro>" + resultado + "</descerro></descerro>";
                    return xmlRetorno;
                }*/

                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList usur_matricula = doc.GetElementsByTagName("usur_matricula");
                XmlNodeList usur_nome = doc.GetElementsByTagName("usur_nome");
                XmlNodeList usur_apelido = doc.GetElementsByTagName("usur_apelido");
                XmlNodeList usur_email = doc.GetElementsByTagName("usur_email");
                XmlNodeList usur_senha = doc.GetElementsByTagName("usur_senha");
                XmlNodeList perf_cod_perfil = doc.GetElementsByTagName("perf_cod_perfil");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList usur_dt_criacao = doc.GetElementsByTagName("usur_dt_criacao");
                XmlNodeList usur_ativo = doc.GetElementsByTagName("usur_ativo");


                objusur.Usur_matricula = usur_matricula[0].InnerText.ToString();
                objusur.Usur_nome = usur_nome[0].InnerText.ToString();
                objusur.Usur_apelido = usur_apelido[0].InnerText.ToString();
                objusur.Usur_email = usur_email[0].InnerText.ToString();
                objusur.Usur_senha = usur_senha[0].InnerText.ToString();
                objusur.Perf_cod_perf = Convert.ToInt32(perf_cod_perfil[0].InnerText.ToString());
                objusur.Emen_cod_empresa = Convert.ToInt32(emen_cod_empr[0].InnerText.ToString());
                objusur.Usur_dt_usur = Convert.ToDateTime(usur_dt_criacao[0].InnerText.Trim());
                objusur.Usur_ativo = Convert.ToBoolean(usur_ativo[0].InnerText.ToString());



                int cod_usur_retorno = 0;

                if (usur_cd_usur[0].InnerText.Trim() == "")
                {

                    cod_usur_retorno = clsUsur.InserirUsuario(objusur);

                }
                else
                {
                    objusur.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());
                    cod_usur_retorno = objusur.Usur_cd_usur;
                    clsUsur.AlterarUsuario(objusur);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<usuario>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<usur_cd_usur>" + cod_usur_retorno + "</usur_cd_usur>";
                xmlRetorno = xmlRetorno + "</usuario>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método retornará a lista de todos os usuários cadastrados no sistema.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarUsuariosTodos()
        {
            //aqui instâncio a classe de usuário
            Classes.clsUsuario clsUsur = new Classes.clsUsuario();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsUsur = new DataSet();
                dsUsur = clsUsur.SelecionarTodosUsuarios();

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<usuarios>";
                if (dsUsur.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsUsur.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<usuario>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsUsur.Tables[0].Rows[i]["usur_cd_usur"].ToString().Trim() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<usur_matricula>" + dsUsur.Tables[0].Rows[i]["usur_matricula"].ToString().Trim() + "</usur_matricula>";
                        xmlRetorno = xmlRetorno + "<usur_nome>" + dsUsur.Tables[0].Rows[i]["usur_nome"].ToString().Trim() + "</usur_nome>";
                        xmlRetorno = xmlRetorno + "<usur_apelido>" + dsUsur.Tables[0].Rows[i]["usur_apelido"].ToString().Trim() + "</usur_apelido>";
                        xmlRetorno = xmlRetorno + "<usur_email>" + dsUsur.Tables[0].Rows[i]["usur_email"].ToString().Trim() + "</usur_email>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsUsur.Tables[0].Rows[i]["emen_cod_empr"].ToString().Trim() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<perf_cod_perfil>" + dsUsur.Tables[0].Rows[i]["perf_cod_perfil"].ToString().Trim() + "</perf_cod_perfil>";
                        xmlRetorno = xmlRetorno + "<usur_dt_criacao>" + Convert.ToDateTime(dsUsur.Tables[0].Rows[i]["usur_dt_criacao"].ToString().Trim()).ToString("MM/dd/yyyy") + "</usur_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_ativo>" + dsUsur.Tables[0].Rows[i]["usur_ativo"].ToString().Trim() + "</usur_ativo>";
                        xmlRetorno = xmlRetorno + "</usuario>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "<usuario></usuario>";
                }

                xmlRetorno = xmlRetorno + "</usuarios>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método retornará a lista de todos os usuários cadastrados no sistema.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarProdutosMEEmpresaUsuario(string xmlString)
        {
            //instâncio a classe de usuários
            Classes.clsProdutoMaterial clsProdutosMateriais = new Classes.clsProdutoMaterial();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);



                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");

                DataSet dsTipoProdutoMaterial = new DataSet();
                string familia = "";
                dsTipoProdutoMaterial = clsProdutosMateriais.SelecionarProdutosMEEmpresaUsuario(usur_cd_usur[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                if (dsTipoProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    //tb_prma.prma_cod_prma, tb_prma.prma_descricao, tb_prma.famp_cd_famp]
                    xmlRetorno = xmlRetorno + "<produtos>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsTipoProdutoMaterial.Tables[0].Rows.Count - 1; i++)
                    {

                        xmlRetorno = xmlRetorno + "<produto>";
                        if (dsTipoProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "1")
                        {
                            familia = "Pallet";
                        }
                        else if (dsTipoProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "2")
                        {
                            familia = "Topo";
                        }
                        else if (dsTipoProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "3" || dsTipoProdutoMaterial.Tables[0].Rows[i][""].ToString() == "5")
                        {
                            familia = "Folha";
                        }
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsTipoProdutoMaterial.Tables[0].Rows[i]["prma_cod_prma"].ToString().Trim() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsTipoProdutoMaterial.Tables[0].Rows[i]["prma_descricao"].ToString().Trim() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<prma_tipo>" + familia + "</prma_tipo>";
                        xmlRetorno = xmlRetorno + "</produto>";
                    }
                    xmlRetorno = xmlRetorno + "<produtos>";
                }
                else
                {

                    xmlRetorno = xmlRetorno + "<tipoproduto>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</tipoproduto>";
                }


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }
        [WebMethod]
        ///<summary>
        /// Este método retornará a lista de todos os usuários cadastrados no sistema.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarUsuariosAtivos()
        {
            //aqui instâncio a classe de usuário
            Classes.clsUsuario clsUsur = new Classes.clsUsuario();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsUsur = new DataSet();
                dsUsur = clsUsur.SelecionarUsuariosAtivos();

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<usuarios>";


                if (dsUsur.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsUsur.Tables[0].Rows.Count - 1; i++)
                    {

                        xmlRetorno = xmlRetorno + "<usuario>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsUsur.Tables[0].Rows[i]["usur_cd_usur"].ToString().Trim() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<usur_matricula>" + dsUsur.Tables[0].Rows[i]["usur_matricula"].ToString().Trim() + "</usur_matricula>";
                        xmlRetorno = xmlRetorno + "<usur_nome>" + dsUsur.Tables[0].Rows[i]["usur_nome"].ToString().Trim() + "</usur_nome>";
                        xmlRetorno = xmlRetorno + "<usur_apelido>" + dsUsur.Tables[0].Rows[i]["usur_apelido"].ToString().Trim() + "</usur_apelido>";
                        xmlRetorno = xmlRetorno + "<usur_email>" + dsUsur.Tables[0].Rows[i]["usur_email"].ToString().Trim() + "</usur_email>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsUsur.Tables[0].Rows[i]["emen_cod_empr"].ToString().Trim() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<perf_cod_perfil>" + dsUsur.Tables[0].Rows[i]["perf_cod_perfil"].ToString().Trim() + "</perf_cod_perfil>";
                        xmlRetorno = xmlRetorno + "<usur_dt_criacao>" + Convert.ToDateTime(dsUsur.Tables[0].Rows[i]["usur_dt_criacao"].ToString().Trim()).ToString("MM/dd/yyyy") + "</usur_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_ativo>" + dsUsur.Tables[0].Rows[i]["usur_ativo"].ToString().Trim() + "</usur_ativo>";
                        xmlRetorno = xmlRetorno + "</usuario>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "<usuario></usuario>";
                }

                xmlRetorno = xmlRetorno + "</usuarios>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o id da unidade de medida a ser selecionada
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>

        public string SelecionarUsuarioPorCodigo(string xmlString)
        {
            //instâncio a classe de usuários
            Classes.clsUsuario clsUsur = new Classes.clsUsuario();

            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarUmUsuario(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + resultado + "</descerro></ErroRetorno>";
                    return xmlRetorno;
                }

                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");

                DataSet dsUsur = new DataSet();
                int i = 0;
                dsUsur = clsUsur.SelecionarUsuario(Convert.ToInt32(usur_cd_usur[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                if (dsUsur.Tables[0].Rows.Count > 0)
                {

                    xmlRetorno = xmlRetorno + "<usuarios>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<usuario>";
                    xmlRetorno = xmlRetorno + "<usur_matricula>" + dsUsur.Tables[0].Rows[i]["usur_matricula"].ToString().Trim() + "</usur_matricula>";
                    xmlRetorno = xmlRetorno + "<usur_nome>" + dsUsur.Tables[0].Rows[i]["usur_nome"].ToString().Trim() + "</usur_nome>";
                    xmlRetorno = xmlRetorno + "<usur_apelido>" + dsUsur.Tables[0].Rows[i]["usur_apelido"].ToString().Trim() + "</usur_apelido>";
                    xmlRetorno = xmlRetorno + "<usur_email>" + dsUsur.Tables[0].Rows[i]["usur_email"].ToString().Trim() + "</usur_email>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsUsur.Tables[0].Rows[i]["emen_cod_empr"].ToString().Trim() + "</emen_cod_empr>";
                    xmlRetorno = xmlRetorno + "<perf_cod_perfil>" + dsUsur.Tables[0].Rows[i]["perf_cod_perfil"].ToString().Trim() + "</perf_cod_perfil>";
                    xmlRetorno = xmlRetorno + "<usur_dt_criacao>" + Convert.ToDateTime(dsUsur.Tables[0].Rows[i]["usur_dt_criacao"].ToString().Trim()).ToString("MM/dd/yyyy") + "</usur_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_ativo>" + dsUsur.Tables[0].Rows[i]["usur_ativo"].ToString().Trim() + "</usur_ativo>";
                    xmlRetorno = xmlRetorno + "</usuario>";
                    xmlRetorno = xmlRetorno + "</usuarios>";
                }
                else
                {

                    xmlRetorno = xmlRetorno + "<usuarios>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</usuarios>";
                }


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o id do usuário do qual desejamos saber as atividades as quais o mesmo tem permissão
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>

        public string SelecionarPermissoesUsuarioTodas(string xmlString)
        {
            //instâncio a classe de usuários
            Classes.clsAtividades clsAtividades = new Classes.clsAtividades();

            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarAtividadesUsuario(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + resultado + "</descerro></ErroRetorno>";
                    return xmlRetorno;
                }

                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");

                DataSet dsAtiv = new DataSet();

                dsAtiv = clsAtividades.SelecionarTodasPermissoesUsuario(Convert.ToInt32(usur_cd_usur[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<atividades>";

                if (dsAtiv.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsAtiv.Tables[0].Rows.Count - 1; i++)
                    {


                        xmlRetorno = xmlRetorno + "<atividade>";
                        xmlRetorno = xmlRetorno + "<ativ_cod_ativ>" + dsAtiv.Tables[0].Rows[i]["ativ_cod_ativ"].ToString().Trim() + "</ativ_cod_ativ>";
                        xmlRetorno = xmlRetorno + "<ativ_nm_ativ>" + dsAtiv.Tables[0].Rows[i]["ativ_nm_ativ"].ToString().Trim() + "</ativ_nm_ativ>";
                        xmlRetorno = xmlRetorno + "</atividade>";

                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }

                xmlRetorno = xmlRetorno + "</atividades>";





                //return single table inside of dataset


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método recebera os dados da empresa para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarEmpresa(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

            //aqui o objeto da empresa
            Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                /*string resultado = clsValidar.ValidarCadastroAlteracaoEmpresaXML(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList emen_cnpj = doc.GetElementsByTagName("emen_cnpj");
                XmlNodeList emen_nome = doc.GetElementsByTagName("emen_nome");
                XmlNodeList emen_nomered = doc.GetElementsByTagName("emen_nomered");
                XmlNodeList emen_lograd = doc.GetElementsByTagName("emen_lograd");
                XmlNodeList emen_numero = doc.GetElementsByTagName("emen_numero");
                XmlNodeList emen_comple = doc.GetElementsByTagName("emen_comple");
                XmlNodeList emen_bairro = doc.GetElementsByTagName("emen_bairro");
                XmlNodeList emen_cep = doc.GetElementsByTagName("emen_cep");
                XmlNodeList emen_uf = doc.GetElementsByTagName("emen_uf");
                XmlNodeList emen_cidade = doc.GetElementsByTagName("emen_cidade");
                XmlNodeList emen_ddd = doc.GetElementsByTagName("emen_ddd");
                XmlNodeList emen_tel = doc.GetElementsByTagName("emen_tel");
                XmlNodeList emen_email = doc.GetElementsByTagName("emen_email");
                XmlNodeList emen_dt_criacao = doc.GetElementsByTagName("emen_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList emen_ativo = doc.GetElementsByTagName("emen_ativo");
                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");
                XmlNodeList emen_forn_emen = doc.GetElementsByTagName("emen_forn_emen");
                XmlNodeList emen_carregamento_emen = doc.GetElementsByTagName("emen_carregamento_emen");
                XmlNodeList emen_nr_folhas_kit = doc.GetElementsByTagName("emen_nr_folhas_kit");


                objEmpresa.Emen_cnpj = emen_cnpj[0].InnerText.Trim();
                objEmpresa.Emen_nome = emen_nome[0].InnerText.Trim();
                objEmpresa.Emen_nomered = emen_nomered[0].InnerText.Trim();
                objEmpresa.Emen_lograd = emen_lograd[0].InnerText.Trim();
                objEmpresa.Emen_numero = emen_numero[0].InnerText.Trim();
                objEmpresa.Emen_comple = emen_comple[0].InnerText.Trim();
                objEmpresa.Emen_bairro = emen_bairro[0].InnerText.Trim();
                objEmpresa.Emen_cep = emen_cep[0].InnerText.Trim();
                objEmpresa.Emen_uf = emen_uf[0].InnerText.Trim();
                objEmpresa.Emen_cidade = emen_cidade[0].InnerText.Trim();
                objEmpresa.Emen_ddd = emen_ddd[0].InnerText.Trim();
                objEmpresa.Emen_tel = emen_tel[0].InnerText.Trim();
                objEmpresa.Emen_email = emen_email[0].InnerText.Trim();
                objEmpresa.Emen_dt_criacao = Convert.ToDateTime(emen_dt_criacao[0].InnerText.Trim());
                objEmpresa.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());
                objEmpresa.Emen_ativo = Convert.ToBoolean(emen_ativo[0].InnerText.Trim());
                objEmpresa.Emen_forn_emen = Convert.ToBoolean(emen_forn_emen[0].InnerText.Trim());
                objEmpresa.ObjGrupo.Grem_cd_grem = Convert.ToInt32(grem_cd_grem[0].InnerText.Trim());
                objEmpresa.Emen_carregamento_emen = Convert.ToBoolean(emen_carregamento_emen[0].InnerText.Trim());
                objEmpresa.Emen_nr_folhas_kit = Convert.ToInt32(emen_nr_folhas_kit[0].InnerText.Trim());
                int emen_cod_empr_retorno = 0;

                if (emen_cod_empr[0].InnerText.Trim() == "")
                {

                    emen_cod_empr_retorno = clsEmpresa.cadastraEmpresa(objEmpresa);

                }
                else
                {
                    objEmpresa.Emen_cod_empr = Convert.ToInt32(emen_cod_empr[0].InnerText.Trim());
                    emen_cod_empr_retorno = objEmpresa.Emen_cod_empr;
                    clsEmpresa.alterarEmpresa(objEmpresa);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<empresa>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<emen_cod_empr>" + emen_cod_empr_retorno + "</emen_cod_empr>";
                xmlRetorno = xmlRetorno + "</empresa>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todas as empresas cadastradas na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEmpresasTodas()
        {
            //aqui instâncio a classe de empresas
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

            //aqui o objeto da empresa
            Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsEmpresas = new DataSet();
                dsEmpresas = clsEmpresa.SelecionarTodasEmpresas();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<empresas>";

                if (dsEmpresas.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsEmpresas.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<empresa>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsEmpresas.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<emen_cnpj>" + dsEmpresas.Tables[0].Rows[i]["emen_cnpj"].ToString() + "</emen_cnpj>";
                        xmlRetorno = xmlRetorno + "<emen_nome>" + dsEmpresas.Tables[0].Rows[i]["emen_nome"].ToString() + "</emen_nome>";
                        xmlRetorno = xmlRetorno + "<emen_nomered>" + dsEmpresas.Tables[0].Rows[i]["emen_nomered"].ToString() + "</emen_nomered>";
                        xmlRetorno = xmlRetorno + "<emen_lograd>" + dsEmpresas.Tables[0].Rows[i]["emen_lograd"].ToString() + "</emen_lograd>";
                        xmlRetorno = xmlRetorno + "<emen_numero>" + dsEmpresas.Tables[0].Rows[i]["emen_numero"].ToString() + "</emen_numero>";
                        xmlRetorno = xmlRetorno + "<emen_comple>" + dsEmpresas.Tables[0].Rows[i]["emen_comple"].ToString() + "</emen_comple>";
                        xmlRetorno = xmlRetorno + "<emen_bairro>" + dsEmpresas.Tables[0].Rows[i]["emen_bairro"].ToString() + "</emen_bairro>";
                        xmlRetorno = xmlRetorno + "<emen_cep>" + dsEmpresas.Tables[0].Rows[i]["emen_cep"].ToString() + "</emen_cep>";
                        xmlRetorno = xmlRetorno + "<emen_uf>" + dsEmpresas.Tables[0].Rows[i]["emen_uf"].ToString() + "</emen_uf>";
                        xmlRetorno = xmlRetorno + "<emen_ddd>" + dsEmpresas.Tables[0].Rows[i]["emen_ddd"].ToString() + "</emen_ddd>";
                        xmlRetorno = xmlRetorno + "<emen_tel>" + dsEmpresas.Tables[0].Rows[i]["emen_tel"].ToString() + "</emen_tel>";
                        xmlRetorno = xmlRetorno + "<emen_email>" + dsEmpresas.Tables[0].Rows[i]["emen_email"].ToString() + "</emen_email>";
                        xmlRetorno = xmlRetorno + "<emen_dt_criacao>" + Convert.ToDateTime(dsEmpresas.Tables[0].Rows[i]["emen_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</emen_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEmpresas.Tables[0].Rows[i]["usur_cod_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<emen_ativo>" + dsEmpresas.Tables[0].Rows[i]["emen_ativo"].ToString() + "</emen_ativo>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsEmpresas.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsEmpresas.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
                        xmlRetorno = xmlRetorno + "<emen_forn_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_forn_emen"].ToString() + "</emen_forn_emen>";
                        xmlRetorno = xmlRetorno + "<emen_carregamento_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_carregamento_emen"].ToString() + "</emen_carregamento_emen>";
                        xmlRetorno = xmlRetorno + "</empresa>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</empresas>";


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todas as empresas cadastradas na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEmpresasAtivas()
        {
            //aqui instâncio a classe de empresas
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

            //aqui o objeto da empresa
            Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsEmpresas = new DataSet();
                dsEmpresas = clsEmpresa.SelecionarEmpresasAtivas();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<empresas>";

                if (dsEmpresas.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsEmpresas.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<empresa>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsEmpresas.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<emen_cnpj>" + dsEmpresas.Tables[0].Rows[i]["emen_cnpj"].ToString() + "</emen_cnpj>";
                        xmlRetorno = xmlRetorno + "<emen_nome>" + dsEmpresas.Tables[0].Rows[i]["emen_nome"].ToString() + "</emen_nome>";
                        xmlRetorno = xmlRetorno + "<emen_nomered>" + dsEmpresas.Tables[0].Rows[i]["emen_nomered"].ToString() + "</emen_nomered>";
                        xmlRetorno = xmlRetorno + "<emen_lograd>" + dsEmpresas.Tables[0].Rows[i]["emen_lograd"].ToString() + "</emen_lograd>";
                        xmlRetorno = xmlRetorno + "<emen_numero>" + dsEmpresas.Tables[0].Rows[i]["emen_numero"].ToString() + "</emen_numero>";
                        xmlRetorno = xmlRetorno + "<emen_comple>" + dsEmpresas.Tables[0].Rows[i]["emen_comple"].ToString() + "</emen_comple>";
                        xmlRetorno = xmlRetorno + "<emen_bairro>" + dsEmpresas.Tables[0].Rows[i]["emen_bairro"].ToString() + "</emen_bairro>";
                        xmlRetorno = xmlRetorno + "<emen_cep>" + dsEmpresas.Tables[0].Rows[i]["emen_cep"].ToString() + "</emen_cep>";
                        xmlRetorno = xmlRetorno + "<emen_uf>" + dsEmpresas.Tables[0].Rows[i]["emen_uf"].ToString() + "</emen_uf>";
                        xmlRetorno = xmlRetorno + "<emen_ddd>" + dsEmpresas.Tables[0].Rows[i]["emen_ddd"].ToString() + "</emen_ddd>";
                        xmlRetorno = xmlRetorno + "<emen_tel>" + dsEmpresas.Tables[0].Rows[i]["emen_tel"].ToString() + "</emen_tel>";
                        xmlRetorno = xmlRetorno + "<emen_email>" + dsEmpresas.Tables[0].Rows[i]["emen_email"].ToString() + "</emen_email>";
                        xmlRetorno = xmlRetorno + "<emen_dt_criacao>" + Convert.ToDateTime(dsEmpresas.Tables[0].Rows[i]["emen_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</emen_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEmpresas.Tables[0].Rows[i]["usur_cod_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<emen_ativo>" + dsEmpresas.Tables[0].Rows[i]["emen_ativo"].ToString() + "</emen_ativo>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsEmpresas.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsEmpresas.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
                        xmlRetorno = xmlRetorno + "<emen_forn_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_forn_emen"].ToString() + "</emen_forn_emen>";
                        xmlRetorno = xmlRetorno + "<emen_carregamento_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_carregamento_emen"].ToString() + "</emen_carregamento_emen>";
                        xmlRetorno = xmlRetorno + "</empresa>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</empresas>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarEmpresaPorCodigo(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

            //aqui o objeto da empresa
            Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                /*string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");


                DataSet dsEmpresas = new DataSet();
                dsEmpresas = clsEmpresa.SelecionarEmpresa(Convert.ToInt32(emen_cod_empr[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<empresa>";

                int i = 0;
                if (dsEmpresas.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsEmpresas.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                    xmlRetorno = xmlRetorno + "<emen_cnpj>" + dsEmpresas.Tables[0].Rows[i]["emen_cnpj"].ToString() + "</emen_cnpj>";
                    xmlRetorno = xmlRetorno + "<emen_nome>" + dsEmpresas.Tables[0].Rows[i]["emen_nome"].ToString() + "</emen_nome>";
                    xmlRetorno = xmlRetorno + "<emen_nomered>" + dsEmpresas.Tables[0].Rows[i]["emen_nomered"].ToString() + "</emen_nomered>";
                    xmlRetorno = xmlRetorno + "<emen_lograd>" + dsEmpresas.Tables[0].Rows[i]["emen_lograd"].ToString() + "</emen_lograd>";
                    xmlRetorno = xmlRetorno + "<emen_numero>" + dsEmpresas.Tables[0].Rows[i]["emen_numero"].ToString() + "</emen_numero>";
                    xmlRetorno = xmlRetorno + "<emen_comple>" + dsEmpresas.Tables[0].Rows[i]["emen_comple"].ToString() + "</emen_comple>";
                    xmlRetorno = xmlRetorno + "<emen_bairro>" + dsEmpresas.Tables[0].Rows[i]["emen_bairro"].ToString() + "</emen_bairro>";
                    xmlRetorno = xmlRetorno + "<emen_cep>" + dsEmpresas.Tables[0].Rows[i]["emen_cep"].ToString() + "</emen_cep>";
                    xmlRetorno = xmlRetorno + "<emen_uf>" + dsEmpresas.Tables[0].Rows[i]["emen_uf"].ToString() + "</emen_uf>";
                    xmlRetorno = xmlRetorno + "<emen_cidade>" + dsEmpresas.Tables[0].Rows[i]["emen_cidade"].ToString() + "</emen_cidade>";
                    xmlRetorno = xmlRetorno + "<emen_ddd>" + dsEmpresas.Tables[0].Rows[i]["emen_ddd"].ToString() + "</emen_ddd>";
                    xmlRetorno = xmlRetorno + "<emen_tel>" + dsEmpresas.Tables[0].Rows[i]["emen_tel"].ToString() + "</emen_tel>";
                    xmlRetorno = xmlRetorno + "<emen_email>" + dsEmpresas.Tables[0].Rows[i]["emen_email"].ToString() + "</emen_email>";
                    xmlRetorno = xmlRetorno + "<emen_dt_criacao>" + Convert.ToDateTime(dsEmpresas.Tables[0].Rows[i]["emen_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</emen_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEmpresas.Tables[0].Rows[i]["usur_cod_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<emen_ativo>" + dsEmpresas.Tables[0].Rows[i]["emen_ativo"].ToString() + "</emen_ativo>";
                    xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsEmpresas.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                    xmlRetorno = xmlRetorno + "<grem_nome>" + dsEmpresas.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
                    xmlRetorno = xmlRetorno + "<emen_forn_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_forn_emen"].ToString() + "</emen_forn_emen>";
                    xmlRetorno = xmlRetorno + "<emen_carregamento_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_carregamento_emen"].ToString() + "</emen_carregamento_emen>";
                    xmlRetorno = xmlRetorno + "<emen_nr_folhas_kit>" + dsEmpresas.Tables[0].Rows[i]["emen_nr_folhas_kit"].ToString() + "</emen_nr_folhas_kit>";

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</empresa>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do perfil para trazer a relação de atividades vinculadas ao perfil
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarPerfilAtividade(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsPerfil clsPerfil = new Classes.clsPerfil();

            //aqui o objeto da empresa
            Objetos.objPerfil objPerfil = new Objetos.objPerfil();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarPerfilAtividade(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList perf_cod_perf = doc.GetElementsByTagName("perf_cod_perf");


                int perf_cod_perf_ret = Convert.ToInt32(perf_cod_perf[0].InnerText.ToString());
                DataSet dsperfil = clsPerfil.SelecionarPerfilAtividade(perf_cod_perf_ret);

                //aqui começarei a montar o objeto do perfil (xml) para retorno


                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<perfil>";
                if (dsperfil.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<perf_cod_perf>" + perf_cod_perf_ret + "</perf_cod_perf>";
                    xmlRetorno = xmlRetorno + "<perf_nm_perfil>" + dsperfil.Tables[0].Rows[0]["perf_nm_perfil"].ToString() + "</perf_nm_perfil>";
                    xmlRetorno = xmlRetorno + "<atividades>";

                    for (int i = 0; i <= dsperfil.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<atividade>";
                        xmlRetorno = xmlRetorno + "<ativ_cod_ativ>" + dsperfil.Tables[0].Rows[i]["ativ_cod_ativ"].ToString().Trim() + "</ativ_cod_ativ>";
                        xmlRetorno = xmlRetorno + "<ativ_nm_ativ>" + dsperfil.Tables[0].Rows[i]["ativ_nm_ativ"].ToString().Trim() + "</ativ_nm_ativ>";
                        xmlRetorno = xmlRetorno + "</atividade>";
                    }
                    xmlRetorno = xmlRetorno + "</atividades>";
                    xmlRetorno = xmlRetorno + "</perfil>";
                }
                else
                {

                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</perfil>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Não apresenta parâmtro de entrada
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido, com o perfil e a lista de atividades a ele relacionadas.</returns>
        ///</summary>
        public string SelecionaPerfisTodos()
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsPerfil clsPerfil = new Classes.clsPerfil();

            //aqui o objeto da empresa
            Objetos.objPerfil objPerfil = new Objetos.objPerfil();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                DataSet dsperfil = clsPerfil.SelecionarTodosPerfil();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<perfis>";

                if (dsperfil.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsperfil.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<perfil>";
                        xmlRetorno = xmlRetorno + "<perf_cod_perf>" + dsperfil.Tables[0].Rows[i]["perf_cod_perf"].ToString().Trim() + "</perf_cod_perf>";
                        xmlRetorno = xmlRetorno + "<perf_nm_perfil>" + dsperfil.Tables[0].Rows[i]["perf_nm_perfil"].ToString().Trim() + "</perf_nm_perfil>";
                        xmlRetorno = xmlRetorno + "</perfil>";
                    }
                    xmlRetorno = xmlRetorno + "</perfis>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</perfis>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Não apresenta parâmtro de entrada
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido com a lista de todas as atividades cadastradas</returns>
        ///</summary>
        public string SelecionarAtividadesTodas()
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsAtividades clsAtividades = new Classes.clsAtividades();

            //aqui o objeto da empresa
            Objetos.objAtividade objAtividade = new Objetos.objAtividade();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                DataSet dsAtividades = clsAtividades.SelecionarAtividadesTodas();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<atividades>";

                if (dsAtividades.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsAtividades.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<atividade>";
                        xmlRetorno = xmlRetorno + "<ativ_cod_ativ>" + dsAtividades.Tables[0].Rows[i]["ativ_cod_ativ"].ToString().Trim() + "</ativ_cod_ativ>";
                        xmlRetorno = xmlRetorno + "<ativ_nm_ativ>" + dsAtividades.Tables[0].Rows[i]["ativ_nm_ativ"].ToString().Trim() + "</ativ_nm_ativ>";
                        xmlRetorno = xmlRetorno + "</atividade>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</atividades>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do perfil para trazer a relação de atividades vinculadas ao perfil
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarPerfil(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsPerfil clsPerfil = new Classes.clsPerfil();

            //aqui o objeto do perfil
            Objetos.objPerfil objPerfil = new Objetos.objPerfil();

            //aqui o objeto da atividade
            Objetos.objAtividade objAtividade = new Objetos.objAtividade();


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarPerfilCadastroAlteracao(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList perf_cod_perf = doc.GetElementsByTagName("perf_cod_perf");
                XmlNodeList perf_nm_perfil = doc.GetElementsByTagName("perf_nm_perfil");
                XmlNodeList ativ_cod_ativ = doc.GetElementsByTagName("ativ_cod_ativ");

                objPerfil.Perf_nm_perfil = perf_nm_perfil[0].InnerText.ToString();

                for (int i = 0; i <= ativ_cod_ativ.Count - 1; i++)
                {
                    objAtividade.Ativ_cod_ativ = Convert.ToInt32(ativ_cod_ativ[i].InnerText.ToString());
                    objPerfil.ObjAtividade.Add(objAtividade);
                    objAtividade = new Objetos.objAtividade();
                }

                //aqui verifico se o código veio zerado caso senha vindo zerado inicio a inserção
                if (perf_cod_perf[0].InnerText.ToString() == "")
                {
                    //Aqui vou realizar a inserção do perfil
                    int perf_cod_perf_ret = 0;
                    perf_cod_perf_ret = clsPerfil.CadastrarPerfil(objPerfil);
                    //aqui começarei a montar o objeto do perfil (xml) para retorno
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<perfil>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<perf_cod_perf>" + perf_cod_perf_ret + "</perf_cod_perf>";
                }
                else //alteraçao
                {
                    objPerfil.Perf_cod_perf = Convert.ToInt32(perf_cod_perf[0].InnerText.ToString());
                    clsPerfil.alterarPerfil(objPerfil);
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<perfil>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<perf_cod_perf>" + objPerfil.Perf_cod_perf + "</perf_cod_perf>";
                }
                xmlRetorno = xmlRetorno + "</perfil>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do grupo para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarGrupo(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsGrupo clsGrupo = new Classes.clsGrupo();

            //aqui o objeto da empresa
            Objetos.objGrupo objGrupo = new Objetos.objGrupo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarCadastroAlteracaoGrupoXML(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList grem_nome = doc.GetElementsByTagName("grem_nome");
                XmlNodeList grem_descricao = doc.GetElementsByTagName("grem_descricao");
                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");



                objGrupo.Grem_nome = grem_nome[0].InnerText.Trim();
                objGrupo.Grem_descricao = grem_descricao[0].InnerText.Trim();

                int grem_cd_grem_retorno = 0;

                if (grem_cd_grem[0].InnerText.Trim() == "")
                {

                    grem_cd_grem_retorno = clsGrupo.CadastrarGrupo(objGrupo);

                }
                else
                {
                    objGrupo.Grem_cd_grem = Convert.ToInt32(grem_cd_grem[0].InnerText.Trim());
                    grem_cd_grem_retorno = objGrupo.Grem_cd_grem;
                    clsGrupo.alterarGrupo(objGrupo);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<grupo>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<grem_cd_grem>" + grem_cd_grem_retorno + "</grem_cd_grem>";
                xmlRetorno = xmlRetorno + "</grupo>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os grupos cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarGruposTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsGrupo clsGrupo = new Classes.clsGrupo();

            //aqui o objeto da empresa
            Objetos.objGrupo objGrupo = new Objetos.objGrupo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsGrupos = new DataSet();
                dsGrupos = clsGrupo.SelecionarTodosGrupos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<grupos>";
                if (dsGrupos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsGrupos.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<grupo>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsGrupos.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsGrupos.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
                        xmlRetorno = xmlRetorno + "<grem_descricao>" + dsGrupos.Tables[0].Rows[i]["grem_descricao"].ToString() + "</grem_descricao>";
                        xmlRetorno = xmlRetorno + "</grupo>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</grupos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'><status>Erro</status><descerro>" + e.Message + "</descerro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os grupos cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarGruposProdutosTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsGrupoProdutos clsGrupo = new Classes.clsGrupoProdutos();

            //aqui o objeto da empresa
            Objetos.objGppm objGppm = new Objetos.objGppm();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsGrupos = new DataSet();
                dsGrupos = clsGrupo.SelecionarGruposProdutosTodos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<gruposProdutos>";
                if (dsGrupos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsGrupos.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<grupoproduto>";
                        xmlRetorno = xmlRetorno + "<gppm_cd_gppm>" + dsGrupos.Tables[0].Rows[i]["gppm_cd_gppm"].ToString() + "</gppm_cd_gppm>";
                        xmlRetorno = xmlRetorno + "<gppm_descricao>" + dsGrupos.Tables[0].Rows[i]["gppm_descricao"].ToString() + "</gppm_descricao>";
                        xmlRetorno = xmlRetorno + "</grupoproduto>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</gruposProdutos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'><status>Erro</status><descerro>" + e.Message + "</descerro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código da emrpesa da qual necessitamos saber o grupo
        /// o mesmo irá retornar um xml com os dados do grupo da empresa
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarGrupoPorCodigoEmpresa(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsGrupoProdutos clsGrupo = new Classes.clsGrupoProdutos();

            //aqui o objeto da empresa
            Objetos.objGrupo objGrupo = new Objetos.objGrupo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList emen_cd_empr = doc.GetElementsByTagName("emen_cod_empr");


                DataSet dsGrupos = new DataSet();
                dsGrupos = clsGrupo.selecionaRProdutosPorCodigoEmpresa(Convert.ToInt32(emen_cd_empr[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<grupo>";
				if (dsGrupos.Tables[0].Rows.Count > 0)
				{
					xmlRetorno = xmlRetorno + "<status>sucesso</status>";
				}
				else
				{
					xmlRetorno = xmlRetorno + "<status>falha</status>";
				}

				if (dsGrupos.Tables[0].Rows.Count > 0)
				{
					for (int i = 0; i <= dsGrupos.Tables[0].Rows.Count - 1; i++)
					{
						xmlRetorno = xmlRetorno + "<detalhes>";
						xmlRetorno = xmlRetorno + "<gppm_cd_gppm>" + dsGrupos.Tables[0].Rows[i]["gppm_cd_gppm"].ToString().Trim() + "</gppm_cd_gppm>";
						xmlRetorno = xmlRetorno + "<gppm_descricao>" + dsGrupos.Tables[0].Rows[i]["gppm_descricao"].ToString().Trim() + "</gppm_descricao>";
						xmlRetorno = xmlRetorno + "</detalhes>";
					}
                }
                xmlRetorno = xmlRetorno + "</grupo>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }
        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código da emrpesa da qual necessitamos saber o grupo
        /// o mesmo irá retornar um xml com os dados do grupo da empresa
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarGrupoPorCodigo(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsGrupo clsGrupo = new Classes.clsGrupo();

            //aqui o objeto da empresa
            Objetos.objGrupo objGrupo = new Objetos.objGrupo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");


                DataSet dsGrupos = new DataSet();
                dsGrupos = clsGrupo.SelecionarGrupoPorCodigo(Convert.ToInt32(grem_cd_grem[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<grupo>";
                if (dsGrupos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsGrupos.Tables[0].Rows[0]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                    xmlRetorno = xmlRetorno + "<grem_nome>" + dsGrupos.Tables[0].Rows[0]["grem_nome"].ToString() + "</grem_nome>";
                    xmlRetorno = xmlRetorno + "<grem_descricao>" + dsGrupos.Tables[0].Rows[0]["grem_descricao"].ToString() + "</grem_descricao>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</grupo>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código do usuário da qual necessitamos saber as atividades extras
        /// o mesmo irá retornar um xml com os dados das atividades extras
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarAtividadesExtrasPorCodigoUsuario(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsAtividades clsAtividades = new Classes.clsAtividades();

            //aqui o objeto da empresa
            Objetos.objAtividade objAtividade = new Objetos.objAtividade();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarAtividadesExtrasUsuarios(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");


                DataSet dsAtividadesExtras = new DataSet();
                dsAtividadesExtras = clsAtividades.SelecionarAtividadesExtrasUsuarios(Convert.ToInt32(usur_cd_usur[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<atividade>";
                if (dsAtividadesExtras.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<atividades>";
                    for (int i = 0; i <= dsAtividadesExtras.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<atividade>";
                        xmlRetorno = xmlRetorno + "<ativ_cod_ativ>" + dsAtividadesExtras.Tables[0].Rows[i]["ativ_cod_ativ"].ToString() + "</ativ_cod_ativ>";
                        xmlRetorno = xmlRetorno + "<ativ_nm_ativ>" + dsAtividadesExtras.Tables[0].Rows[i]["ativ_nm_ativ"].ToString() + "</ativ_nm_ativ>";
                        xmlRetorno = xmlRetorno + "</atividade>";
                    }
                    xmlRetorno = xmlRetorno + "</atividades>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</atividade>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o o código do usuário e uma lista das atividades extras que devem ser vinculadas ao mesmo.
        /// o mesmo irá retornar um xml com o status do cadastramento apenas
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarAtividadesExtrasDoUsuario(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsAtividadeExtra clsAtividadeExtra = new Classes.clsAtividadeExtra();

            //aqui o objeto da empresa
            Objetos.objAtividadeExtra objAtividadeExtra = new Objetos.objAtividadeExtra();
            List<Objetos.objAtividadeExtra> LstobjAtividadeExtra = new List<Objetos.objAtividadeExtra>();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.CadastrarAlterarAtividadeExtraUsuario(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList ativ_cod_ativ = doc.GetElementsByTagName("ativ_cod_ativ");

                //aqui vou montar o objeto e adiciona-lo a lista de objetos que será recebida pela função que realizará 
                //o cadastro.

                int usur_cd_usur_recebido = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());

                for (int i = 0; i <= ativ_cod_ativ.Count - 1; i++)
                {
                    objAtividadeExtra = new Objetos.objAtividadeExtra();
                    objAtividadeExtra.Ativ_cod_ativ = Convert.ToInt32(ativ_cod_ativ[i].InnerText.ToString());
                    objAtividadeExtra.Usur_cd_usur = usur_cd_usur_recebido;
                    LstobjAtividadeExtra.Add(objAtividadeExtra);
                }

                clsAtividadeExtra.cadastrarAlterarAtividadesExtra(LstobjAtividadeExtra);

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<atividade>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "</atividade>";

                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código do perfil da qual necessitamos saber as atividades que não estão ligadas
        /// ao mesmo.
        /// o mesmo irá retornar um xml com os dados das atividades que não estão vinculadas ao perfil informado.
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarAtividadesForaPerfil(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsAtividades clsAtividades = new Classes.clsAtividades();

            //aqui o objeto da empresa
            Objetos.objAtividade objAtividade = new Objetos.objAtividade();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarAtivdadesForaPerfil(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList perf_cod_perf = doc.GetElementsByTagName("perf_cod_perf");


                DataSet dsAtividades = new DataSet();
                dsAtividades = clsAtividades.SelecionarAtividadesForaPerfil(Convert.ToInt32(perf_cod_perf[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<atividades>";

                if (dsAtividades.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsAtividades.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                        xmlRetorno = xmlRetorno + "<atividade>";
                        xmlRetorno = xmlRetorno + "<ativ_cod_ativ>" + dsAtividades.Tables[0].Rows[i]["ativ_cod_ativ"].ToString() + "</ativ_cod_ativ>";
                        xmlRetorno = xmlRetorno + "<ativ_nm_ativ>" + dsAtividades.Tables[0].Rows[i]["ativ_nm_ativ"].ToString() + "</ativ_nm_ativ>";
                        xmlRetorno = xmlRetorno + "</atividade>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</atividades>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do arnazem para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarArmazem(string xmlString)
        {

            //aqui instâncio a classe de empresas
            Classes.clsArmazem clsArmazem = new Classes.clsArmazem();

            //aqui o objeto da empresa
            Objetos.objArma objArma = new Objetos.objArma();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);




                XmlNodeList arma_cod_arma = doc.GetElementsByTagName("arma_cod_arma");
                XmlNodeList arma_descricao = doc.GetElementsByTagName("arma_descricao");
                XmlNodeList arma_ativo = doc.GetElementsByTagName("arma_ativo");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cod_usur");

                objArma.Arma_descricao = arma_descricao[0].InnerText.Trim();
                objArma.Arma_ativo = Convert.ToBoolean(arma_ativo[0].InnerText.Trim());
                objArma.Arma_dt_criacao = DateTime.Now;
                objArma.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());

                int arma_cod_arma_retorno = 0;

                if (arma_cod_arma[0].InnerText.Trim() == "")
                {
                    arma_cod_arma_retorno = clsArmazem.cadastraArmazem(objArma);
                }
                else
                {
                    objArma.Arma_cod_arma = Convert.ToInt32(arma_cod_arma[0].InnerText.Trim());
                    arma_cod_arma_retorno = objArma.Arma_cod_arma;
                    clsArmazem.AlterarArmazem(objArma);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<armazem>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<arma_cod_arma>" + arma_cod_arma_retorno + "</arma_cod_arma>";
                xmlRetorno = xmlRetorno + "</armazem>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera o id da uma empresa e então criará um xml com todos os armazens vinculados aquela empresa
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarArmazensTodos()
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsArmazem clsArmazem = new Classes.clsArmazem();

            //aqui o objeto da empresa
            Objetos.objArma objArma = new Objetos.objArma();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {





                DataSet dsArmazens = new DataSet();
                dsArmazens = clsArmazem.SelecionarArmazensTodos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<armazens>";

                if (dsArmazens.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dsArmazens.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                        xmlRetorno = xmlRetorno + "<armazen>";
                        xmlRetorno = xmlRetorno + "<arma_cod_arma>" + dsArmazens.Tables[0].Rows[i]["arma_cod_arma"].ToString() + "</arma_cod_arma>";
                        xmlRetorno = xmlRetorno + "<arma_descricao>" + dsArmazens.Tables[0].Rows[i]["arma_descricao"].ToString() + "</arma_descricao>";
                        xmlRetorno = xmlRetorno + "<arma_ativo>" + dsArmazens.Tables[0].Rows[i]["arma_ativo"].ToString() + "</arma_ativo>";
                        xmlRetorno = xmlRetorno + "</armazen>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</armazens>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }


        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do arnazem para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarFamiliaProdutos(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsFamiliaProdutos clsFamiliaProdutos = new Classes.clsFamiliaProdutos();

            //aqui o objeto da empresa
            Objetos.objFamiliaProdutos objFamiliaProdutos = new Objetos.objFamiliaProdutos();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.CadastroAlteracaoFamiliaProdutos(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList famp_cd_famp = doc.GetElementsByTagName("famp_cd_famp");

                XmlNodeList famp_descricao = doc.GetElementsByTagName("famp_descricao");
                XmlNodeList famp_dt_criacao = doc.GetElementsByTagName("famp_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");


                objFamiliaProdutos.Famp_descricao = famp_descricao[0].InnerText.Trim();
                objFamiliaProdutos.Famp_dt_criaca = Convert.ToDateTime(famp_dt_criacao[0].InnerText.Trim());
                objFamiliaProdutos.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());

                int famp_cd_famp_retorno = 0;

                if (famp_cd_famp[0].InnerText.Trim() == "")
                {
                    famp_cd_famp_retorno = clsFamiliaProdutos.cadastraFamiliaprodutos(objFamiliaProdutos);
                }
                else
                {
                    objFamiliaProdutos.Famp_cd_famp = Convert.ToInt32(famp_cd_famp[0].InnerText.Trim());
                    famp_cd_famp_retorno = objFamiliaProdutos.Famp_cd_famp;
                    clsFamiliaProdutos.alterarFamiliaProduto(objFamiliaProdutos);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<familiaproduto>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<Famp_cd_famp>" + famp_cd_famp_retorno + "</Famp_cd_famp>";
                xmlRetorno = xmlRetorno + "</familiaproduto>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todas as empresas cadastradas na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarFamiliaProdutosTodas()
        {
            //aqui instâncio a classe de empresas
            Classes.clsFamiliaProdutos clsFamiliaProdutos = new Classes.clsFamiliaProdutos();

            //aqui o objeto da empresa
            Objetos.objFamiliaProdutos objFamiliaProdutos = new Objetos.objFamiliaProdutos();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsFamiliaProdutos = new DataSet();
                dsFamiliaProdutos = clsFamiliaProdutos.SelecionarTodasFamiliaProdutos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<familiasprodutos>";

                if (dsFamiliaProdutos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsFamiliaProdutos.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<familia>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsFamiliaProdutos.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<famp_descricao>" + dsFamiliaProdutos.Tables[0].Rows[i]["famp_descricao"].ToString() + "</famp_descricao>";
                        xmlRetorno = xmlRetorno + "<famp_dt_criacao>" + dsFamiliaProdutos.Tables[0].Rows[i]["famp_dt_criacao"].ToString() + "</famp_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsFamiliaProdutos.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "</familia>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</familiasprodutos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados dos tipo de familia de produtos/materiais
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarTipoProdutosMateriais(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsTipoProdutosMateriais clsTipoProdutosMateriais = new Classes.clsTipoProdutosMateriais();

            //aqui o objeto da empresa
            Objetos.objTipoProdutosMateriais objTipoProdutosMateriais = new Objetos.objTipoProdutosMateriais();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.CadastrarTipoProdutosMateriais(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList tpma_cod_tipo = doc.GetElementsByTagName("tpma_cod_tipo");
                XmlNodeList tpma_descricao = doc.GetElementsByTagName("tpma_descricao");
                XmlNodeList tpma_data_criacao = doc.GetElementsByTagName("tpma_data_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");


                objTipoProdutosMateriais.Tpma_descricao = tpma_descricao[0].InnerText.Trim();
                objTipoProdutosMateriais.Tpma_data_criacao = Convert.ToDateTime(tpma_data_criacao[0].InnerText.Trim());
                objTipoProdutosMateriais.User_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());

                int tpma_cod_tipo_retorno = 0;

                if (tpma_cod_tipo[0].InnerText.Trim() == "")
                {
                    tpma_cod_tipo_retorno = clsTipoProdutosMateriais.CadastrarTipoProdutosMateriais(objTipoProdutosMateriais);
                }
                else
                {
                    objTipoProdutosMateriais.Tpma_cod_tipo = Convert.ToInt32(tpma_cod_tipo[0].InnerText.Trim());
                    tpma_cod_tipo_retorno = objTipoProdutosMateriais.Tpma_cod_tipo;
                    clsTipoProdutosMateriais.AlterarTipoProdutosMateriais(objTipoProdutosMateriais);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<tipoproduto>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + tpma_cod_tipo_retorno + "</tpma_cod_tipo>";
                xmlRetorno = xmlRetorno + "</tipoproduto>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o id do tipo de produto/material
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>

        public string SelecionarTipoProdutosMateriaisPorCodigo(string xmlString)
        {
            //instâncio a classe de usuários
            Classes.clsTipoProdutosMateriais clsTipoProdutosMateriais = new Classes.clsTipoProdutosMateriais();

            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.SelecionarUmTipoProdutosMateriais(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList tpma_cod_tipo = doc.GetElementsByTagName("tpma_cod_tipo");

                DataSet dsTipoProdutoMaterial = new DataSet();
                int i = 0;
                dsTipoProdutoMaterial = clsTipoProdutosMateriais.SecionarUmTipoProdutosMateriais(Convert.ToInt32(tpma_cod_tipo[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                if (dsTipoProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<tipoproduto>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsTipoProdutoMaterial.Tables[0].Rows[i]["tpma_cod_tipo"].ToString().Trim() + "</tpma_cod_tipo>";
                    xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsTipoProdutoMaterial.Tables[0].Rows[i]["tpma_descricao"].ToString().Trim() + "</tpma_descricao>";
                    xmlRetorno = xmlRetorno + "<tpma_data_criacao>" + dsTipoProdutoMaterial.Tables[0].Rows[i]["tpma_data_criacao"].ToString().Trim() + "</tpma_data_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsTipoProdutoMaterial.Tables[0].Rows[i]["usur_cd_usur"].ToString().Trim() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "</tipoproduto>";
                }
                else
                {

                    xmlRetorno = xmlRetorno + "<tipoproduto>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</tipoproduto>";
                }


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os tipo de de movimentacao cadastrados na base
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTiposMovimentacaoTodas()
        {
            //aqui instâncio a classe de empresas
            Classes.clsTipoMovimentacao clsTipoMovimentacao = new Classes.clsTipoMovimentacao();

            //aqui o objeto da empresa
            Objetos.objTipoMovimentacao objTipoMovimentacao = new Objetos.objTipoMovimentacao();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsTipoMobimentacao = new DataSet();
                dsTipoMobimentacao = clsTipoMovimentacao.SecionarTodosTiposMovimentacao();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<tiposmovimentacao>";

                if (dsTipoMobimentacao.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsTipoMobimentacao.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<tipomovimentacao>";
                        xmlRetorno = xmlRetorno + "<tpmo_cod_tpmo>" + dsTipoMobimentacao.Tables[0].Rows[i]["tpmo_cod_tpmo"].ToString() + "</tpmo_cod_tpmo>";
                        xmlRetorno = xmlRetorno + "<Tpmo_tipo_movimento>" + dsTipoMobimentacao.Tables[0].Rows[i]["Tpmo_tipo_movimento"].ToString() + "</Tpmo_tipo_movimento>";
                        xmlRetorno = xmlRetorno + "<tpmo_descricao>" + dsTipoMobimentacao.Tables[0].Rows[i]["tpmo_descricao"].ToString() + "</tpmo_descricao>";
                        xmlRetorno = xmlRetorno + "<tpmo_dt_criacao>" + dsTipoMobimentacao.Tables[0].Rows[i]["tpmo_dt_criacao"].ToString() + "</tpmo_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsTipoMobimentacao.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "</tipomovimentacao>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</tiposmovimentacao>";


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o id do tipo de produto/material
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>

        public string SelecionarTipoMovimentacaoPorCodigo(string xmlString)
        {
            //aqui instâncio a classe de empresas
            Classes.clsTipoMovimentacao clsTipoMovimentacao = new Classes.clsTipoMovimentacao();

            //aqui o objeto da empresa
            Objetos.objTipoMovimentacao objTipoMovimentacao = new Objetos.objTipoMovimentacao();


            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.SelecionarUmTipoMovimentacao(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList tpmo_cod_tpmo = doc.GetElementsByTagName("tpmo_cod_tpmo");

                DataSet dsTipoMovimentacao = new DataSet();
                int i = 0;
                dsTipoMovimentacao = clsTipoMovimentacao.SecionarUmTiposMovimentacao(Convert.ToInt32(tpmo_cod_tpmo[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                if (dsTipoMovimentacao.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<tipomovimentacao>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<tpmo_cod_tpmo>" + dsTipoMovimentacao.Tables[0].Rows[i]["tpmo_cod_tpmo"].ToString() + "</tpmo_cod_tpmo>";
                    xmlRetorno = xmlRetorno + "<Tpmo_tipo_movimento>" + dsTipoMovimentacao.Tables[0].Rows[i]["Tpmo_tipo_movimento"].ToString() + "</Tpmo_tipo_movimento>";
                    xmlRetorno = xmlRetorno + "<tpmo_descricao>" + dsTipoMovimentacao.Tables[0].Rows[i]["tpmo_descricao"].ToString() + "</tpmo_descricao>";
                    xmlRetorno = xmlRetorno + "<tpmo_dt_criacao>" + dsTipoMovimentacao.Tables[0].Rows[i]["tpmo_dt_criacao"].ToString() + "</tpmo_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsTipoMovimentacao.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "</tipomovimentacao>";
                }
                else
                {

                    xmlRetorno = xmlRetorno + "<tipomovimentacao>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</tipomovimentacao>";
                }


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados da unidade de medida que deseja cadastrar
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarUnidadeMedida(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsUnidadeMedida clsUnidadeMedida = new Classes.clsUnidadeMedida();

            //aqui o objeto da empresa
            Objetos.objUnidadeMedida objUnidadeMedida = new Objetos.objUnidadeMedida();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.CadastrarAlterarUnidadesMedida(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList unme_cod_unme = doc.GetElementsByTagName("unme_cod_unme");
                XmlNodeList unme_descricao = doc.GetElementsByTagName("unme_descricao");
                XmlNodeList unme_id_reg = doc.GetElementsByTagName("unme_id_reg");
                XmlNodeList unme_dt_criacao = doc.GetElementsByTagName("unme_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");


                objUnidadeMedida.Unme_descricao = unme_descricao[0].InnerText.Trim();
                objUnidadeMedida.Unme_id_reg = unme_id_reg[0].InnerText.Trim();
                objUnidadeMedida.Unme_dt_criacao = Convert.ToDateTime(unme_dt_criacao[0].InnerText.Trim());
                objUnidadeMedida.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());

                int unme_cod_unme_retorno = 0;

                if (unme_cod_unme[0].InnerText.Trim() == "")
                {
                    unme_cod_unme_retorno = clsUnidadeMedida.cadastrarUnidadeMedida(objUnidadeMedida);
                }
                else
                {
                    objUnidadeMedida.Unme_cod_unme = Convert.ToInt32(unme_cod_unme[0].InnerText.Trim());
                    unme_cod_unme_retorno = objUnidadeMedida.Unme_cod_unme;
                    clsUnidadeMedida.AlterarUnidadeMedida(objUnidadeMedida);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<unidademedida>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<unme_cod_unme>" + unme_cod_unme_retorno + "</unme_cod_unme>";
                xmlRetorno = xmlRetorno + "</unidademedida>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todas as unidades de medida cadastradas
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarUnidadesMedidaTodas()
        {
            //aqui instâncio a classe de empresas
            Classes.clsUnidadeMedida clsUnidadeMedida = new Classes.clsUnidadeMedida();

            //aqui o objeto da empresa
            Objetos.objUnidadeMedida objUnidadeMedida = new Objetos.objUnidadeMedida();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsuUnidadeMedida = new DataSet();
                dsuUnidadeMedida = clsUnidadeMedida.SelecionarTodasUnidadesMedida();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<unidadesmedida>";
                if (dsuUnidadeMedida.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsuUnidadeMedida.Tables[0].Rows.Count - 1; i++)
                    {

                        xmlRetorno = xmlRetorno + "<unidademedida>";
                        xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsuUnidadeMedida.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                        xmlRetorno = xmlRetorno + "<unme_descricao>" + dsuUnidadeMedida.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                        xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsuUnidadeMedida.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                        xmlRetorno = xmlRetorno + "<unme_dt_criacao>" + dsuUnidadeMedida.Tables[0].Rows[i]["unme_dt_criacao"].ToString() + "</unme_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsuUnidadeMedida.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "</unidademedida>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</unidadesmedida>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o id da unidade de medida a ser selecionada
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>

        public string SelecionarUnidadeMedidaPorCodigo(string xmlString)
        {
            //instâncio a classe de usuários
            Classes.clsUnidadeMedida clsUnidadeMedida = new Classes.clsUnidadeMedida();

            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.SelecionarUmaUnidadeMedida(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList unme_cod_unme = doc.GetElementsByTagName("unme_cod_unme");

                DataSet dsunidadeMedida = new DataSet();
                int i = 0;
                dsunidadeMedida = clsUnidadeMedida.SelecionarUmaUnidadeMedida(Convert.ToInt32(unme_cod_unme[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno = "<unidademedida>";
                if (dsunidadeMedida.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsunidadeMedida.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                    xmlRetorno = xmlRetorno + "<unme_descricao>" + dsunidadeMedida.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                    xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsunidadeMedida.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                    xmlRetorno = xmlRetorno + "<unme_dt_criacao>" + dsunidadeMedida.Tables[0].Rows[i]["unme_dt_criacao"].ToString() + "</unme_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsunidadeMedida.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</unidademedida>";

                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do produto/material para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarProdutoMaterial(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa
            Objetos.objProdutoMaterial objProdutoMaterial = new Objetos.objProdutoMaterial();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                /*string resultado = clsValidar.CadastroAlteracaoProdutoMaterial(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList prma_pn = doc.GetElementsByTagName("prma_pn");
                XmlNodeList prma_descricao = doc.GetElementsByTagName("prma_descricao");
                XmlNodeList tpma_cod_tipo = doc.GetElementsByTagName("tpma_cod_tipo");
                XmlNodeList unme_cod_unme = doc.GetElementsByTagName("unme_cod_unme");
                XmlNodeList prma_fator_conservacao = doc.GetElementsByTagName("prma_fator_conservacao");
                XmlNodeList prma_preco = doc.GetElementsByTagName("prma_preco");
                XmlNodeList famp_cd_famp = doc.GetElementsByTagName("famp_cd_famp");
                XmlNodeList prma_dt_criacao = doc.GetElementsByTagName("prma_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList prma_ativo = doc.GetElementsByTagName("prma_ativo");
                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");
                XmlNodeList gppm_cod_gppm = doc.GetElementsByTagName("gppm_cod_gppm");

                objProdutoMaterial.Prma_pn = prma_pn[0].InnerText.Trim();
                objProdutoMaterial.Prma_descricao = prma_descricao[0].InnerText.Trim();
                objProdutoMaterial.Tpma_cod_tipo = Convert.ToInt32(tpma_cod_tipo[0].InnerText.Trim());
                objProdutoMaterial.Unme_cod_unme = Convert.ToInt32(unme_cod_unme[0].InnerText.Trim());
                objProdutoMaterial.Prma_fator_conservacao = Convert.ToDecimal(prma_fator_conservacao[0].InnerText.Trim());
                objProdutoMaterial.Prma_preco = Convert.ToDecimal(prma_preco[0].InnerText.Trim());
                objProdutoMaterial.Famp_cd_famp = Convert.ToInt32(famp_cd_famp[0].InnerText.Trim());
                objProdutoMaterial.Prma_dt_criacao = Convert.ToDateTime(prma_dt_criacao[0].InnerText.Trim());
                objProdutoMaterial.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());
                objProdutoMaterial.Prma_ativo = Convert.ToBoolean(prma_ativo[0].InnerText.Trim());
                objProdutoMaterial.Grem_cd_grem = Convert.ToInt32(grem_cd_grem[0].InnerText.Trim());
                objProdutoMaterial.Gppm_cd_gppm = gppm_cod_gppm[0].InnerText.Trim();


                int prma_cod_prma_retorno = 0;

                if (prma_cod_prma[0].InnerText.Trim() == "")
                {

                    prma_cod_prma_retorno = clsProdutoMaterial.cadastraProdutoMaterial(objProdutoMaterial);

                }
                else
                {
                    objProdutoMaterial.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText.Trim());
                    prma_cod_prma_retorno = objProdutoMaterial.Prma_cod_prma;
                    clsProdutoMaterial.alterarProdutoMaterial(objProdutoMaterial);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<prma_cod_prma>" + prma_cod_prma_retorno + "</prma_cod_prma>";
                xmlRetorno = xmlRetorno + "</produtosmateriais>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os produtos/materiais, cadastrados na base de dados
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarProdutoMaterialTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa
            Objetos.objProdutoMaterial objProdutoMaterial = new Objetos.objProdutoMaterial();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsProdutoMaterial = new DataSet();
                dsProdutoMaterial = clsProdutoMaterial.SelecionarProdutoMaterialTodos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutoMaterial.Tables[0].Rows.Count - 1; i++)
                    {

                        xmlRetorno = xmlRetorno + "<produtomaterial>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Replace(".", ",") + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_preco"].ToString().Replace(".", ",") + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<prma_dt_criacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_dt_criacao"].ToString() + "</prma_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsProdutoMaterial.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<prma_ativo>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_ativo"].ToString() + "</prma_ativo>";
                        xmlRetorno = xmlRetorno + "<unme_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                        xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                        xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_descricao"].ToString() + "</tpma_descricao>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";


                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }



        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os produtos/materiais, cadastrados na base de dados
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarProdutoMaterialPorGrupo(string xmlString)
        {
            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa
            Objetos.objProdutoMaterial objProdutoMaterial = new Objetos.objProdutoMaterial();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);

            XmlNodeList gppm_cd_gpmm = doc.GetElementsByTagName("gppm_cd_gpmm");
            XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsProdutoMaterial = new DataSet();
                dsProdutoMaterial = clsProdutoMaterial.SelecionarProdutoMaterialPorGrupo(gppm_cd_gpmm[0].InnerText, emen_cod_empr[0].InnerText);
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutoMaterial.Tables[0].Rows.Count - 1; i++)
                    {

                        xmlRetorno = xmlRetorno + "<produtomaterial>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Replace(".", ",") + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_preco"].ToString().Replace(".", ",") + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<prma_dt_criacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_dt_criacao"].ToString() + "</prma_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsProdutoMaterial.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<prma_ativo>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_ativo"].ToString() + "</prma_ativo>";
                        xmlRetorno = xmlRetorno + "<unme_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                        xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                        xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_descricao"].ToString() + "</tpma_descricao>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";


                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os produtos/materiais, cadastrados na base de dados
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosProdutoMaterialAtivoInativo()
        {
            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa
            Objetos.objProdutoMaterial objProdutoMaterial = new Objetos.objProdutoMaterial();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsProdutoMaterial = new DataSet();
                dsProdutoMaterial = clsProdutoMaterial.SelecionarTodosProdutoMaterialAtivoInativo();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutoMaterial.Tables[0].Rows.Count - 1; i++)
                    {

                        xmlRetorno = xmlRetorno + "<produtomaterial>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Replace(".", ",") + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_preco"].ToString().Replace(".", ",") + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<prma_dt_criacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_dt_criacao"].ToString() + "</prma_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsProdutoMaterial.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<prma_ativo>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_ativo"].ToString() + "</prma_ativo>";
                        xmlRetorno = xmlRetorno + "<unme_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                        xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                        xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_descricao"].ToString() + "</tpma_descricao>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";


                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Recebera um xml contendo o código do produto ou material que deseja selecionar
        /// o mesmo irá retornar uma lista com todos os produtos/materiais, cadastrados na base de dados
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarProdutoMaterialPorCodigo(string xmlString)
        {

            //instâncio a classe de usuários
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.SelecionarUmProdutoMaterial(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");

                DataSet dsProdutoMaterial = new DataSet();
                int i = 0;
                dsProdutoMaterial = clsProdutoMaterial.SelecionarUmProdutoMaterial(Convert.ToInt32(prma_cod_prma[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno = "<produtomaterial>";
                if (dsProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";

                    xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";

                    xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                    xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                    xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                    xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Replace(".", ",") + "</prma_fator_conservacao>";
                    xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_preco"].ToString().Replace(".", ",") + "</prma_preco>";
                    xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                    xmlRetorno = xmlRetorno + "<prma_dt_criacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_dt_criacao"].ToString() + "</prma_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsProdutoMaterial.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<prma_ativo>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_ativo"].ToString() + "</prma_ativo>";
                    xmlRetorno = xmlRetorno + "<unme_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                    xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                    xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_descricao"].ToString() + "</tpma_descricao>";
                    xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                    xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                    xmlRetorno = xmlRetorno + "<gppm_cd_gppm>" + dsProdutoMaterial.Tables[0].Rows[i]["gppm_cd_gppm"].ToString() + "</gppm_cd_gppm>";


                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</produtomaterial>";

                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do endereço para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarEndereco(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsEndereco clsEndereco = new Classes.clsEndereco();

            //aqui o objeto da empresa
            Objetos.objEndereco objEndereco = new Objetos.objEndereco();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.CadastrarAlterarEnderecoEnvio(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList ende_cod_ende = doc.GetElementsByTagName("ende_cod_ende");
                XmlNodeList ende_descricao = doc.GetElementsByTagName("ende_descricao");
                XmlNodeList ende_ativo = doc.GetElementsByTagName("ende_ativo");
                XmlNodeList ende_dt_criacao = doc.GetElementsByTagName("ende_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");

                objEndereco.Ende_descricao = ende_descricao[0].InnerText.Trim();
                objEndereco.Ende_ativo = Convert.ToBoolean(ende_ativo[0].InnerText.Trim());
                objEndereco.Ende_dt_criacao = Convert.ToDateTime(ende_dt_criacao[0].InnerText.Trim());
                objEndereco.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());



                int ende_cod_ende_retorno = 0;

                if (ende_cod_ende[0].InnerText.Trim() == "")
                {

                    ende_cod_ende_retorno = clsEndereco.Cadastrarendereco(objEndereco);

                }
                else
                {
                    objEndereco.Ende_cod_ende = Convert.ToInt32(ende_cod_ende[0].InnerText.Trim());
                    ende_cod_ende_retorno = objEndereco.Ende_cod_ende;
                    clsEndereco.alterarEndereco(objEndereco);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<endereco>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<ende_cod_ende>" + ende_cod_ende_retorno + "</ende_cod_ende>";
                xmlRetorno = xmlRetorno + "</endereco>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os enderços cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEnderecosTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsEndereco clsEndereco = new Classes.clsEndereco();

            //aqui o objeto da empresa
            Objetos.objEndereco objEndereco = new Objetos.objEndereco();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsEnderecos = new DataSet();
                dsEnderecos = clsEndereco.SelecionarTodosEnderecos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<enderecos>";
                if (dsEnderecos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsEnderecos.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<endereco>";
                        xmlRetorno = xmlRetorno + "<ende_cod_ende>" + dsEnderecos.Tables[0].Rows[i]["ende_cod_ende"].ToString() + "</ende_cod_ende>";
                        xmlRetorno = xmlRetorno + "<ende_descricao>" + dsEnderecos.Tables[0].Rows[i]["ende_descricao"].ToString() + "</ende_descricao>";
                        xmlRetorno = xmlRetorno + "<ende_ativo>" + dsEnderecos.Tables[0].Rows[i]["ende_ativo"].ToString() + "</ende_ativo>";
                        xmlRetorno = xmlRetorno + "<ende_dt_criacao>" + dsEnderecos.Tables[0].Rows[i]["ende_dt_criacao"].ToString() + "</ende_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEnderecos.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "</endereco>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</enderecos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código do endereço que desejamos buscar
        /// o mesmo irá retornar um xml com os dados do endereço
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEnderecoPorCodigo(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsEndereco clsEndereco = new Classes.clsEndereco();

            //aqui o objeto da empresa
            Objetos.objEndereco objEndereco = new Objetos.objEndereco();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.SelecionarUmEndereco(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList ende_cod_ende = doc.GetElementsByTagName("ende_cod_ende");


                DataSet dsEnderecos = new DataSet();
                dsEnderecos = clsEndereco.SelecionarUmEndereco(Convert.ToInt32(ende_cod_ende[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<endereco>";
                if (dsEnderecos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<ende_cod_ende>" + dsEnderecos.Tables[0].Rows[0]["ende_cod_ende"].ToString() + "</ende_cod_ende>";
                    xmlRetorno = xmlRetorno + "<ende_descricao>" + dsEnderecos.Tables[0].Rows[0]["ende_descricao"].ToString() + "</ende_descricao>";
                    xmlRetorno = xmlRetorno + "<ende_ativo>" + dsEnderecos.Tables[0].Rows[0]["ende_ativo"].ToString() + "</ende_ativo>";
                    xmlRetorno = xmlRetorno + "<ende_dt_criacao>" + dsEnderecos.Tables[0].Rows[0]["ende_dt_criacao"].ToString() + "</ende_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEnderecos.Tables[0].Rows[0]["usur_cd_usur"].ToString() + "</usur_cd_usur>";

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</endereco>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }





        [WebMethod]
        ///<summary>
        /// Este método recebera os dados dos tipo de veículo a ser cadastrado ou alterado
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarTipoVeiculo(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsTipoVeiculo clsTipoVeiculo = new Classes.clsTipoVeiculo();

            //aqui o objeto da empresa
            Objetos.objTipoVeiculo objTipoVeiculo = new Objetos.objTipoVeiculo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.CadastrarAlterarTipoVeiculoEnvio(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList tpve_cod_tpve = doc.GetElementsByTagName("tpve_cod_tpve");
                XmlNodeList tpve_descricao = doc.GetElementsByTagName("tpve_descricao");
                XmlNodeList tpve_dt_criacao = doc.GetElementsByTagName("tpve_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList tpve_ativo = doc.GetElementsByTagName("tpve_ativo");


                objTipoVeiculo.Tpve_descricao = tpve_descricao[0].InnerText.Trim();
                objTipoVeiculo.Tpve_descricao = tpve_descricao[0].InnerText.Trim();
                objTipoVeiculo.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());
                objTipoVeiculo.Tpve_dt_criacao = Convert.ToDateTime(tpve_dt_criacao[0].InnerText.Trim());
                objTipoVeiculo.Tpve_ativo = Convert.ToBoolean(tpve_ativo[0].InnerText.Trim());
                int tpve_cod_tpve_retorno = 0;

                if (tpve_cod_tpve[0].InnerText.Trim() == "")
                {
                    tpve_cod_tpve_retorno = clsTipoVeiculo.CadastrarTipoVeiculo(objTipoVeiculo);
                }
                else
                {
                    objTipoVeiculo.Tpve_cod_tpve = Convert.ToInt32(tpve_cod_tpve[0].InnerText.Trim());
                    tpve_cod_tpve_retorno = objTipoVeiculo.Tpve_cod_tpve;
                    clsTipoVeiculo.alterarTipoVeiculo(objTipoVeiculo);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<tipoveiculo>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<tpve_cod_tpve>" + tpve_cod_tpve_retorno + "</tpve_cod_tpve>";
                xmlRetorno = xmlRetorno + "</tipoveiculo>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Não apresenta parâmtro de entrada
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido, com o perfil e a lista de atividades a ele relacionadas.</returns>
        ///</summary>
        public string SelecionarTiposVeiculosTodos()
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsTipoVeiculo clsTipoVeiculo = new Classes.clsTipoVeiculo();

            //aqui o objeto da empresa
            Objetos.objTipoVeiculo objTipoVeiculo = new Objetos.objTipoVeiculo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {



                DataSet dsTipoVeiculo = clsTipoVeiculo.SelecionarTodosTiposVeiculos();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<tiposveiculos>";

                if (dsTipoVeiculo.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsTipoVeiculo.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<tipoveiculo>";
                        xmlRetorno = xmlRetorno + "<tpve_cod_tpve>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_cod_tpve"].ToString().Trim() + "</tpve_cod_tpve>";
                        xmlRetorno = xmlRetorno + "<tpve_descricao>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_descricao"].ToString().Trim() + "</tpve_descricao>";
                        xmlRetorno = xmlRetorno + "<tpve_dt_criacao>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_dt_criacao"].ToString().Trim() + "</tpve_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsTipoVeiculo.Tables[0].Rows[i]["usur_cd_usur"].ToString().Trim() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<tpve_ativo>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_ativo"].ToString().Trim() + "</tpve_ativo>";
                        xmlRetorno = xmlRetorno + "</tipoveiculo>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<tipoveiculo>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</tipoveiculo>";
                }

                xmlRetorno = xmlRetorno + "</tiposveiculos>";
                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Não apresenta parâmtro de entrada
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido, com o perfil e a lista de atividades a ele relacionadas.</returns>
        ///</summary>
        public string SelecionarTiposVeiculosativos()
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsTipoVeiculo clsTipoVeiculo = new Classes.clsTipoVeiculo();

            //aqui o objeto da empresa
            Objetos.objTipoVeiculo objTipoVeiculo = new Objetos.objTipoVeiculo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {



                DataSet dsTipoVeiculo = clsTipoVeiculo.SelecionarTiposVeiculosAtivos();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<tiposveiculos>";

                if (dsTipoVeiculo.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsTipoVeiculo.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<tipoveiculo>";
                        xmlRetorno = xmlRetorno + "<tpve_cod_tpve>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_cod_tpve"].ToString().Trim() + "</tpve_cod_tpve>";
                        xmlRetorno = xmlRetorno + "<tpve_descricao>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_descricao"].ToString().Trim() + "</tpve_descricao>";
                        xmlRetorno = xmlRetorno + "<tpve_dt_criacao>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_dt_criacao"].ToString().Trim() + "</tpve_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsTipoVeiculo.Tables[0].Rows[i]["usur_cd_usur"].ToString().Trim() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<tpve_ativo>" + dsTipoVeiculo.Tables[0].Rows[i]["tpve_ativo"].ToString().Trim() + "</tpve_ativo>";
                        xmlRetorno = xmlRetorno + "</tipoveiculo>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<tipoveiculo>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</tipoveiculo>";
                }

                xmlRetorno = xmlRetorno + "</tiposveiculos>";
                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// XML de entrada em formato pré-definido enviando o código do ´tipo de veículo a ser consultado.
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido, com o perfil e a lista de atividades a ele relacionadas.</returns>
        ///</summary>
        public string SelecionarTipoVeiculoPorCodigo(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsTipoVeiculo clsTipoVeiculo = new Classes.clsTipoVeiculo();

            //aqui o objeto da empresa
            Objetos.objTipoVeiculo objTipoVeiculo = new Objetos.objTipoVeiculo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.SelecionarUmTipodeVeiculoEnvio(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList tpve_cod_tpve = doc.GetElementsByTagName("tpve_cod_tpve");

                DataSet dsTipoVeiculo = clsTipoVeiculo.SelecionarUmTiposVeiculos(Convert.ToInt32(tpve_cod_tpve[0].InnerText));

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                xmlRetorno = xmlRetorno + "<tipoveiculo>";
                if (dsTipoVeiculo.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<tpve_cod_tpve>" + dsTipoVeiculo.Tables[0].Rows[0]["tpve_cod_tpve"].ToString().Trim() + "</tpve_cod_tpve>";
                    xmlRetorno = xmlRetorno + "<tpve_descricao>" + dsTipoVeiculo.Tables[0].Rows[0]["tpve_descricao"].ToString().Trim() + "</tpve_descricao>";
                    xmlRetorno = xmlRetorno + "<tpve_dt_criacao>" + dsTipoVeiculo.Tables[0].Rows[0]["tpve_dt_criacao"].ToString().Trim() + "</tpve_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsTipoVeiculo.Tables[0].Rows[0]["usur_cd_usur"].ToString().Trim() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<tpve_ativo>" + dsTipoVeiculo.Tables[0].Rows[0]["tpve_ativo"].ToString().Trim() + "</tpve_ativo>";
                }
                else
                {

                    xmlRetorno = xmlRetorno + "</perfis>";
                }

                xmlRetorno = xmlRetorno + "</tipoveiculo>";
                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do veículo para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarVeiculo(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsVeiculos clsVeiculos = new Classes.clsVeiculos();
            Classes.clsTipoVeiculo clsTipoVeiculo = new Classes.clsTipoVeiculo();

            //aqui o objeto da empresa
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();
            Objetos.objTipoVeiculo objTipoVeiculo = new Objetos.objTipoVeiculo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                /*string resultado = clsValidar.CadastrarAlterarVeiculo(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList veic_cod_veic = doc.GetElementsByTagName("veic_cod_veic");
                XmlNodeList tpve_cod_tpve = doc.GetElementsByTagName("tpve_cod_tpve");
                XmlNodeList tpve_descricao = doc.GetElementsByTagName("tpve_descricao");
                XmlNodeList veic_descricao = doc.GetElementsByTagName("veic_descricao");
                XmlNodeList veic_placa = doc.GetElementsByTagName("veic_placa");
                XmlNodeList veic_dt_criacao = doc.GetElementsByTagName("veic_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList veic_ativo = doc.GetElementsByTagName("veic_ativo");




                objVeiculos.Veic_ativo = Convert.ToBoolean(veic_ativo[0].InnerText.Trim());
                objVeiculos.Veic_descricao = veic_descricao[0].InnerText.Trim();
                objVeiculos.Veic_placa = veic_placa[0].InnerText.Trim();
                objVeiculos.Veic_dt_criacao = Convert.ToDateTime(veic_dt_criacao[0].InnerText.Trim());
                objVeiculos.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());


                int tpve_cod_tpve_retorno = 0;

                if (tpve_cod_tpve[0].InnerText.Trim() == "")
                {
                    objTipoVeiculo.Tpve_descricao = tpve_descricao[0].InnerText.Trim();
                    objTipoVeiculo.Tpve_dt_criacao = Convert.ToDateTime(veic_dt_criacao[0].InnerText.Trim());
                    objTipoVeiculo.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());
                    objTipoVeiculo.Tpve_ativo = Convert.ToBoolean(veic_ativo[0].InnerText.Trim());
                    tpve_cod_tpve_retorno = clsTipoVeiculo.CadastrarTipoVeiculo(objTipoVeiculo);
                }
                else
                {

                    tpve_cod_tpve_retorno = Convert.ToInt32(tpve_cod_tpve[0].InnerText.Trim());
                }
                objVeiculos.Tpve_cod_tpve = tpve_cod_tpve_retorno;

                int veic_cod_veic_retorno = 0;

                if (veic_cod_veic[0].InnerText.Trim() == "")
                {
                    veic_cod_veic_retorno = clsVeiculos.CadastrarVeiculo(objVeiculos);
                }
                else
                {
                    objVeiculos.Veic_cod_veic = Convert.ToInt32(veic_cod_veic[0].InnerText.Trim());
                    veic_cod_veic_retorno = objVeiculos.Veic_cod_veic;
                    clsVeiculos.alterarVeiculo(objVeiculos);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<veiculo>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<veic_cod_veic>" + veic_cod_veic_retorno + "</veic_cod_veic>";
                xmlRetorno = xmlRetorno + "</veiculo>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os veículos cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarVeiculosTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsVeiculos clsVeiculos = new Classes.clsVeiculos();

            //aqui o objeto da empresa
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsVeiculos = new DataSet();
                dsVeiculos = clsVeiculos.SelecionarTodosVeiculos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<veiculos>";
                if (dsVeiculos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsVeiculos.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<veiculo>";
                        xmlRetorno = xmlRetorno + "<veic_cod_veic>" + dsVeiculos.Tables[0].Rows[i]["veic_cod_veic"].ToString() + "</veic_cod_veic>";
                        xmlRetorno = xmlRetorno + "<tpve_cod_tpve>" + dsVeiculos.Tables[0].Rows[i]["tpve_cod_tpve"].ToString() + "</tpve_cod_tpve>";
                        xmlRetorno = xmlRetorno + "<tpve_descricao>" + dsVeiculos.Tables[0].Rows[i]["tpve_descricao"].ToString() + "</tpve_descricao>";
                        xmlRetorno = xmlRetorno + "<veic_descricao>" + dsVeiculos.Tables[0].Rows[i]["veic_descricao"].ToString() + "</veic_descricao>";
                        xmlRetorno = xmlRetorno + "<veic_placa>" + dsVeiculos.Tables[0].Rows[i]["veic_placa"].ToString() + "</veic_placa>";
                        xmlRetorno = xmlRetorno + "<veic_dt_criacao>" + dsVeiculos.Tables[0].Rows[i]["veic_dt_criacao"].ToString() + "</veic_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsVeiculos.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<veic_ativo>" + dsVeiculos.Tables[0].Rows[i]["veic_ativo"].ToString() + "</veic_ativo>";
                        xmlRetorno = xmlRetorno + "</veiculo>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</veiculos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// XML de entrada em formato pré-definido enviando o código do ´veículo a ser consultado.
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido, com o perfil e a lista de atividades a ele relacionadas.</returns>
        ///</summary>
        public string SelecionarVeiculoPorCodigo(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsVeiculos clsVeiculos = new Classes.clsVeiculos();

            //aqui o objeto da empresa
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.SelecionarUmVeiculo(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }

                XmlNodeList veic_cod_veic = doc.GetElementsByTagName("veic_cod_veic");

                DataSet dsVeiculos = clsVeiculos.SelecionarUmVeiculos(Convert.ToInt32(veic_cod_veic[0].InnerText));

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                xmlRetorno = xmlRetorno + "<veiculo>";
                if (dsVeiculos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<veic_cod_veic>" + dsVeiculos.Tables[0].Rows[0]["veic_cod_veic"].ToString() + "</veic_cod_veic>";
                    xmlRetorno = xmlRetorno + "<tpve_cod_tpve>" + dsVeiculos.Tables[0].Rows[0]["tpve_cod_tpve"].ToString() + "</tpve_cod_tpve>";
                    xmlRetorno = xmlRetorno + "<veic_descricao>" + dsVeiculos.Tables[0].Rows[0]["veic_descricao"].ToString() + "</veic_descricao>";
                    xmlRetorno = xmlRetorno + "<veic_placa>" + dsVeiculos.Tables[0].Rows[0]["veic_placa"].ToString() + "</veic_placa>";
                    xmlRetorno = xmlRetorno + "<veic_dt_criacao>" + dsVeiculos.Tables[0].Rows[0]["veic_dt_criacao"].ToString() + "</veic_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsVeiculos.Tables[0].Rows[0]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<veic_ativo>" + dsVeiculos.Tables[0].Rows[0]["veic_ativo"].ToString() + "</veic_ativo>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }

                xmlRetorno = xmlRetorno + "</veiculo>";
                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do usuário para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarMotorista(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsMotorista clsMotorista = new Classes.clsMotorista();

            //aqui o objeto da empresa
            Objetos.objMotorista objMotorista = new Objetos.objMotorista();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList moto_cod_moto = doc.GetElementsByTagName("moto_cod_moto");
                XmlNodeList moto_cpf_moto = doc.GetElementsByTagName("moto_cpf_moto");
                XmlNodeList moto_cnh = doc.GetElementsByTagName("moto_cnh");
                XmlNodeList moto_nome = doc.GetElementsByTagName("moto_nome");
                XmlNodeList moto_lograd = doc.GetElementsByTagName("moto_lograd");
                XmlNodeList moto_num = doc.GetElementsByTagName("moto_num");
                XmlNodeList moto_uf = doc.GetElementsByTagName("moto_uf");
                XmlNodeList moto_comple = doc.GetElementsByTagName("moto_comple");
                XmlNodeList moto_bairro = doc.GetElementsByTagName("moto_bairro");
                XmlNodeList moto_cep = doc.GetElementsByTagName("moto_cep");
                XmlNodeList moto_ddd = doc.GetElementsByTagName("moto_ddd");
                XmlNodeList moto_tel = doc.GetElementsByTagName("moto_tel");
                XmlNodeList moto_email = doc.GetElementsByTagName("moto_email");
                XmlNodeList moto_dt_criacao = doc.GetElementsByTagName("moto_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");




                objMotorista.Moto_cpf_moto = moto_cpf_moto[0].InnerText.ToString();
                objMotorista.Moto_cnh = moto_cnh[0].InnerText.ToString();
                objMotorista.Moto_nome = moto_nome[0].InnerText.ToString();
                objMotorista.Moto_lograd = moto_lograd[0].InnerText.ToString();
                objMotorista.Moto_num = moto_num[0].InnerText.ToString();
                objMotorista.Moto_uf = moto_uf[0].InnerText.ToString();
                objMotorista.Moto_comple = moto_comple[0].InnerText.ToString();
                objMotorista.Moto_bairro = moto_bairro[0].InnerText.ToString();
                objMotorista.Moto_cep = moto_cep[0].InnerText.ToString();
                objMotorista.Moto_ddd = moto_ddd[0].InnerText.ToString();
                objMotorista.Moto_tel = moto_tel[0].InnerText.ToString();
                objMotorista.Moto_email = moto_email[0].InnerText.ToString();
                objMotorista.Moto_dt_criacao = Convert.ToDateTime(moto_dt_criacao[0].InnerText.ToString());
                objMotorista.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());





                int cod_moto_retorno = 0;

                if (moto_cod_moto[0].InnerText.Trim() == "")
                {

                    cod_moto_retorno = clsMotorista.InserirMotorista(objMotorista);

                }
                else
                {
                    objMotorista.Moto_cod_moto = Convert.ToInt32(moto_cod_moto[0].InnerText.Trim());
                    cod_moto_retorno = objMotorista.Moto_cod_moto;
                    clsMotorista.AlterarMotorista(objMotorista);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<motorista>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<moto_cod_moto>" + cod_moto_retorno + "</moto_cod_moto>";
                xmlRetorno = xmlRetorno + "</motorista>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do usuário para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarNivelEstoque(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsNivelEstoque clsNivelEstoque = new Classes.clsNivelEstoque();

            //aqui o objeto da empresa
            Objetos.objNVES objNVES = new Objetos.objNVES();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XmlNodeList nves_cod_nves = doc.GetElementsByTagName("nves_cod_nves");
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList nves_baixo = doc.GetElementsByTagName("nves_baixo");
                XmlNodeList nves_regular = doc.GetElementsByTagName("nves_regular");
                XmlNodeList nves_alto = doc.GetElementsByTagName("nves_alto");
                XmlNodeList nves_risco = doc.GetElementsByTagName("nves_risco");
                XmlNodeList nves_dt_criacao = doc.GetElementsByTagName("nves_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");


                if (nves_cod_nves[0].InnerText.ToString() != "")
                {
                    objNVES.Nves_cod_nves = Convert.ToInt32(nves_cod_nves[0].InnerText.ToString());
                }
                objNVES.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText.ToString());
                objNVES.Emen_cod_empr = Convert.ToInt32(emen_cod_empr[0].InnerText.ToString());
                objNVES.Nves_baixo = Convert.ToInt32(nves_baixo[0].InnerText.ToString());
                objNVES.nves_regular = Convert.ToInt32(nves_regular[0].InnerText.ToString());
                objNVES.nves_alto = Convert.ToInt32(nves_alto[0].InnerText.ToString());
                objNVES.nves_risco = Convert.ToInt32(nves_risco[0].InnerText.ToString());
                objNVES.nves_dt_criacao = Convert.ToDateTime(nves_dt_criacao[0].InnerText.ToString());
                objNVES.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());

                int nves_cod_nves_retorno = 0;

                if (nves_cod_nves[0].InnerText.Trim() == "")
                {

                    nves_cod_nves_retorno = clsNivelEstoque.InserirNivelEstoque(objNVES);

                }
                else
                {
                    objNVES.Nves_cod_nves = Convert.ToInt32(nves_cod_nves[0].InnerText.ToString());
                    nves_cod_nves_retorno = objNVES.Nves_cod_nves;
                    clsNivelEstoque.AlterarNivelEstoque(objNVES);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<NivelEstoque>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<nves_cod_nves>" + nves_cod_nves_retorno + "</nves_cod_nves>";
                xmlRetorno = xmlRetorno + "</NivelEstoque>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método retornará a lista de todos os motoristas cadastrados no sistema.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarMotoristasTodos()
        {
            //aqui instâncio a classe de usuário
            Classes.clsMotorista clsMotorista = new Classes.clsMotorista();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsMotorista = new DataSet();
                dsMotorista = clsMotorista.SelecionarMotoristasTotos();

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<motoristas>";
                if (dsMotorista.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsMotorista.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<motorista>";
                        xmlRetorno = xmlRetorno + "<moto_cod_moto>" + dsMotorista.Tables[0].Rows[i]["moto_cod_moto"].ToString().Trim() + "</moto_cod_moto>";
                        xmlRetorno = xmlRetorno + "<moto_cpf_moto>" + dsMotorista.Tables[0].Rows[i]["moto_cpf_moto"].ToString().Trim() + "</moto_cpf_moto>";
                        xmlRetorno = xmlRetorno + "<moto_cnh>" + dsMotorista.Tables[0].Rows[i]["moto_cnh"].ToString().Trim() + "</moto_cnh>";
                        xmlRetorno = xmlRetorno + "<moto_nome>" + dsMotorista.Tables[0].Rows[i]["moto_nome"].ToString().Trim() + "</moto_nome>";
                        xmlRetorno = xmlRetorno + "<moto_lograd>" + dsMotorista.Tables[0].Rows[i]["moto_lograd"].ToString().Trim() + "</moto_lograd>";
                        xmlRetorno = xmlRetorno + "<moto_num>" + dsMotorista.Tables[0].Rows[i]["moto_num"].ToString().Trim() + "</moto_num>";
                        xmlRetorno = xmlRetorno + "<moto_uf>" + dsMotorista.Tables[0].Rows[i]["moto_uf"].ToString().Trim() + "</moto_uf>";
                        xmlRetorno = xmlRetorno + "<moto_comple>" + dsMotorista.Tables[0].Rows[i]["moto_comple"].ToString().Trim() + "</moto_comple>";
                        xmlRetorno = xmlRetorno + "<moto_bairro>" + dsMotorista.Tables[0].Rows[i]["moto_bairro"].ToString().Trim() + "</moto_bairro>";
                        xmlRetorno = xmlRetorno + "<moto_cep>" + dsMotorista.Tables[0].Rows[i]["moto_cep"].ToString().Trim() + "</moto_cep>";
                        xmlRetorno = xmlRetorno + "<moto_ddd>" + dsMotorista.Tables[0].Rows[i]["moto_ddd"].ToString().Trim() + "</moto_ddd>";
                        xmlRetorno = xmlRetorno + "<moto_tel>" + dsMotorista.Tables[0].Rows[i]["moto_tel"].ToString().Trim() + "</moto_tel>";
                        xmlRetorno = xmlRetorno + "<moto_email>" + dsMotorista.Tables[0].Rows[i]["moto_email"].ToString().Trim() + "</moto_email>";
                        xmlRetorno = xmlRetorno + "<moto_dt_criacao>" + Convert.ToDateTime(dsMotorista.Tables[0].Rows[i]["moto_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</moto_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsMotorista.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "</motorista>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "<motorista></motorista>";
                }

                xmlRetorno = xmlRetorno + "</motoristas>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método retornará a lista de todos os motoristas cadastrados no sistema.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarMotoristasPorCodigo(string xmlString)
        {
            //aqui instâncio a classe de usuário
            Classes.clsMotorista clsMotorista = new Classes.clsMotorista();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList moto_cod_moto = doc.GetElementsByTagName("moto_cod_moto");


                DataSet dsMotorista = new DataSet();
                dsMotorista = clsMotorista.SelecionarMotoristaPorCodigo(Convert.ToInt32(moto_cod_moto[0].InnerText.Trim()));

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                if (dsMotorista.Tables[0].Rows.Count > 0)
                {

                    for (int i = 0; i <= dsMotorista.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<motorista>";
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                        xmlRetorno = xmlRetorno + "<moto_cod_moto>" + dsMotorista.Tables[0].Rows[i]["moto_cod_moto"].ToString().Trim() + "</moto_cod_moto>";
                        xmlRetorno = xmlRetorno + "<moto_cpf_moto>" + dsMotorista.Tables[0].Rows[i]["moto_cpf_moto"].ToString().Trim() + "</moto_cpf_moto>";
                        xmlRetorno = xmlRetorno + "<moto_cnh>" + dsMotorista.Tables[0].Rows[i]["moto_cnh"].ToString().Trim() + "</moto_cnh>";
                        xmlRetorno = xmlRetorno + "<moto_nome>" + dsMotorista.Tables[0].Rows[i]["moto_nome"].ToString().Trim() + "</moto_nome>";
                        xmlRetorno = xmlRetorno + "<moto_lograd>" + dsMotorista.Tables[0].Rows[i]["moto_lograd"].ToString().Trim() + "</moto_lograd>";
                        xmlRetorno = xmlRetorno + "<moto_num>" + dsMotorista.Tables[0].Rows[i]["moto_num"].ToString().Trim() + "</moto_num>";
                        xmlRetorno = xmlRetorno + "<moto_uf>" + dsMotorista.Tables[0].Rows[i]["moto_uf"].ToString().Trim() + "</moto_uf>";
                        xmlRetorno = xmlRetorno + "<moto_comple>" + dsMotorista.Tables[0].Rows[i]["moto_comple"].ToString().Trim() + "</moto_comple>";
                        xmlRetorno = xmlRetorno + "<moto_bairro>" + dsMotorista.Tables[0].Rows[i]["moto_bairro"].ToString().Trim() + "</moto_bairro>";
                        xmlRetorno = xmlRetorno + "<moto_cep>" + dsMotorista.Tables[0].Rows[i]["moto_cep"].ToString().Trim() + "</moto_cep>";
                        xmlRetorno = xmlRetorno + "<moto_ddd>" + dsMotorista.Tables[0].Rows[i]["moto_ddd"].ToString().Trim() + "</moto_ddd>";
                        xmlRetorno = xmlRetorno + "<moto_tel>" + dsMotorista.Tables[0].Rows[i]["moto_tel"].ToString().Trim() + "</moto_tel>";
                        xmlRetorno = xmlRetorno + "<moto_email>" + dsMotorista.Tables[0].Rows[i]["moto_email"].ToString().Trim() + "</moto_email>";
                        xmlRetorno = xmlRetorno + "<moto_dt_criacao>" + Convert.ToDateTime(dsMotorista.Tables[0].Rows[i]["moto_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</moto_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsMotorista.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "</motorista>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<motorista>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</motorista>";
                }



                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método retornará a lista de todos os motoristas cadastrados no sistema.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarNivesEstoquePorCodigo(string xmlString)
        {
            //aqui instâncio a classe de usuário
            Classes.clsNivelEstoque clsNivelEstoque = new Classes.clsNivelEstoque();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList nves_cod_nves = doc.GetElementsByTagName("nves_cod_nves");


                DataSet dsNivel = new DataSet();
                dsNivel = clsNivelEstoque.SelecionarNivelPorCodigo(Convert.ToInt32(nves_cod_nves[0].InnerText.Trim()));

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                if (dsNivel.Tables[0].Rows.Count > 0)
                {

                    for (int i = 0; i <= dsNivel.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<NivelEstoque>";
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsNivel.Tables[0].Rows[i]["prma_cod_prma"].ToString().Trim() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<nves_baixo>" + dsNivel.Tables[0].Rows[i]["nves_baixo"].ToString().Trim() + "</nves_baixo>";
                        xmlRetorno = xmlRetorno + "<nves_regular>" + dsNivel.Tables[0].Rows[i]["nves_regular"].ToString().Trim() + "</nves_regular>";
                        xmlRetorno = xmlRetorno + "<nves_alto>" + dsNivel.Tables[0].Rows[i]["nves_alto"].ToString().Trim() + "</nves_alto>";
                        xmlRetorno = xmlRetorno + "<nves_risco>" + dsNivel.Tables[0].Rows[i]["nves_risco"].ToString().Trim() + "</nves_risco>";
                        xmlRetorno = xmlRetorno + "</NivelEstoque>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<NivelEstoque>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</NivelEstoque>";
                }



                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o id da unidade de medida a ser selecionada
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>

        public string SelecionarFamiliaProdutoPorCodigo(string xmlString)
        {
            //instâncio a classe de usuários
            Classes.clsFamiliaProdutos clsFamiliaProdutos = new Classes.clsFamiliaProdutos();

            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarFamiliaProdutoPorCodigo(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></descerro>";
                    return xmlRetorno;
                }

                XmlNodeList famp_cd_famp = doc.GetElementsByTagName("famp_cod_famp");

                DataSet dsFamp = new DataSet();
                int i = 0;
                dsFamp = clsFamiliaProdutos.SelecionarFamiliaPorCodigo(Convert.ToInt32(famp_cd_famp[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                if (dsFamp.Tables[0].Rows.Count > 0)
                {

                    xmlRetorno = xmlRetorno + "<familiaprodutos>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<familiaproduto>";
                    xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsFamp.Tables[0].Rows[i]["famp_cd_famp"].ToString().Trim() + "</famp_cd_famp>";
                    xmlRetorno = xmlRetorno + "<famp_descricao>" + dsFamp.Tables[0].Rows[i]["famp_descricao"].ToString().Trim() + "</famp_descricao>";
                    xmlRetorno = xmlRetorno + "<famp_dt_criacao>" + Convert.ToDateTime(dsFamp.Tables[0].Rows[i]["famp_dt_criacao"].ToString().Trim()).ToString("MM/dd/yyyy") + "</famp_dt_criacao>";
                    xmlRetorno = xmlRetorno + "</familiaproduto>";
                    xmlRetorno = xmlRetorno + "</familiaprodutos>";

                }
                else
                {

                    xmlRetorno = xmlRetorno + "<familiaprodutos>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</familiaprodutos>";
                }


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><erroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os tipode de produtos/materiais cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTipoProdutosMateriaisTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsTipoProdutosMateriais clsTipoProdutosMateriais = new Classes.clsTipoProdutosMateriais();

            //aqui o objeto da empresa
            Objetos.objTipoProdutosMateriais objTipoProdutosMateriais = new Objetos.objTipoProdutosMateriais();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsTipoProdutosMaterais = new DataSet();
                dsTipoProdutosMaterais = clsTipoProdutosMateriais.SelecionarTodosTipoProdutosMateriais();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<tiposprodutos>";
                if (dsTipoProdutosMaterais.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsTipoProdutosMaterais.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<tipoproduto>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsTipoProdutosMaterais.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsTipoProdutosMaterais.Tables[0].Rows[i]["tpma_descricao"].ToString() + "</tpma_descricao>";
                        xmlRetorno = xmlRetorno + "</tipoproduto>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</tiposprodutos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'><status>Erro</status><descerro>" + e.Message + "</descerro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Não apresenta parâmtro de entrada
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido, com o perfil e a lista de atividades a ele relacionadas.</returns>
        ///</summary>
        public string SelecionarMotivoOcorrenciaTodos()
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsMotivoOcorrencia clsMotivoOcorrencia = new Classes.clsMotivoOcorrencia();

            //aqui o objeto da empresa
            Objetos.objMotivoOcorrencia objMotivoOcorrencia = new Objetos.objMotivoOcorrencia();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                DataSet dsMoticoOcorrencia = clsMotivoOcorrencia.SelecionarMotivoOcorrenciaTodos();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<motivosocorrencias>";

                if (dsMoticoOcorrencia.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsMoticoOcorrencia.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<motivoocorrencia>";
                        xmlRetorno = xmlRetorno + "<mtoc_cod_mtoc>" + dsMoticoOcorrencia.Tables[0].Rows[i]["mtoc_cod_mtoc"].ToString().Trim() + "</mtoc_cod_mtoc>";
                        xmlRetorno = xmlRetorno + "<mtoc_descricao>" + dsMoticoOcorrencia.Tables[0].Rows[i]["mtoc_descricao"].ToString().Trim() + "</mtoc_descricao>";
                        xmlRetorno = xmlRetorno + "</motivoocorrencia>";
                    }
                    xmlRetorno = xmlRetorno + "</motivosocorrencias>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</motivosocorrencias>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Não apresenta parâmtro de entrada
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido, com o perfil e a lista de atividades a ele relacionadas.</returns>
        ///</summary>
        public string SelecionarTipoOperacaoTodos()
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsTipoOperacao clsTipoOperacao = new Classes.clsTipoOperacao();

            //aqui o objeto da empresa
            Objetos.objTipoOperacao objTipoOperacao = new Objetos.objTipoOperacao();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                DataSet dsTipoOperacao = clsTipoOperacao.SelecionaTipoOperacaoTodos();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<tiposoperacao>";

                if (dsTipoOperacao.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsTipoOperacao.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<tipooperacao>";
                        xmlRetorno = xmlRetorno + "<tpop_cod_tpop>" + dsTipoOperacao.Tables[0].Rows[i]["tpop_cod_tpop"].ToString().Trim() + "</tpop_cod_tpop>";
                        xmlRetorno = xmlRetorno + "<tpop_descricao>" + dsTipoOperacao.Tables[0].Rows[i]["tpop_descricao"].ToString().Trim() + "</tpop_descricao>";
                        xmlRetorno = xmlRetorno + "</tipooperacao>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</tiposoperacao>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarNotaFiscalPorCodigoSumarizada(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsUsuario clsusur = new Classes.clsUsuario();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                string[] numNt = canf_numnt[0].InnerText.Split('|');
                string notasInformadas = "";
                //aqui vou pegar a empresa do usuário logado
                DataSet dsUsur = clsusur.SelecionarUsuario(Convert.ToInt32(usur_cod_usur[0].InnerText));
                string emen_cod_empr = "";
                if (dsUsur.Tables[0].Rows.Count > 0)
                {
                    emen_cod_empr = dsUsur.Tables[0].Rows[0]["emen_cod_empr"].ToString();
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Código do usuário logado não encontrado</descerro></retornoErro>";
                    return xmlRetorno;
                }

                for (int a = 0; a <= numNt.Count() - 1; a++)
                {
                    notasInformadas = notasInformadas + "'" + numNt[a].ToString().Trim() + "'" + ",";
                }

                notasInformadas = notasInformadas.Substring(0, notasInformadas.Length - 1);



                DataSet dsNf = new DataSet();
                dsNf = clsNotaFiscal.SelecionarNotaFiscaNumeroSumarizada(notasInformadas, emen_cod_empr);


                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<notasfiscais>";
                if (dsNf.Tables[0].Rows.Count > 0)
                {
                    //troquei origem e destino temporariamente
                    //depois voltar
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int h = 0; h <= dsNf.Tables[0].Rows.Count - 1; h++)
                    {
                        xmlRetorno = xmlRetorno + "<notafiscal>";
                        xmlRetorno = xmlRetorno + "<canf_cod_canf>" + dsNf.Tables[0].Rows[h]["canf_cod_canf"].ToString() + "</canf_cod_canf>";
                        xmlRetorno = xmlRetorno + "<canf_numnt>" + dsNf.Tables[0].Rows[h]["canf_numnt"].ToString() + "</canf_numnt>";
                        xmlRetorno = xmlRetorno + "<canf_serienf>" + dsNf.Tables[0].Rows[h]["canf_serienf"].ToString() + "</canf_serienf>";
                        xmlRetorno = xmlRetorno + "<canf_chavenfe>" + dsNf.Tables[0].Rows[h]["canf_chavenfe"].ToString() + "</canf_chavenfe>";
                        xmlRetorno = xmlRetorno + "<canf_dt_emissao>" + dsNf.Tables[0].Rows[h]["canf_dt_emissao"].ToString() + "</canf_dt_emissao>";
                        xmlRetorno = xmlRetorno + "<canf_dt_cancelamento>" + dsNf.Tables[0].Rows[h]["canf_dt_cancelamento"].ToString() + "</canf_dt_cancelamento>";
                        xmlRetorno = xmlRetorno + "<canf_transito>" + dsNf.Tables[0].Rows[h]["canf_transito"].ToString() + "</canf_transito>";
                        xmlRetorno = xmlRetorno + "<moto_cod_moto>" + dsNf.Tables[0].Rows[h]["moto_cod_moto"].ToString() + "</moto_cod_moto>";
                        xmlRetorno = xmlRetorno + "<moto_nome>" + dsNf.Tables[0].Rows[h]["moto_nome"].ToString().ToString().Trim() + "</moto_nome>";
                        xmlRetorno = xmlRetorno + "<veic_cod_veic>" + dsNf.Tables[0].Rows[h]["veic_cod_veic"].ToString() + "</veic_cod_veic>";
                        xmlRetorno = xmlRetorno + "<veic_placa>" + dsNf.Tables[0].Rows[h]["veic_placa"].ToString() + "</veic_placa>";
                        xmlRetorno = xmlRetorno + "<canf_id_reg>" + dsNf.Tables[0].Rows[h]["canf_id_reg"].ToString() + "</canf_id_reg>";
                        xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsNf.Tables[0].Rows[h]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                        xmlRetorno = xmlRetorno + "<unme_descricao>" + dsNf.Tables[0].Rows[h]["unme_descricao"].ToString() + "</unme_descricao>";
                        xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsNf.Tables[0].Rows[h]["unme_id_reg"].ToString() + "</unme_id_reg>";
                        xmlRetorno = xmlRetorno + "<tran_cod_tran>" + dsNf.Tables[0].Rows[h]["tran_cod_tran"].ToString() + "</tran_cod_tran>";
                        xmlRetorno = xmlRetorno + "<tran_cnpj>" + dsNf.Tables[0].Rows[h]["tran_cnpj"].ToString() + "</tran_cnpj>";
                        xmlRetorno = xmlRetorno + "<tran_nome>" + dsNf.Tables[0].Rows[h]["tran_nome"].ToString() + "</tran_nome>";
                        xmlRetorno = xmlRetorno + "<tran_nomered>" + dsNf.Tables[0].Rows[h]["tran_nomered"].ToString() + "</tran_nomered>";
                        xmlRetorno = xmlRetorno + "<tran_lograd>" + dsNf.Tables[0].Rows[h]["tran_lograd"].ToString() + "</tran_lograd>";
                        xmlRetorno = xmlRetorno + "<tran_num>" + dsNf.Tables[0].Rows[h]["tran_num"].ToString() + "</tran_num>";
                        xmlRetorno = xmlRetorno + "<tran_uf>" + dsNf.Tables[0].Rows[h]["tran_uf"].ToString() + "</tran_uf>";
                        xmlRetorno = xmlRetorno + "<tran_compl>" + dsNf.Tables[0].Rows[h]["tran_compl"].ToString() + "</tran_compl>";
                        xmlRetorno = xmlRetorno + "<tran_bairro>" + dsNf.Tables[0].Rows[h]["tran_bairro"].ToString() + "</tran_bairro>";
                        xmlRetorno = xmlRetorno + "<tran_cep>" + dsNf.Tables[0].Rows[h]["tran_cep"].ToString() + "</tran_cep>";
                        xmlRetorno = xmlRetorno + "<tran_ddd>" + dsNf.Tables[0].Rows[h]["tran_ddd"].ToString() + "</tran_ddd>";
                        xmlRetorno = xmlRetorno + "<tran_tel>" + dsNf.Tables[0].Rows[h]["tran_tel"].ToString() + "</tran_tel>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr_orig>" + dsNf.Tables[0].Rows[h]["emen_cod_empr_orig"].ToString() + "</emen_cod_empr_orig>";
                        xmlRetorno = xmlRetorno + "<emen_cnpj_orig>" + dsNf.Tables[0].Rows[h]["emen_cnpj_orig"].ToString() + "</emen_cnpj_orig>";
                        xmlRetorno = xmlRetorno + "<emen_nome_orig>" + dsNf.Tables[0].Rows[h]["emen_nome_orig"].ToString() + "</emen_nome_orig>";
                        xmlRetorno = xmlRetorno + "<emen_nomered_orig>" + dsNf.Tables[0].Rows[h]["emen_nomered_orig"].ToString() + "</emen_nomered_orig>";
                        xmlRetorno = xmlRetorno + "<emen_lograd_orig>" + dsNf.Tables[0].Rows[h]["emen_lograd_orig"].ToString() + "</emen_lograd_orig>";
                        xmlRetorno = xmlRetorno + "<emen_numero_orig>" + dsNf.Tables[0].Rows[h]["emen_numero_orig"].ToString() + "</emen_numero_orig>";
                        xmlRetorno = xmlRetorno + "<emen_uf_orig>" + dsNf.Tables[0].Rows[h]["emen_uf_orig"].ToString() + "</emen_uf_orig>";
                        xmlRetorno = xmlRetorno + "<emen_comple_orig>" + dsNf.Tables[0].Rows[h]["emen_comple_orig"].ToString() + "</emen_comple_orig>";
                        xmlRetorno = xmlRetorno + "<emen_bairro_orig>" + dsNf.Tables[0].Rows[h]["emen_bairro_orig"].ToString() + "</emen_bairro_orig>";
                        xmlRetorno = xmlRetorno + "<emen_cep_orig>" + dsNf.Tables[0].Rows[h]["emen_cep_orig"].ToString() + "</emen_cep_orig>";
                        xmlRetorno = xmlRetorno + "<emen_ddd_orig>" + dsNf.Tables[0].Rows[h]["emen_ddd_orig"].ToString() + "</emen_ddd_orig>";
                        xmlRetorno = xmlRetorno + "<emen_tel_orig>" + dsNf.Tables[0].Rows[h]["emen_tel_orig"].ToString() + "</emen_tel_orig>";
                        xmlRetorno = xmlRetorno + "<emen_email_orig>" + dsNf.Tables[0].Rows[h]["emen_email_orig"].ToString() + "</emen_email_orig>";
                        xmlRetorno = xmlRetorno + "<emen_cnpj_dest>" + dsNf.Tables[0].Rows[h]["emen_cnpj_dest"].ToString() + "</emen_cnpj_dest>";
                        xmlRetorno = xmlRetorno + "<emen_nome_dest>" + dsNf.Tables[0].Rows[h]["emen_nome_dest"].ToString() + "</emen_nome_dest>";
                        xmlRetorno = xmlRetorno + "<emen_nomered_dest>" + dsNf.Tables[0].Rows[h]["emen_nomered_dest"].ToString() + "</emen_nomered_dest>";
                        xmlRetorno = xmlRetorno + "<emen_lograd_dest>" + dsNf.Tables[0].Rows[h]["emen_lograd_dest"].ToString() + "</emen_lograd_dest>";
                        xmlRetorno = xmlRetorno + "<emen_numero_dest>" + dsNf.Tables[0].Rows[h]["emen_numero_dest"].ToString() + "</emen_numero_dest>";
                        xmlRetorno = xmlRetorno + "<emen_uf_dest>" + dsNf.Tables[0].Rows[h]["emen_uf_dest"].ToString() + "</emen_uf_dest>";
                        xmlRetorno = xmlRetorno + "<emen_cidade>" + dsNf.Tables[0].Rows[h]["emen_cidade"].ToString() + "</emen_cidade>";
                        xmlRetorno = xmlRetorno + "<emen_comple_dest>" + dsNf.Tables[0].Rows[h]["emen_comple_dest"].ToString() + "</emen_comple_dest>";
                        xmlRetorno = xmlRetorno + "<emen_bairro_dest>" + dsNf.Tables[0].Rows[h]["emen_bairro_dest"].ToString() + "</emen_bairro_dest>";
                        xmlRetorno = xmlRetorno + "<emen_cep_dest>" + dsNf.Tables[0].Rows[h]["emen_cep_dest"].ToString() + "</emen_cep_dest>";
                        xmlRetorno = xmlRetorno + "<emen_ddd_dest>" + dsNf.Tables[0].Rows[h]["emen_ddd_dest"].ToString() + "</emen_ddd_dest>";
                        xmlRetorno = xmlRetorno + "<emen_tel_dest>" + dsNf.Tables[0].Rows[h]["emen_tel_dest"].ToString() + "</emen_tel_dest>";
                        xmlRetorno = xmlRetorno + "<emen_email_dest>" + dsNf.Tables[0].Rows[h]["emen_email_dest"].ToString() + "</emen_email_dest>";

                        DataSet dsItnf = clsNotaFiscal.SelecionarNotaFiscaNumeroSumarizada("'" + dsNf.Tables[0].Rows[h]["canf_numnt"].ToString() + "'", emen_cod_empr);

                        xmlRetorno = xmlRetorno + "<itensNf>";
                        int i = 0;
                        for (i = 0; i <= dsItnf.Tables[0].Rows.Count - 1; i++)
                        {
                            xmlRetorno = xmlRetorno + "<itenNf>";

                            xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsItnf.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                            xmlRetorno = xmlRetorno + "<prma_pn>" + dsItnf.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                            xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsItnf.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                            xmlRetorno = xmlRetorno + "<prma_descricao>" + dsItnf.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                            xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsItnf.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                            xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsItnf.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                            xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsItnf.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                            xmlRetorno = xmlRetorno + "<prma_preco>" + dsItnf.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                            xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsItnf.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                            xmlRetorno = xmlRetorno + "<itnf_cod_itnf>" + dsItnf.Tables[0].Rows[i]["itnf_cod_itnf"].ToString() + "</itnf_cod_itnf>";
                            xmlRetorno = xmlRetorno + "<itnf_quantidade>" + dsItnf.Tables[0].Rows[i]["itnf_quantidade"].ToString() + "</itnf_quantidade>";
                            xmlRetorno = xmlRetorno + "<itnf_cod_kit_lata>" + dsItnf.Tables[0].Rows[i]["itnf_cod_kit_lata"].ToString() + "</itnf_cod_kit_lata>";
                            xmlRetorno = xmlRetorno + "<itnf_qtd_kit_lata>" + dsItnf.Tables[0].Rows[i]["itnf_qtd_kit_lata"].ToString() + "</itnf_qtd_kit_lata>";
                            xmlRetorno = xmlRetorno + "<itnf_id_reg>" + dsItnf.Tables[0].Rows[i]["itnf_id_reg"].ToString() + "</itnf_id_reg>";
                            xmlRetorno = xmlRetorno + "<kits_cod_kits>" + dsItnf.Tables[0].Rows[i]["kits_cod_kits"].ToString() + "</kits_cod_kits>";
                            xmlRetorno = xmlRetorno + "<kits_descricao>" + dsItnf.Tables[0].Rows[i]["kits_descricao"].ToString() + "</kits_descricao>";
                            xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsItnf.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                            xmlRetorno = xmlRetorno + "<kits_quant_pallets>" + dsItnf.Tables[0].Rows[i]["kits_quant_pallets"].ToString() + "</kits_quant_pallets>";
                            xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsItnf.Tables[0].Rows[i]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                            xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsItnf.Tables[0].Rows[i]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                            xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsItnf.Tables[0].Rows[i]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";

                            xmlRetorno = xmlRetorno + "</itenNf>";
                        }
                        xmlRetorno = xmlRetorno + "</itensNf>";
                        xmlRetorno = xmlRetorno + "</notafiscal>";

                        h = h + i - 1;
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</notasfiscais>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o numero da nf 
        /// o mesmo irá retornar um xml com os dados da nf
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarNotaFiscalPorCodigo(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList canf_numnf = doc.GetElementsByTagName("canf_numnf");


                DataSet dsNf = new DataSet();
                dsNf = clsNotaFiscal.SelecionarNotaFiscaNumero(canf_numnf[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<notafiscal>";
                if (dsNf.Tables[0].Rows.Count > 0)
                {
                    //troquei origem e destino temporariamente
                    //depois voltar
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<canf_cod_canf>" + dsNf.Tables[0].Rows[0]["canf_cod_canf"].ToString() + "</canf_cod_canf>";
                    xmlRetorno = xmlRetorno + "<canf_numnt>" + dsNf.Tables[0].Rows[0]["canf_numnt"].ToString() + "</canf_numnt>";
                    xmlRetorno = xmlRetorno + "<canf_serienf>" + dsNf.Tables[0].Rows[0]["canf_serienf"].ToString() + "</canf_serienf>";
                    xmlRetorno = xmlRetorno + "<canf_chavenfe>" + dsNf.Tables[0].Rows[0]["canf_chavenfe"].ToString() + "</canf_chavenfe>";
                    xmlRetorno = xmlRetorno + "<canf_dt_emissao>" + dsNf.Tables[0].Rows[0]["canf_dt_emissao"].ToString() + "</canf_dt_emissao>";
                    xmlRetorno = xmlRetorno + "<canf_dt_cancelamento>" + dsNf.Tables[0].Rows[0]["canf_dt_cancelamento"].ToString() + "</canf_dt_cancelamento>";
                    xmlRetorno = xmlRetorno + "<canf_transito>" + dsNf.Tables[0].Rows[0]["canf_transito"].ToString() + "</canf_transito>";
                    xmlRetorno = xmlRetorno + "<moto_cod_moto>" + dsNf.Tables[0].Rows[0]["moto_cod_moto"].ToString() + "</moto_cod_moto>";
                    xmlRetorno = xmlRetorno + "<moto_nome>" + dsNf.Tables[0].Rows[0]["moto_nome"].ToString().ToString().Trim() + "</moto_nome>";
                    xmlRetorno = xmlRetorno + "<veic_cod_veic>" + dsNf.Tables[0].Rows[0]["veic_cod_veic"].ToString() + "</veic_cod_veic>";
                    xmlRetorno = xmlRetorno + "<veic_placa>" + dsNf.Tables[0].Rows[0]["veic_placa"].ToString() + "</veic_placa>";
                    xmlRetorno = xmlRetorno + "<canf_id_reg>" + dsNf.Tables[0].Rows[0]["canf_id_reg"].ToString() + "</canf_id_reg>";
                    xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsNf.Tables[0].Rows[0]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                    xmlRetorno = xmlRetorno + "<unme_descricao>" + dsNf.Tables[0].Rows[0]["unme_descricao"].ToString() + "</unme_descricao>";
                    xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsNf.Tables[0].Rows[0]["unme_id_reg"].ToString() + "</unme_id_reg>";
                    xmlRetorno = xmlRetorno + "<tran_cod_tran>" + dsNf.Tables[0].Rows[0]["tran_cod_tran"].ToString() + "</tran_cod_tran>";
                    xmlRetorno = xmlRetorno + "<tran_cnpj>" + dsNf.Tables[0].Rows[0]["tran_cnpj"].ToString() + "</tran_cnpj>";
                    xmlRetorno = xmlRetorno + "<tran_nome>" + dsNf.Tables[0].Rows[0]["tran_nome"].ToString() + "</tran_nome>";
                    xmlRetorno = xmlRetorno + "<tran_nomered>" + dsNf.Tables[0].Rows[0]["tran_nomered"].ToString() + "</tran_nomered>";
                    xmlRetorno = xmlRetorno + "<tran_lograd>" + dsNf.Tables[0].Rows[0]["tran_lograd"].ToString() + "</tran_lograd>";
                    xmlRetorno = xmlRetorno + "<tran_num>" + dsNf.Tables[0].Rows[0]["tran_num"].ToString() + "</tran_num>";
                    xmlRetorno = xmlRetorno + "<tran_uf>" + dsNf.Tables[0].Rows[0]["tran_uf"].ToString() + "</tran_uf>";
                    xmlRetorno = xmlRetorno + "<tran_compl>" + dsNf.Tables[0].Rows[0]["tran_compl"].ToString() + "</tran_compl>";
                    xmlRetorno = xmlRetorno + "<tran_bairro>" + dsNf.Tables[0].Rows[0]["tran_bairro"].ToString() + "</tran_bairro>";
                    xmlRetorno = xmlRetorno + "<tran_cep>" + dsNf.Tables[0].Rows[0]["tran_cep"].ToString() + "</tran_cep>";
                    xmlRetorno = xmlRetorno + "<tran_ddd>" + dsNf.Tables[0].Rows[0]["tran_ddd"].ToString() + "</tran_ddd>";
                    xmlRetorno = xmlRetorno + "<tran_tel>" + dsNf.Tables[0].Rows[0]["tran_tel"].ToString() + "</tran_tel>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr_orig>" + dsNf.Tables[0].Rows[0]["emen_cod_empr_orig"].ToString() + "</emen_cod_empr_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cnpj_orig>" + dsNf.Tables[0].Rows[0]["emen_cnpj_orig"].ToString() + "</emen_cnpj_orig>";
                    xmlRetorno = xmlRetorno + "<emen_nome_orig>" + dsNf.Tables[0].Rows[0]["emen_nome_orig"].ToString() + "</emen_nome_orig>";
                    xmlRetorno = xmlRetorno + "<emen_nomered_orig>" + dsNf.Tables[0].Rows[0]["emen_nomered_orig"].ToString() + "</emen_nomered_orig>";
                    xmlRetorno = xmlRetorno + "<emen_lograd_orig>" + dsNf.Tables[0].Rows[0]["emen_lograd_orig"].ToString() + "</emen_lograd_orig>";
                    xmlRetorno = xmlRetorno + "<emen_numero_orig>" + dsNf.Tables[0].Rows[0]["emen_numero_orig"].ToString() + "</emen_numero_orig>";
                    xmlRetorno = xmlRetorno + "<emen_uf_orig>" + dsNf.Tables[0].Rows[0]["emen_uf_orig"].ToString() + "</emen_uf_orig>";
                    xmlRetorno = xmlRetorno + "<emen_comple_orig>" + dsNf.Tables[0].Rows[0]["emen_comple_orig"].ToString() + "</emen_comple_orig>";
                    xmlRetorno = xmlRetorno + "<emen_bairro_orig>" + dsNf.Tables[0].Rows[0]["emen_bairro_orig"].ToString() + "</emen_bairro_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cep_orig>" + dsNf.Tables[0].Rows[0]["emen_cep_orig"].ToString() + "</emen_cep_orig>";
                    xmlRetorno = xmlRetorno + "<emen_ddd_orig>" + dsNf.Tables[0].Rows[0]["emen_ddd_orig"].ToString() + "</emen_ddd_orig>";
                    xmlRetorno = xmlRetorno + "<emen_tel_orig>" + dsNf.Tables[0].Rows[0]["emen_tel_orig"].ToString() + "</emen_tel_orig>";
                    xmlRetorno = xmlRetorno + "<emen_email_orig>" + dsNf.Tables[0].Rows[0]["emen_email_orig"].ToString() + "</emen_email_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cnpj_dest>" + dsNf.Tables[0].Rows[0]["emen_cnpj_dest"].ToString() + "</emen_cnpj_dest>";
                    xmlRetorno = xmlRetorno + "<emen_nome_dest>" + dsNf.Tables[0].Rows[0]["emen_nome_dest"].ToString() + "</emen_nome_dest>";
                    xmlRetorno = xmlRetorno + "<emen_nomered_dest>" + dsNf.Tables[0].Rows[0]["emen_nomered_dest"].ToString() + "</emen_nomered_dest>";
                    xmlRetorno = xmlRetorno + "<emen_lograd_dest>" + dsNf.Tables[0].Rows[0]["emen_lograd_dest"].ToString() + "</emen_lograd_dest>";
                    xmlRetorno = xmlRetorno + "<emen_numero_dest>" + dsNf.Tables[0].Rows[0]["emen_numero_dest"].ToString() + "</emen_numero_dest>";
                    xmlRetorno = xmlRetorno + "<emen_uf_dest>" + dsNf.Tables[0].Rows[0]["emen_uf_dest"].ToString() + "</emen_uf_dest>";
                    xmlRetorno = xmlRetorno + "<emen_cidade>" + dsNf.Tables[0].Rows[0]["emen_cidade"].ToString() + "</emen_cidade>";
                    xmlRetorno = xmlRetorno + "<emen_comple_dest>" + dsNf.Tables[0].Rows[0]["emen_comple_dest"].ToString() + "</emen_comple_dest>";
                    xmlRetorno = xmlRetorno + "<emen_bairro_dest>" + dsNf.Tables[0].Rows[0]["emen_bairro_dest"].ToString() + "</emen_bairro_dest>";
                    xmlRetorno = xmlRetorno + "<emen_cep_dest>" + dsNf.Tables[0].Rows[0]["emen_cep_dest"].ToString() + "</emen_cep_dest>";
                    xmlRetorno = xmlRetorno + "<emen_ddd_dest>" + dsNf.Tables[0].Rows[0]["emen_ddd_dest"].ToString() + "</emen_ddd_dest>";
                    xmlRetorno = xmlRetorno + "<emen_tel_dest>" + dsNf.Tables[0].Rows[0]["emen_tel_dest"].ToString() + "</emen_tel_dest>";
                    xmlRetorno = xmlRetorno + "<emen_email_dest>" + dsNf.Tables[0].Rows[0]["emen_email_dest"].ToString() + "</emen_email_dest>";








                    xmlRetorno = xmlRetorno + "<itensNf>";
                    for (int i = 0; i <= dsNf.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<itenNf>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsNf.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsNf.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsNf.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsNf.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsNf.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsNf.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsNf.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsNf.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsNf.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<itnf_cod_itnf>" + dsNf.Tables[0].Rows[i]["itnf_cod_itnf"].ToString() + "</itnf_cod_itnf>";
                        xmlRetorno = xmlRetorno + "<itnf_quantidade>" + dsNf.Tables[0].Rows[i]["itnf_quantidade"].ToString() + "</itnf_quantidade>";
                        xmlRetorno = xmlRetorno + "<itnf_cod_kit_lata>" + dsNf.Tables[0].Rows[i]["itnf_cod_kit_lata"].ToString() + "</itnf_cod_kit_lata>";
                        xmlRetorno = xmlRetorno + "<itnf_qtd_kit_lata>" + dsNf.Tables[0].Rows[i]["itnf_qtd_kit_lata"].ToString() + "</itnf_qtd_kit_lata>";
                        xmlRetorno = xmlRetorno + "<itnf_id_reg>" + dsNf.Tables[0].Rows[i]["itnf_id_reg"].ToString() + "</itnf_id_reg>";
                        xmlRetorno = xmlRetorno + "<kits_cod_kits>" + dsNf.Tables[0].Rows[i]["kits_cod_kits"].ToString() + "</kits_cod_kits>";
                        xmlRetorno = xmlRetorno + "<kits_descricao>" + dsNf.Tables[0].Rows[i]["kits_descricao"].ToString() + "</kits_descricao>";
                        xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsNf.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                        xmlRetorno = xmlRetorno + "<kits_quant_pallets>" + dsNf.Tables[0].Rows[i]["kits_quant_pallets"].ToString() + "</kits_quant_pallets>";
                        xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsNf.Tables[0].Rows[i]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                        xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsNf.Tables[0].Rows[i]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                        xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsNf.Tables[0].Rows[i]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";





                        xmlRetorno = xmlRetorno + "</itenNf>";
                    }
                    xmlRetorno = xmlRetorno + "</itensNf>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</notafiscal>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        //--------------------------------------
        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o numero da nf 
        /// o mesmo irá retornar um xml com os dados da nf
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarNotaFiscalPorCodigoMultiplas(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList canf_numnf = doc.GetElementsByTagName("canf_numnf");


                DataSet dsNf = new DataSet();
                dsNf = clsNotaFiscal.SelecionarNotaFiscaNumeroMultiplas(canf_numnf[0].InnerText.ToString().Replace("|", ","));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<notafiscal>";
                if (dsNf.Tables[0].Rows.Count > 0)
                {
                    //troquei origem e destino temporariamente
                    //depois voltar
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<canf_cod_canf>" + dsNf.Tables[0].Rows[0]["canf_cod_canf"].ToString() + "</canf_cod_canf>";
                    xmlRetorno = xmlRetorno + "<canf_numnt>" + dsNf.Tables[0].Rows[0]["canf_numnt"].ToString() + "</canf_numnt>";
                    xmlRetorno = xmlRetorno + "<canf_serienf>" + dsNf.Tables[0].Rows[0]["canf_serienf"].ToString() + "</canf_serienf>";
                    xmlRetorno = xmlRetorno + "<canf_chavenfe>" + dsNf.Tables[0].Rows[0]["canf_chavenfe"].ToString() + "</canf_chavenfe>";
                    xmlRetorno = xmlRetorno + "<canf_dt_emissao>" + dsNf.Tables[0].Rows[0]["canf_dt_emissao"].ToString() + "</canf_dt_emissao>";
                    xmlRetorno = xmlRetorno + "<canf_dt_cancelamento>" + dsNf.Tables[0].Rows[0]["canf_dt_cancelamento"].ToString() + "</canf_dt_cancelamento>";
                    xmlRetorno = xmlRetorno + "<canf_transito>" + dsNf.Tables[0].Rows[0]["canf_transito"].ToString() + "</canf_transito>";
                    xmlRetorno = xmlRetorno + "<moto_cod_moto>" + dsNf.Tables[0].Rows[0]["moto_cod_moto"].ToString() + "</moto_cod_moto>";
                    xmlRetorno = xmlRetorno + "<moto_nome>" + dsNf.Tables[0].Rows[0]["moto_nome"].ToString().ToString().Trim() + "</moto_nome>";
                    xmlRetorno = xmlRetorno + "<veic_cod_veic>" + dsNf.Tables[0].Rows[0]["veic_cod_veic"].ToString() + "</veic_cod_veic>";
                    xmlRetorno = xmlRetorno + "<veic_placa>" + dsNf.Tables[0].Rows[0]["veic_placa"].ToString() + "</veic_placa>";
                    xmlRetorno = xmlRetorno + "<canf_id_reg>" + dsNf.Tables[0].Rows[0]["canf_id_reg"].ToString() + "</canf_id_reg>";
                    xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsNf.Tables[0].Rows[0]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                    xmlRetorno = xmlRetorno + "<unme_descricao>" + dsNf.Tables[0].Rows[0]["unme_descricao"].ToString() + "</unme_descricao>";
                    xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsNf.Tables[0].Rows[0]["unme_id_reg"].ToString() + "</unme_id_reg>";
                    xmlRetorno = xmlRetorno + "<tran_cod_tran>" + dsNf.Tables[0].Rows[0]["tran_cod_tran"].ToString() + "</tran_cod_tran>";
                    xmlRetorno = xmlRetorno + "<tran_cnpj>" + dsNf.Tables[0].Rows[0]["tran_cnpj"].ToString() + "</tran_cnpj>";
                    xmlRetorno = xmlRetorno + "<tran_nome>" + dsNf.Tables[0].Rows[0]["tran_nome"].ToString() + "</tran_nome>";
                    xmlRetorno = xmlRetorno + "<tran_nomered>" + dsNf.Tables[0].Rows[0]["tran_nomered"].ToString() + "</tran_nomered>";
                    xmlRetorno = xmlRetorno + "<tran_lograd>" + dsNf.Tables[0].Rows[0]["tran_lograd"].ToString() + "</tran_lograd>";
                    xmlRetorno = xmlRetorno + "<tran_num>" + dsNf.Tables[0].Rows[0]["tran_num"].ToString() + "</tran_num>";
                    xmlRetorno = xmlRetorno + "<tran_uf>" + dsNf.Tables[0].Rows[0]["tran_uf"].ToString() + "</tran_uf>";
                    xmlRetorno = xmlRetorno + "<tran_compl>" + dsNf.Tables[0].Rows[0]["tran_compl"].ToString() + "</tran_compl>";
                    xmlRetorno = xmlRetorno + "<tran_bairro>" + dsNf.Tables[0].Rows[0]["tran_bairro"].ToString() + "</tran_bairro>";
                    xmlRetorno = xmlRetorno + "<tran_cep>" + dsNf.Tables[0].Rows[0]["tran_cep"].ToString() + "</tran_cep>";
                    xmlRetorno = xmlRetorno + "<tran_ddd>" + dsNf.Tables[0].Rows[0]["tran_ddd"].ToString() + "</tran_ddd>";
                    xmlRetorno = xmlRetorno + "<tran_tel>" + dsNf.Tables[0].Rows[0]["tran_tel"].ToString() + "</tran_tel>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr_orig>" + dsNf.Tables[0].Rows[0]["emen_cod_empr_orig"].ToString() + "</emen_cod_empr_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cnpj_orig>" + dsNf.Tables[0].Rows[0]["emen_cnpj_orig"].ToString() + "</emen_cnpj_orig>";
                    xmlRetorno = xmlRetorno + "<emen_nome_orig>" + dsNf.Tables[0].Rows[0]["emen_nome_orig"].ToString() + "</emen_nome_orig>";
                    xmlRetorno = xmlRetorno + "<emen_nomered_orig>" + dsNf.Tables[0].Rows[0]["emen_nomered_orig"].ToString() + "</emen_nomered_orig>";
                    xmlRetorno = xmlRetorno + "<emen_lograd_orig>" + dsNf.Tables[0].Rows[0]["emen_lograd_orig"].ToString() + "</emen_lograd_orig>";
                    xmlRetorno = xmlRetorno + "<emen_numero_orig>" + dsNf.Tables[0].Rows[0]["emen_numero_orig"].ToString() + "</emen_numero_orig>";
                    xmlRetorno = xmlRetorno + "<emen_uf_orig>" + dsNf.Tables[0].Rows[0]["emen_uf_orig"].ToString() + "</emen_uf_orig>";
                    xmlRetorno = xmlRetorno + "<emen_comple_orig>" + dsNf.Tables[0].Rows[0]["emen_comple_orig"].ToString() + "</emen_comple_orig>";
                    xmlRetorno = xmlRetorno + "<emen_bairro_orig>" + dsNf.Tables[0].Rows[0]["emen_bairro_orig"].ToString() + "</emen_bairro_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cep_orig>" + dsNf.Tables[0].Rows[0]["emen_cep_orig"].ToString() + "</emen_cep_orig>";
                    xmlRetorno = xmlRetorno + "<emen_ddd_orig>" + dsNf.Tables[0].Rows[0]["emen_ddd_orig"].ToString() + "</emen_ddd_orig>";
                    xmlRetorno = xmlRetorno + "<emen_tel_orig>" + dsNf.Tables[0].Rows[0]["emen_tel_orig"].ToString() + "</emen_tel_orig>";
                    xmlRetorno = xmlRetorno + "<emen_email_orig>" + dsNf.Tables[0].Rows[0]["emen_email_orig"].ToString() + "</emen_email_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cnpj_dest>" + dsNf.Tables[0].Rows[0]["emen_cnpj_dest"].ToString() + "</emen_cnpj_dest>";
                    xmlRetorno = xmlRetorno + "<emen_nome_dest>" + dsNf.Tables[0].Rows[0]["emen_nome_dest"].ToString() + "</emen_nome_dest>";
                    xmlRetorno = xmlRetorno + "<emen_nomered_dest>" + dsNf.Tables[0].Rows[0]["emen_nomered_dest"].ToString() + "</emen_nomered_dest>";
                    xmlRetorno = xmlRetorno + "<emen_lograd_dest>" + dsNf.Tables[0].Rows[0]["emen_lograd_dest"].ToString() + "</emen_lograd_dest>";
                    xmlRetorno = xmlRetorno + "<emen_numero_dest>" + dsNf.Tables[0].Rows[0]["emen_numero_dest"].ToString() + "</emen_numero_dest>";
                    xmlRetorno = xmlRetorno + "<emen_uf_dest>" + dsNf.Tables[0].Rows[0]["emen_uf_dest"].ToString() + "</emen_uf_dest>";
                    xmlRetorno = xmlRetorno + "<emen_cidade>" + dsNf.Tables[0].Rows[0]["emen_cidade"].ToString() + "</emen_cidade>";
                    xmlRetorno = xmlRetorno + "<emen_comple_dest>" + dsNf.Tables[0].Rows[0]["emen_comple_dest"].ToString() + "</emen_comple_dest>";
                    xmlRetorno = xmlRetorno + "<emen_bairro_dest>" + dsNf.Tables[0].Rows[0]["emen_bairro_dest"].ToString() + "</emen_bairro_dest>";
                    xmlRetorno = xmlRetorno + "<emen_cep_dest>" + dsNf.Tables[0].Rows[0]["emen_cep_dest"].ToString() + "</emen_cep_dest>";
                    xmlRetorno = xmlRetorno + "<emen_ddd_dest>" + dsNf.Tables[0].Rows[0]["emen_ddd_dest"].ToString() + "</emen_ddd_dest>";
                    xmlRetorno = xmlRetorno + "<emen_tel_dest>" + dsNf.Tables[0].Rows[0]["emen_tel_dest"].ToString() + "</emen_tel_dest>";
                    xmlRetorno = xmlRetorno + "<emen_email_dest>" + dsNf.Tables[0].Rows[0]["emen_email_dest"].ToString() + "</emen_email_dest>";

                    xmlRetorno = xmlRetorno + "<itensNf>";
                    for (int i = 0; i <= dsNf.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<itenNf>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsNf.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsNf.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsNf.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsNf.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsNf.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsNf.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsNf.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsNf.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsNf.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<kits_cod_kits>" + dsNf.Tables[0].Rows[i]["kits_cod_kits"].ToString() + "</kits_cod_kits>";
                        xmlRetorno = xmlRetorno + "<kits_descricao>" + dsNf.Tables[0].Rows[i]["kits_descricao"].ToString() + "</kits_descricao>";
                        xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsNf.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                        xmlRetorno = xmlRetorno + "<kits_quant_pallets>" + dsNf.Tables[0].Rows[i]["kits_quant_pallets"].ToString() + "</kits_quant_pallets>";
                        xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsNf.Tables[0].Rows[i]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                        xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsNf.Tables[0].Rows[i]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                        xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsNf.Tables[0].Rows[i]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";





                        xmlRetorno = xmlRetorno + "</itenNf>";
                    }
                    xmlRetorno = xmlRetorno + "</itensNf>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</notafiscal>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }
        //------------------------------------

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o codigo do grupo 
        /// o mesmo irá retornar um xml com os dados da nf
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosProdutosGrupoConferencia(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");


                DataSet dsProdutosMaterias = new DataSet();
                dsProdutosMaterias = clsProdutoMaterial.SelecionarTodosProdutosGrupoConferencia(grem_cd_grem[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                if (dsProdutosMaterias.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";

                    xmlRetorno = xmlRetorno + "<itensNf>";
                    for (int i = 0; i <= dsProdutosMaterias.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<itenNf>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutosMaterias.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutosMaterias.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsProdutosMaterias.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<kits_cod_kits>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_cod_kits"].ToString() + "</kits_cod_kits>";
                        xmlRetorno = xmlRetorno + "<kits_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_descricao"].ToString() + "</kits_descricao>";
                        xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                        xmlRetorno = xmlRetorno + "<kits_quant_pallets>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_pallets"].ToString() + "</kits_quant_pallets>";
                        xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                        xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                        xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";

                        xmlRetorno = xmlRetorno + "</itenNf>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "<itensNf>";

                }
                xmlRetorno = xmlRetorno + "</itensNf>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados da diferença na conferencia para cadastro
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarDiferencaConferencia(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();


            //aqui instâncio a classe de empresas
            Classes.clsDiferencaConferencia clsDiferencaConferencia = new Classes.clsDiferencaConferencia();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
            Classes.clsMovimentoEstoque clsMovimentoEstoque = new Classes.clsMovimentoEstoque();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsArim clsArim = new Classes.clsArim();
            Classes.clsProdutoMaterial clsprodutoamaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa
            Objetos.ObjDiferencaConferencia ObjDiferencaConferencia = new Objetos.ObjDiferencaConferencia();
            Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objArim objArim = new Objetos.objArim();



            //criando a lista para poder criar um array dos objetos

            List<Objetos.ObjDiferencaConferencia> lstObjDiferencaConferencia = new List<Objetos.ObjDiferencaConferencia>();
            List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();

            List<Objetos.objSaldoEstoque> lstobjobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";
            string canf_cod_canf_gravacao = "0";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);
				string caminho = Convert.ToString(HttpRuntime.AppDomainAppPath);
				
				
                XmlNodeList itnf_cod_itnf = doc.GetElementsByTagName("itnf_cod_itnf");
                XmlNodeList dirc_qtde_recbto = doc.GetElementsByTagName("dirc_qtde_recbto");
                XmlNodeList dirc_qtde_nf = doc.GetElementsByTagName("dirc_qtde_nf");
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");

                XmlNodeList canf_num_canf = doc.GetElementsByTagName("canf_cod_canf");

                

                var lstPrma = prma_cod_prma.Cast<XmlNode>()
							   .Select(node => node.InnerText)
							   .Select(value => int.Parse(value)).ToList();


				for (int kk = 0; kk <= lstPrma.Count - 1; kk++)
				{
					if (kk > 0)
					{
						if (lstPrma[kk].ToString() == lstPrma[kk - 1].ToString())
						{
							lstPrma.RemoveAt(kk);
							kk = kk - 1;
						}
					}
				}

				XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList dirc_dt_criacao = doc.GetElementsByTagName("dirc_dt_criacao");
                XmlNodeList status_op = doc.GetElementsByTagName("status_op");
                
                XmlNodeList foto_mantem_foto = doc.GetElementsByTagName("foto_mantem_foto");
				XmlNodeList prma = doc.GetElementsByTagName("prma");
				XmlNodeList emen_empr_origem = doc.GetElementsByTagName("emen_empr_origem");

				

				//aqui seleciono o código da nf
				string canf_cod_canf = "0";

                int emen_cod_empr_origem = 0;
                int emen_cod_empr_destino = 0;
                DataSet ds = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestino(canf_num_canf[0].InnerText, Convert.ToInt32(usur_cd_usur[0].InnerText), emen_empr_origem[0].InnerText);
                string gravou = "";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    canf_cod_canf = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                    emen_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_origem"].ToString());
                    emen_cod_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal não encontrada</descerro></retornoErro>";
                }

                Classes.clsNotaFiscal clsnota = new Classes.clsNotaFiscal();
                DataSet dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacao(canf_num_canf[0].InnerText, emen_cod_empr_origem, 3);

                if (dsnota.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota fiscal já conferida</descerro></retornoErro>";
                    return xmlRetorno;
                }
                dsnota = new DataSet();
                dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacao(canf_num_canf[0].InnerText, emen_cod_empr_origem, 2);

                if (dsnota.Tables[0].Rows.Count == 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não foi realizado o recebimento para a nota fiscal informada</descerro></retornoErro>";
                    return xmlRetorno;
                }

                for (int i = 0; i <= itnf_cod_itnf.Count - 1; i++)
                {
                    if (itnf_cod_itnf[i].InnerText.Trim() != "")
                    {
                        ObjDiferencaConferencia.Itnf_cod_itnf = Convert.ToInt32(itnf_cod_itnf[i].InnerText.Trim());
                    }

                    //DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoFornecedora(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()), emen_cod_empr_destino);
                    DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutoCodigoGrupo(Convert.ToInt32(lstPrma[i].ToString()), Convert.ToInt32(ds.Tables[0].Rows[0]["grem_cd_grem_dest"].ToString()));


                    //ObjDiferencaConferencia.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[i].InnerText.Trim());
                    ObjDiferencaConferencia.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_interno"].ToString());
                    ObjDiferencaConferencia.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
                    ObjDiferencaConferencia.Dirc_qtde_recbto = Convert.ToDouble(dirc_qtde_recbto[i].InnerText.Trim());

                    DataSet dsFamp = clsprodutoamaterial.SelecionarUmProdutoMaterial(ObjDiferencaConferencia.Prma_cod_prma);

                    if (dsFamp.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "4")
                    {
                        if (itnf_cod_itnf[i].InnerText.Trim() == "")
                        {
                            if (dirc_qtde_nf[i].InnerText.Trim() != "")
                            {

                                ObjDiferencaConferencia.Dirc_qtde_nf = Convert.ToDouble(dsFamp.Tables[0].Rows[0]["prma_fator_conservacao"].ToString()) * Convert.ToDouble(dirc_qtde_nf[i].InnerText.Trim());
                            }
                            else
                            {
                                ObjDiferencaConferencia.Dirc_qtde_nf = 0;
                            }
                        }
                        else
                        {
                            ObjDiferencaConferencia.Dirc_qtde_nf = Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString()) * Convert.ToDouble(dirc_qtde_nf[i].InnerText.Trim());
                        }

                    }
                    else if (dsFamp.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "3" || ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "5")
                    {
                        ObjDiferencaConferencia.Dirc_qtde_nf = ObjDiferencaConferencia.Dirc_qtde_recbto;
                    }
                    else
                    {
                        ObjDiferencaConferencia.Dirc_qtde_nf = ObjDiferencaConferencia.Dirc_qtde_recbto;
                    }
                    ObjDiferencaConferencia.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[i].InnerText.Trim());
                    ObjDiferencaConferencia.Dirc_dt_criacao = DateTime.Now;

                    objMovimentoEstoque.Moes_dt_criacao = ObjDiferencaConferencia.Dirc_dt_criacao;
                    objMovimentoEstoque.Usur_cod_usur = ObjDiferencaConferencia.Usur_cd_usur;
                    objMovimentoEstoque.Canf_cod_canf = ObjDiferencaConferencia.Canf_cod_canf;
                    objMovimentoEstoque.Moes_quantidade = ObjDiferencaConferencia.Dirc_qtde_recbto;
                    objMovimentoEstoque.Prma_cod_prma = ObjDiferencaConferencia.Prma_cod_prma;
                    objMovimentoEstoque.Tpmo_cod_tpmo = Convert.ToInt32(status_op[i].InnerText.Trim());

                    objSaldoEstoque.Prma_cod_prma = ObjDiferencaConferencia.Prma_cod_prma;
                    objSaldoEstoque.Tpop_cod_tpop = objMovimentoEstoque.Tpmo_cod_tpmo; //codigo do novo status do produto.
                    objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;

                    lstObjDiferencaConferencia.Add(ObjDiferencaConferencia);
                    lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                    lstobjobjSaldoEstoque.Add(objSaldoEstoque);

                    ObjDiferencaConferencia = new Objetos.ObjDiferencaConferencia();
                    objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                    objSaldoEstoque = new Objetos.objSaldoEstoque();


                    for (int k = 0; k <= lstobjobjSaldoEstoque.Count() - 1; k++)
                    {
                        lstobjobjSaldoEstoque[k].Emen_cod_empresa = emen_cod_empr_destino;
                    }
                }
                gravou = clsDiferencaConferencia.cadastraDiferencaConferencia(lstObjDiferencaConferencia, lstobjMovimentoEstoque, lstobjobjSaldoEstoque, emen_cod_empr_origem, emen_cod_empr_destino);

                //aqui vou verificar se temos latas avariadas ou faltantes e gravar na tabela moes
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                DataSet ds1 = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestino(canf_num_canf[0].InnerText, Convert.ToInt32(usur_cd_usur[0].InnerText), emen_empr_origem[0].InnerText);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    canf_cod_canf_gravacao = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                }

				for (int i = 0; i <= prma.Count - 1; i++)
				{
					DataSet dsFamp = clsprodutoamaterial.SelecionarUmProdutoMaterial(Convert.ToInt32(prma[i].ChildNodes[0].InnerText));
					if (dsFamp.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "4")
					{
						if (Convert.ToInt32(prma[i].ChildNodes[0].InnerText) > 0)
						{

							objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
							objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(prma[i].ChildNodes[2].InnerText);
							objMovimentoEstoque.Tpmo_cod_tpmo = 15;
							DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutoCodigoGrupo(Convert.ToInt32(prma[i].ChildNodes[0].InnerText), Convert.ToInt32(ds.Tables[0].Rows[0]["grem_cd_grem_dest"].ToString()));
							objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_interno"].ToString());
							objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf_gravacao);
							objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cd_usur[0].InnerText);
							clsMovimentoEstoque.cadastrarMovimentoEstoqueAvariadasFaltantes(objMovimentoEstoque, emen_cod_empr_destino.ToString());
						}


						objMovimentoEstoque = new Objetos.objMovimentoEstoque();

						if (Convert.ToInt32(prma[i].ChildNodes[0].InnerText) > 0)
						{
							objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
							objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(prma[i].ChildNodes[1].InnerText);
							DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutoCodigoGrupo(Convert.ToInt32(prma[i].ChildNodes[0].InnerText), Convert.ToInt32(ds.Tables[0].Rows[0]["grem_cd_grem_dest"].ToString()));
							objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_interno"].ToString());
							objMovimentoEstoque.Tpmo_cod_tpmo = 16;
							objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf_gravacao);
							objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cd_usur[0].InnerText);
							clsMovimentoEstoque.cadastrarMovimentoEstoqueAvariadasFaltantes(objMovimentoEstoque, emen_cod_empr_destino.ToString());
						}
					}
				}

                if (gravou == "")
                {
                    if (!Convert.ToBoolean(foto_mantem_foto[0].InnerText.Trim()))
                    {
                        clsArim.ApagarImangesImagem(Convert.ToInt32(canf_cod_canf), emen_cod_empr_origem);
                    }

                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<diferenca>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</diferenca>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }

                return xmlRetorno;
				
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o número da nota_fiscal
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarCarregamento(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

			//aqui instâncio a classe de empresas
			Classes.clsVeiculos clsVeiculos = new clsVeiculos();
            Classes.clsCarregamento clsCarregamento = new Classes.clsCarregamento();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsKits clsKits = new Classes.clsKits();


            //aqui o objeto da empresa
            Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objKits objKits = new Objetos.objKits();
            Objetos.objMotorista objMotorista = new Objetos.objMotorista();
			Objetos.objMotorista objMotorista1 = new Objetos.objMotorista();
			Objetos.objTransportadora objTranportadora = new Objetos.objTransportadora();
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();
			Objetos.objVeiculos objVeiculos1 = new Objetos.objVeiculos();
			Objetos.objArim objArim = new Objetos.objArim();


            //aqui vou criar as listas dos objetos
            List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();
            List<Objetos.objKits> lstobjobjKits = new List<Objetos.objKits>();
            List<Objetos.objArim> lstobjArim = new List<Objetos.objArim>();


            DataSet ds = new DataSet();
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string gravou = "";

                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                string[] numNt = canf_numnt[0].InnerText.Split('|');
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                //XmlNodeList moes_dt_criacao = doc.GetElementsByTagName("moes_dt_criacao");
                XmlNodeList moto_cod_moto = doc.GetElementsByTagName("moto_cod_moto");
                //XmlNodeList veic_cod_veic = doc.GetElementsByTagName("veic_cod_veic");
                XmlNodeList foto_nome = doc.GetElementsByTagName("foto_nome");
                XmlNodeList tran_cod_tran = doc.GetElementsByTagName("tran_cod_tran");
                XmlNodeList veic_plac = doc.GetElementsByTagName("veic_plac");
                XmlNodeList foto_base_64 = doc.GetElementsByTagName("foto_base_64");

				int empr_cod_empr_origem_folha = 0;
				int empr_cd_empr_destino_folha = 0;

				string nome_motorista = moto_cod_moto[0].InnerText.ToUpper();
				//aqui irei verificar se o motorista já existe na base.
				//Caso exista irei pegar o id, caso contrário irei cadastra-lo é pegar o id

				clsMotorista clsMoto = new clsMotorista();
				DataSet dsMoto = clsMoto.SelecionarMotoristaPorNome(nome_motorista.ToUpper().Trim());
				
				if (dsMoto.Tables[0].Rows.Count > 0)
				{
					objMotorista.Moto_cod_moto = Convert.ToInt32(dsMoto.Tables[0].Rows[0]["moto_cod_moto"].ToString());
				}
				else
				{
					objMotorista1.Moto_nome = nome_motorista;
					objMotorista1.Moto_dt_criacao = DateTime.Now;
					objMotorista1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
					objMotorista.Moto_cod_moto = clsMoto.InserirMotorista(objMotorista1);
					
				}

				string veic_plac_aux = "";
                if (veic_plac[0] != null)
                {
                    veic_plac_aux = veic_plac[0].InnerText.ToUpper();
                }

				//aqui vou verificar se a placa já está cadastrada na base
				//caso esteja pegarei o id existente, caso contrário irei cadastrar o veículo
				//e pegar o código do mesmo.
				if (veic_plac_aux.Trim() != "")
				{
					DataSet Dsveic = new DataSet();
					Dsveic = clsVeiculos.SelecionarUmVeiculoPlaca(veic_plac_aux.Trim().ToUpper());

					if (Dsveic.Tables[0].Rows.Count > 0)
					{
						objVeiculos.Veic_cod_veic = Convert.ToInt32(Dsveic.Tables[0].Rows[0]["veic_cod_veic"].ToString());
					}
					else
					{
						objVeiculos1.Veic_placa = veic_plac_aux.Trim().ToUpper();
						objVeiculos1.Veic_dt_criacao = DateTime.Now;
						objVeiculos1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText.ToString());
						//aqui como a placa não existe na tabela de veículos, irei cadastrar a mesma.
						objVeiculos.Veic_cod_veic = clsVeiculos.CadastrarVeiculo(objVeiculos1);
					}
				}

                if (tran_cod_tran[0] != null)
                {
                    if (tran_cod_tran[0].InnerText.Trim() != "")
                    {
                        objTranportadora.Tran_cod_tran = Convert.ToInt32(tran_cod_tran[0].InnerText);
                    }
                }

                //aqui vou selecionar a nota fiscal para pegar as empresas e os itens.

                for (int k = 0; k <= numNt.Count() - 1; k++)
                {
                    //ds = clsNotaFiscal.SelecionarNotaFiscaNumero(numNt[k].ToString());
                    ds = clsNotaFiscal.SelecionarNotaFiscaNumeroSemMovimentacao(numNt[k].ToString());
                    int empr_cod_empr_origem = 0;
                    int empr_cd_empr_destino = 0;
                    int quantidade_folhas = 0;
                    int quantidade_paletes = 0;
                    int quantidade_folhas_aux = 0;
                    int quantidade_paletes_aux = 0;
                    string canf_cod_canf = "";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit
                        canf_cod_canf = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                        DataSet dsKit = new DataSet();
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString().Trim() == "4")
                            {

                                dsKit = clsKits.SelecionarKitProduto(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                                if (dsKit.Tables[0].Rows.Count > 0)
                                {
                                    objKits.Prma_cod_prma = Convert.ToInt32(dsKit.Tables[0].Rows[0]["prma_cod_prma"].ToString());
                                    objKits.Kits_quant_prod_prin = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString());
                                    objKits.Kits_quant_folha = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_folha"].ToString());
									objKits.Kits_quant_por_camada = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_por_camada"].ToString());
									objKits.Quantidade_nf = Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString().Trim());
                                    objKits.Fator_conservacao = Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Trim());
                                    objKits.Famp_cd_famp = Convert.ToInt32(ds.Tables[0].Rows[i]["famp_cd_famp"].ToString().Trim());
                                    lstobjobjKits.Add(objKits);
                                    objKits = new Objetos.objKits();
                                }
                                else
                                {
                                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Kit não existente para o produto</descerro></retornoErro>";
                                    return xmlRetorno;
                                }
                            }

                        }

                        empr_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_origem"].ToString());
                        empr_cd_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());

						empr_cod_empr_origem_folha = empr_cod_empr_origem;
						empr_cd_empr_destino_folha = empr_cd_empr_destino;


						Classes.clsNotaFiscal clsnota = new Classes.clsNotaFiscal();
                        DataSet dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacaoPorCodigo(canf_cod_canf, 1);

                        if (dsnota.Tables[0].Rows.Count > 0)
                        {
                            xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota fiscal já carregada</descerro></retornoErro>";
                            return xmlRetorno;
                        }



                        //aqui vou montar o objeto de atualização do estoque
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            //aqui irei montar a quantidade a ser colocada em cada produto dependendo da família do mesmo:
                            //1 pallet, 2 top, 3 folhas

                            objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(ds.Tables[0].Rows[i]["canf_cod_canf"].ToString());
                            objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                            objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString());
                            objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
							objMovimentoEstoque.Familia_produto = ds.Tables[0].Rows[i]["famp_cd_famp"].ToString();

							objMovimentoEstoque.Tpmo_cod_tpmo = 1;

                            if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "4")
                            {
                                //aqui irei descobrir o fator de conversão e assim calcular a quatidade correta

                                // objMovimentoEstoque.Moes_quantidade = Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString()) * Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString());
                                objMovimentoEstoque.Moes_quantidade = Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString()) * Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString());
                            }
                            else if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "2" || ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "1")
                            {
                                objMovimentoEstoque.Moes_quantidade = Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString()) * Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString()); ;
                                quantidade_paletes = Convert.ToInt32(objMovimentoEstoque.Moes_quantidade);
                            }
                            /*else if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "3")
                            {

								//folha

                                objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(lstobjobjKits[0].Kits_quant_folha * quantidade_paletes); ;
                            }*/
							int achou = 0;
							for (int t = 0; t <= lstobjMovimentoEstoque.Count - 1; t++)
							{
								if (objMovimentoEstoque.Prma_cod_prma == lstobjMovimentoEstoque[t].Prma_cod_prma)
								{
									lstobjMovimentoEstoque[t].Moes_quantidade = lstobjMovimentoEstoque[t].Moes_quantidade + objMovimentoEstoque.Moes_quantidade;
									achou = 1;
								}
							}

							if (achou == 0)
							{
								if (objMovimentoEstoque.Moes_quantidade > 0)
								{
									lstobjMovimentoEstoque.Add(objMovimentoEstoque);
								}
							}

							if (objMovimentoEstoque.Moes_quantidade > 0)
							{
								objSaldoEstoque.Tpop_cod_tpop = 1;
								objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
								objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
								objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
								objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;

								lstobjSaldoEstoque.Add(objSaldoEstoque);
							}
                            objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                            objSaldoEstoque = new Objetos.objSaldoEstoque();


                        }

                        string caminhoImagemOrigem = System.Configuration.ConfigurationManager.AppSettings["caminhoImagemOrigem"];

                        for (int j = 0; j <= foto_nome.Count - 1; j++)
                        {
                            

                            objArim.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
                            objArim.Arim_imagem = empr_cod_empr_origem.ToString() + foto_nome[j].InnerText;
                            objArim.Tpmo_cod_tpmo = 1;
                            objArim.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                            objArim.Arim_dt_criacao = DateTime.Now;
                            objArim.Emen_cod_empr = empr_cod_empr_origem;
                            lstobjArim.Add(objArim);
                            objArim = new Objetos.objArim();

                            //aqui salvarei a imagem a partir da string base64

                            string x = foto_base_64[j].InnerText;
                            // Convert Base64 String to byte[]
                            byte[] imageBytes = Convert.FromBase64String(x);

                           
                            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                            // Convert byte[] to Image
                            ms.Write(imageBytes, 0, imageBytes.Length);
                            System.Drawing.Image image = (Bitmap)Bitmap.FromStream(ms);

                            image.Save(Path.Combine(caminhoImagemOrigem, empr_cod_empr_origem.ToString() + foto_nome[j].InnerText), System.Drawing.Imaging.ImageFormat.Jpeg);
                         
                        }
                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal não encontrada</descerro></retornoErro>";
                        return xmlRetorno;
                    }
                }

				//aqui irei pegar o dataset da nf para calcular as folhas inerentes ao sistema
				bool CalculoFolha = false;
				double quant_folhas = 0;
				double quant_palet = 0;
				double qt = 0;
				string prma_folha = "";
				for (int g = 0; g <= ds.Tables[0].Rows.Count - 1; g++)
				{
					if (ds.Tables[0].Rows[g]["famp_cd_famp"].ToString().Trim() == "3" && CalculoFolha==false) //folha)
					{
						prma_folha = ds.Tables[0].Rows[g]["prma_cod_prma"].ToString();

						for (int k = 0; k <= lstobjMovimentoEstoque.Count - 1; k++)
						{
							if (lstobjMovimentoEstoque[k].Familia_produto == "4") //folha)
							{
								//aqui vou pegar o kit relativo a este produto e calular a quantidade de folhas
								for (int jj = 0; jj <= lstobjobjKits.Count - 1; jj++)
								{
									if (lstobjobjKits[jj].Prma_cod_prma.ToString() == lstobjMovimentoEstoque[k].Prma_cod_prma.ToString())
									{
										for (int mm = 0; mm <= ds.Tables[0].Rows.Count - 1; mm++)
										{
											if (lstobjobjKits[jj].Prma_cod_prma.ToString() == ds.Tables[0].Rows[mm]["prma_cod_prma"].ToString())
											{
												//calculando a quantidade de latas pelo fatoe de conversão
												qt = Convert.ToDouble(lstobjobjKits[jj].Fator_conservacao.ToString()) * Convert.ToDouble(ds.Tables[0].Rows[mm]["itnf_quantidade"].ToString());
											}
										}

										for (int tt = 0; tt <= lstobjobjKits.Count - 1; tt++)
										{
											if (lstobjobjKits[jj].Prma_cod_prma.ToString() == lstobjobjKits[tt].Prma_cod_prma.ToString())
											{
												//aqui vou calcular as folhas para este produto pelo seu kit
												quant_palet = Math.Ceiling(qt / Convert.ToInt32(lstobjobjKits[tt].Kits_quant_prod_prin));
												quant_folhas = (quant_folhas) + (qt /(Convert.ToInt32(lstobjobjKits[tt].Kits_quant_por_camada)));
												quant_folhas = quant_folhas + quant_palet;
												// = (qt / lstobjobjKits[jj].Kits_quant_por_camada) + quantidade_pla;
											}
										}
										CalculoFolha = true;
									}
								}


							}
						}
					}
				}

				//aqui vou inserir as folhas
				objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(ds.Tables[0].Rows[0]["canf_cod_canf"].ToString());
				objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
				objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(prma_folha);
				objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
				objMovimentoEstoque.Tpmo_cod_tpmo = 1;
				objMovimentoEstoque.Moes_quantidade = quant_folhas;
				lstobjMovimentoEstoque.Add(objMovimentoEstoque);

				objSaldoEstoque.Tpop_cod_tpop = 1;
				objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
				objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
				objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem_folha;
				objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino_folha;
				lstobjSaldoEstoque.Add(objSaldoEstoque);



				gravou = clsCarregamento.cadastraCarregamento(lstobjMovimentoEstoque, lstobjSaldoEstoque, objMotorista.Moto_cod_moto, objVeiculos.Veic_cod_veic, objTranportadora.Tran_cod_tran, veic_plac_aux, lstobjArim);

                //aqui vou recriar as imagens


                if (gravou == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<carregamento>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</carregamento>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o número da nota_fiscal
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarRecebimento(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
            //aqui instâncio a classe de empresas
            Classes.clsRecebimento clsRecebimento = new Classes.clsRecebimento();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsGrupo clsGrupo = new Classes.clsGrupo();
			clsMotorista clsMoto = new clsMotorista();
			clsVeiculos clsVeiculos = new clsVeiculos();

			//aqui o objeto da empresa
			Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objKits objKits = new Objetos.objKits();
            Objetos.objMotorista objMotorista = new Objetos.objMotorista();
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();
            Objetos.objTransportadora objTranportadora = new Objetos.objTransportadora();
			Objetos.objMotorista objMotorista1 = new Objetos.objMotorista();
			Objetos.objVeiculos objVeiculos1 = new Objetos.objVeiculos();

			//aqui vou criar as listas dos objetos
			List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();
            List<Objetos.objKits> lstobjobjKits = new List<Objetos.objKits>();

            DataSet ds = new DataSet();
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";
            int empr_cod_empr_origem = 0;
            int empr_cd_empr_destino = 0;

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

				//string caminho = Convert.ToString(HttpRuntime.AppDomainAppPath);
				//doc.Save(caminho + "/confereME.xml");



				XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string canf_cod_canf = "";
                string gravou = "";
                string cpf_motorista = "";

                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                string[] numNt = canf_numnt[0].InnerText.Split('|');
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                XmlNodeList moes_dt_criacao = doc.GetElementsByTagName("moes_dt_criacao");
                XmlNodeList moto_cod_moto = doc.GetElementsByTagName("moto_cod_moto");
                XmlNodeList cpfmoto = doc.GetElementsByTagName("cpfmoto");
                //XmlNodeList veic_cod_veic = doc.GetElementsByTagName("veic_cod_veic");
                XmlNodeList tran_cod_tran = doc.GetElementsByTagName("tran_cod_tran");
                XmlNodeList veic_plac = doc.GetElementsByTagName("veic_plac");
				XmlNodeList emen_empr_origem = doc.GetElementsByTagName("emen_empr_origem");
				string nome_motorista = moto_cod_moto[0].InnerText.ToString().Trim().ToUpper();

                using (var writer = new StreamWriter(@System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "/" + canf_numnt[0].InnerText + ".xml"))
                {
                    // Escreve uma string formatada no arquivo
                    writer.Write(xmlString);

                };

                if (cpfmoto != null)
                {
                    cpf_motorista = cpfmoto[0].InnerText.ToString().Trim().ToUpper();
                }

                DataSet dsMoto = clsMoto.SelecionarMotoristaPorNome(nome_motorista.ToUpper().Trim());
				
				if (dsMoto.Tables[0].Rows.Count > 0)
				{
					objMotorista.Moto_cod_moto = Convert.ToInt32(dsMoto.Tables[0].Rows[0]["moto_cod_moto"].ToString());
				}
				else
				{
					objMotorista1.Moto_nome = nome_motorista;
					objMotorista1.Moto_dt_criacao = DateTime.Now;
					objMotorista1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
					objMotorista.Moto_cod_moto = clsMoto.InserirMotorista(objMotorista1);
				}

				string veic_plac_aux = "";
				string placa_velha = "";
				if (veic_plac[0] != null)
				{
					veic_plac_aux = veic_plac[0].InnerText.ToUpper();
				}

                

				//aqui vou verificar se a placa já está cadastrada na base
				//caso esteja pegarei o id existente, caso contrário irei cadastrar o veículo
				//e pegar o código do mesmo.
				if (veic_plac_aux.Trim() != "")
				{
					DataSet Dsveic = new DataSet();
					Dsveic = clsVeiculos.SelecionarUmVeiculoPlaca(veic_plac_aux.Trim().ToUpper());

					if (Dsveic.Tables[0].Rows.Count > 0)
					{
						objVeiculos.Veic_cod_veic = Convert.ToInt32(Dsveic.Tables[0].Rows[0]["veic_cod_veic"].ToString());
					}
					else
					{
						objVeiculos1.Veic_placa = veic_plac_aux.Trim().ToUpper();
						objVeiculos1.Veic_dt_criacao = DateTime.Now;
						objVeiculos1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText.ToString());
						//aqui como a placa não existe na tabela de veículos, irei cadastrar a mesma.
						objVeiculos.Veic_cod_veic = clsVeiculos.CadastrarVeiculo(objVeiculos1);
					}
				}



				if (tran_cod_tran[0].InnerText.Trim() != "")
                {
                    objTranportadora.Tran_cod_tran = Convert.ToInt32(tran_cod_tran[0].InnerText);
                }

                for (int l = 0; l <= numNt.Count() - 1; l++)
                {
                    //aqui vou selecionar a nota fiscal para pegar as empresas e os itens.
                    ds = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestino(numNt[l].ToString(), Convert.ToInt32(usur_cod_usur[0].InnerText), emen_empr_origem[0].InnerText);

                    double quantidade_paletes = 0;
                    double quantiadae_produto_lata = 0;
                    double quantidade_folhas = 0;


                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        canf_cod_canf = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                        //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit

                        DataSet dsKit = new DataSet();
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString().Trim() == "4")
                            {
                                dsKit = new DataSet();
                                dsKit = clsKits.SelecionarKitProduto(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()));

                                objKits.Prma_cod_prma = Convert.ToInt32(dsKit.Tables[0].Rows[0]["prma_cod_prma"].ToString());
                                objKits.Kits_quant_prod_prin = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString());
                                objKits.Kits_quant_folha = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_folha"].ToString());

								objKits.Kits_quant_por_camada = Convert.ToDouble(dsKit.Tables[0].Rows[0]["Kits_quant_por_camada"].ToString());

								objKits.Quantidade_nf = Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString().Trim());
                                objKits.Fator_conservacao = Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Trim());
								lstobjobjKits.Add(objKits);
								objKits = new Objetos.objKits();

							}

                        }
						double quantidade_paletes_produto = 0;
                        for (int j = 0; j <= ds.Tables[0].Rows.Count - 1; j++)
                        {
                            if (ds.Tables[0].Rows[j]["famp_cd_famp"].ToString() == "4")
                            {

								for (int mm = 0;mm<=lstobjobjKits.Count-1;mm++)
								{
									if (ds.Tables[0].Rows[j]["prma_cod_prma"].ToString() == lstobjobjKits[mm].Prma_cod_prma.ToString())
									{
										quantidade_paletes_produto = Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows[j]["itnf_quantidade"].ToString().Trim()) / Convert.ToDouble(lstobjobjKits[mm].Kits_quant_prod_prin) * Convert.ToDouble(ds.Tables[0].Rows[j]["prma_fator_conservacao"].ToString()));
										quantiadae_produto_lata = Convert.ToDouble(ds.Tables[0].Rows[j]["itnf_quantidade"].ToString().Trim()) * Convert.ToDouble(ds.Tables[0].Rows[j]["prma_fator_conservacao"].ToString());
										//quantidade_paletes = Convert.ToInt32(Convert.ToDouble(ds.Tables[0].Rows[j]["itnf_quantidade"].ToString().Trim()) * Convert.ToDouble(ds.Tables[0].Rows[0]["prma_fator_conservacao"].ToString()) / objKits.Kits_quant_prod_prin);
										quantidade_paletes = Math.Ceiling(quantidade_paletes + Convert.ToInt32(Convert.ToDouble(ds.Tables[0].Rows[j]["itnf_quantidade"].ToString().Trim()) * 1000) / lstobjobjKits[mm].Kits_quant_prod_prin);
										quantidade_folhas = quantidade_folhas + (Convert.ToDouble(Convert.ToDouble(ds.Tables[0].Rows[j]["itnf_quantidade"].ToString().Trim()) * 1000) / lstobjobjKits[mm].Kits_quant_por_camada) + quantidade_paletes_produto; 
									}
								}
                            }

                        }

                    



                        empr_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_origem"].ToString());
                        empr_cd_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());
                        Classes.clsNotaFiscal clsnota = new Classes.clsNotaFiscal();
                        DataSet dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacao(canf_numnt[0].InnerText, empr_cod_empr_origem, 2);
											

                        if (dsnota.Tables[0].Rows.Count > 0)
                        {
                            xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota fiscal já recebida</descerro></retornoErro>";
                            return xmlRetorno;
                        }
                        //aqui vou montar o objeto de atualização do estoque
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            //aqui irei montar a quantidade a ser colocada em cada produto dependendo da família do mesmo:
                            //1 pallet, 2 top, 3 folhas

                            //DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoFornecedora(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()), empr_cd_empr_destino);

                            //DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExterno(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()));

                            DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutoCodigoGrupo(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()), Convert.ToInt32(ds.Tables[0].Rows[i]["grem_cd_grem_dest"].ToString()));

                            objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(ds.Tables[0].Rows[i]["canf_cod_canf"].ToString());
                            objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                            objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_interno"].ToString());
                            //objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString());
                            objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                            objMovimentoEstoque.Tpmo_cod_tpmo = 2;
                            if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "4")
                            {
                                quantiadae_produto_lata = Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString().Trim()) * Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString());
                                //quantidade_paletes = Convert.ToInt32(Convert.ToDouble(ds.Tables[0].Rows[j]["itnf_quantidade"].ToString().Trim()) * Convert.ToDouble(ds.Tables[0].Rows[0]["prma_fator_conservacao"].ToString()) / objKits.Kits_quant_prod_prin);
                                //quantidade_paletes = quantidade_paletes + Convert.ToInt32(Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString().Trim()) * 1000) / objKits.Kits_quant_prod_prin;

                                objMovimentoEstoque.Moes_quantidade = quantiadae_produto_lata;
                            }
                            else if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "2" || ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "1")
                            {
                              
                                    objMovimentoEstoque.Moes_quantidade = quantidade_paletes;
                              
                            }
                            else if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "3")
                            {
                                objMovimentoEstoque.Moes_quantidade = quantidade_folhas;
                            }

							bool achou = false;
							for (int jj = 0; jj <= lstobjMovimentoEstoque.Count - 1; jj++)
							{
								if (objMovimentoEstoque.Prma_cod_prma == lstobjMovimentoEstoque[jj].Prma_cod_prma)
								{
									lstobjMovimentoEstoque[jj].Moes_quantidade = objMovimentoEstoque.Moes_quantidade;
									achou = true;
								}
							}
							if (achou == false)
							{
								lstobjMovimentoEstoque.Add(objMovimentoEstoque);
							}

							objSaldoEstoque.Tpop_cod_tpop = 2;
                            objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
                            objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;

                            lstobjSaldoEstoque.Add(objSaldoEstoque);
                            objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                            objSaldoEstoque = new Objetos.objSaldoEstoque();


                        }

                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal não encontrada</descerro></retornoErro>";
                        return xmlRetorno;
                    }
                }
				clsVeiculos clsVeic = new clsVeiculos();
				DataSet veicDs = clsVeic.SelecionarUmVeiculos(Convert.ToInt32(ds.Tables[0].Rows[0]["veic_cod_veic"].ToString()));

				if (veicDs.Tables[0].Rows.Count > 0)
				{
					placa_velha = veicDs.Tables[0].Rows[0]["veic_placa"].ToString();
				}


				gravou = clsRecebimento.cadastraRecebimento(lstobjMovimentoEstoque, lstobjSaldoEstoque, empr_cod_empr_origem, empr_cd_empr_destino, objMotorista.Moto_cod_moto, objVeiculos.Veic_cod_veic, canf_cod_canf, objTranportadora.Tran_cod_tran.ToString(), veic_plac_aux, placa_velha, cpf_motorista);
                if (gravou == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<carregamento>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</carregamento>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os grupos cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarKitsTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsKits clsKits = new Classes.clsKits();

            //aqui o objeto da empresa
            Objetos.objKits objKits = new Objetos.objKits();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsKits = new DataSet();
                dsKits = clsKits.SelecionarKitsTodos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<kits>";
                if (dsKits.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsKits.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<kit>";
                        xmlRetorno = xmlRetorno + "<kits_cod_kits>" + dsKits.Tables[0].Rows[i]["kits_cod_kits"].ToString() + "</kits_cod_kits>";
                        xmlRetorno = xmlRetorno + "<kits_descricao>" + dsKits.Tables[0].Rows[i]["kits_descricao"].ToString() + "</kits_descricao>";
                        xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsKits.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";

                        xmlRetorno = xmlRetorno + "<kits_quant_pallets>" + dsKits.Tables[0].Rows[i]["kits_quant_pallets"].ToString() + "</kits_quant_pallets>";
                        xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsKits.Tables[0].Rows[i]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                        xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsKits.Tables[0].Rows[i]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                        xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsKits.Tables[0].Rows[i]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsKits.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsKits.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<kits_dt_criacao>" + dsKits.Tables[0].Rows[i]["kits_dt_criacao"].ToString() + "</kits_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsKits.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsKits.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                        xmlRetorno = xmlRetorno + "</kit>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</kits>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do kit para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarKit(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsKits clsKits = new Classes.clsKits();

            //aqui o objeto da empresa
            Objetos.objKits objKits = new Objetos.objKits();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.CadastroAlteracaoProdutoMaterial(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList kits_cod_kits = doc.GetElementsByTagName("kits_cod_kits");
                XmlNodeList kits_descricao = doc.GetElementsByTagName("kits_descricao");
                XmlNodeList kits_dt_criacao = doc.GetElementsByTagName("kits_dt_criacao");
                XmlNodeList kits_quant_prod_prin = doc.GetElementsByTagName("kits_quant_prod_prin");
                XmlNodeList kits_quant_pallets = doc.GetElementsByTagName("kits_quant_pallets");
                XmlNodeList kits_quant_topo = doc.GetElementsByTagName("kits_quant_topo");
                XmlNodeList kits_quant_folha = doc.GetElementsByTagName("kits_quant_folha");
                XmlNodeList kits_quant_por_camada = doc.GetElementsByTagName("kits_quant_por_camada");
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");


                objKits.Kits_descricao = kits_descricao[0].InnerText.Trim();
                objKits.Kits_quant_prod_prin = Convert.ToDouble(kits_quant_prod_prin[0].InnerText.Trim());
                objKits.Kits_quant_pallets = Convert.ToDouble(kits_quant_pallets[0].InnerText.Trim());
                objKits.Kits_quant_topo = Convert.ToDouble(kits_quant_topo[0].InnerText.Trim());
                objKits.Kits_quant_folha = Convert.ToDouble(kits_quant_folha[0].InnerText.Trim());
                objKits.Kits_quant_por_camada = Convert.ToDouble(kits_quant_por_camada[0].InnerText.Trim());
                objKits.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText.Trim());
                objKits.Kits_dt_criacao = Convert.ToDateTime(kits_dt_criacao[0].InnerText.Trim());
                objKits.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());


                int kits_cod_kits_retorno = 0;

                if (kits_cod_kits[0].InnerText.Trim() == "")
                {

                    kits_cod_kits_retorno = clsKits.Cadastrarkit(objKits);

                }
                else
                {
                    objKits.Kits_cod_kits = Convert.ToInt32(kits_cod_kits[0].InnerText.Trim());
                    kits_cod_kits_retorno = objKits.Kits_cod_kits;
                    clsKits.alterarKit(objKits);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<kit>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<kits_cod_kits>" + kits_cod_kits_retorno + "</kits_cod_kits>";
                xmlRetorno = xmlRetorno + "</kit>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código do kit 
        /// o mesmo irá retornar um xml com os dados do kit
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarKitPorCodigo(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsKits clsKits = new Classes.clsKits();

            //aqui o objeto da empresa
            Objetos.objKits objKits = new Objetos.objKits();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList kits_cod_kits = doc.GetElementsByTagName("kits_cod_kits");


                DataSet dsKits = new DataSet();
                dsKits = clsKits.SelecionarKitsporCodigo(Convert.ToInt32(kits_cod_kits[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<kit>";
                if (dsKits.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<kits_cod_kits>" + dsKits.Tables[0].Rows[0]["kits_cod_kits"].ToString() + "</kits_cod_kits>";
                    xmlRetorno = xmlRetorno + "<kits_descricao>" + dsKits.Tables[0].Rows[0]["kits_descricao"].ToString() + "</kits_descricao>";
                    xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsKits.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                    xmlRetorno = xmlRetorno + "<kits_quant_pallets>" + dsKits.Tables[0].Rows[0]["kits_quant_pallets"].ToString() + "</kits_quant_pallets>";
                    xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsKits.Tables[0].Rows[0]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                    xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsKits.Tables[0].Rows[0]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                    xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsKits.Tables[0].Rows[0]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsKits.Tables[0].Rows[0]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                    xmlRetorno = xmlRetorno + "<prma_descricao>" + dsKits.Tables[0].Rows[0]["prma_descricao"].ToString() + "</prma_descricao>";
                    xmlRetorno = xmlRetorno + "<kits_dt_criacao>" + dsKits.Tables[0].Rows[0]["kits_dt_criacao"].ToString() + "</kits_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsKits.Tables[0].Rows[0]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsKits.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</kit>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Não apresenta parâmtro de entrada
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido com a lista de todas as atividades cadastradas</returns>
        ///</summary>
        public string SelecionarMapementoProdutoTodos()
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();

            //aqui o objeto da empresa
            Objetos.objAtividade objAtividade = new Objetos.objAtividade();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                DataSet ds = clsMapeamentoProdutos.SelecionarMapeamentoProdutoTodos();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<mapeamentosprodutos>";

                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<mapeamentoproduto>";
                        xmlRetorno = xmlRetorno + "<mama_cod_mama>" + ds.Tables[0].Rows[i]["mama_cod_mama"].ToString().Trim() + "</mama_cod_mama>";
                        xmlRetorno = xmlRetorno + "<mama_ativo>" + ds.Tables[0].Rows[i]["mama_ativo"].ToString().Trim() + "</mama_ativo>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem_interno>" + ds.Tables[0].Rows[i]["grem_cd_grem_interno"].ToString().Trim() + "</grem_cd_grem_interno>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_interno>" + ds.Tables[0].Rows[i]["prma_cod_prma_interno"].ToString().Trim() + "</prma_cod_prma_interno>";
                        xmlRetorno = xmlRetorno + "<prma_descricao_interno>" + ds.Tables[0].Rows[i]["prma_descricao_interno"].ToString().Trim() + "</prma_descricao_interno>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem_externo>" + ds.Tables[0].Rows[i]["grem_cd_grem_externo"].ToString().Trim() + "</grem_cd_grem_externo>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_externo>" + ds.Tables[0].Rows[i]["prma_cod_prma_externo"].ToString().Trim() + "</prma_cod_prma_externo>";
                        xmlRetorno = xmlRetorno + "<prma_descricao_externo>" + ds.Tables[0].Rows[i]["prma_descricao_externo"].ToString().Trim() + "</prma_descricao_externo>";
                        xmlRetorno = xmlRetorno + "<grem_nome_interno>" + ds.Tables[0].Rows[i]["grem_nome_interno"].ToString().Trim() + "</grem_nome_interno>";
                        xmlRetorno = xmlRetorno + "<grem_nome_externo>" + ds.Tables[0].Rows[i]["grem_nome_externo"].ToString().Trim() + "</grem_nome_externo>";
                        xmlRetorno = xmlRetorno + "</mapeamentoproduto>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</mapeamentosprodutos>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o codigo do grupo 
        /// o mesmo irá retornar um xml com os dados dos produtos pertencentes ao grupo
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosProdutosPorGrupo(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");


                DataSet dsProdutosMaterias = new DataSet();
                dsProdutosMaterias = clsProdutoMaterial.SelecionarTodosProdutosGrupo(grem_cd_grem[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutosMaterias.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutosMaterias.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<produtomaterial>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<produto>" + dsProdutosMaterias.Tables[0].Rows[i]["produto"].ToString() + "</produto>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutosMaterias.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutosMaterias.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<kit_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_descricao"].ToString() + "</kit_descricao>";
                        xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                        xmlRetorno = xmlRetorno + "<kits_quant_pallet>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_pallets"].ToString() + "</kits_quant_pallet>";
                        xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                        xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                        xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsProdutosMaterias.Tables[0].Rows[i]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";




                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }


        }

        [WebMethod]
        public string SelecionarMapeamentoProdutoPorCodigo(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();

            //aqui o objeto da empresa
            Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList mama_cod_mama = doc.GetElementsByTagName("mama_cod_mama");


                DataSet ds = new DataSet();
                ds = clsMapeamentoProdutos.SelecionarMapeamentoProdutoPorCodigo(Convert.ToInt32(mama_cod_mama[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<mapeamentoproduto>";

                int i = 0;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma_empr>" + ds.Tables[0].Rows[i]["prma_cod_prma_empr"].ToString() + "</prma_cod_prma_empr>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma_interno>" + ds.Tables[0].Rows[i]["prma_cod_prma_interno"].ToString() + "</prma_cod_prma_interno>";
                    xmlRetorno = xmlRetorno + "<mama_ativo>" + ds.Tables[0].Rows[i]["mama_ativo"].ToString() + "</mama_ativo>";
                    xmlRetorno = xmlRetorno + "<grem_cd_grem_interno>" + ds.Tables[0].Rows[i]["grem_cd_grem_interno"].ToString() + "</grem_cd_grem_interno>";
                    xmlRetorno = xmlRetorno + "<grem_cd_grem_externo>" + ds.Tables[0].Rows[i]["grem_cd_grem_externo"].ToString() + "</grem_cd_grem_externo>";

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</mapeamentoproduto>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do mapeamento para cadastro
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarAlterarMapeamentoProdutoMaterial(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();

            //aqui o objeto da empresa
            Objetos.objMapeamentoProdutosMaterias objMapeamentoProdutosMaterias = new Objetos.objMapeamentoProdutosMaterias();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                /*string resultado = clsValidar.ValidarCadastroAlteracaoEmpresaXML(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList mama_cod_mama = doc.GetElementsByTagName("mama_cod_mama");
                XmlNodeList prma_cod_prma_empr = doc.GetElementsByTagName("prma_cod_prma_empr");
                XmlNodeList prma_cod_prma_interno = doc.GetElementsByTagName("prma_cod_prma_interno");
                XmlNodeList mama_ativo = doc.GetElementsByTagName("mama_ativo");
                XmlNodeList grem_cd_grem_interno = doc.GetElementsByTagName("grem_cd_grem_interno");
                XmlNodeList grem_cd_grem_externo = doc.GetElementsByTagName("grem_cd_grem_externo");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");

                objMapeamentoProdutosMaterias.Prma_cod_prma_empr = Convert.ToInt32(prma_cod_prma_empr[0].InnerText.Trim());
                objMapeamentoProdutosMaterias.Prma_cod_prma_interno = Convert.ToInt32(prma_cod_prma_interno[0].InnerText.Trim());
                objMapeamentoProdutosMaterias.Mama_ativo = Convert.ToBoolean(mama_ativo[0].InnerText.Trim());
                objMapeamentoProdutosMaterias.Grem_cd_grem_interno = Convert.ToInt32(grem_cd_grem_interno[0].InnerText.Trim());
                objMapeamentoProdutosMaterias.Grem_cd_grem_externo = Convert.ToInt32(grem_cd_grem_externo[0].InnerText.Trim());
                objMapeamentoProdutosMaterias.Mama_dt_criacao = DateTime.Now;
                objMapeamentoProdutosMaterias.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());

                int mama_cod_mama_retorno = 0;

                if (mama_cod_mama[0].InnerText == "")
                {
                    mama_cod_mama_retorno = clsMapeamentoProdutos.cadastraMapeamentoProdutoMaterial(objMapeamentoProdutosMaterias);
                }
                else
                {
                    mama_cod_mama_retorno = Convert.ToInt32(mama_cod_mama[0].InnerText);
                    objMapeamentoProdutosMaterias.Mama_cod_mama = mama_cod_mama_retorno;
                    clsMapeamentoProdutos.AlterarMapeamentoProdutoMaterial(objMapeamentoProdutosMaterias);

                }



                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<mapeamentoproduto>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<mama_cod_mama>" + mama_cod_mama + "</mama_cod_mama>";
                xmlRetorno = xmlRetorno + "</mapeamentoproduto>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarNivelEstoqueEmpresa(string xmlString)
        {


            //aqui instâncio a classe de empresas
            Classes.clsNivelEstoque clsNivelEstoque = new Classes.clsNivelEstoque();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");


                DataSet ds = new DataSet();
                ds = clsNivelEstoque.SelecionarNivelEstoqueEmpresa(Convert.ToInt32(emen_cod_empr[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<nivelEstoque>";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<nivel>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + ds.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<emen_nome>" + ds.Tables[0].Rows[i]["emen_nome"].ToString() + "</emen_nome>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + ds.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + ds.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + ds.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<nves_cod_nves>" + ds.Tables[0].Rows[i]["nves_cod_nves"].ToString() + "</nves_cod_nves>";
                        xmlRetorno = xmlRetorno + "<nves_baixo>" + ds.Tables[0].Rows[i]["nves_baixo"].ToString() + "</nves_baixo>";
                        xmlRetorno = xmlRetorno + "<nves_regular>" + ds.Tables[0].Rows[i]["nves_regular"].ToString() + "</nves_regular>";
                        xmlRetorno = xmlRetorno + "<nves_alto>" + ds.Tables[0].Rows[i]["nves_alto"].ToString() + "</nves_alto>";
                        xmlRetorno = xmlRetorno + "<nves_risco>" + ds.Tables[0].Rows[i]["nves_risco"].ToString() + "</nves_risco>";
                        xmlRetorno = xmlRetorno + "</nivel>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</nivelEstoque>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        public string SelecionarInventarioPorCodigo(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsInventario clsInventario = new Classes.clsInventario();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");


                DataSet ds = new DataSet();
                ds = clsInventario.SelecionarInventario(Convert.ToInt32(emen_cod_empr[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<inventarios>";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<inventario>";
                        xmlRetorno = xmlRetorno + "<inve_cod_inve>" + ds.Tables[0].Rows[i]["inve_cod_inve"].ToString() + "</inve_cod_inve>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + ds.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + ds.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + ds.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<inve_quantidade>" + ds.Tables[0].Rows[i]["inve_quantidade"].ToString() + "</inve_quantidade>";
                        xmlRetorno = xmlRetorno + "<inve_dt_criacao>" + ds.Tables[0].Rows[i]["inve_dt_criacao"].ToString() + "</inve_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + ds.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<inve_operacao>" + ds.Tables[0].Rows[i]["inve_operacao"].ToString() + "</inve_operacao>";
                        xmlRetorno = xmlRetorno + "<tpmo_descricao>" + ds.Tables[0].Rows[i]["tpmo_descricao"].ToString() + "</tpmo_descricao>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_pallet>" + ds.Tables[0].Rows[i]["prma_cod_prma_pallet"].ToString() + "</prma_cod_prma_pallet>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_topo>" + ds.Tables[0].Rows[i]["prma_cod_prma_topo"].ToString() + "</prma_cod_prma_topo>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_divisor>" + ds.Tables[0].Rows[i]["prma_cod_prma_divisor"].ToString() + "</prma_cod_prma_divisor>";
                        xmlRetorno = xmlRetorno + "</inventario>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</inventarios>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o codigo da família 
        /// o mesmo irá retornar um xml com os dados dos produtos pertencentes ao grupo
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosProdutosPorFamilia(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList famp_cd_famp = doc.GetElementsByTagName("famp_cd_famp");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");



                DataSet dsProdutosMaterias = new DataSet();
                dsProdutosMaterias = clsProdutoMaterial.SelecionarTodosProdutosFamilia(famp_cd_famp[0].InnerText.ToString(), emen_cod_empr[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutosMaterias.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutosMaterias.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<produtomaterial>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutosMaterias.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutosMaterias.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }


        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do inventario para cadastro
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarInventario(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsInventario clsInve = new Classes.clsInventario();
            Classes.clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();

            //aqui o objeto da empresa
            Objetos.objInventario objInve = new Objetos.objInventario();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);
                bool qtd = false;


                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList inve_quantidade = doc.GetElementsByTagName("inve_quantidade");
                XmlNodeList inve_operacao = doc.GetElementsByTagName("inve_operacao");
                XmlNodeList tpmo_cod_tpmo = doc.GetElementsByTagName("tpmo_cod_tpmo");
                XmlNodeList prma_cod_prma_pallet = doc.GetElementsByTagName("prma_cod_prma_pallet");
                XmlNodeList prma_cod_prma_topo = doc.GetElementsByTagName("prma_cod_prma_topo");
                XmlNodeList prma_cod_prma_divisor = doc.GetElementsByTagName("prma_cod_prma_divisor");
                XmlNodeList inve_dt_criacao = doc.GetElementsByTagName("inve_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList emen_cod_empr_forn = doc.GetElementsByTagName("emen_cod_empr_forn");
                objInve.Emen_cod_empr = Convert.ToInt32(emen_cod_empr[0].InnerText.ToString().Trim());
                objInve.Inve_dt_criacao = Convert.ToDateTime(inve_dt_criacao[0].InnerText.ToString().Trim());
                objInve.Inve_operacao = inve_operacao[0].InnerText.ToString().Trim();
                objInve.Tpmo_cod_tpmo = Convert.ToInt32(tpmo_cod_tpmo[0].InnerText.ToString().Trim());
                objInve.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText.ToString().Trim());
                string erro1 = "";
                //aqui vou verificar para fazer o ajuste da quantidade caso a operação seja contagem de estoque
                if (objInve.Inve_operacao == "Contagem Estoque")
                {

                    DataSet dskit = clsKits.SelecionarKitProduto(objInve.Prma_cod_prma);
                    if (dskit.Tables[0].Rows.Count > 0)
                    {


                        DataSet dsQuantidadeAtual = new DataSet();
                        dsQuantidadeAtual = clsSaldoEstoque.SelecionarEstoqueProdutoDisponivel(objInve.Emen_cod_empr.ToString(), prma_cod_prma[0].InnerText);

                        double dsQuantidadeAtual2 = Convert.ToInt32(Convert.ToInt32(dsQuantidadeAtual.Tables[0].Rows[0]["sles_quantidade"].ToString()) / Convert.ToInt32(dskit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString()));
                        //dsQuantidadeAtual2 = dsQuantidadeAtual2 / Convert.ToInt32(dskit.Tables[0].Rows[0]["kits_quant_prod_prin"]);

                        if (dsQuantidadeAtual.Tables[0].Rows.Count > 0)
                        {
                            if (dsQuantidadeAtual2 > Convert.ToDouble(inve_quantidade[0].InnerText))
                            {
                                inve_quantidade[0].InnerText = Convert.ToString(dsQuantidadeAtual2 - Convert.ToDouble(inve_quantidade[0].InnerText));
                            }
                            else if(dsQuantidadeAtual2 == Convert.ToDouble(inve_quantidade[0].InnerText))
                            {
                                inve_quantidade[0].InnerText = Convert.ToString(dsQuantidadeAtual2);

                                qtd = true;
                            }
                            else
                            {
                                erro1 = "Quantidade informada maior do que a quantidade em estoque";
                            }
                            
                        }
                    }

                }



                objInve.Inve_quantidade = Convert.ToDouble(inve_quantidade[0].InnerText.ToString().Trim());
                objInve.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText.ToString().Trim());
                objInve.Prma_cod_prma_divisor = Convert.ToInt32(prma_cod_prma_divisor[0].InnerText.ToString().Trim());
                objInve.Prma_cod_prma_pallet = Convert.ToInt32(prma_cod_prma_pallet[0].InnerText.ToString().Trim());
                objInve.Prma_cod_prma_topo = Convert.ToInt32(prma_cod_prma_topo[0].InnerText.ToString().Trim());
                objInve.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString().Trim());
                objInve.Emen_cod_empr_forn = Convert.ToInt32(emen_cod_empr_forn[0].InnerText.ToString().Trim());

                double quantidadeProduto = 0;
                double quantidadeDivisor = 0;
                //aqui vou pegar o kit do produto para poder calcular a quantidade do produto e tambem a quantidade de divisores
                DataSet ds = new DataSet();
                ds = clsKits.SelecionarKitProduto(objInve.Prma_cod_prma);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    quantidadeProduto = objInve.Inve_quantidade * Convert.ToDouble(ds.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString());
                    quantidadeDivisor = objInve.Inve_quantidade * Convert.ToDouble(ds.Tables[0].Rows[0]["kits_quant_folha"].ToString());

                    string erro = "";
                    if (erro1 == "")
                    {
                        erro = clsInve.CadastrarInventario(objInve, quantidadeProduto, quantidadeDivisor, qtd);
                    }
                    else
                    {
                        erro = erro1;
                    }

                    if (erro == "")
                    {

                        xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                        xmlRetorno = xmlRetorno + "<Inventario>";
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                        xmlRetorno = xmlRetorno + "</Inventario>";
                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + erro + "</descerro></retornoErro>";
                        return xmlRetorno;
                    }
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Kit não cadastrado para o produto</descerro></retornoErro>";
                }
                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarInventarioPorCodigoDoInventario(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsInventario clsInventario = new Classes.clsInventario();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList inve_cod_inve = doc.GetElementsByTagName("inve_cod_inve");


                DataSet ds = new DataSet();
                ds = clsInventario.SelecionarInventarioPorCodigo(Convert.ToInt32(inve_cod_inve[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";



                if (ds.Tables[0].Rows.Count > 0)
                {

                    xmlRetorno = xmlRetorno + "<inventario>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<inve_cod_inve>" + ds.Tables[0].Rows[0]["inve_cod_inve"].ToString() + "</inve_cod_inve>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma>" + ds.Tables[0].Rows[0]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                    xmlRetorno = xmlRetorno + "<prma_descricao>" + ds.Tables[0].Rows[0]["prma_descricao"].ToString() + "</prma_descricao>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr>" + ds.Tables[0].Rows[0]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                    xmlRetorno = xmlRetorno + "<inve_quantidade>" + ds.Tables[0].Rows[0]["inve_quantidade"].ToString() + "</inve_quantidade>";
                    xmlRetorno = xmlRetorno + "<inve_dt_criacao>" + ds.Tables[0].Rows[0]["inve_dt_criacao"].ToString() + "</inve_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + ds.Tables[0].Rows[0]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<inve_operacao>" + ds.Tables[0].Rows[0]["inve_operacao"].ToString() + "</inve_operacao>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma_pallet>" + ds.Tables[0].Rows[0]["prma_cod_prma_pallet"].ToString() + "</prma_cod_prma_pallet>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma_topo>" + ds.Tables[0].Rows[0]["prma_cod_prma_topo"].ToString() + "</prma_cod_prma_topo>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma_divisor>" + ds.Tables[0].Rows[0]["prma_cod_prma_divisor"].ToString() + "</prma_cod_prma_divisor>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr_forn>" + ds.Tables[0].Rows[0]["emen_cod_empr_forn"].ToString() + "</emen_cod_empr_forn>";
                    xmlRetorno = xmlRetorno + "</inventario>";
                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarNotaFiscalEmpresaOrigemouDestino(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList empr_cod_empr = doc.GetElementsByTagName("emem_cod_empr");
                XmlNodeList tiponota = doc.GetElementsByTagName("tiponota");


                DataSet ds = new DataSet();
                ds = clsNotaFiscal.SelecionarNotaFiscaempresaOrigemDestino(Convert.ToInt32(empr_cod_empr[0].InnerText.Trim()), tiponota[0].InnerText.Trim());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";



                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<notasFiscais>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<notaFiscal>";
                        xmlRetorno = xmlRetorno + "<canf_cod_canf>" + ds.Tables[0].Rows[i]["canf_cod_canf"].ToString() + "</canf_cod_canf>";
                        xmlRetorno = xmlRetorno + "<canf_numnt>" + ds.Tables[0].Rows[i]["canf_numnt"].ToString() + "</canf_numnt>";
                        xmlRetorno = xmlRetorno + "<canf_serienf>" + ds.Tables[0].Rows[i]["canf_serienf"].ToString() + "</canf_serienf>";
                        xmlRetorno = xmlRetorno + "<canf_dt_emissao>" + ds.Tables[0].Rows[i]["canf_dt_emissao"].ToString() + "</canf_dt_emissao>";
                        xmlRetorno = xmlRetorno + "<tempo_decorrido>" + ds.Tables[0].Rows[i]["tempo_decorrido"].ToString() + "</tempo_decorrido>";
                        xmlRetorno = xmlRetorno + "<dt_carregamento>" + ds.Tables[0].Rows[i]["dt_carregamento"].ToString() + "</dt_carregamento>";

                        xmlRetorno = xmlRetorno + "</notaFiscal>";
                    }
                    xmlRetorno = xmlRetorno + "</notasFiscais>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código da emrpesa da qual necessitamos saber o grupo
        /// o mesmo irá retornar um xml com os dados do grupo da empresa
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEstoqueempresa(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");

                //aqui irei pegar todos os produtos relativos a empresa

                DataSet dsProdutos = clsProdutoMaterial.SelecionarTodosProdutosEmpresa(emen_cod_empr[0].InnerText);

                if (dsProdutos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<produtos>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutos.Tables[0].Rows.Count - 1; i++)
                    {

                        //aqui eu vou pegar os produtos que tem na tabela sles com os status que serão considerados no
                        //relatório de posição de estoque.

                        DataSet dsEstoque = clsSaldoEstoque.SelecionarEstoqueProduto(emen_cod_empr[0].InnerText, dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString());

                        if (dsEstoque.Tables[0].Rows.Count == 0)
                        {

                            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
                            DataSet DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExterno(Convert.ToInt32(dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                            if (DsMapeamento.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= DsMapeamento.Tables[0].Rows.Count - 1; k++)
                                {
                                    dsEstoque = clsSaldoEstoque.SelecionarEstoqueProduto(emen_cod_empr[0].InnerText, DsMapeamento.Tables[0].Rows[k]["prma_cod_prma_empr"].ToString());


                                    string em_conferencia = "0";
                                    string disponivel = "0";
                                    string entrada_linha = "0";
                                    string expedicao = "0";

                                    for (int j = 0; j <= dsEstoque.Tables[0].Rows.Count - 1; j++)
                                    {
                                        if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "2")
                                        {
                                            em_conferencia = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                        }
                                        else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "3")
                                        {
                                            disponivel = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                        }
                                        else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "5")
                                        {
                                            entrada_linha = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                        }
                                        else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "6")
                                        {
                                            expedicao = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                        }


                                    }
                                    if (em_conferencia != "0" || disponivel != "0" || expedicao != "0" || entrada_linha != "0")
                                    {
                                        xmlRetorno = xmlRetorno + "<produto>";
                                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                                        xmlRetorno = xmlRetorno + "<em_conferencia>" + em_conferencia + "</em_conferencia>";
                                        xmlRetorno = xmlRetorno + "<disponivel>" + disponivel + "</disponivel>";
                                        xmlRetorno = xmlRetorno + "<entrada_linha>" + entrada_linha + "</entrada_linha>";
                                        xmlRetorno = xmlRetorno + "<expedicao>" + expedicao + "</expedicao>";

                                        xmlRetorno = xmlRetorno + "</produto>";
                                    }

                                }
                            }


                        }
                        else
                        {
                            string em_conferencia = "0";
                            string disponivel = "0";
                            string entrada_linha = "0";
                            string expedicao = "0";

                            for (int j = 0; j <= dsEstoque.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "2")
                                {
                                    em_conferencia = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "3")
                                {
                                    disponivel = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "5")
                                {
                                    entrada_linha = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "6")
                                {
                                    expedicao = dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString();
                                }


                            }
                            xmlRetorno = xmlRetorno + "<produto>";
                            xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                            xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                            xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                            xmlRetorno = xmlRetorno + "<em_conferencia>" + em_conferencia + "</em_conferencia>";
                            xmlRetorno = xmlRetorno + "<disponivel>" + disponivel + "</disponivel>";
                            xmlRetorno = xmlRetorno + "<entrada_linha>" + entrada_linha + "</entrada_linha>";
                            xmlRetorno = xmlRetorno + "<expedicao>" + expedicao + "</expedicao>";

                            xmlRetorno = xmlRetorno + "</produto>";

                        }


                    }
                    xmlRetorno = xmlRetorno + "</produtos>";

                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não existem produtos cadastrados para a empresa</descerro></retornoErro>";
                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o codigo do grupo 
        /// o mesmo irá retornar um xml com os dados dos produtos pertencentes ao grupo
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosProdutosEmpresa(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");


                DataSet dsProdutosMaterias = new DataSet();
                dsProdutosMaterias = clsProdutoMaterial.SelecionarTodosProdutosEmpresa(emen_cod_empr[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutosMaterias.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutosMaterias.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<produtomaterial>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutosMaterias.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutosMaterias.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }


        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código da emrpesa da qual necessitamos saber o grupo
        /// o mesmo irá retornar um xml com os dados do grupo da empresa
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEstoqueConsolidadoempresa(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();
            Classes.clsKits clsKits = new Classes.clsKits();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");

                //aqui irei pegar todos os produtos relativos a empresa

                DataSet dsProdutos = clsProdutoMaterial.SelecionarTodosProdutosFamilia("4", emen_cod_empr[0].InnerText);



                if (dsProdutos.Tables[0].Rows.Count > 0)
                {


                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<produtos>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutos.Tables[0].Rows.Count - 1; i++)
                    {
                   
                        //aqui eu vou pegar os produtos que tem na tabela sles com os status que serão considerados no
                        //relatório de posição de estoque.

                        DataSet dsEstoque = clsSaldoEstoque.SelecionarEstoqueProduto(emen_cod_empr[0].InnerText, dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString());

                        if (dsEstoque.Tables[0].Rows.Count == 0)
                        {
                            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
                            DataSet DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExterno(Convert.ToInt32(dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                            if (DsMapeamento.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k <= DsMapeamento.Tables[0].Rows.Count - 1; k++)
                                {
                                    dsEstoque = clsSaldoEstoque.SelecionarEstoqueProduto(emen_cod_empr[0].InnerText, DsMapeamento.Tables[0].Rows[k]["prma_cod_prma_empr"].ToString());


                                    string em_conferencia = "0";
                                    string disponivel = "0";
                                    string entrada_linha = "0";
                                    string expedicao = "0";

                                    double quantidade = 0;

                                    DataSet dskit = new DataSet();

                                    for (int j = 0; j <= dsEstoque.Tables[0].Rows.Count - 1; j++)
                                    {
                                        dskit = clsKits.SelecionarKitProduto(Convert.ToInt32(dsEstoque.Tables[0].Rows[j]["prma_cod_prma"].ToString()));
                                        if (dskit.Tables[0].Rows.Count > 0)
                                        {
                                            quantidade = Math.Round(Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString()) / Convert.ToDouble(dskit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString())),2);
                                        }


                                        if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "2")
                                        {
                                            em_conferencia = Convert.ToString(Math.Round(quantidade,2));
                                        }
                                        else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "3")
                                        {
                                            disponivel = Convert.ToString(Math.Round(quantidade,2));
                                        }
                                        else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "5")
                                        {
                                            entrada_linha = Convert.ToString(Math.Round(quantidade,2));
                                        }
                                        else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "6")
                                        {
                                            expedicao = Convert.ToString(Math.Round(quantidade,2));
                                        }


                                    }


                                    if (em_conferencia != "0" || disponivel != "0" || entrada_linha != "0" || expedicao != "0")
                                    {
                                        xmlRetorno = xmlRetorno + "<produto>";
                                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                                        xmlRetorno = xmlRetorno + "<em_conferencia>" + em_conferencia + "</em_conferencia>";
                                        xmlRetorno = xmlRetorno + "<disponivel>" + disponivel + "</disponivel>";
                                        xmlRetorno = xmlRetorno + "<entrada_linha>" + entrada_linha + "</entrada_linha>";
                                        xmlRetorno = xmlRetorno + "<expedicao>" + expedicao + "</expedicao>";

                                        

                                        xmlRetorno = xmlRetorno + "</produto>";
                                    }
                                }

                            }
                            else
                            {
                                string em_conferencia = "0";
                                string disponivel = "0";
                                string entrada_linha = "0";
                                string expedicao = "0";

                                double quantidade = 0;

                                DataSet dskit = new DataSet();

                                dsEstoque = clsSaldoEstoque.SelecionarEstoqueProduto(emen_cod_empr[0].InnerText, dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString());

                                for (int l = 0; l <= dsEstoque.Tables[0].Rows.Count - 1; l++)
                                {
                                    dskit = clsKits.SelecionarKitProduto(Convert.ToInt32(dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                                    if (dskit.Tables[0].Rows.Count > 0)
                                    {
                                        quantidade = Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[l]["sles_quantidade"].ToString()) / Convert.ToDouble(dskit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString()));
                                    }


                                    if (dsEstoque.Tables[0].Rows[l]["tpmo_cod_tpmo"].ToString() == "2")
                                    {
                                        Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[i]["sles_quantidade"].ToString()));
                                        em_conferencia = Convert.ToString(Math.Round(quantidade,2));
                                    }
                                    else if (dsEstoque.Tables[0].Rows[l]["tpmo_cod_tpmo"].ToString() == "3")
                                    {
                                        Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[i]["sles_quantidade"].ToString()));
                                        disponivel = Convert.ToString(Math.Round(quantidade,2));
                                    }
                                    else if (dsEstoque.Tables[0].Rows[l]["tpmo_cod_tpmo"].ToString() == "5")
                                    {
                                        Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[i]["sles_quantidade"].ToString()));
                                        entrada_linha = Convert.ToString(Math.Round(quantidade,2));
                                    }
                                    else if (dsEstoque.Tables[0].Rows[i]["tpmo_cod_tpmo"].ToString() == "6")
                                    {
                                        Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[i]["sles_quantidade"].ToString()));
                                        expedicao = Convert.ToString(Math.Round(quantidade,2));
                                    }


                                }
                                if (em_conferencia != "0" || disponivel != "0" || entrada_linha != "0" || expedicao != "0")
                                {
                                    xmlRetorno = xmlRetorno + "<produto>";
                                    xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                                    xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                                    xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                                    xmlRetorno = xmlRetorno + "<em_conferencia>" + em_conferencia + "</em_conferencia>";
                                    xmlRetorno = xmlRetorno + "<disponivel>" + disponivel + "</disponivel>";
                                    xmlRetorno = xmlRetorno + "<entrada_linha>" + entrada_linha + "</entrada_linha>";
                                    xmlRetorno = xmlRetorno + "<expedicao>" + expedicao + "</expedicao>";

                                    xmlRetorno = xmlRetorno + "</produto>";
                                }
                            }

                        }
                        else
                        {
                            string em_conferencia = "0";
                            string disponivel = "0";
                            string entrada_linha = "0";
                            string expedicao = "0";
                            double quantidade = 0;

                            for (int j = 0; j <= dsEstoque.Tables[0].Rows.Count - 1; j++)
                            {
                                DataSet dskit = clsKits.SelecionarKitProduto(Convert.ToInt32(dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                                if (dskit.Tables[0].Rows.Count > 0)
                                {
                                    quantidade = Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[j]["sles_quantidade"].ToString()) / Convert.ToDouble(dskit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString()));
                                }


                                if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "2")
                                {
                                    Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[0]["sles_quantidade"].ToString()));
                                    em_conferencia = Convert.ToString(Math.Round(quantidade,2));
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "3")
                                {
                                    Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[0]["sles_quantidade"].ToString()));
                                    disponivel = Convert.ToString(Math.Round(quantidade,2));
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "5")
                                {
                                    Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[0]["sles_quantidade"].ToString()));
                                    entrada_linha = Convert.ToString(Math.Round(quantidade,2));
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "6")
                                {
                                    Convert.ToDouble(Convert.ToDouble(dsEstoque.Tables[0].Rows[0]["sles_quantidade"].ToString()));
                                    expedicao = Convert.ToString(Math.Round(quantidade,2));
                                }
                            }

                            if (em_conferencia != "0" || disponivel != "0" || entrada_linha != "0" || expedicao != "0")
                            {
                                xmlRetorno = xmlRetorno + "<produto>";
                                xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                                xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                                xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                                xmlRetorno = xmlRetorno + "<em_conferencia>" + em_conferencia + "</em_conferencia>";
                                xmlRetorno = xmlRetorno + "<disponivel>" + disponivel + "</disponivel>";
                                xmlRetorno = xmlRetorno + "<entrada_linha>" + entrada_linha + "</entrada_linha>";
                                xmlRetorno = xmlRetorno + "<expedicao>" + expedicao + "</expedicao>";

                                xmlRetorno = xmlRetorno + "</produto>";
                            }

                        }



                    }
                    xmlRetorno = xmlRetorno + "</produtos>";

                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não existem produtos cadastrados para a empresa</descerro></retornoErro>";
                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método apenas fará o fechamento do estoque
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string FechamentoEstoque(string xmlString)
        {
            //aqui instâncio a classe de usuário
            Classes.clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");

                string gravou;
                gravou = clsSaldoEstoque.FechamentoEstoque(emen_cod_empr[0].InnerText);

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<FechamentoEstoque>";
                if (gravou == "")
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }

                xmlRetorno = xmlRetorno + "</FechamentoEstoque>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código da emrpesa da qual necessitamos saber o grupo
        /// o mesmo irá retornar um xml com os dados do grupo da empresa
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEstoqueFechadoempresa(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList fees_dt_fechamento = doc.GetElementsByTagName("fees_dt_fechamento");


                //aqui irei pegar todos os produtos relativos a empresa

                DataSet dsProdutos = clsProdutoMaterial.SelecionarTodosProdutosEmpresa(emen_cod_empr[0].InnerText);



                if (dsProdutos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<produtos>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutos.Tables[0].Rows.Count - 1; i++)
                    {
                        //aqui eu vou pegar os produtos que tem na tabela sles com os status que serão considerados no
                        //relatório de posição de estoque.
                        DataSet dsEstoque = clsSaldoEstoque.SelecionarEstoqueFechadoProduto(emen_cod_empr[0].InnerText, dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString(), fees_dt_fechamento[0].InnerText);


                        if (dsEstoque.Tables[0].Rows.Count == 0)
                        {
                            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
                            DataSet DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExterno(Convert.ToInt32(dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                            if (DsMapeamento.Tables[0].Rows.Count > 0)
                            {
                                dsEstoque = clsSaldoEstoque.SelecionarEstoqueFechadoProduto(emen_cod_empr[0].InnerText, DsMapeamento.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString(), fees_dt_fechamento[0].InnerText);
                            }

                        }

                        if (dsEstoque.Tables[0].Rows.Count > 0)
                        {
                            string em_conferencia = "0";
                            string disponivel = "0";
                            string entrada_linha = "0";
                            string expedicao = "0";

                            for (int j = 0; j <= dsEstoque.Tables[0].Rows.Count - 1; j++)
                            {
                                if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "2")
                                {
                                    em_conferencia = dsEstoque.Tables[0].Rows[j]["fees_quantidade"].ToString();
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "3")
                                {
                                    disponivel = dsEstoque.Tables[0].Rows[j]["fees_quantidade"].ToString();
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "5")
                                {
                                    entrada_linha = dsEstoque.Tables[0].Rows[j]["fees_quantidade"].ToString();
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "6")
                                {
                                    expedicao = dsEstoque.Tables[0].Rows[j]["fees_quantidade"].ToString();
                                }


                            }
                            xmlRetorno = xmlRetorno + "<produto>";
                            xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                            xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                            xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                            xmlRetorno = xmlRetorno + "<em_conferencia>" + em_conferencia + "</em_conferencia>";
                            xmlRetorno = xmlRetorno + "<disponivel>" + disponivel + "</disponivel>";
                            xmlRetorno = xmlRetorno + "<entrada_linha>" + entrada_linha + "</entrada_linha>";
                            xmlRetorno = xmlRetorno + "<expedicao>" + expedicao + "</expedicao>";

                            xmlRetorno = xmlRetorno + "</produto>";
                        }
                    }
                    xmlRetorno = xmlRetorno + "</produtos>";

                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não existem produtos cadastrados para a empresa</descerro></retornoErro>";
                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código da emrpesa da qual necessitamos saber o grupo
        /// o mesmo irá retornar um xml com os dados do grupo da empresa
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEstoqueFechamentoConsolidadoempresa(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();
            Classes.clsKits clsKits = new Classes.clsKits();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList fees_dt_fechamento = doc.GetElementsByTagName("fees_dt_fechamento");

                //aqui irei pegar todos os produtos relativos a empresa

                DataSet dsProdutos = clsProdutoMaterial.SelecionarTodosProdutosFamilia("4", emen_cod_empr[0].InnerText);

                if (dsProdutos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<produtos>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutos.Tables[0].Rows.Count - 1; i++)
                    {
                        //aqui eu vou pegar os produtos que tem na tabela sles com os status que serão considerados no
                        //relatório de posição de estoque.
                        DataSet dsEstoque = clsSaldoEstoque.SelecionarEstoqueFechadoProduto(emen_cod_empr[0].InnerText, dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString(), fees_dt_fechamento[0].InnerText);

                        if (dsEstoque.Tables[0].Rows.Count == 0)
                        {
                            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
                            DataSet DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExterno(Convert.ToInt32(dsProdutos.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                            if (DsMapeamento.Tables[0].Rows.Count > 0)
                            {
                                dsEstoque = clsSaldoEstoque.SelecionarEstoqueFechadoProduto(emen_cod_empr[0].InnerText, DsMapeamento.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString(), fees_dt_fechamento[0].InnerText);
                            }
                        }

                        if (dsEstoque.Tables[0].Rows.Count == 0)
                        {
                            xmlRetorno = xmlRetorno + "<produto>";
                            xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                            xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                            xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                            xmlRetorno = xmlRetorno + "<em_conferencia>0</em_conferencia>";
                            xmlRetorno = xmlRetorno + "<disponivel>0</disponivel>";
                            xmlRetorno = xmlRetorno + "<entrada_linha>0</entrada_linha>";
                            xmlRetorno = xmlRetorno + "<expedicao>0</expedicao>";

                            xmlRetorno = xmlRetorno + "</produto>";
                        }
                        else
                        {
                            string em_conferencia = "0";
                            string disponivel = "0";
                            string entrada_linha = "0";
                            string expedicao = "0";

                            int quantidade = 0;

                            DataSet dskit = new DataSet();

                            for (int j = 0; j <= dsEstoque.Tables[0].Rows.Count - 1; j++)
                            {
                                dskit = clsKits.SelecionarKitProduto(Convert.ToInt32(dsEstoque.Tables[0].Rows[j]["prma_cod_prma"].ToString()));

                                quantidade = Convert.ToInt32(Convert.ToInt32(dsEstoque.Tables[0].Rows[j]["fees_quantidade"].ToString()) / Convert.ToInt32(dskit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString()));


                                if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "2")
                                {
                                    em_conferencia = Convert.ToString(quantidade);
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "3")
                                {
                                    disponivel = Convert.ToString(quantidade);
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "5")
                                {
                                    entrada_linha = Convert.ToString(quantidade);
                                }
                                else if (dsEstoque.Tables[0].Rows[j]["tpmo_cod_tpmo"].ToString() == "6")
                                {
                                    expedicao = Convert.ToString(quantidade);
                                }


                            }
                            xmlRetorno = xmlRetorno + "<produto>";
                            xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutos.Tables[0].Rows[i]["prma_cod_prma"] + "</prma_cod_prma>";
                            xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutos.Tables[0].Rows[i]["prma_pn"] + "</prma_pn>";
                            xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutos.Tables[0].Rows[i]["prma_descricao"] + "</prma_descricao>";
                            xmlRetorno = xmlRetorno + "<em_conferencia>" + em_conferencia + "</em_conferencia>";
                            xmlRetorno = xmlRetorno + "<disponivel>" + disponivel + "</disponivel>";
                            xmlRetorno = xmlRetorno + "<entrada_linha>" + entrada_linha + "</entrada_linha>";
                            xmlRetorno = xmlRetorno + "<expedicao>" + expedicao + "</expedicao>";

                            xmlRetorno = xmlRetorno + "</produto>";
                        }
                    }
                    xmlRetorno = xmlRetorno + "</produtos>";

                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não existem produtos cadastrados para a empresa</descerro></retornoErro>";
                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código da emrpesa da qual necessitamos saber o grupo
        /// o mesmo irá retornar um xml com os dados do grupo da empresa
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEstoqueFechadoDatas()
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui irei pegar todos os produtos relativos a empresa

                DataSet dsFechamento = clsSaldoEstoque.SelecionarEstoqueFechamentoDatas();

                if (dsFechamento.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<fechamentoEstoque>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsFechamento.Tables[0].Rows.Count - 1; i++)
                    {
                        //aqui eu vou pegar os produtos que tem na tabela sles com os status que serão considerados no
                        //relatório de posição de estoque.
                        xmlRetorno = xmlRetorno + "<fechamento>";
                        xmlRetorno = xmlRetorno + "<fees_dt_fechamento>" + Convert.ToDateTime(dsFechamento.Tables[0].Rows[i]["fees_dt_fechamento"].ToString()).ToShortDateString() + "</fees_dt_fechamento>";
                        xmlRetorno = xmlRetorno + "</fechamento>";
                    }
                    xmlRetorno = xmlRetorno + "</fechamentoEstoque>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não existem produtos cadastrados para a empresa</descerro></retornoErro>";
                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o número da nota_fiscal
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string EntradaLinhaCadastrar(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas

            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();
            Classes.clsEntradaLinha clsEntradaLinha = new Classes.clsEntradaLinha();
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();
            Classes.clsInventario clsInventario = new Classes.clsInventario();


            //aqui o objeto da empresa
            Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();
            Objetos.objKits objKits = new Objetos.objKits();
            Objetos.objMapeamentoProdutosMaterias objMapeamentoProdutosMaterias = new Objetos.objMapeamentoProdutosMaterias();
            Objetos.objInventario objInventario = new Objetos.objInventario();



            DataSet ds = new DataSet();
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string gravou = "";




                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                DateTime data_criacao = DateTime.Now;
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList emen_cod_emen = doc.GetElementsByTagName("emen_cod_emen");
                XmlNodeList qtd_pallet = doc.GetElementsByTagName("qtd_pallet");
                XmlNodeList empresaFornecedora = doc.GetElementsByTagName("empresaFornecedora");


                XmlNodeList pallet = doc.GetElementsByTagName("pallet");
                XmlNodeList folha = doc.GetElementsByTagName("folha");
                XmlNodeList topo = doc.GetElementsByTagName("topo");


                int quantidade_folhas = 0;
                int quantidade_paletes = Convert.ToInt32(qtd_pallet[0].InnerText);
                int quantidade_produto_princiapl = 0;



                //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit

                DataSet dsKit = new DataSet();

                dsKit = clsKits.SelecionarKitProduto(Convert.ToInt32(prma_cod_prma[0].InnerText));
                objKits.Prma_cod_prma = Convert.ToInt32(dsKit.Tables[0].Rows[0]["prma_cod_prma"].ToString());
                objKits.Kits_quant_prod_prin = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString());
                objKits.Kits_quant_folha = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_folha"].ToString());
                quantidade_folhas = Convert.ToInt32(objKits.Kits_quant_folha * quantidade_paletes);
                quantidade_produto_princiapl = Convert.ToInt32(objKits.Kits_quant_prod_prin * quantidade_paletes);


                DataSet dsEmpresa = clsEmpresa.SelecionarEmpresa(Convert.ToInt32(empresaFornecedora[0].InnerText));

                //aqui vou buscar o mapeamento do produto principal (latas)
                //DataSet DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(objKits.Prma_cod_prma, Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));

                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_produto_princiapl;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText);
                objSaldoEstoque.Tpop_cod_tpop = 0;
                lstobjSaldoEstoque.Add(objSaldoEstoque);


                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 9;//13;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);

                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();

                //aqui vou buscar os demais produtos que serão topo, palete e folha da e realizar o mapeamwento dos mesmos
                //precisarei destes para fazer a movimentação do estoque dos itens citados
                /*DataSet dsProduto = new DataSet();
                dsProduto = clsProdutoMaterial.SelecionarTodosProdutosFamilia("1", emen_cod_emen[0].InnerText);*/

                //aqui vou realizar o mapeamento do produto para poder realizar a atualização da forma correta
                /*DsMapeamento = new DataSet();
                DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(Convert.ToInt32(dsProduto.Tables[0].Rows[0]["prma_cod_prma"].ToString()), Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));*/

                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_paletes;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(pallet[0].InnerText);



                objSaldoEstoque.Tpop_cod_tpop = 6;
                lstobjSaldoEstoque.Add(objSaldoEstoque);


                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 9;//6;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();


                /*dsProduto = new DataSet();
                dsProduto = clsProdutoMaterial.SelecionarTodosProdutosFamilia("2", emen_cod_emen[0].InnerText);*/

                /*DsMapeamento = new DataSet();
                DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(Convert.ToInt32(dsProduto.Tables[0].Rows[0]["prma_cod_prma"].ToString()), Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));*/
                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_paletes;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(topo[0].InnerText);
                objSaldoEstoque.Tpop_cod_tpop = 6;
                lstobjSaldoEstoque.Add(objSaldoEstoque);


                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 9;//6;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();

                /*dsProduto = new DataSet();
                dsProduto = clsProdutoMaterial.SelecionarTodosProdutosFamilia("3", emen_cod_emen[0].InnerText);*/

                /*DsMapeamento = new DataSet();
                DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(Convert.ToInt32(dsProduto.Tables[0].Rows[0]["prma_cod_prma"].ToString()), Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));*/
                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_folhas;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(folha[0].InnerText);
                objSaldoEstoque.Tpop_cod_tpop = 5;
                lstobjSaldoEstoque.Add(objSaldoEstoque);


                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 9;//5;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();

                gravou = clsEntradaLinha.cadastraEntradaLinha(lstobjMovimentoEstoque, lstobjSaldoEstoque, Convert.ToInt32(emen_cod_emen[0].InnerText));

                //aqui vou realizar a gravação na tabela de inventário
                objInventario.Emen_cod_empr = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objInventario.Emen_cod_empr_forn = Convert.ToInt32(empresaFornecedora[0].InnerText);
                objInventario.Inve_dt_criacao = DateTime.Now;
                objInventario.Inve_operacao = "Entrada Linha";
                objInventario.Inve_quantidade = quantidade_paletes;
                objInventario.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText);
                objInventario.Tpmo_cod_tpmo = 13;
                objInventario.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                objInventario.Prma_cod_prma_pallet = Convert.ToInt32(pallet[0].InnerText);
                objInventario.Prma_cod_prma_topo = Convert.ToInt32(topo[0].InnerText);
                objInventario.Prma_cod_prma_divisor = Convert.ToInt32(folha[0].InnerText);

                var retornoCadinv = clsInventario.CadastrarInventarioEntradaLinha(objInventario);


                if (gravou == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<EntradaLinha>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</EntradaLinha>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o número da nota_fiscal
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string RetornoLinhaCadastrar(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas

            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();
            Classes.clsEntradaLinha clsEntradaLinha = new Classes.clsEntradaLinha();
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();


            //aqui o objeto da empresa
            Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();
            Objetos.objKits objKits = new Objetos.objKits();
            Objetos.objMapeamentoProdutosMaterias objMapeamentoProdutosMaterias = new Objetos.objMapeamentoProdutosMaterias();



            DataSet ds = new DataSet();
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string gravou = "";




                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                DateTime data_criacao = DateTime.Now;
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList emen_cod_emen = doc.GetElementsByTagName("emen_cod_emen");
                XmlNodeList qtd_pallet = doc.GetElementsByTagName("qtd_pallet");
                XmlNodeList empresaFornecedora = doc.GetElementsByTagName("empresaFornecedora");

                XmlNodeList pallet = doc.GetElementsByTagName("pallet");
                XmlNodeList folha = doc.GetElementsByTagName("folha");
                XmlNodeList topo = doc.GetElementsByTagName("topo");

                int quantidade_folhas = 0;
                int quantidade_paletes = Convert.ToInt32(qtd_pallet[0].InnerText);
                int quantidade_produto_princiapl = 0;



                //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit

                DataSet dsKit = new DataSet();

                dsKit = clsKits.SelecionarKitProduto(Convert.ToInt32(prma_cod_prma[0].InnerText));
                objKits.Prma_cod_prma = Convert.ToInt32(dsKit.Tables[0].Rows[0]["prma_cod_prma"].ToString());
                objKits.Kits_quant_prod_prin = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_prod_prin"].ToString());
                objKits.Kits_quant_folha = Convert.ToDouble(dsKit.Tables[0].Rows[0]["kits_quant_folha"].ToString());
                quantidade_folhas = Convert.ToInt32(objKits.Kits_quant_folha * quantidade_paletes);
                quantidade_produto_princiapl = Convert.ToInt32(objKits.Kits_quant_prod_prin * quantidade_paletes);


                DataSet dsEmpresa = clsEmpresa.SelecionarEmpresa(Convert.ToInt32(empresaFornecedora[0].InnerText));
                //aqui vou buscar o mapeamento do produto principal (latas)

                //DataSet DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(objKits.Prma_cod_prma, Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));



                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_produto_princiapl;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText);
                objSaldoEstoque.Tpop_cod_tpop = -1;
                lstobjSaldoEstoque.Add(objSaldoEstoque);

                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 10;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();

                //aqui vou buscar os demais produtos que serão topo, palete e folha da e realizar o mapeamwento dos mesmos
                //precisarei destes para fazer a movimentação do estoque dos itens citados
                DataSet dsProduto = new DataSet();
                dsProduto = clsProdutoMaterial.SelecionarTodosProdutosFamilia("1", emen_cod_emen[0].InnerText);

                //aqui vou realizar o mapeamento do produto para poder realizar a atualização da forma correta
                /*DsMapeamento = new DataSet();

                DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(Convert.ToInt32(dsProduto.Tables[0].Rows[0]["prma_cod_prma"].ToString()), Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));*/
                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_paletes;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(pallet[0].InnerText);
                objSaldoEstoque.Tpop_cod_tpop = 10;
                lstobjSaldoEstoque.Add(objSaldoEstoque);


                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 10;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();


                /*dsProduto = new DataSet();
                dsProduto = clsProdutoMaterial.SelecionarTodosProdutosFamilia("2", emen_cod_emen[0].InnerText);*/

                /*DsMapeamento = new DataSet();
                DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(Convert.ToInt32(dsProduto.Tables[0].Rows[0]["prma_cod_prma"].ToString()), Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));*/
                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_paletes;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(topo[0].InnerText);
                objSaldoEstoque.Tpop_cod_tpop = 10;
                lstobjSaldoEstoque.Add(objSaldoEstoque);


                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 10;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();

                /*dsProduto = new DataSet();
                dsProduto = clsProdutoMaterial.SelecionarTodosProdutosFamilia("3", emen_cod_emen[0].InnerText);*/

                /*DsMapeamento = new DataSet();
                DsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(Convert.ToInt32(dsProduto.Tables[0].Rows[0]["prma_cod_prma"].ToString()), Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));*/
                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objSaldoEstoque.Sles_quantidade = quantidade_folhas;
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(folha[0].InnerText);
                objSaldoEstoque.Tpop_cod_tpop = -4;
                lstobjSaldoEstoque.Add(objSaldoEstoque);


                objMovimentoEstoque.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                objMovimentoEstoque.Moes_dt_criacao = data_criacao;
                objMovimentoEstoque.Moes_quantidade = objSaldoEstoque.Sles_quantidade;
                objMovimentoEstoque.Tpmo_cod_tpmo = 10;
                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                objSaldoEstoque = new Objetos.objSaldoEstoque();

                gravou = clsEntradaLinha.cadastraRetornoLinha(lstobjMovimentoEstoque, lstobjSaldoEstoque, Convert.ToInt32(emen_cod_emen[0].InnerText));

                //aqui vou realizar a gravação na tabela de inventário

                Objetos.objInventario objInventario = new Objetos.objInventario();
                Classes.clsInventario clsInventario = new Classes.clsInventario();

                objInventario.Emen_cod_empr = Convert.ToInt32(emen_cod_emen[0].InnerText);
                objInventario.Emen_cod_empr_forn = Convert.ToInt32(empresaFornecedora[0].InnerText);
                objInventario.Inve_dt_criacao = DateTime.Now;
                objInventario.Inve_operacao = "Retorno Linha";
                objInventario.Inve_quantidade = quantidade_paletes;
                objInventario.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText);
                objInventario.Tpmo_cod_tpmo = 10;
                objInventario.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                objInventario.Prma_cod_prma_pallet = Convert.ToInt32(pallet[0].InnerText);
                objInventario.Prma_cod_prma_topo = Convert.ToInt32(topo[0].InnerText);
                objInventario.Prma_cod_prma_divisor = Convert.ToInt32(folha[0].InnerText);
                var retornoCadinv = clsInventario.CadastrarInventarioEntradaLinha(objInventario);

                if (gravou == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<EntradaLinha>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</EntradaLinha>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os grupos cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosGruposClientes()
        {
            //aqui instâncio a classe de empresas
            Classes.clsGrupo clsGrupo = new Classes.clsGrupo();

            //aqui o objeto da empresa
            Objetos.objGrupo objGrupo = new Objetos.objGrupo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsGrupos = new DataSet();
                dsGrupos = clsGrupo.SelecionarTodosGruposClientes();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<grupos>";
                if (dsGrupos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsGrupos.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<grupo>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsGrupos.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsGrupos.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
                        xmlRetorno = xmlRetorno + "<grem_descricao>" + dsGrupos.Tables[0].Rows[i]["grem_descricao"].ToString() + "</grem_descricao>";
                        xmlRetorno = xmlRetorno + "</grupo>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</grupos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'><status>Erro</status><descerro>" + e.Message + "</descerro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os grupos cadastrados na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosGruposFornecedores()
        {
            //aqui instâncio a classe de empresas
            Classes.clsGrupo clsGrupo = new Classes.clsGrupo();

            //aqui o objeto da empresa
            Objetos.objGrupo objGrupo = new Objetos.objGrupo();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsGrupos = new DataSet();
                dsGrupos = clsGrupo.SelecionarTodosGruposFornecedores();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<grupos>";
                if (dsGrupos.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsGrupos.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<grupo>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsGrupos.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsGrupos.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
                        xmlRetorno = xmlRetorno + "<grem_descricao>" + dsGrupos.Tables[0].Rows[i]["grem_descricao"].ToString() + "</grem_descricao>";
                        xmlRetorno = xmlRetorno + "</grupo>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</grupos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'><status>Erro</status><descerro>" + e.Message + "</descerro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todas as empresas cadastradas na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEmpresasFornecedoras()
        {
            //aqui instâncio a classe de empresas
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

            //aqui o objeto da empresa
            Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsEmpresas = new DataSet();
                dsEmpresas = clsEmpresa.SelecionarEmpresasFornecedoras();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<empresas>";

                if (dsEmpresas.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsEmpresas.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<empresa>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsEmpresas.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<emen_cnpj>" + dsEmpresas.Tables[0].Rows[i]["emen_cnpj"].ToString() + "</emen_cnpj>";
                        xmlRetorno = xmlRetorno + "<emen_nome>" + dsEmpresas.Tables[0].Rows[i]["emen_nome"].ToString() + "</emen_nome>";
                        xmlRetorno = xmlRetorno + "<emen_nomered>" + dsEmpresas.Tables[0].Rows[i]["emen_nomered"].ToString() + "</emen_nomered>";
                        xmlRetorno = xmlRetorno + "<emen_lograd>" + dsEmpresas.Tables[0].Rows[i]["emen_lograd"].ToString() + "</emen_lograd>";
                        xmlRetorno = xmlRetorno + "<emen_numero>" + dsEmpresas.Tables[0].Rows[i]["emen_numero"].ToString() + "</emen_numero>";
                        xmlRetorno = xmlRetorno + "<emen_comple>" + dsEmpresas.Tables[0].Rows[i]["emen_comple"].ToString() + "</emen_comple>";
                        xmlRetorno = xmlRetorno + "<emen_bairro>" + dsEmpresas.Tables[0].Rows[i]["emen_bairro"].ToString() + "</emen_bairro>";
                        xmlRetorno = xmlRetorno + "<emen_cep>" + dsEmpresas.Tables[0].Rows[i]["emen_cep"].ToString() + "</emen_cep>";
                        xmlRetorno = xmlRetorno + "<emen_uf>" + dsEmpresas.Tables[0].Rows[i]["emen_uf"].ToString() + "</emen_uf>";
                        xmlRetorno = xmlRetorno + "<emen_ddd>" + dsEmpresas.Tables[0].Rows[i]["emen_ddd"].ToString() + "</emen_ddd>";
                        xmlRetorno = xmlRetorno + "<emen_tel>" + dsEmpresas.Tables[0].Rows[i]["emen_tel"].ToString() + "</emen_tel>";
                        xmlRetorno = xmlRetorno + "<emen_email>" + dsEmpresas.Tables[0].Rows[i]["emen_email"].ToString() + "</emen_email>";
                        xmlRetorno = xmlRetorno + "<emen_dt_criacao>" + Convert.ToDateTime(dsEmpresas.Tables[0].Rows[i]["emen_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</emen_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEmpresas.Tables[0].Rows[i]["usur_cod_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<emen_ativo>" + dsEmpresas.Tables[0].Rows[i]["emen_ativo"].ToString() + "</emen_ativo>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsEmpresas.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsEmpresas.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
                        xmlRetorno = xmlRetorno + "<emen_forn_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_forn_emen"].ToString() + "</emen_forn_emen>";
                        xmlRetorno = xmlRetorno + "<emen_carregamento_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_carregamento_emen"].ToString() + "</emen_carregamento_emen>";
                        xmlRetorno = xmlRetorno + "</empresa>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</empresas>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarEmpresasIH()
        {

            
			//aqui instâncio a classe de empresas
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

            //aqui o objeto da empresa
            Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsEmpresas = new DataSet();
                dsEmpresas = clsEmpresa.SelecionarEmpresasIH();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<empresa>";

				xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
				xmlRetorno = xmlRetorno + "<empresas>";

				if (dsEmpresas.Tables[0].Rows.Count > 0)
				{
					xmlRetorno = xmlRetorno + "<status>sucesso</status>";
					for (int i = 0; i <= dsEmpresas.Tables[0].Rows.Count - 1; i++)
					{
						xmlRetorno = xmlRetorno + "<empresa>";
						xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsEmpresas.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
						xmlRetorno = xmlRetorno + "<emen_nome>" + dsEmpresas.Tables[0].Rows[i]["emen_nomered"].ToString() + "</emen_nome>";
						xmlRetorno = xmlRetorno + "</empresa>";
					}
				}
				else
				{
					xmlRetorno = xmlRetorno + "<status>falha</status>";
				}
				xmlRetorno = xmlRetorno + "</empresas>";


				return xmlRetorno;
			}
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


		[WebMethod]
		public string SelecionarEmpresasOS()
		{


			//aqui instâncio a classe de empresas
			Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

			//aqui o objeto da empresa
			Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

			//aqui variável para criação do xml de retorno
			string xmlRetorno = "";

			try
			{
				DataSet dsEmpresas = new DataSet();
				dsEmpresas = clsEmpresa.SelecionarEmpresasOS();
				xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
				xmlRetorno = xmlRetorno + "<empresa>";

				xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
				xmlRetorno = xmlRetorno + "<empresas>";

				if (dsEmpresas.Tables[0].Rows.Count > 0)
				{
					xmlRetorno = xmlRetorno + "<status>sucesso</status>";
					for (int i = 0; i <= dsEmpresas.Tables[0].Rows.Count - 1; i++)
					{
						xmlRetorno = xmlRetorno + "<empresa>";
						xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsEmpresas.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
						xmlRetorno = xmlRetorno + "<emen_nome>" + dsEmpresas.Tables[0].Rows[i]["emen_nomered"].ToString() + "</emen_nome>";
						xmlRetorno = xmlRetorno + "</empresa>";
					}
				}
				else
				{
					xmlRetorno = xmlRetorno + "<status>falha</status>";
				}
				xmlRetorno = xmlRetorno + "</empresas>";


				return xmlRetorno;
			}
			catch (Exception e)
			{
				xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
				return xmlRetorno;
			}
		}


		[WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o numero da nf 
        /// o mesmo irá retornar um xml com os dados da nf
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarNotaFiscalNumeroEmpresaOrigem(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList canf_numnf = doc.GetElementsByTagName("canf_numnf");
                XmlNodeList emen_cnpj = doc.GetElementsByTagName("emen_cnpj");


                DataSet dsNf = new DataSet();
                dsNf = clsNotaFiscal.SelecionarNotaFiscalNumeroEmpresaOrigem(emen_cnpj[0].InnerText.ToString(), canf_numnf[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<notafiscal>";
                if (dsNf.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<canf_cod_canf>" + dsNf.Tables[0].Rows[0]["canf_cod_canf"].ToString() + "</canf_cod_canf>";
                    xmlRetorno = xmlRetorno + "<canf_numnt>" + dsNf.Tables[0].Rows[0]["canf_numnt"].ToString() + "</canf_numnt>";
                    xmlRetorno = xmlRetorno + "<canf_serienf>" + dsNf.Tables[0].Rows[0]["canf_serienf"].ToString() + "</canf_serienf>";
                    xmlRetorno = xmlRetorno + "<canf_chavenfe>" + dsNf.Tables[0].Rows[0]["canf_chavenfe"].ToString() + "</canf_chavenfe>";
                    xmlRetorno = xmlRetorno + "<canf_dt_emissao>" + dsNf.Tables[0].Rows[0]["canf_dt_emissao"].ToString() + "</canf_dt_emissao>";
                    xmlRetorno = xmlRetorno + "<canf_dt_cancelamento>" + dsNf.Tables[0].Rows[0]["canf_dt_cancelamento"].ToString() + "</canf_dt_cancelamento>";
                    xmlRetorno = xmlRetorno + "<canf_transito>" + dsNf.Tables[0].Rows[0]["canf_transito"].ToString() + "</canf_transito>";
                    xmlRetorno = xmlRetorno + "<moto_cod_moto>" + dsNf.Tables[0].Rows[0]["moto_cod_moto"].ToString() + "</moto_cod_moto>";
                    xmlRetorno = xmlRetorno + "<moto_nome>" + dsNf.Tables[0].Rows[0]["moto_nome"].ToString().ToString().Trim() + "</moto_nome>";
                    xmlRetorno = xmlRetorno + "<veic_cod_veic>" + dsNf.Tables[0].Rows[0]["veic_cod_veic"].ToString() + "</veic_cod_veic>";
                    xmlRetorno = xmlRetorno + "<veic_placa>" + dsNf.Tables[0].Rows[0]["veic_placa"].ToString() + "</veic_placa>";
                    xmlRetorno = xmlRetorno + "<canf_id_reg>" + dsNf.Tables[0].Rows[0]["canf_id_reg"].ToString() + "</canf_id_reg>";
                    xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsNf.Tables[0].Rows[0]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                    xmlRetorno = xmlRetorno + "<unme_descricao>" + dsNf.Tables[0].Rows[0]["unme_descricao"].ToString() + "</unme_descricao>";
                    xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsNf.Tables[0].Rows[0]["unme_id_reg"].ToString() + "</unme_id_reg>";
                    xmlRetorno = xmlRetorno + "<tran_cod_tran>" + dsNf.Tables[0].Rows[0]["tran_cod_tran"].ToString() + "</tran_cod_tran>";
                    xmlRetorno = xmlRetorno + "<tran_cnpj>" + dsNf.Tables[0].Rows[0]["tran_cnpj"].ToString() + "</tran_cnpj>";
                    xmlRetorno = xmlRetorno + "<tran_nome>" + dsNf.Tables[0].Rows[0]["tran_nome"].ToString() + "</tran_nome>";
                    xmlRetorno = xmlRetorno + "<tran_nomered>" + dsNf.Tables[0].Rows[0]["tran_nomered"].ToString() + "</tran_nomered>";
                    xmlRetorno = xmlRetorno + "<tran_lograd>" + dsNf.Tables[0].Rows[0]["tran_lograd"].ToString() + "</tran_lograd>";
                    xmlRetorno = xmlRetorno + "<tran_num>" + dsNf.Tables[0].Rows[0]["tran_num"].ToString() + "</tran_num>";
                    xmlRetorno = xmlRetorno + "<tran_uf>" + dsNf.Tables[0].Rows[0]["tran_uf"].ToString() + "</tran_uf>";
                    xmlRetorno = xmlRetorno + "<tran_compl>" + dsNf.Tables[0].Rows[0]["tran_compl"].ToString() + "</tran_compl>";
                    xmlRetorno = xmlRetorno + "<tran_bairro>" + dsNf.Tables[0].Rows[0]["tran_bairro"].ToString() + "</tran_bairro>";
                    xmlRetorno = xmlRetorno + "<tran_cep>" + dsNf.Tables[0].Rows[0]["tran_cep"].ToString() + "</tran_cep>";
                    xmlRetorno = xmlRetorno + "<tran_ddd>" + dsNf.Tables[0].Rows[0]["tran_ddd"].ToString() + "</tran_ddd>";
                    xmlRetorno = xmlRetorno + "<tran_tel>" + dsNf.Tables[0].Rows[0]["tran_tel"].ToString() + "</tran_tel>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr_orig>" + dsNf.Tables[0].Rows[0]["emen_cod_empr_orig"].ToString() + "</emen_cod_empr_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cnpj_orig>" + dsNf.Tables[0].Rows[0]["emen_cnpj_orig"].ToString() + "</emen_cnpj_orig>";
                    xmlRetorno = xmlRetorno + "<emen_nome_orig>" + dsNf.Tables[0].Rows[0]["emen_nome_orig"].ToString() + "</emen_nome_orig>";
                    xmlRetorno = xmlRetorno + "<emen_nomered_orig>" + dsNf.Tables[0].Rows[0]["emen_nomered_orig"].ToString() + "</emen_nomered_orig>";
                    xmlRetorno = xmlRetorno + "<emen_lograd_orig>" + dsNf.Tables[0].Rows[0]["emen_lograd_orig"].ToString() + "</emen_lograd_orig>";
                    xmlRetorno = xmlRetorno + "<emen_numero_orig>" + dsNf.Tables[0].Rows[0]["emen_numero_orig"].ToString() + "</emen_numero_orig>";
                    xmlRetorno = xmlRetorno + "<emen_uf_orig>" + dsNf.Tables[0].Rows[0]["emen_uf_orig"].ToString() + "</emen_uf_orig>";
                    xmlRetorno = xmlRetorno + "<emen_comple_orig>" + dsNf.Tables[0].Rows[0]["emen_comple_orig"].ToString() + "</emen_comple_orig>";
                    xmlRetorno = xmlRetorno + "<emen_bairro_orig>" + dsNf.Tables[0].Rows[0]["emen_bairro_orig"].ToString() + "</emen_bairro_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cep_orig>" + dsNf.Tables[0].Rows[0]["emen_cep_orig"].ToString() + "</emen_cep_orig>";
                    xmlRetorno = xmlRetorno + "<emen_ddd_orig>" + dsNf.Tables[0].Rows[0]["emen_ddd_orig"].ToString() + "</emen_ddd_orig>";
                    xmlRetorno = xmlRetorno + "<emen_tel_orig>" + dsNf.Tables[0].Rows[0]["emen_tel_orig"].ToString() + "</emen_tel_orig>";
                    xmlRetorno = xmlRetorno + "<emen_email_orig>" + dsNf.Tables[0].Rows[0]["emen_email_orig"].ToString() + "</emen_email_orig>";
                    xmlRetorno = xmlRetorno + "<emen_cnpj_dest>" + dsNf.Tables[0].Rows[0]["emen_cnpj_dest"].ToString() + "</emen_cnpj_dest>";
                    xmlRetorno = xmlRetorno + "<emen_nome_dest>" + dsNf.Tables[0].Rows[0]["emen_nome_dest"].ToString() + "</emen_nome_dest>";
                    xmlRetorno = xmlRetorno + "<emen_nomered_dest>" + dsNf.Tables[0].Rows[0]["emen_nomered_dest"].ToString() + "</emen_nomered_dest>";
                    xmlRetorno = xmlRetorno + "<emen_lograd_dest>" + dsNf.Tables[0].Rows[0]["emen_lograd_dest"].ToString() + "</emen_lograd_dest>";
                    xmlRetorno = xmlRetorno + "<emen_numero_dest>" + dsNf.Tables[0].Rows[0]["emen_numero_dest"].ToString() + "</emen_numero_dest>";
                    xmlRetorno = xmlRetorno + "<emen_uf_dest>" + dsNf.Tables[0].Rows[0]["emen_uf_dest"].ToString() + "</emen_uf_dest>";
                    xmlRetorno = xmlRetorno + "<emen_comple_dest>" + dsNf.Tables[0].Rows[0]["emen_comple_dest"].ToString() + "</emen_comple_dest>";
                    xmlRetorno = xmlRetorno + "<emen_bairro_dest>" + dsNf.Tables[0].Rows[0]["emen_bairro_dest"].ToString() + "</emen_bairro_dest>";
                    xmlRetorno = xmlRetorno + "<emen_cep_dest>" + dsNf.Tables[0].Rows[0]["emen_cep_dest"].ToString() + "</emen_cep_dest>";
                    xmlRetorno = xmlRetorno + "<emen_ddd_dest>" + dsNf.Tables[0].Rows[0]["emen_ddd_dest"].ToString() + "</emen_ddd_dest>";
                    xmlRetorno = xmlRetorno + "<emen_tel_dest>" + dsNf.Tables[0].Rows[0]["emen_tel_dest"].ToString() + "</emen_tel_dest>";
                    xmlRetorno = xmlRetorno + "<emen_email_dest>" + dsNf.Tables[0].Rows[0]["emen_email_dest"].ToString() + "</emen_email_dest>";


                    xmlRetorno = xmlRetorno + "<itensNf>";
                    for (int i = 0; i <= dsNf.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<itenNf>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsNf.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsNf.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsNf.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsNf.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsNf.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsNf.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsNf.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsNf.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsNf.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<itnf_cod_itnf>" + dsNf.Tables[0].Rows[i]["itnf_cod_itnf"].ToString() + "</itnf_cod_itnf>";
                        xmlRetorno = xmlRetorno + "<itnf_quantidade>" + dsNf.Tables[0].Rows[i]["itnf_quantidade"].ToString() + "</itnf_quantidade>";
                        xmlRetorno = xmlRetorno + "<itnf_cod_kit_lata>" + dsNf.Tables[0].Rows[i]["itnf_cod_kit_lata"].ToString() + "</itnf_cod_kit_lata>";
                        xmlRetorno = xmlRetorno + "<itnf_qtd_kit_lata>" + dsNf.Tables[0].Rows[i]["itnf_qtd_kit_lata"].ToString() + "</itnf_qtd_kit_lata>";
                        xmlRetorno = xmlRetorno + "<itnf_id_reg>" + dsNf.Tables[0].Rows[i]["itnf_id_reg"].ToString() + "</itnf_id_reg>";
                        xmlRetorno = xmlRetorno + "<kits_cod_kits>" + dsNf.Tables[0].Rows[i]["kits_cod_kits"].ToString() + "</kits_cod_kits>";
                        xmlRetorno = xmlRetorno + "<kits_descricao>" + dsNf.Tables[0].Rows[i]["kits_descricao"].ToString() + "</kits_descricao>";
                        xmlRetorno = xmlRetorno + "<kits_quant_prod_prin>" + dsNf.Tables[0].Rows[i]["kits_quant_prod_prin"].ToString() + "</kits_quant_prod_prin>";
                        xmlRetorno = xmlRetorno + "<kits_quant_pallets>" + dsNf.Tables[0].Rows[i]["kits_quant_pallets"].ToString() + "</kits_quant_pallets>";
                        xmlRetorno = xmlRetorno + "<kits_quant_topo>" + dsNf.Tables[0].Rows[i]["kits_quant_topo"].ToString() + "</kits_quant_topo>";
                        xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsNf.Tables[0].Rows[i]["kits_quant_folha"].ToString() + "</kits_quant_folha>";
                        xmlRetorno = xmlRetorno + "<kits_quant_por_camada>" + dsNf.Tables[0].Rows[i]["kits_quant_por_camada"].ToString() + "</kits_quant_por_camada>";





                        xmlRetorno = xmlRetorno + "</itenNf>";
                    }
                    xmlRetorno = xmlRetorno + "</itensNf>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</notafiscal>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Recebera um xml contendo o código do produto ou material que deseja selecionar
        /// o mesmo irá retornar uma lista com todos os produtos/materiais, cadastrados na base de dados
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarUmProdutoMaterialPorPN(string xmlString)
        {

            //instâncio a classe de usuários
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui instâncio a classe para fazer a validação do xml de entrada
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);




                XmlNodeList prma_pn = doc.GetElementsByTagName("prma_pn");
                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");


                DataSet dsProdutoMaterial = new DataSet();
                int i = 0;

                if (prma_pn[0].InnerText.ToString().IndexOf(@"/") > 0 || prma_pn[0].InnerText.ToString().IndexOf(".") > 0)
                {
                    dsProdutoMaterial = clsProdutoMaterial.SelecionarUmProdutoMaterialPorPNStr(prma_pn[0].InnerText.ToString().Replace(@"/", "").Replace(".", ""), grem_cd_grem[0].InnerText);
                }
                else
                {
                    dsProdutoMaterial = clsProdutoMaterial.SelecionarUmProdutoMaterialPorPN(prma_pn[0].InnerText.ToString(), grem_cd_grem[0].InnerText);
                }
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno = "<produtomaterial>";
                if (dsProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";

                    xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";

                    xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                    xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                    xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                    xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Replace(".", ",") + "</prma_fator_conservacao>";
                    xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_preco"].ToString().Replace(".", ",") + "</prma_preco>";
                    xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                    xmlRetorno = xmlRetorno + "<prma_dt_criacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_dt_criacao"].ToString() + "</prma_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsProdutoMaterial.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<prma_ativo>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_ativo"].ToString() + "</prma_ativo>";
                    xmlRetorno = xmlRetorno + "<unme_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                    xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                    xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_descricao"].ToString() + "</tpma_descricao>";
                    xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                    xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</produtomaterial>";

                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o número da nota_fiscal
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string cadastraNotaFiscal(string xmlString)
        {

            //aqui instâncio a classe de empresas

            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsTransportadora clsTransp = new Classes.clsTransportadora();



            //aqui o objeto da empresa
            Objetos.objNotaFiscal objNotaFiscal = new Objetos.objNotaFiscal();
            Objetos.objItensNotaFiscal objItensNotaFiscal = new Objetos.objItensNotaFiscal();
            Objetos.objXmlmo objXmlmo = new Objetos.objXmlmo();
            Objetos.objTransportadora objTransp = new Objetos.objTransportadora();


            //aqui vou criar as listas dos objetos
            List<Objetos.objItensNotaFiscal> lstobjItensNotaFiscal = new List<Objetos.objItensNotaFiscal>();


            DataSet ds = new DataSet();
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";


            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string gravou = "";
                int tran_codigo = 0;


                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                XmlNodeList canf_serie = doc.GetElementsByTagName("canf_serie");
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                XmlNodeList canf_dt_emissao = doc.GetElementsByTagName("canf_dt_emissao");
                XmlNodeList emen_cod_empr_origem = doc.GetElementsByTagName("emen_cod_empr_origem");
                XmlNodeList emen_cd_empr_destino = doc.GetElementsByTagName("emen_cd_empr_destino");
                XmlNodeList tran_cod_tran = doc.GetElementsByTagName("tran_cod_tran");


                XmlNodeList canf_chavenfe = doc.GetElementsByTagName("canf_chavenfe");
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList itnf_quantidade = doc.GetElementsByTagName("itnf_quantidade");



                if (tran_cod_tran[0] != null)
                {
                    tran_codigo = Convert.ToInt32(tran_cod_tran[0].InnerText);
                }

                XmlNodeList xmlo = doc.GetElementsByTagName("xmlo");



                objNotaFiscal.Canf_numnt = canf_numnt[0].InnerText;
                objNotaFiscal.Canf_serienf = canf_serie[0].InnerText;
                objNotaFiscal.Canf_chavenfe = canf_chavenfe[0].InnerText.ToString();
                objNotaFiscal.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText.ToString());
                objNotaFiscal.Canf_dt_emissao = Convert.ToDateTime(canf_dt_emissao[0].InnerText.ToString());
                objNotaFiscal.Emen_cod_empr_origem = Convert.ToInt32(emen_cod_empr_origem[0].InnerText.ToString());
                objNotaFiscal.Emen_cd_empr_destino = Convert.ToInt32(emen_cd_empr_destino[0].InnerText.ToString());
                objNotaFiscal.Tran_cod_tran = tran_codigo;
                objNotaFiscal.Canf_dt_criacao = DateTime.Now;



                for (int i = 0; i <= prma_cod_prma.Count - 1; i++)
                {
                    objItensNotaFiscal.Itnf_dt_criacao = objNotaFiscal.Canf_dt_criacao;
                    objItensNotaFiscal.Usur_cd_usur = objNotaFiscal.Usur_cd_usur;
                    objItensNotaFiscal.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[i].InnerText);
                    objItensNotaFiscal.Itnf_quantidade = Convert.ToDouble(itnf_quantidade[i].InnerText.Replace(".", ","));
					bool achou = false;
					for (int kk = 0;kk <= lstobjItensNotaFiscal.Count -1;kk++)
					{
						if (lstobjItensNotaFiscal[kk].Prma_cod_prma == objItensNotaFiscal.Prma_cod_prma)
						{
							lstobjItensNotaFiscal[kk].Itnf_quantidade = lstobjItensNotaFiscal[kk].Itnf_quantidade + objItensNotaFiscal.Itnf_quantidade;
							achou = true;
						}
					}
					if (achou == false)
					{
						lstobjItensNotaFiscal.Add(objItensNotaFiscal);
					}
					objItensNotaFiscal = new Objetos.objItensNotaFiscal();
				}

                objXmlmo.Usur_cd_usur = objNotaFiscal.Usur_cd_usur;
                objXmlmo.Xlmo_dt_criacao = objNotaFiscal.Canf_dt_criacao;
                objXmlmo.Xmlo_dt_emissao = objNotaFiscal.Canf_dt_emissao;
                objXmlmo.Xlmo_xmlnf_xlmo = xmlo[0].InnerText;

                gravou = clsNotaFiscal.cadastraNotaFiscal(objNotaFiscal, lstobjItensNotaFiscal, objXmlmo);

                if (gravou == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<notafiscal>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</notafiscal>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do inventario para cadastro
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarKitsBonsRuins(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsQrcod clsQrcod = new Classes.clsQrcod();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();
            Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();



            //aqui o objeto da empresa
            Objetos.objQrcode objQrcode = new Objetos.objQrcode();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XmlNodeList qrcd_txqrcd = doc.GetElementsByTagName("qrcd_txqrcd");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                XmlNodeList qrcd_qtd_qrcd = doc.GetElementsByTagName("qrcd_qtd_qrcd");
                XmlNodeList qrcd_bol_conj_bom = doc.GetElementsByTagName("qrcd_bol_conj_bom");
                XmlNodeList empresaFornecedora = doc.GetElementsByTagName("empresaFornecedora");

                XmlNodeList folha = doc.GetElementsByTagName("folha");
                XmlNodeList pallet = doc.GetElementsByTagName("pallet");
                XmlNodeList topo = doc.GetElementsByTagName("topo");

                objQrcode.Qrcd_txqrcd = qrcd_txqrcd[0].InnerText.ToString().Trim();
                objQrcode.Emen_cod_empr = Convert.ToInt32(emen_cod_empr[0].InnerText.ToString().Trim());
                objQrcode.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText.ToString().Trim());
                objQrcode.Qrcd_dt_criacao = DateTime.Now;
                objQrcode.Qrcd_qtd_qrcd = Convert.ToInt32(qrcd_qtd_qrcd[0].InnerText.ToString().Trim());
                objQrcode.Qrcd_bol_conj_bom = Convert.ToBoolean(qrcd_bol_conj_bom[0].InnerText.ToString().Trim());
                objQrcode.Qrcd_cod_pallet = Convert.ToInt32(pallet[0].InnerText);
                objQrcode.Qrcd_cod_topo = Convert.ToInt32(topo[0].InnerText);
                objQrcode.Qrcd_cod_folha = Convert.ToInt32(folha[0].InnerText);


                DataSet dsProduto = new DataSet();
                dsProduto = clsProdutoMaterial.SelecionarTodosProdutosFamilia("3", emen_cod_empr[0].InnerText);

                DataSet dsEmpresa = clsEmpresa.SelecionarEmpresa(Convert.ToInt32(empresaFornecedora[0].InnerText));

                Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();


                int qtd_folhas_kit = -1;

                DataSet dsQuantidadeKit = clsEmpresa.SelecionarFolhasKitEmpresa(Convert.ToInt32(emen_cod_empr[0].InnerText));

                if (Convert.ToDouble(dsQuantidadeKit.Tables[0].Rows[0]["emen_nr_folhas_kit"].ToString()) > 0)
                {
                    qtd_folhas_kit = Convert.ToInt32(dsQuantidadeKit.Tables[0].Rows[0]["emen_nr_folhas_kit"].ToString());
                }
                else
                {
                    qtd_folhas_kit = 794;
                }



                DataSet dsmapa = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodEmpresaFornecedora(Convert.ToInt32(dsProduto.Tables[0].Rows[0]["prma_cod_prma"].ToString()), Convert.ToInt32(dsEmpresa.Tables[0].Rows[0]["grem_cd_grem"].ToString()));

                objSaldoEstoque.Emen_cod_empresa = Convert.ToInt32(emen_cod_empr[0].InnerText);
                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(folha[0].InnerText);
                objSaldoEstoque.Sles_quantidade = qtd_folhas_kit; //fixo por enquanto, depois mudar para configuração
                objSaldoEstoque.Tpop_cod_tpop = -3;



                string erro = clsQrcod.CadastrarKitsBonsRuins(objQrcode, objSaldoEstoque);



                if (erro == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<qrcode>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</qrcode>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Ocorreu um erro no cadastro do kit</descerro></retornoErro>";
                }

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }
        [WebMethod]
        public string CadastrarCarregamentoOnSite(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsCarregamento clsCarregamento = new Classes.clsCarregamento();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();
			clsMotorista clsMoto = new clsMotorista();
			clsVeiculos clsVeiculos = new clsVeiculos();

			//aqui o objeto da empresa
			Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();

            Objetos.objMotorista objMotorista = new Objetos.objMotorista();
            Objetos.objTransportadora objTranportadora = new Objetos.objTransportadora();
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();
            Objetos.objArim objArim = new Objetos.objArim();

			Objetos.objMotorista objMotorista1 = new Objetos.objMotorista();
			Objetos.objVeiculos objVeiculos1 = new Objetos.objVeiculos();



			//aqui vou criar as listas dos objetos
			List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();
            List<Objetos.objKits> lstobjobjKits = new List<Objetos.objKits>();
            List<Objetos.objArim> lstobjArim = new List<Objetos.objArim>();


            DataSet ds = new DataSet();
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string gravou = "";



                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                string[] numNt = canf_numnt[0].InnerText.Split('|');
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                //XmlNodeList moes_dt_criacao = doc.GetElementsByTagName("moes_dt_criacao");
                XmlNodeList moto_cod_moto = doc.GetElementsByTagName("moto_cod_moto");
                //XmlNodeList veic_cod_veic = doc.GetElementsByTagName("veic_cod_veic");
                XmlNodeList foto_nome = doc.GetElementsByTagName("foto_nome");
                XmlNodeList tran_cod_tran = doc.GetElementsByTagName("tran_cod_tran");
                XmlNodeList veic_plac = doc.GetElementsByTagName("veic_plac");
                XmlNodeList foto_base_64 = doc.GetElementsByTagName("foto_base_64");

				string nome_motorista = moto_cod_moto[0].InnerText.ToUpper().Trim();

                if (tran_cod_tran[0] != null)
                {
                    if (tran_cod_tran[0].InnerText.Trim() != "")
                    {
                        objTranportadora.Tran_cod_tran = Convert.ToInt32(tran_cod_tran[0].InnerText);
                    }
                }

				DataSet dsMoto = clsMoto.SelecionarMotoristaPorNome(nome_motorista.ToUpper().Trim());
				if (dsMoto.Tables[0].Rows.Count > 0)
				{
					objMotorista.Moto_cod_moto = Convert.ToInt32(dsMoto.Tables[0].Rows[0]["moto_cod_moto"].ToString());
				}
				else
				{
					objMotorista1.Moto_nome = nome_motorista;
					objMotorista1.Moto_dt_criacao = DateTime.Now;
					objMotorista1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
					objMotorista.Moto_cod_moto = clsMoto.InserirMotorista(objMotorista1);

				}

				string veic_plac_aux = "";
				if (veic_plac[0] != null)
				{
					veic_plac_aux = veic_plac[0].InnerText.ToUpper();
				}

				//aqui vou verificar se a placa já está cadastrada na base
				//caso esteja pegarei o id existente, caso contrário irei cadastrar o veículo
				//e pegar o código do mesmo.
				if (veic_plac_aux.Trim() != "")
				{
					DataSet Dsveic = new DataSet();
					Dsveic = clsVeiculos.SelecionarUmVeiculoPlaca(veic_plac_aux.Trim().ToUpper());

					if (Dsveic.Tables[0].Rows.Count > 0)
					{
						objVeiculos.Veic_cod_veic = Convert.ToInt32(Dsveic.Tables[0].Rows[0]["veic_cod_veic"].ToString());
					}
					else
					{
						objVeiculos1.Veic_placa = veic_plac_aux.Trim().ToUpper();
						objVeiculos1.Veic_dt_criacao = DateTime.Now;
						objVeiculos1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText.ToString());
						//aqui como a placa não existe na tabela de veículos, irei cadastrar a mesma.
						objVeiculos.Veic_cod_veic = clsVeiculos.CadastrarVeiculo(objVeiculos1);
					}
				}

				for (int k = 0; k <= numNt.Count() - 1; k++)
                {
                    //aqui vou selecionar a nota fiscal para pegar as empresas e os itens.
                    //ds = clsNotaFiscal.SelecionarNotaFiscaNumero(numNt[k].ToString());
                    ds = clsNotaFiscal.SelecionarNotaFiscaNumeroSemMovimentacaoEmpresaOrigem(numNt[k].ToString(), usur_cod_usur[0].InnerText);

                    int empr_cod_empr_origem = 0;
                    int empr_cd_empr_destino = 0;
                    int grem_cd_grem_orig = 0;
                    int grem_cd_grem_dest = 0;

                    string canf_cod_canf = "";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit
                        canf_cod_canf = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                        empr_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_origem"].ToString());
                        empr_cd_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());
                        grem_cd_grem_dest = Convert.ToInt32(ds.Tables[0].Rows[0]["grem_cd_grem_dest"].ToString());
                        grem_cd_grem_orig = Convert.ToInt32(ds.Tables[0].Rows[0]["grem_cd_grem_orig"].ToString());
                        Classes.clsNotaFiscal clsnota = new Classes.clsNotaFiscal();
                        DataSet dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacaoPorCodigo(canf_cod_canf, 1);

                        if (dsnota.Tables[0].Rows.Count > 0)
                        {
                            xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota fiscal já carregada</descerro></retornoErro>";
                            return xmlRetorno;
                        }
                        //aqui vou montar o objeto de atualização do estoque
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            //aqui irei montar a quantidade a ser colocada em cada produto dependendo da família do mesmo:
                            //1 pallet, 2 top, 3 folhas

                            objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(ds.Tables[0].Rows[i]["canf_cod_canf"].ToString());

                            objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;

                            objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString());
                            objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                            objMovimentoEstoque.Tpmo_cod_tpmo = 1;

                            //aqui vou calcular o produto, a quantidade e realizar o mapeamento para poder atualizar a tabela estoque
                            objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString()) / Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString()));
                            lstobjMovimentoEstoque.Add(objMovimentoEstoque);

                            //aqui ire fazer o mapeamento
                            DataSet dsMapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoGrupos(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()), grem_cd_grem_dest, grem_cd_grem_orig);
                            //DataSet dsMapeamento = clsProdutoMaterial.SelecionarUmProdutoMaterial(objMovimentoEstoque.Prma_cod_prma);

                            if (dsMapeamento.Tables[0].Rows.Count > 0)
                            {
                                objSaldoEstoque.Tpop_cod_tpop = -2;
                                objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()));
                                objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
                                objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
                                objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;
                                lstobjSaldoEstoque.Add(objSaldoEstoque);
                            }
                            else
                            {
                                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Faltando mapeamento para o produto: " + ds.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</descerro></retornoErro>";
                                return xmlRetorno;
                            }
                            objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                            objSaldoEstoque = new Objetos.objSaldoEstoque();
                        }

                        string caminhoImagemOrigem = System.Configuration.ConfigurationManager.AppSettings["caminhoImagemOrigem"];
                        for (int j = 0; j <= foto_nome.Count - 1; j++)
                        {
                            
                            
                            objArim.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
                            objArim.Arim_imagem = empr_cod_empr_origem.ToString() + foto_nome[j].InnerText;
                            objArim.Tpmo_cod_tpmo = 1;
                            objArim.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                            objArim.Arim_dt_criacao = DateTime.Now;
                            objArim.Emen_cod_empr = empr_cod_empr_origem;
                            lstobjArim.Add(objArim);
                            objArim = new Objetos.objArim();

                            string x = foto_base_64[j].InnerText;

                            byte[] imageBytes = Convert.FromBase64String(x);
                            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                            ms.Write(imageBytes, 0, imageBytes.Length);
                            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                            image.Save(Path.Combine(caminhoImagemOrigem, empr_cod_empr_origem.ToString() + foto_nome[j].InnerText), System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal não encontrada</descerro></retornoErro>";
                    }
                }
                gravou = clsCarregamento.cadastraCarregamento(lstobjMovimentoEstoque, lstobjSaldoEstoque, objMotorista.Moto_cod_moto, objVeiculos.Veic_cod_veic, objTranportadora.Tran_cod_tran, veic_plac_aux.ToUpper(), lstobjArim);
                if (gravou == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<carregamento>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</carregamento>";
                }



                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarQrcode(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsQrcod clsQrcod = new Classes.clsQrcod();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList qrcd_txqrcd = doc.GetElementsByTagName("qrcd_txqrcd");


                DataSet ds = new DataSet();
                ds = clsQrcod.SelecionarQrcode(qrcd_txqrcd[0].InnerText.Trim());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<qrcode>";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";

                    xmlRetorno = xmlRetorno + "<qrcd_cod_qrcd>" + ds.Tables[0].Rows[0]["qrcd_cod_qrcd"].ToString().Trim() + "</qrcd_cod_qrcd>";
                    xmlRetorno = xmlRetorno + "<qrcd_txqrcd>" + ds.Tables[0].Rows[0]["qrcd_txqrcd"].ToString().Trim() + "</qrcd_txqrcd>";
                    xmlRetorno = xmlRetorno + "<qrcd_dt_criacao>" + ds.Tables[0].Rows[0]["qrcd_dt_criacao"].ToString().Trim() + "</qrcd_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<emen_cod_empr>" + ds.Tables[0].Rows[0]["emen_cod_empr"].ToString().Trim() + "</emen_cod_empr>";
                    xmlRetorno = xmlRetorno + "<qrcd_qtd_qrcd>" + ds.Tables[0].Rows[0]["qrcd_qtd_qrcd"].ToString().Trim() + "</qrcd_qtd_qrcd>";
                    xmlRetorno = xmlRetorno + "<qrcd_bol_conj_bom>" + ds.Tables[0].Rows[0]["qrcd_bol_conj_bom"].ToString().Trim() + "</qrcd_bol_conj_bom>";
                    xmlRetorno = xmlRetorno + "<emen_nomered>" + ds.Tables[0].Rows[0]["emen_nomered"].ToString().Trim() + "</emen_nomered>";
                    xmlRetorno = xmlRetorno + "<usur_nome>" + ds.Tables[0].Rows[0]["usur_nome"].ToString().Trim() + "</usur_nome>";



                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</qrcode>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarTransportadoraCNPJ(string xmlString)
        {



            //aqui instâncio a classe de empresas
            Classes.clsTransportadora clsTransportadora = new Classes.clsTransportadora();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList tran_cnpj = doc.GetElementsByTagName("tran_cnpj");


                DataSet ds = new DataSet();
                ds = clsTransportadora.SelecionarTransportadoraCNPJ(tran_cnpj[0].InnerText.Trim());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<transportadora>";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<tran_cod_tran>" + ds.Tables[0].Rows[0]["tran_cod_tran"].ToString() + "</tran_cod_tran>";
                    xmlRetorno = xmlRetorno + "<tran_cnpj>" + ds.Tables[0].Rows[0]["tran_cnpj"].ToString() + "</tran_cnpj>";
                    xmlRetorno = xmlRetorno + "<tran_nome>" + ds.Tables[0].Rows[0]["tran_nome"].ToString() + "</tran_nome>";
                    xmlRetorno = xmlRetorno + "<tran_nomered>" + ds.Tables[0].Rows[0]["tran_nomered"].ToString() + "</tran_nomered>";
                    xmlRetorno = xmlRetorno + "<tran_lograd>" + ds.Tables[0].Rows[0]["tran_lograd"].ToString() + "</tran_lograd>";
                    xmlRetorno = xmlRetorno + "<tran_num>" + ds.Tables[0].Rows[0]["tran_num"].ToString() + "</tran_num>";
                    xmlRetorno = xmlRetorno + "<tran_uf>" + ds.Tables[0].Rows[0]["tran_uf"].ToString() + "</tran_uf>";
                    xmlRetorno = xmlRetorno + "<tran_compl>" + ds.Tables[0].Rows[0]["tran_compl"].ToString() + "</tran_compl>";
                    xmlRetorno = xmlRetorno + "<tran_bairro>" + ds.Tables[0].Rows[0]["tran_bairro"].ToString() + "</tran_bairro>";
                    xmlRetorno = xmlRetorno + "<tran_cep>" + ds.Tables[0].Rows[0]["tran_cep"].ToString() + "</tran_cep>";
                    xmlRetorno = xmlRetorno + "<tran_ddd>" + ds.Tables[0].Rows[0]["tran_ddd"].ToString() + "</tran_ddd>";
                    xmlRetorno = xmlRetorno + "<tran_tel>" + ds.Tables[0].Rows[0]["tran_tel"].ToString() + "</tran_tel>";
                    xmlRetorno = xmlRetorno + "<tran_email>" + ds.Tables[0].Rows[0]["tran_email"].ToString() + "</tran_email>";
                    xmlRetorno = xmlRetorno + "<tran_ativo>" + ds.Tables[0].Rows[0]["tran_ativo"].ToString() + "</tran_ativo>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</transportadora>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        public string SelecionarKitProduto(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsKits clsKits = new Classes.clsKits();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");


                DataSet dsKit = new DataSet();
                dsKit = clsKits.SelecionarKitProduto(Convert.ToInt32(prma_cod_prma[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<kit>";
                if (dsKit.Tables[0].Rows.Count > 0)
                {


                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsKit.Tables[0].Rows[0]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                    xmlRetorno = xmlRetorno + "<Kits_quant_prod_prin>" + dsKit.Tables[0].Rows[0]["Kits_quant_prod_prin"].ToString() + "</Kits_quant_prod_prin>";
                    xmlRetorno = xmlRetorno + "<kits_quant_folha>" + dsKit.Tables[0].Rows[0]["kits_quant_folha"].ToString() + "</kits_quant_folha>";


                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</kit>";



                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarMapeamentoProdutoCodigoGrupo(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList grem_cd_grem = doc.GetElementsByTagName("grem_cd_grem");


                DataSet dsMap = new DataSet();
                dsMap = clsMapeamentoProdutos.SelecionarMapeamentoProdutoCodigoGrupo(Convert.ToInt32(prma_cod_prma[0].InnerText.Trim()), Convert.ToInt32(grem_cd_grem[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<kit>";
                if (dsMap.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</kit>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera um xml com o número da nota_fiscal
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarRecebimentoInhouse(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsRecebimento clsRecebimento = new Classes.clsRecebimento();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();

            //aqui o objeto da empresa
            Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objKits objKits = new Objetos.objKits();
            Objetos.objMotorista objMotorista = new Objetos.objMotorista();
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();
            Objetos.objTransportadora objTranportadora = new Objetos.objTransportadora();

            //aqui vou criar as listas dos objetos
            List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();
            List<Objetos.objKits> lstobjobjKits = new List<Objetos.objKits>();

			Objetos.objMotorista objMotorista1 = new Objetos.objMotorista();
			Objetos.objVeiculos objVeiculos1 = new Objetos.objVeiculos();
			clsMotorista clsMoto = new clsMotorista();
			clsVeiculos clsVeiculos = new clsVeiculos();

			DataSet ds = new DataSet();
            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";
            int empr_cod_empr_origem = 0;
            int empr_cd_empr_destino = 0;

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string canf_cod_canf = "";
                string gravou = "";

                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                string[] numNt = canf_numnt[0].InnerText.Split('|');
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                XmlNodeList moes_dt_criacao = doc.GetElementsByTagName("moes_dt_criacao");
                XmlNodeList moto_cod_moto = doc.GetElementsByTagName("moto_cod_moto");
                //XmlNodeList veic_cod_veic = doc.GetElementsByTagName("veic_cod_veic");
                XmlNodeList tran_cod_tran = doc.GetElementsByTagName("tran_cod_tran");
                XmlNodeList veic_plac = doc.GetElementsByTagName("veic_plac");
				XmlNodeList emen_empr_origem = doc.GetElementsByTagName("emen_empr_origem");

                using (var writer = new StreamWriter(@System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "/" + canf_numnt[0].InnerText + ".xml"))
                {
                    // Escreve uma string formatada no arquivo
                    writer.Write(xmlString);

                };

                string nome_motorista = moto_cod_moto[0].InnerText.ToString().Trim().ToUpper();
				
				if (tran_cod_tran[0].InnerText.Trim() != "")
                {
                    objTranportadora.Tran_cod_tran = Convert.ToInt32(tran_cod_tran[0].InnerText);
                }

				DataSet dsMoto = clsMoto.SelecionarMotoristaPorNome(nome_motorista.ToUpper().Trim());
				
				if (dsMoto.Tables[0].Rows.Count > 0)
				{
					objMotorista.Moto_cod_moto = Convert.ToInt32(dsMoto.Tables[0].Rows[0]["moto_cod_moto"].ToString());
				}
				else
				{
					objMotorista1.Moto_nome = nome_motorista;
					objMotorista1.Moto_dt_criacao = DateTime.Now;
					objMotorista1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
					objMotorista.Moto_cod_moto = clsMoto.InserirMotorista(objMotorista1);

				}

				string placa_velha = "";
				string veic_plac_aux = "";
				if (veic_plac[0] != null)
				{
					veic_plac_aux = veic_plac[0].InnerText.ToUpper();
				}

				//aqui vou verificar se a placa já está cadastrada na base
				//caso esteja pegarei o id existente, caso contrário irei cadastrar o veículo
				//e pegar o código do mesmo.
				if (veic_plac_aux.Trim() != "")
				{
					DataSet Dsveic = new DataSet();
					Dsveic = clsVeiculos.SelecionarUmVeiculoPlaca(veic_plac_aux.Trim().ToUpper());

					if (Dsveic.Tables[0].Rows.Count > 0)
					{
						objVeiculos.Veic_cod_veic = Convert.ToInt32(Dsveic.Tables[0].Rows[0]["veic_cod_veic"].ToString());
					}
					else
					{
						objVeiculos1.Veic_placa = veic_plac_aux.Trim().ToUpper();
						objVeiculos1.Veic_dt_criacao = DateTime.Now;
						objVeiculos1.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText.ToString());
						//aqui como a placa não existe na tabela de veículos, irei cadastrar a mesma.
						objVeiculos.Veic_cod_veic = clsVeiculos.CadastrarVeiculo(objVeiculos1);
					}
				}
				//aqui vou selecionar a nota fiscal para pegar as empresas e os itens.
				for (int l = 0; l <= numNt.Count() - 1; l++)
                {

                    ds = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestinoInHouse(numNt[l].ToString(), Convert.ToInt32(usur_cod_usur[0].InnerText), emen_empr_origem[0].InnerText);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        canf_cod_canf = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                        //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit

                        empr_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_origem"].ToString());
                        empr_cd_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());

                        Classes.clsNotaFiscal clsnota = new Classes.clsNotaFiscal();
                        DataSet dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacao(canf_numnt[0].InnerText, empr_cod_empr_origem, 2);

                        if (dsnota.Tables[0].Rows.Count > 0)
                        {
                            xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota fiscal já recebida</descerro></retornoErro>";
                            return xmlRetorno;
                        }

						//aqui vou montar o objeto de atualização do estoque
						for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            //aqui irei montar a quantidade a ser colocada em cada produto dependendo da família do mesmo:
                            //1 pallet, 2 top, 3 folhas

                            //Aqui farei o mapemanto do produto para poder ter o código dele como a fornecedora

                            DataSet DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoFornecedora(Convert.ToInt32(ds.Tables[0].Rows[i]["prma_cod_prma"].ToString()), empr_cd_empr_destino);


                            objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(ds.Tables[0].Rows[i]["canf_cod_canf"].ToString());
                            objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                            objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                            objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                            objMovimentoEstoque.Tpmo_cod_tpmo = 2;

                            if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "3" || ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "5")
                            {
                                objMovimentoEstoque.Moes_quantidade = Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString()) / Convert.ToDouble(ds.Tables[0].Rows[i]["prma_fator_conservacao"].ToString());
                            }
                            else
                            {
                                objMovimentoEstoque.Moes_quantidade = Convert.ToDouble(ds.Tables[0].Rows[i]["itnf_quantidade"].ToString());
                            }

                            lstobjMovimentoEstoque.Add(objMovimentoEstoque);


                            objSaldoEstoque.Tpop_cod_tpop = 2;
                            objSaldoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                            objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;

                            lstobjSaldoEstoque.Add(objSaldoEstoque);
                            objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                            objSaldoEstoque = new Objetos.objSaldoEstoque();


                        }

                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal não encontrada</descerro></retornoErro>";
                        return xmlRetorno;
                    }
                }

				clsVeiculos clsVeic = new clsVeiculos();
				DataSet veicDs = clsVeic.SelecionarUmVeiculos(Convert.ToInt32(ds.Tables[0].Rows[0]["veic_cod_veic"].ToString()));

				if (veicDs.Tables[0].Rows.Count > 0)
				{
					placa_velha = veicDs.Tables[0].Rows[0]["veic_placa"].ToString();
				}


				gravou = clsRecebimento.cadastraRecebimento(lstobjMovimentoEstoque, lstobjSaldoEstoque, empr_cod_empr_origem, empr_cd_empr_destino, objMotorista.Moto_cod_moto, objVeiculos.Veic_cod_veic, canf_cod_canf, objTranportadora.Tran_cod_tran.ToString(), veic_plac[0].InnerText, placa_velha);
                if (gravou == "")
                {
                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<recebimentoInHouse>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</recebimentoInHouse>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                }


                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        public void CadastrarErroNotaFiscaNumero(string xmlString)
        {


            //aqui instâncio a classe de empresas
            Classes.clsImportacaoNota clsImportacaoNota = new Classes.clsImportacaoNota();


            //aqui o objeto da empresa
            Objetos.objImportacaoNota objImportacaoNota = new Objetos.objImportacaoNota();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList nfer_numnota_nfer = doc.GetElementsByTagName("nfer_numnota_nfer");
                XmlNodeList nfer_erro_nfer = doc.GetElementsByTagName("nfer_erro_nfer");

                objImportacaoNota.Nfer_numnota_nfer = nfer_numnota_nfer[0].InnerText;
                objImportacaoNota.Nfer_erro_nfer = nfer_erro_nfer[0].InnerText;


                DataSet Dsimportacao = clsImportacaoNota.SelecionarErroNotaFiscaNumero(objImportacaoNota.Nfer_numnota_nfer);

                if (Dsimportacao.Tables[0].Rows.Count == 0)
                {
                    clsImportacaoNota.CadastrarErroNotaFiscal(objImportacaoNota);
                }
                else
                {
                    clsImportacaoNota.UpdateErroNotaFiscaNumero(objImportacaoNota);
                }

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";

            }
        }

        [WebMethod]

        public void DeletarErroNotaFiscaNumero(string xmlString)
        {


            //aqui instâncio a classe de empresas
            Classes.clsImportacaoNota clsImportacaoNota = new Classes.clsImportacaoNota();


            //aqui o objeto da empresa
            Objetos.objImportacaoNota objImportacaoNota = new Objetos.objImportacaoNota();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList nfer_numnota_nfer = doc.GetElementsByTagName("nfer_numnota_nfer");

                clsImportacaoNota.DeletarErroNotaFiscaNumero(nfer_numnota_nfer[0].InnerText);


            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";

            }
        }

        [WebMethod]
        public string SelecionarMotivoOcorrenciaPorCodigo(string xmlString)
        {


            //aqui instâncio a classe de empresas
            Classes.clsMotivoOcorrencia clsMotivoOcorrencia = new Classes.clsMotivoOcorrencia();

            //aqui o objeto da empresa
            Objetos.objMotivoOcorrencia objMotivoOcorrencia = new Objetos.objMotivoOcorrencia();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList mtoc_cod_mtoc = doc.GetElementsByTagName("mtoc_cod_mtoc");


                DataSet dsMotivo = new DataSet();
                dsMotivo = clsMotivoOcorrencia.SelecionarMotivoOcorrenciaPorCodigo(Convert.ToInt32(mtoc_cod_mtoc[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<grupo>";
                if (dsMotivo.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<mtoc_cod_mtoc>" + dsMotivo.Tables[0].Rows[0]["mtoc_cod_mtoc"].ToString() + "</mtoc_cod_mtoc>";
                    xmlRetorno = xmlRetorno + "<mtoc_descricao>" + dsMotivo.Tables[0].Rows[0]["mtoc_descricao"].ToString() + "</mtoc_descricao>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</grupo>";

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string AlterarCadastrarOcorrencia(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.


            //aqui instâncio a classe de empresas
            Classes.clsMotivoOcorrencia clsMotivoOcorrencia = new Classes.clsMotivoOcorrencia();

            //aqui o objeto da empresa
            Objetos.objMotivoOcorrencia objMotivoOcorrencia = new Objetos.objMotivoOcorrencia();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);


                XmlNodeList mtoc_descricao = doc.GetElementsByTagName("mtoc_descricao");
                XmlNodeList mtoc_cod_mtoc = doc.GetElementsByTagName("mtoc_cod_mtoc");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");



                objMotivoOcorrencia.Mtoc_descricao = mtoc_descricao[0].InnerText.Trim();
                objMotivoOcorrencia.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());
                objMotivoOcorrencia.Mtoc_dt_criacao = DateTime.Now;

                int mtoc_cod_mtoc_ret = 0;

                if (mtoc_cod_mtoc[0].InnerText.Trim() == "")
                {

                    mtoc_cod_mtoc_ret = clsMotivoOcorrencia.CadastrarMotivoOcorrencia(objMotivoOcorrencia);

                }
                else
                {

                    objMotivoOcorrencia.Mtoc_cod_mtoc = Convert.ToInt32(mtoc_cod_mtoc[0].InnerText.Trim());

                    clsMotivoOcorrencia.alterarMotivoOcorrencia(objMotivoOcorrencia);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<motivoOcorrencia>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<mtoc_cod_mtoc>" + mtoc_cod_mtoc_ret + "</mtoc_cod_mtoc>";
                xmlRetorno = xmlRetorno + "</motivoOcorrencia>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string CadastrarOcorrencia(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.


            //aqui instâncio a classe de empresas
            Classes.clsRegistroNaoConformidade clsRegistroNaoConformidade = new Classes.clsRegistroNaoConformidade();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsFotoOcorrencia clsFotoOcorrencia = new Classes.clsFotoOcorrencia();

            //aqui o objeto da empresa
            Objetos.objRegistroNaoConformidade objRegistroNaoConformidade = new Objetos.objRegistroNaoConformidade();
            Objetos.objFotoOcorrencias objFotoOcorrencias = new Objetos.objFotoOcorrencias();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                XmlNodeList canf_numnf = doc.GetElementsByTagName("canf_numnf");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList causado_por = doc.GetElementsByTagName("causado_por");
                XmlNodeList arma_cod_arma = doc.GetElementsByTagName("arma_cod_arma");
                XmlNodeList famp_cd_famp = doc.GetElementsByTagName("famp_cd_famp");
                XmlNodeList mtoc_cod_mtoc = doc.GetElementsByTagName("mtoc_cod_mtoc");
                XmlNodeList quantidade = doc.GetElementsByTagName("quantidade");
                XmlNodeList unme_cod_unme = doc.GetElementsByTagName("unme_cod_unme");
                XmlNodeList comentario = doc.GetElementsByTagName("comentario");
                XmlNodeList foto_nome = doc.GetElementsByTagName("foto_nome");
                XmlNodeList foto_base_64 = doc.GetElementsByTagName("foto_base_64");




                objRegistroNaoConformidade.Emen_cod_empr = Convert.ToInt32(emen_cod_empr[0].InnerText.Trim());
                objRegistroNaoConformidade.Rnco_causador_rnco = causado_por[0].InnerText.Trim();
                objRegistroNaoConformidade.Famp_cod_famp = Convert.ToInt32(famp_cd_famp[0].InnerText.Trim());
                objRegistroNaoConformidade.Mtoc_cod_mtoc = Convert.ToInt32(mtoc_cod_mtoc[0].InnerText.Trim());
                objRegistroNaoConformidade.Arma_cod_arma = Convert.ToInt32(arma_cod_arma[0].InnerText);

                if (canf_numnf[0].InnerText != "")
                {
                    DataSet dsnota = clsNotaFiscal.SelecionarNotaFiscaNumero(canf_numnf[0].InnerText);

                    if (dsnota.Tables[0].Rows.Count > 0)
                    {
                        objRegistroNaoConformidade.Canf_cod_canf = Convert.ToInt32(dsnota.Tables[0].Rows[0]["canf_cod_canf"].ToString());
                    }
                    else
                    {
                        objRegistroNaoConformidade.Canf_cod_canf = Convert.ToInt32(canf_numnf[0].InnerText);
                    }
                }

                objRegistroNaoConformidade.Usur_cd_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                objRegistroNaoConformidade.Rnco_obs = comentario[0].InnerText;
                objRegistroNaoConformidade.Rnco_dt_criacao = DateTime.Now;
                if (quantidade[0].InnerText != "")
                {
                    objRegistroNaoConformidade.Rnco_quant_rnc = Convert.ToDouble(quantidade[0].InnerText);
                }
                else
                {
                    objRegistroNaoConformidade.Rnco_quant_rnc = 0;
                }
                if (unme_cod_unme[0].InnerText != "")
                {
                    objRegistroNaoConformidade.Unme_cod_unm = Convert.ToInt32(unme_cod_unme[0].InnerText);
                }
                else
                {
                    objRegistroNaoConformidade.Unme_cod_unm = 0;
                }



                int num_rnc = 0;
                num_rnc = clsRegistroNaoConformidade.cadastraRegistroNaoConformidade(objRegistroNaoConformidade);

                if (num_rnc > 0)
                {

                    if (foto_nome.Count > 0)
                    {

                        string caminhoImagemOrigem = System.Configuration.ConfigurationManager.AppSettings["caminhoImagemOrigem"];


                        for (int i = 0; i <= foto_nome.Count - 1; i++)
                        {
                            objFotoOcorrencias.Rnco_cod_rnco = num_rnc;
                            objFotoOcorrencias.Ftoc_nomeimagem_ftoc = foto_nome[i].InnerText;
                            clsFotoOcorrencia.cadastrarFotoOcorrencia(objFotoOcorrencias);

                            //aqui salvarei a imagem a partir da string base64

                            string x = foto_base_64[i].InnerText;
                            // Convert Base64 String to byte[]
                            byte[] imageBytes = Convert.FromBase64String(x);
                            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                            // Convert byte[] to Image
                            ms.Write(imageBytes, 0, imageBytes.Length);
                            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

                            image.Save(Path.Combine(caminhoImagemOrigem, foto_nome[i].InnerText), System.Drawing.Imaging.ImageFormat.Jpeg);
                            /*Size tamanho = new Size(image.Width * 5, image.Height * 5);
                            image = new Bitmap(image, tamanho);
                            Bitmap newBitmap = new Bitmap(image);
                            newBitmap.SetResolution(1000, 1000);
                            newBitmap.Save(Path.Combine(caminhoImagemOrigem, foto_nome[i].InnerText), System.Drawing.Imaging.ImageFormat.Jpeg);*/
                        }
                    }

                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<motivoOcorrencia>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<mtoc_cod_mtoc>" + num_rnc + "</mtoc_cod_mtoc>";
                    xmlRetorno = xmlRetorno + "</motivoOcorrencia>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Ocorreu uma falha na gravação da ocorrência.  Tente novamente e persistindo o erro entre em contato com o administrador</descerro></retornoErro>";
                }

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]

        public string SelecionarTodosRegistrosNaoConformidade()
        {


            //aqui instâncio a classe de empresas
            Classes.clsRegistroNaoConformidade clsRegistroNaoConformidade = new Classes.clsRegistroNaoConformidade();


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {


                DataSet dsRnco = clsRegistroNaoConformidade.SelecionarTodosRegistrosNaoConformidade();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<rnco>";

                if (dsRnco.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";

                    for (int i = 0; i <= dsRnco.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<registros>";
                        xmlRetorno = xmlRetorno + "<rnco_cod_rnco>" + dsRnco.Tables[0].Rows[i]["rnco_cod_rnco"].ToString().Trim() + "</rnco_cod_rnco>";
                        xmlRetorno = xmlRetorno + "<emen_nomered>" + dsRnco.Tables[0].Rows[i]["emen_nomered"].ToString().Trim() + "</emen_nomered>";
                        xmlRetorno = xmlRetorno + "<canf_numnt>" + dsRnco.Tables[0].Rows[i]["canf_numnt"].ToString().Trim() + "</canf_numnt>";
                        xmlRetorno = xmlRetorno + "<usur_nome>" + dsRnco.Tables[0].Rows[i]["usur_nome"].ToString().Trim() + "</usur_nome>";
                        xmlRetorno = xmlRetorno + "<mtoc_descricao>" + dsRnco.Tables[0].Rows[i]["mtoc_descricao"].ToString().Trim() + "</mtoc_descricao>";
                        xmlRetorno = xmlRetorno + "<rnco_dt_criacao>" + dsRnco.Tables[0].Rows[i]["rnco_dt_criacao"].ToString().Trim() + "</rnco_dt_criacao>";
                        xmlRetorno = xmlRetorno + "</registros>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }

                xmlRetorno = xmlRetorno + "</rnco>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarErroNotaFiscaTodos()
        {


            //aqui instâncio a classe de empresas
            Classes.clsImportacaoNota clsImportacaoNota = new Classes.clsImportacaoNota();


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                DataSet dsErroImp = clsImportacaoNota.SelecionarErroNotaFiscaTodos();

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<impErro>";

                if (dsErroImp.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";

                    for (int i = 0; i <= dsErroImp.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<nfimperros>";
                        xmlRetorno = xmlRetorno + "<nfer_cod_nfer>" + dsErroImp.Tables[0].Rows[i]["nfer_cod_nfer"].ToString().Trim() + "</nfer_cod_nfer>";
                        xmlRetorno = xmlRetorno + "<nfer_numnota_nfer>" + dsErroImp.Tables[0].Rows[i]["nfer_numnota_nfer"].ToString().Trim() + "</nfer_numnota_nfer>";
                        xmlRetorno = xmlRetorno + "<nfer_erro_nfer>" + dsErroImp.Tables[0].Rows[i]["nfer_erro_nfer"].ToString().Trim() + "</nfer_erro_nfer>";
                        xmlRetorno = xmlRetorno + "<nfer_dt_nfer>" + dsErroImp.Tables[0].Rows[i]["nfer_dt_nfer"].ToString().Trim() + "</nfer_dt_nfer>";
                        xmlRetorno = xmlRetorno + "</nfimperros>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }

                xmlRetorno = xmlRetorno + "</impErro>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método recebera o id da uma empresa e então criará um xml com todos os armazens vinculados aquela empresa
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarArmazenPorCodigo(string xmlString)
        {
            //aqui instâncio a classe de empresas
            Classes.clsArmazem clsArmazem = new Classes.clsArmazem();



            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);



                XmlNodeList arma_cod_arma = doc.GetElementsByTagName("arma_cod_arma");



                DataSet dsArmazens = new DataSet();
                dsArmazens = clsArmazem.SelecionarArmazensTodos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";


                if (dsArmazens.Tables[0].Rows.Count > 0)
                {


                    xmlRetorno = xmlRetorno + "<armazen>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<arma_cod_arma>" + dsArmazens.Tables[0].Rows[0]["arma_cod_arma"].ToString() + "</arma_cod_arma>";
                    xmlRetorno = xmlRetorno + "<arma_descricao>" + dsArmazens.Tables[0].Rows[0]["arma_descricao"].ToString() + "</arma_descricao>";
                    xmlRetorno = xmlRetorno + "<arma_ativo>" + dsArmazens.Tables[0].Rows[0]["arma_ativo"].ToString() + "</arma_ativo>";
                    xmlRetorno = xmlRetorno + "</armazen>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<armazen>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</armazen>";

                }

                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }


        }

        [WebMethod]
        public string SelecionarInventarioMEPorCodigo(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsInventario clsInventario = new Classes.clsInventario();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");


                DataSet ds = new DataSet();
                ds = clsInventario.SelecionarInventarioME(Convert.ToInt32(emen_cod_empr[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<inventarios>";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<inventario>";
                        xmlRetorno = xmlRetorno + "<inve_cod_inve>" + ds.Tables[0].Rows[i]["inve_cod_inve"].ToString() + "</inve_cod_inve>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + ds.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + ds.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<emen_cod_empr>" + ds.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
                        xmlRetorno = xmlRetorno + "<tpmo_descricao>" + ds.Tables[0].Rows[i]["tpmo_descricao"].ToString() + "</tpmo_descricao>";
                        xmlRetorno = xmlRetorno + "<inve_quantidade>" + ds.Tables[0].Rows[i]["inve_quantidade"].ToString() + "</inve_quantidade>";
                        xmlRetorno = xmlRetorno + "<inve_dt_criacao>" + ds.Tables[0].Rows[i]["inve_dt_criacao"].ToString() + "</inve_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + ds.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<inve_operacao>" + ds.Tables[0].Rows[i]["inve_operacao"].ToString() + "</inve_operacao>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_pallet>" + ds.Tables[0].Rows[i]["prma_cod_prma_pallet"].ToString() + "</prma_cod_prma_pallet>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_topo>" + ds.Tables[0].Rows[i]["prma_cod_prma_topo"].ToString() + "</prma_cod_prma_topo>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma_divisor>" + ds.Tables[0].Rows[i]["prma_cod_prma_divisor"].ToString() + "</prma_cod_prma_divisor>";
                        xmlRetorno = xmlRetorno + "</inventario>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</inventarios>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o codigo do tipo 
        /// o mesmo irá retornar um xml com os dados dos produtos pertencentes ao grupo
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosProdutosPorTipo(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList tpma_cod_tipo = doc.GetElementsByTagName("tpma_cod_tpma");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");



                DataSet dsProdutosMaterias = new DataSet();
                dsProdutosMaterias = clsProdutoMaterial.SelecionarTodosProdutosTipo(tpma_cod_tipo[0].InnerText.ToString(), emen_cod_empr[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutosMaterias.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutosMaterias.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<produtomaterial>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutosMaterias.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutosMaterias.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }


        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do inventario para cadastro
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarInventarioME(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsInventario clsInve = new Classes.clsInventario();

            //aqui o objeto da empresa
            Objetos.objInventario objInve = new Objetos.objInventario();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");
                XmlNodeList inve_quantidade = doc.GetElementsByTagName("inve_quantidade");
                XmlNodeList tpmo_cod_tpmo = doc.GetElementsByTagName("tpmo_cod_tpmo");
                XmlNodeList inve_operacao = doc.GetElementsByTagName("inve_operacao");
                XmlNodeList inve_dt_criacao = doc.GetElementsByTagName("inve_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList emen_cod_empr_forn = doc.GetElementsByTagName("emen_cod_empr_forn");

                objInve.Emen_cod_empr = Convert.ToInt32(emen_cod_empr[0].InnerText.ToString().Trim());
                objInve.Inve_dt_criacao = Convert.ToDateTime(inve_dt_criacao[0].InnerText.ToString().Trim());
                objInve.Tpmo_cod_tpmo = Convert.ToInt32(tpmo_cod_tpmo[0].InnerText.ToString().Trim());
                objInve.Inve_operacao = inve_operacao[0].InnerText.ToString().Trim();
                objInve.Inve_quantidade = Convert.ToDouble(inve_quantidade[0].InnerText.ToString().Trim());
                objInve.Prma_cod_prma = Convert.ToInt32(prma_cod_prma[0].InnerText.ToString().Trim());
                objInve.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString().Trim());
                objInve.Emen_cod_empr_forn = Convert.ToInt32(emen_cod_empr_forn[0].InnerText.ToString().Trim());


                string erro = clsInve.CadastrarInventarioME(objInve);

                if (erro == "")
                {

                    xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                    xmlRetorno = xmlRetorno + "<Inventario>";
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "</Inventario>";
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + erro + "</descerro></retornoErro>";
                    return xmlRetorno;
                }
                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarTransportadoraTodas()
        {



            //aqui instâncio a classe de empresas
            Classes.clsTransportadora clsTransportadora = new Classes.clsTransportadora();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {




                DataSet ds = new DataSet();
                ds = clsTransportadora.SelecionarTransportadoraTodas();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<transportadoras>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<transportadora>";
                        xmlRetorno = xmlRetorno + "<tran_cod_tran>" + ds.Tables[0].Rows[i]["tran_cod_tran"].ToString() + "</tran_cod_tran>";
                        xmlRetorno = xmlRetorno + "<tran_cnpj>" + ds.Tables[0].Rows[i]["tran_cnpj"].ToString() + "</tran_cnpj>";
                        xmlRetorno = xmlRetorno + "<tran_nome>" + ds.Tables[0].Rows[i]["tran_nome"].ToString() + "</tran_nome>";
                        xmlRetorno = xmlRetorno + "<tran_nomered>" + ds.Tables[0].Rows[i]["tran_nomered"].ToString() + "</tran_nomered>";
                        xmlRetorno = xmlRetorno + "<tran_lograd>" + ds.Tables[0].Rows[i]["tran_lograd"].ToString() + "</tran_lograd>";
                        xmlRetorno = xmlRetorno + "<tran_num>" + ds.Tables[0].Rows[i]["tran_num"].ToString() + "</tran_num>";
                        xmlRetorno = xmlRetorno + "<tran_uf>" + ds.Tables[0].Rows[i]["tran_uf"].ToString() + "</tran_uf>";
                        xmlRetorno = xmlRetorno + "<tran_compl>" + ds.Tables[0].Rows[i]["tran_compl"].ToString() + "</tran_compl>";
                        xmlRetorno = xmlRetorno + "<tran_bairro>" + ds.Tables[0].Rows[i]["tran_bairro"].ToString() + "</tran_bairro>";
                        xmlRetorno = xmlRetorno + "<tran_cep>" + ds.Tables[0].Rows[i]["tran_cep"].ToString() + "</tran_cep>";
                        xmlRetorno = xmlRetorno + "<tran_ddd>" + ds.Tables[0].Rows[i]["tran_ddd"].ToString() + "</tran_ddd>";
                        xmlRetorno = xmlRetorno + "<tran_tel>" + ds.Tables[0].Rows[i]["tran_tel"].ToString() + "</tran_tel>";
                        xmlRetorno = xmlRetorno + "<tran_email>" + ds.Tables[0].Rows[i]["tran_email"].ToString() + "</tran_email>";
                        xmlRetorno = xmlRetorno + "<tran_ativo>" + ds.Tables[0].Rows[i]["tran_ativo"].ToString() + "</tran_ativo>";
                        xmlRetorno = xmlRetorno + "</transportadora>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</transportadoras>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarTransportadoraPorCodigo(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsTransportadora clsTransportadora = new Classes.clsTransportadora();

            //aqui o objeto da empresa
            Objetos.objTransportadora objTransportadora = new Objetos.objTransportadora();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);


                XmlNodeList tran_cod_tran = doc.GetElementsByTagName("tran_cod_tran");


                DataSet dsEmpresas = new DataSet();
                dsEmpresas = clsTransportadora.SelecionarTransportadoraTodasPorCodigo(Convert.ToInt32(tran_cod_tran[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<transportadora>";

                int i = 0;
                if (dsEmpresas.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<tran_cod_tran>" + dsEmpresas.Tables[0].Rows[i]["tran_cod_tran"].ToString() + "</tran_cod_tran>";
                    xmlRetorno = xmlRetorno + "<tran_cnpj>" + dsEmpresas.Tables[0].Rows[i]["tran_cnpj"].ToString() + "</tran_cnpj>";
                    xmlRetorno = xmlRetorno + "<tran_nome>" + dsEmpresas.Tables[0].Rows[i]["tran_nome"].ToString() + "</tran_nome>";
                    xmlRetorno = xmlRetorno + "<tran_nomered>" + dsEmpresas.Tables[0].Rows[i]["tran_nomered"].ToString() + "</tran_nomered>";
                    xmlRetorno = xmlRetorno + "<tran_lograd>" + dsEmpresas.Tables[0].Rows[i]["tran_lograd"].ToString() + "</tran_lograd>";
                    xmlRetorno = xmlRetorno + "<tran_numero>" + dsEmpresas.Tables[0].Rows[i]["tran_num"].ToString() + "</tran_numero>";
                    xmlRetorno = xmlRetorno + "<tran_comple>" + dsEmpresas.Tables[0].Rows[i]["tran_compl"].ToString() + "</tran_comple>";
                    xmlRetorno = xmlRetorno + "<tran_bairro>" + dsEmpresas.Tables[0].Rows[i]["tran_bairro"].ToString() + "</tran_bairro>";
                    xmlRetorno = xmlRetorno + "<tran_cep>" + dsEmpresas.Tables[0].Rows[i]["tran_cep"].ToString() + "</tran_cep>";
                    xmlRetorno = xmlRetorno + "<tran_uf>" + dsEmpresas.Tables[0].Rows[i]["tran_uf"].ToString() + "</tran_uf>";
                    xmlRetorno = xmlRetorno + "<tran_ddd>" + dsEmpresas.Tables[0].Rows[i]["tran_ddd"].ToString() + "</tran_ddd>";
                    xmlRetorno = xmlRetorno + "<tran_tel>" + dsEmpresas.Tables[0].Rows[i]["tran_tel"].ToString() + "</tran_tel>";
                    xmlRetorno = xmlRetorno + "<tran_email>" + dsEmpresas.Tables[0].Rows[i]["tran_email"].ToString() + "</tran_email>";
                    xmlRetorno = xmlRetorno + "<tran_dt_criacao>" + Convert.ToDateTime(dsEmpresas.Tables[0].Rows[i]["tran_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</tran_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEmpresas.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                    xmlRetorno = xmlRetorno + "<tran_ativo>" + dsEmpresas.Tables[0].Rows[i]["tran_ativo"].ToString() + "</tran_ativo>";



                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</transportadora>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string CadastrarAlterarTransportadora(string xmlString)
        {

            //aqui instncio a classe de empresas
            Classes.clsTransportadora clsTransportadora = new Classes.clsTransportadora();

            //aqui o objeto da empresa
            Objetos.objTransportadora objTransportadora = new Objetos.objTransportadora();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);




                XmlNodeList tran_cod_tran = doc.GetElementsByTagName("tran_cod_tran");
                XmlNodeList tran_cnpj = doc.GetElementsByTagName("tran_cnpj");
                XmlNodeList tran_nome = doc.GetElementsByTagName("tran_nome");
                XmlNodeList tran_nomered = doc.GetElementsByTagName("tran_nomered");
                XmlNodeList tran_lograd = doc.GetElementsByTagName("tran_lograd");
                XmlNodeList tran_numero = doc.GetElementsByTagName("tran_numero");
                XmlNodeList tran_comple = doc.GetElementsByTagName("tran_comple");
                XmlNodeList tran_bairro = doc.GetElementsByTagName("tran_bairro");
                XmlNodeList tran_cep = doc.GetElementsByTagName("tran_cep");
                XmlNodeList tran_uf = doc.GetElementsByTagName("tran_uf");
                XmlNodeList tran_ddd = doc.GetElementsByTagName("tran_ddd");
                XmlNodeList tran_tel = doc.GetElementsByTagName("tran_tel");
                XmlNodeList tran_email = doc.GetElementsByTagName("tran_email");
                XmlNodeList tran_dt_criacao = doc.GetElementsByTagName("tran_dt_criacao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList tran_ativo = doc.GetElementsByTagName("tran_ativo");



                objTransportadora.Tran_cnpj = tran_cnpj[0].InnerText.Trim();
                objTransportadora.Tran_nome = tran_nome[0].InnerText.Trim();
                objTransportadora.Tran_nomered = tran_nomered[0].InnerText.Trim();
                objTransportadora.Tran_lograd = tran_lograd[0].InnerText.Trim();
                objTransportadora.Tran_numero = tran_numero[0].InnerText.Trim();
                objTransportadora.Tran_comple = tran_comple[0].InnerText.Trim();
                objTransportadora.Tran_bairro = tran_bairro[0].InnerText.Trim();
                objTransportadora.Tran_cep = tran_cep[0].InnerText.Trim();
                objTransportadora.Tran_uf = tran_uf[0].InnerText.Trim();
                objTransportadora.Tran_ddd = tran_ddd[0].InnerText.Trim();
                objTransportadora.Tran_tel = tran_tel[0].InnerText.Trim();
                objTransportadora.Tran_email = tran_email[0].InnerText.Trim();
                objTransportadora.Tran_dt_criacao = Convert.ToDateTime(tran_dt_criacao[0].InnerText.Trim());
                objTransportadora.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.Trim());
                objTransportadora.Tran_ativo = Convert.ToBoolean(tran_ativo[0].InnerText.Trim());

                int tran_cod_tran_retorno = 0;

                if (tran_cod_tran[0].InnerText.Trim() == "")
                {

                    tran_cod_tran_retorno = clsTransportadora.cadastraTransportadora(objTransportadora);

                }
                else
                {
                    objTransportadora.Tran_cod_tran = Convert.ToInt32(tran_cod_tran[0].InnerText.Trim());
                    tran_cod_tran_retorno = objTransportadora.Tran_cod_tran;
                    clsTransportadora.alterarTransportadora(objTransportadora);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<transportadora>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<tran_cod_tran>" + tran_cod_tran_retorno + "</tran_cod_tran>";
                xmlRetorno = xmlRetorno + "</transportadora>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarTodosRegistrosNaoConformidadePorCodigo(string xmlString)
        {
            //aqui instâncio a classe de empresas
            Classes.clsRegistroNaoConformidade clsRegistroNaoConformidade = new Classes.clsRegistroNaoConformidade();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";
            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);
                XmlNodeList rnco_cod_rnco = doc.GetElementsByTagName("rnco_cod_rnco");
                DataSet dsRnco = clsRegistroNaoConformidade.SelecionarTodosRegistrosNaoConformidadePorCodigo(rnco_cod_rnco[0].InnerText.ToString());
                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<rnco>";

                if (dsRnco.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<registros>";
                    xmlRetorno = xmlRetorno + "<emen_nomered>" + dsRnco.Tables[0].Rows[0]["emen_nomered"].ToString().Trim() + "</emen_nomered>";
                    xmlRetorno = xmlRetorno + "<canf_numnt>" + dsRnco.Tables[0].Rows[0]["canf_numnt"].ToString().Trim() + "</canf_numnt>";
                    xmlRetorno = xmlRetorno + "<rnco_obs>" + dsRnco.Tables[0].Rows[0]["rnco_obs"].ToString().Trim() + "</rnco_obs>";
                    xmlRetorno = xmlRetorno + "<usur_nome>" + dsRnco.Tables[0].Rows[0]["usur_nome"].ToString().Trim() + "</usur_nome>";
                    xmlRetorno = xmlRetorno + "<mtoc_descricao>" + dsRnco.Tables[0].Rows[0]["mtoc_descricao"].ToString().Trim() + "</mtoc_descricao>";
                    xmlRetorno = xmlRetorno + "<rnco_causador_rnco>" + dsRnco.Tables[0].Rows[0]["rnco_causador_rnco"].ToString().Trim() + "</rnco_causador_rnco>";
                    xmlRetorno = xmlRetorno + "<arma_descricao>" + dsRnco.Tables[0].Rows[0]["arma_descricao"].ToString().Trim() + "</arma_descricao>";
                    xmlRetorno = xmlRetorno + "<famp_descricao>" + dsRnco.Tables[0].Rows[0]["famp_descricao"].ToString().Trim() + "</famp_descricao>";
                    xmlRetorno = xmlRetorno + "<rnco_quant_rnco>" + dsRnco.Tables[0].Rows[0]["rnco_quant_rnco"].ToString().Trim() + "</rnco_quant_rnco>";
                    xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsRnco.Tables[0].Rows[0]["unme_id_reg"].ToString().Trim() + "</unme_id_reg>";
                    xmlRetorno = xmlRetorno + "<rnco_dt_criacao>" + dsRnco.Tables[0].Rows[0]["rnco_dt_criacao"].ToString().Trim() + "</rnco_dt_criacao>";
                    xmlRetorno = xmlRetorno + "</registros>";

                    string caminho_origem = System.Configuration.ConfigurationSettings.AppSettings["caminhoImagemOrigem"];
                    string caminho_destino = System.Configuration.ConfigurationSettings.AppSettings["caminhoImagemDestino"];

                    for (int i = 0; i <= dsRnco.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<fotos>";
                        xmlRetorno = xmlRetorno + "<ftoc_nomeimagem_ftoc>" + dsRnco.Tables[0].Rows[i]["ftoc_nomeimagem_ftoc"].ToString().Trim() + "</ftoc_nomeimagem_ftoc>"; ;
                        xmlRetorno = xmlRetorno + "</fotos>";
                        if (File.Exists(caminho_origem + "\\" + dsRnco.Tables[0].Rows[i]["ftoc_nomeimagem_ftoc"].ToString().Trim()))
                        {
                            System.IO.File.Delete(caminho_destino + "\\" + dsRnco.Tables[0].Rows[i]["ftoc_nomeimagem_ftoc"].ToString().Trim());
                            System.IO.File.Copy(caminho_origem + "\\" + dsRnco.Tables[0].Rows[i]["ftoc_nomeimagem_ftoc"].ToString().Trim(), caminho_destino + "\\" + dsRnco.Tables[0].Rows[i]["ftoc_nomeimagem_ftoc"].ToString().Trim());
                        }
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</rnco>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todas as empresas cadastradas na base        
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarCarregamentoTodos()
        {
            //aqui instâncio a classe de empresas
            Classes.clsCarregamento clsCarregamento = new Classes.clsCarregamento();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsCarregamento = new DataSet();
                dsCarregamento = clsCarregamento.SelecionarCarregamentoTodos();
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<carregamentos>";

                if (dsCarregamento.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsCarregamento.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<carregamento>";
                        xmlRetorno = xmlRetorno + "<canf_numnt>" + dsCarregamento.Tables[0].Rows[i]["canf_numnt"].ToString() + "</canf_numnt>";
                        xmlRetorno = xmlRetorno + "<canf_cod_canf>" + dsCarregamento.Tables[0].Rows[i]["canf_cod_canf"].ToString() + "</canf_cod_canf>";
                        xmlRetorno = xmlRetorno + "<tran_nomered>" + dsCarregamento.Tables[0].Rows[i]["tran_nomered"].ToString() + "</tran_nomered>";
                        xmlRetorno = xmlRetorno + "<emen_nome_orig>" + dsCarregamento.Tables[0].Rows[i]["emen_nome_orig"].ToString() + "</emen_nome_orig>";
                        xmlRetorno = xmlRetorno + "<emen_nome_dest>" + dsCarregamento.Tables[0].Rows[i]["emen_nome_dest"].ToString() + "</emen_nome_dest>";
                        xmlRetorno = xmlRetorno + "<veic_placa>" + dsCarregamento.Tables[0].Rows[i]["veic_placa"].ToString() + "</veic_placa>";

                        xmlRetorno = xmlRetorno + "<moto_nome>" + dsCarregamento.Tables[0].Rows[i]["moto_nome"].ToString() + "</moto_nome>";
                        xmlRetorno = xmlRetorno + "<moes_dt_criacao>" + dsCarregamento.Tables[0].Rows[i]["moes_dt_criacao"].ToString() + "</moes_dt_criacao>";
                        xmlRetorno = xmlRetorno + "</carregamento>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</carregamentos>";


                return xmlRetorno;
            }
            catch (Exception e)
            {

                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarCarregamentoPorcodifoNF(string xmlString)
        {


            //aqui instâncio a classe de empresas
            Classes.clsCarregamento clsCarregamento = new Classes.clsCarregamento();


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XmlNodeList canf_cod_canf = doc.GetElementsByTagName("canf_cod_canf");

                DataSet dsRnco = clsCarregamento.SelecionarCarregamentoPorcodifoNF(canf_cod_canf[0].InnerText.ToString());

                //aqui começarei a montar o objeto do perfil (xml) para retorno

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<carregamento>";

                if (dsRnco.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<registros>";
                    xmlRetorno = xmlRetorno + "<canf_numnt>" + dsRnco.Tables[0].Rows[0]["canf_numnt"].ToString().Trim() + "</canf_numnt>";
                    xmlRetorno = xmlRetorno + "<tran_nomered>" + dsRnco.Tables[0].Rows[0]["tran_nomered"].ToString().Trim() + "</tran_nomered>";
                    xmlRetorno = xmlRetorno + "<moto_nome>" + dsRnco.Tables[0].Rows[0]["moto_nome"].ToString().Trim() + "</moto_nome>";
                    xmlRetorno = xmlRetorno + "<moes_dt_criacao>" + dsRnco.Tables[0].Rows[0]["moes_dt_criacao"].ToString().Trim() + "</moes_dt_criacao>";
                    xmlRetorno = xmlRetorno + "<emen_nome_orig>" + dsRnco.Tables[0].Rows[0]["emen_nome_orig"].ToString().Trim() + "</emen_nome_orig>";
                    xmlRetorno = xmlRetorno + "<emen_nome_dest>" + dsRnco.Tables[0].Rows[0]["emen_nome_dest"].ToString().Trim() + "</emen_nome_dest>";
                    xmlRetorno = xmlRetorno + "<veic_placa>" + dsRnco.Tables[0].Rows[0]["veic_placa"].ToString().Trim() + "</veic_placa>";
                    xmlRetorno = xmlRetorno + "</registros>";
                    string caminho_origem = System.Configuration.ConfigurationSettings.AppSettings["caminhoImagemOrigem"];
                    string caminho_destino = System.Configuration.ConfigurationSettings.AppSettings["caminhoImagemDestino"];
                    for (int i = 0; i <= dsRnco.Tables[0].Rows.Count - 1; i++)
                    {
                        if (dsRnco.Tables[0].Rows[i]["arim_imagem"].ToString() != "")
                        {
                            if (System.IO.File.Exists(caminho_origem + "\\" + dsRnco.Tables[0].Rows[i]["arim_imagem"].ToString().Trim()))
                            {
                                xmlRetorno = xmlRetorno + "<fotos>";
                                xmlRetorno = xmlRetorno + "<arim_imagem>" + dsRnco.Tables[0].Rows[i]["arim_imagem"].ToString().Trim() + "</arim_imagem>"; ;
                                xmlRetorno = xmlRetorno + "</fotos>";
                                System.IO.File.Delete(caminho_destino + "\\" + dsRnco.Tables[0].Rows[i]["arim_imagem"].ToString().Trim());
                                System.IO.File.Copy(caminho_origem + "\\" + dsRnco.Tables[0].Rows[i]["arim_imagem"].ToString().Trim(), caminho_destino + "\\" + dsRnco.Tables[0].Rows[i]["arim_imagem"].ToString().Trim());
                            }
                        }
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }

                xmlRetorno = xmlRetorno + "</carregamento>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string AlterarSenha(string xmlString)
        {

            //aqui instâncio a classe de empresas
            Classes.clsUsuario clsUsur = new Classes.clsUsuario();



            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");
                XmlNodeList usur_senha = doc.GetElementsByTagName("usur_senha");

                clsUsur.alterarSenha(Convert.ToInt32(usur_cd_usur[0].InnerText), usur_senha[0].InnerText);


                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<usuario>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "</usuario>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><ErroRetorno><status>Erro</status><descerro>" + e.Message + "</descerro></ErroRetorno>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        public string SelecionarUsuarioResponsavelCarregamento(string xmlString)
        {

            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.

            //aqui instâncio a classe de empresas
            Classes.clsNotaFiscal clsnota = new Classes.clsNotaFiscal();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                XmlNodeList emen_cod_empr_origem = doc.GetElementsByTagName("emen_cod_empr_origem");

                DataSet ds = new DataSet();
                ds = clsnota.SelecionarUsuarioResponsavelCarregamento(Convert.ToInt32(emen_cod_empr_origem[0].InnerText), canf_numnt[0].InnerText.Trim());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<usuario>";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<usur_nome>" + ds.Tables[0].Rows[0]["usur_nome"].ToString().Trim() + "</usur_nome>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<usur_nome>Auto Carregamento</usur_nome>";
                }
                xmlRetorno = xmlRetorno + "</usuario>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados da diferença na conferencia para cadastro
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarConferenciaInhouse(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();


			//aqui instâncio a classe de empresas
			clsMotorista clsMoto = new clsMotorista();
			clsVeiculos clsVeiculos = new clsVeiculos();
			Classes.cls_empresa ClsEmpresa = new Classes.cls_empresa();
            Classes.clsRecebimento clsRecebimento = new Classes.clsRecebimento();
            Classes.clsQrcod clsQrcod = new Classes.clsQrcod();
            Classes.clsMovimentoEstoque clsMovimentoEstoque = new Classes.clsMovimentoEstoque();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();
            Classes.clsArim clsArim = new Classes.clsArim();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa

            Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            Objetos.objMovimentoEstoque objMovimentoEstoqueFolhaKilo = new Objetos.objMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objArim objArim = new Objetos.objArim();
            Objetos.objMotorista objMotorista = new Objetos.objMotorista();
			Objetos.objMotorista objMotorista1 = new Objetos.objMotorista();
			Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();

            //criando a lista para poder criar um array dos objetos

            List<Objetos.ObjDiferencaConferencia> lstObjDiferencaConferencia = new List<Objetos.ObjDiferencaConferencia>();
            List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";
            string gravou = "";
            int qtd_folhas_kit = -1;

			bool achou_folha = false;
			int empr_cod_empr_origem = 0;
			int empr_cd_empr_destino = 0;


			try
			{
				int grem_cd_grem_dest = 0;
				int grem_cd_grem_orig = 0;

				//aqui crio un xelemente a a partir do document criado
				XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

				string caminho = Convert.ToString(HttpRuntime.AppDomainAppPath);
				//doc.Save(caminho + "/confereME_2602.xml");


				string cod_prma_topo = "";
                string cod_prma_pallet = "";
                string cod_prma_folha = "";

                string qtd_folha_boa = "";
                string qtd_folha_ruim = "";

                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                XmlNodeList kit_bom_qtd = doc.GetElementsByTagName("kit_bom_qtd");
                XmlNodeList kit_ruin_qtd = doc.GetElementsByTagName("kit_ruin_qtd");
                XmlNodeList pallets_qtd = doc.GetElementsByTagName("pallets_qtd");
                XmlNodeList topos_qtd = doc.GetElementsByTagName("topos_qtd");
                XmlNodeList manterfoto = doc.GetElementsByTagName("manterfoto");
                XmlNodeList folhaskilo = doc.GetElementsByTagName("folhaskilo");
				XmlNodeList emen_empr_origem = doc.GetElementsByTagName("emen_empr_origem");
				


				string canf_cod_canf = "";

                //aqui vou selecionar a nota fiscal para pegar as empresas e os itens.
                DataSet ds = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestino(canf_numnt[0].InnerText, Convert.ToInt32(usur_cod_usur[0].InnerText), emen_empr_origem[0].InnerText);

                empr_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_orig"].ToString());
                empr_cd_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_dest"].ToString());


                

				
				DataSet dsQuantidadeKit = ClsEmpresa.SelecionarFolhasKitEmpresa(empr_cod_empr_origem);

                if (Convert.ToDouble(dsQuantidadeKit.Tables[0].Rows[0]["emen_nr_folhas_kit"].ToString()) > 0)
                {
                    qtd_folhas_kit = Convert.ToInt32(dsQuantidadeKit.Tables[0].Rows[0]["emen_nr_folhas_kit"].ToString());
                }
                else
                {
                    qtd_folhas_kit = 794;
                }


                Classes.clsNotaFiscal clsnota = new Classes.clsNotaFiscal();
                DataSet dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacao(canf_numnt[0].InnerText, empr_cod_empr_origem, 2);

				if (dsnota.Tables[0].Rows.Count <= 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não foi realizado o recebimento para a nota fiscal informada</descerro></retornoErro>";
                    return xmlRetorno;
                }
				dsnota = new DataSet();
				dsnota = clsnota.SelecionarNotaFiscaempresaComMovimentacao(canf_numnt[0].InnerText, empr_cod_empr_origem, 3);

                if (dsnota.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal já recebida</descerro></retornoErro>";
                    return xmlRetorno;
                }

				//Aqui verificarei se os itens vindos no xml de conferencia estão todos na nota recebida
				//Tipo no recebimento da nota veio sómente pallet, então nos itens da nota temos apenas pallets
				//porém no xml de conferencia foi colocado que vieram topos também.

				//Não existindo o itnf para nenhum produto da família top, pegarei o código do produto e adicionarei
				//na tabela tb_itnf um item com o código e a quantidade zero

				if (ds.Tables[0].Rows.Count > 0)
				{
					//pegando o código da nf para fazer as devidas verificações
					canf_cod_canf = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();

					//aqui irei verificar se temos um pallet na nota fiscal caso a quantidade para o pallet esteja
					//informada
					if (Convert.ToInt32(pallets_qtd[0].InnerText.ToString()) > 0)
					{
						//verifica se temos pallets na nota

						DataSet dsPallet = clsnota.SelecionarProdutoFamiliaNF(canf_numnt[0].InnerText, empr_cod_empr_origem.ToString(), empr_cd_empr_destino.ToString(), "1");

						if (dsPallet.Tables[0].Rows.Count == 0)
						{
							//aqui vou inserir o item na nf

							clsProdutoMaterial clsProduto = new clsProdutoMaterial();
							DataSet Dsprod = clsProduto.SelecionarTodosProdutosFamilia("1", empr_cod_empr_origem.ToString());


							Objetos.objItensNotaFiscal objItensNotaFiscal = new Objetos.objItensNotaFiscal();

							objItensNotaFiscal.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
							objItensNotaFiscal.Itnf_dt_criacao = DateTime.Now;
							objItensNotaFiscal.Itnf_quantidade = 0;
							objItensNotaFiscal.Prma_cod_prma = Convert.ToInt32(Dsprod.Tables[0].Rows[0]["prma_cod_prma"].ToString());
							objItensNotaFiscal.Usur_cd_usur = 5;


							clsnota.cadastrarItemNF(objItensNotaFiscal.Canf_cod_canf, objItensNotaFiscal);

						}

					}

					if (Convert.ToInt32(topos_qtd[0].InnerText) > 0)
					{
						DataSet dsTopo = clsnota.SelecionarProdutoFamiliaNF(canf_numnt[0].InnerText, empr_cod_empr_origem.ToString(), empr_cd_empr_destino.ToString(), "2");

						if (dsTopo.Tables[0].Rows.Count == 0)
						{
							//aqui irei gravar o item na tabela itnf para a nf informada
							//desta forma ele passará a existir

							//aqui vou buscar o código de um produto topo da empresa origem para cadastrar o mesmo na nf com quantidade zero
							clsProdutoMaterial clsProduto = new clsProdutoMaterial();
							DataSet Dsprod = clsProduto.SelecionarTodosProdutosFamilia("2", empr_cod_empr_origem.ToString());

							if (Dsprod.Tables[0].Rows.Count > 0)
							{
								//aqui vou inserir o item na nf

								Objetos.objItensNotaFiscal objItensNotaFiscal = new Objetos.objItensNotaFiscal();

								objItensNotaFiscal.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
								objItensNotaFiscal.Itnf_dt_criacao = DateTime.Now;
								objItensNotaFiscal.Itnf_quantidade = 0;
								objItensNotaFiscal.Prma_cod_prma = Convert.ToInt32(Dsprod.Tables[0].Rows[0]["prma_cod_prma"].ToString());
								objItensNotaFiscal.Usur_cd_usur = 5;


								clsnota.cadastrarItemNF(objItensNotaFiscal.Canf_cod_canf, objItensNotaFiscal); 

							}

						}
					}

					if (Convert.ToInt32(folhaskilo[0].InnerText) > 0)
					{
						DataSet dsFolha = clsnota.SelecionarProdutoFamiliaNF(canf_numnt[0].InnerText, empr_cod_empr_origem.ToString(), empr_cd_empr_destino.ToString(), "3");

						if (dsFolha.Tables[0].Rows.Count == 0)
						{
							//aqui irei gravar o item na tabela itnf para a nf informada
							//desta forma ele passará a existir

							//aqui vou buscar o código de um produto topo da empresa origem para cadastrar o mesmo na nf com quantidade zero
							clsProdutoMaterial clsProduto = new clsProdutoMaterial();
							DataSet Dsprod = clsProduto.SelecionarTodosProdutosFamilia("3", empr_cod_empr_origem.ToString());

							if (Dsprod.Tables[0].Rows.Count > 0)
							{
								//aqui vou inserir o item na nf

								Objetos.objItensNotaFiscal objItensNotaFiscal = new Objetos.objItensNotaFiscal();

								objItensNotaFiscal.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
								objItensNotaFiscal.Itnf_dt_criacao = DateTime.Now;
								objItensNotaFiscal.Itnf_quantidade = 0;
								objItensNotaFiscal.Prma_cod_prma = Convert.ToInt32(Dsprod.Tables[0].Rows[0]["prma_cod_prma"].ToString());
								objItensNotaFiscal.Usur_cd_usur = 5;


								clsnota.cadastrarItemNF(objItensNotaFiscal.Canf_cod_canf, objItensNotaFiscal);

							}

						}
					}


					//
				}


				ds = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestino(canf_numnt[0].InnerText, Convert.ToInt32(usur_cod_usur[0].InnerText), emen_empr_origem[0].InnerText);
				if (ds.Tables[0].Rows.Count > 0)
                {
                    canf_cod_canf = ds.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                    //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit

                    empr_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cod_empr_origem"].ToString());
                    empr_cd_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());
                    grem_cd_grem_orig = Convert.ToInt32(ds.Tables[0].Rows[0]["grem_cd_grem_orig"].ToString());
                    grem_cd_grem_dest = Convert.ToInt32(ds.Tables[0].Rows[0]["grem_cd_grem_dest"].ToString());

					//Aqu vou varificar o mapeamento de cada produto existente no xml e ver se o mesmo já existe na nf, caso não exista irei
					//inserir com a quantidade zero.




                    //aqui vou pegar os códigos e realizar algumas totalizações
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "3" || ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "5")
                        {
                            cod_prma_folha = ds.Tables[0].Rows[i]["prma_cod_prma"].ToString();
                            qtd_folha_boa = Convert.ToString(Convert.ToInt32(kit_bom_qtd[0].InnerText) * qtd_folhas_kit);
                            qtd_folha_ruim = Convert.ToString(Convert.ToInt32(kit_ruin_qtd[0].InnerText) * qtd_folhas_kit);
							

						}
                        else if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "1") //pallet
                        {
                            cod_prma_pallet = ds.Tables[0].Rows[i]["prma_cod_prma"].ToString();

                        }
                        else if (ds.Tables[0].Rows[i]["famp_cd_famp"].ToString() == "2") //topo
                        {
                            cod_prma_topo = ds.Tables[0].Rows[i]["prma_cod_prma"].ToString();

                        }
                    }

                    //aqui vou montar o objeto de atualização do estoque
                    //aqui irei montar a quantidade a ser colocada em cada produto dependendo da família do mesmo:
                    //1 pallet, 2 top, 3 folhas
                    DataSet DsMapeamentoProdutos = new DataSet();
					//Aqui farei o mapemanto do produto para poder ter o código dele como a fornecedora
					if (cod_prma_folha != "")
					{
						DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoGrupos(Convert.ToInt32(cod_prma_folha), grem_cd_grem_dest, grem_cd_grem_orig );
						
						objMovimentoEstoqueFolhaKilo.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
						objMovimentoEstoqueFolhaKilo.Moes_dt_criacao = DateTime.Now;
						objMovimentoEstoqueFolhaKilo.Moes_quantidade = Convert.ToDouble(folhaskilo[0].InnerText.Replace(".", ","));
						objMovimentoEstoqueFolhaKilo.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
						objMovimentoEstoqueFolhaKilo.Tpmo_cod_tpmo = 14;
						objMovimentoEstoqueFolhaKilo.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
						lstobjMovimentoEstoque.Add(objMovimentoEstoqueFolhaKilo);

						//aqui vou cadastrar as folhas boas
						objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
						objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
						objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(qtd_folha_boa);
						objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
						objMovimentoEstoque.Tpmo_cod_tpmo = 3;
						objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
						objMovimentoEstoque.Moes_folha_boa = true;
						lstobjMovimentoEstoque.Add(objMovimentoEstoque);

						//aqui vou acertar o saldo de estoque das folhas boas
						objSaldoEstoque.Tpop_cod_tpop = 3;
						objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
						objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
						objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
						objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;
						lstobjSaldoEstoque.Add(objSaldoEstoque);

						objMovimentoEstoque = new Objetos.objMovimentoEstoque();
						objSaldoEstoque = new Objetos.objSaldoEstoque();

						//aqui vou acertar a movimentação de estoque de folhas ruins
						objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
						objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
						objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(qtd_folha_ruim);
						objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
						objMovimentoEstoque.Tpmo_cod_tpmo = 3;
						objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
						objMovimentoEstoque.Moes_folha_boa = false;
						lstobjMovimentoEstoque.Add(objMovimentoEstoque);

						//aqui vou acertar o saldo de estoque das folhas ruim
						objSaldoEstoque.Tpop_cod_tpop = 3;
						objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
						objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
						objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
						objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;
						lstobjSaldoEstoque.Add(objSaldoEstoque);

						objMovimentoEstoque = new Objetos.objMovimentoEstoque();
						objSaldoEstoque = new Objetos.objSaldoEstoque();
					}
					else
					{

						//aqui vou pegar o primeiro produto com a família 3 que seria folha para a empresa de destino e então
						//realizar a inserção do mesmo na tabela itnf

						wsngtt.Objetos.objProdutoMaterial objprma = new wsngtt.Objetos.objProdutoMaterial();
						clsProdutoMaterial clsprma = new clsProdutoMaterial();

						DataSet dsProdutoMaterial = clsProdutoMaterial.SelecionarTodosProdutosFamilia("3", empr_cd_empr_destino.ToString());

						
						if (dsProdutoMaterial.Tables[0].Rows.Count <= 0)
						{
							dsProdutoMaterial = clsProdutoMaterial.SelecionarTodosProdutosFamiliaME("5", emen_empr_origem.ToString(), empr_cd_empr_destino.ToString());
						}

						if (dsProdutoMaterial.Tables[0].Rows.Count > 0)
						{

							qtd_folha_boa = Convert.ToString(Convert.ToInt32(kit_bom_qtd[0].InnerText) * qtd_folhas_kit);
							qtd_folha_ruim = Convert.ToString(Convert.ToInt32(kit_ruin_qtd[0].InnerText) * qtd_folhas_kit);


							//aqui não veio folha na nf
							objMovimentoEstoqueFolhaKilo.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
							objMovimentoEstoqueFolhaKilo.Moes_dt_criacao = DateTime.Now;
							objMovimentoEstoqueFolhaKilo.Moes_quantidade = Convert.ToDouble(folhaskilo[0].InnerText.Replace(".", ","));
							objMovimentoEstoqueFolhaKilo.Prma_cod_prma = Convert.ToInt32(dsProdutoMaterial.Tables[0].Rows[0]["prma_cod_prma"].ToString());
							objMovimentoEstoqueFolhaKilo.Tpmo_cod_tpmo = 14;
							objMovimentoEstoqueFolhaKilo.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
							lstobjMovimentoEstoque.Add(objMovimentoEstoqueFolhaKilo);

							//aqui vou cadastrar as folhas boas
							objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
							objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
							objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(qtd_folha_boa);
							objMovimentoEstoque.Prma_cod_prma = objMovimentoEstoqueFolhaKilo.Prma_cod_prma;
							objMovimentoEstoque.Tpmo_cod_tpmo = 3;
							objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
							objMovimentoEstoque.Moes_folha_boa = true;
							lstobjMovimentoEstoque.Add(objMovimentoEstoque);

							//aqui vou acertar o saldo de estoque das folhas boas
							objSaldoEstoque.Tpop_cod_tpop = 3;
							objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
							objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
							objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
							objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;
							lstobjSaldoEstoque.Add(objSaldoEstoque);

							objMovimentoEstoque = new Objetos.objMovimentoEstoque();
							objSaldoEstoque = new Objetos.objSaldoEstoque();

							//aqui vou acertar a movimentação de estoque de folhas ruins
							objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
							objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
							objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(qtd_folha_ruim);
							objMovimentoEstoque.Prma_cod_prma = objMovimentoEstoqueFolhaKilo.Prma_cod_prma;
							objMovimentoEstoque.Tpmo_cod_tpmo = 3;
							objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
							objMovimentoEstoque.Moes_folha_boa = false;
							lstobjMovimentoEstoque.Add(objMovimentoEstoque);

							//aqui vou acertar o saldo de estoque das folhas ruim
							objSaldoEstoque.Tpop_cod_tpop = 3;
							objSaldoEstoque.Prma_cod_prma = objMovimentoEstoqueFolhaKilo.Prma_cod_prma;
							objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
							objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
							objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;
							lstobjSaldoEstoque.Add(objSaldoEstoque);

							objMovimentoEstoque = new Objetos.objMovimentoEstoque();
							objSaldoEstoque = new Objetos.objSaldoEstoque();
						}

					}
                    //aqui farei a gravação para pallets
                    if (cod_prma_pallet != "")
                    {
                        DsMapeamentoProdutos = new DataSet();
                        DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoGrupos(Convert.ToInt32(cod_prma_pallet), grem_cd_grem_dest, grem_cd_grem_orig);

                        objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
                        objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                        objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(pallets_qtd[0].InnerText);
                        objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                        objMovimentoEstoque.Tpmo_cod_tpmo = 3;
                        objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                        lstobjMovimentoEstoque.Add(objMovimentoEstoque);

                        //aqui acertarei o saldo do pallet
                        objSaldoEstoque.Tpop_cod_tpop = 3;
                        objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
                        objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
                        objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
                        objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;
                        lstobjSaldoEstoque.Add(objSaldoEstoque);

                        objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                        objSaldoEstoque = new Objetos.objSaldoEstoque();
                    }
                    //aqui vou realizr a gravação do topo
                    if (cod_prma_topo != "")
                    {
                        DsMapeamentoProdutos = new DataSet();
                        DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoGrupos(Convert.ToInt32(cod_prma_topo), grem_cd_grem_dest, grem_cd_grem_orig);

                        objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(canf_cod_canf);
                        objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                        objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(topos_qtd[0].InnerText);
                        objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                        objMovimentoEstoque.Tpmo_cod_tpmo = 3;
                        objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                        lstobjMovimentoEstoque.Add(objMovimentoEstoque);

                        //aqui vou gravar o saldo de estoque dos topos

                        objSaldoEstoque.Tpop_cod_tpop = 3;
                        objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
                        objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
                        objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
                        objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;
                        lstobjSaldoEstoque.Add(objSaldoEstoque);
                    }
                    gravou = clsRecebimento.cadastraRecebimentoConferencia(lstobjMovimentoEstoque, lstobjSaldoEstoque, empr_cod_empr_origem, empr_cd_empr_destino, objMotorista.Moto_cod_moto, objVeiculos.Veic_cod_veic);
                    clsQrcod.CadastrarConferido(empr_cod_empr_origem.ToString(), Convert.ToInt32(kit_bom_qtd[0].InnerText), Convert.ToInt32(kit_ruin_qtd[0].InnerText));

                    if (gravou == "")
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                        xmlRetorno = xmlRetorno + "<conferencia>";
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                        xmlRetorno = xmlRetorno + "</conferencia>";
                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                    }
                    return xmlRetorno;
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal não encontrada</descerro></retornoErro>";
                }
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do kit para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string EntradaCadastro(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.


            //aqui instâncio a classe de empresas
            Classes.clsEntr clsEntr = new Classes.clsEntr();
            Classes.clsUsuario clsUsuario = new Classes.clsUsuario();

            //aqui o objeto da empresa
            Objetos.objEntr objEntr = new Objetos.objEntr();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList entr_tx_placas = doc.GetElementsByTagName("entr_tx_placas");
                XmlNodeList entr_dt_entrada = doc.GetElementsByTagName("entr_dt_entrada");
                XmlNodeList usur_sq_usuario = doc.GetElementsByTagName("usur_sq_usuario");
                XmlNodeList entr_tx_nf = doc.GetElementsByTagName("entr_tx_nf");

                DataSet dsUsuario = clsUsuario.SelecionarUsuario(Convert.ToInt32(usur_sq_usuario[0].InnerText));

                objEntr.Emen_sq_empresa = Convert.ToInt32(dsUsuario.Tables[0].Rows[0]["emen_cod_empr"].ToString());
                objEntr.Entr_tx_placas = entr_tx_placas[0].InnerText;
                objEntr.Entr_dt_entrada = Convert.ToDateTime(entr_dt_entrada[0].InnerText);
                objEntr.Usur_sq_usuario = Convert.ToInt32(usur_sq_usuario[0].InnerText);
                objEntr.Entr_tx_nf = entr_tx_nf[0].InnerText;


                clsEntr.CadastrarEntrada(objEntr);


                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<entrada>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "</entrada>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera o id da uma empresa e então criará um xml com todos os armazens vinculados aquela empresa
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarEntradas(string xmlString)
        {
            //aqui instâncio a classe de empresas
            Classes.clsEntr clsEntr = new Classes.clsEntr();



            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);



                XmlNodeList usur_sq_usuario = doc.GetElementsByTagName("usur_sq_usuario");



                DataSet dsEntradas = new DataSet();
                dsEntradas = clsEntr.SelecionarEntradas(usur_sq_usuario[0].InnerText);
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";

                xmlRetorno = xmlRetorno + "<consultaEntrada>";
                if (dsEntradas.Tables[0].Rows.Count > 0)
                {

                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsEntradas.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<entradas>";
                        xmlRetorno = xmlRetorno + "<entr_sq_entrada>" + dsEntradas.Tables[0].Rows[i]["entr_sq_entrada"].ToString() + "</entr_sq_entrada>";
                        xmlRetorno = xmlRetorno + "<entr_tx_nf>" + dsEntradas.Tables[0].Rows[i]["entr_tx_nf"].ToString() + "</entr_tx_nf>";
                        xmlRetorno = xmlRetorno + "<entr_tx_placas>" + dsEntradas.Tables[0].Rows[i]["entr_tx_placas"].ToString() + "</entr_tx_placas>";
                        xmlRetorno = xmlRetorno + "<entr_dt_entrada>" + dsEntradas.Tables[0].Rows[i]["entr_dt_entrada"].ToString() + "</entr_dt_entrada>";
                        xmlRetorno = xmlRetorno + "</entradas>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<entradas>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</entradas>";

                }
                xmlRetorno = xmlRetorno + "</consultaEntrada>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do kit para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SaidaCadastro(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.


            //aqui instâncio a classe de empresas
            Classes.clsSaid clsSaid = new Classes.clsSaid();
            Classes.clsUsuario clsUsuario = new Classes.clsUsuario();

            //aqui o objeto da empresa
            Objetos.objSaid objSaid = new Objetos.objSaid();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList said_tx_placas = doc.GetElementsByTagName("said_tx_placas");
                XmlNodeList said_dt_saida = doc.GetElementsByTagName("said_dt_saida");
                XmlNodeList entr_sq_entrada = doc.GetElementsByTagName("entr_sq_entrada");
                XmlNodeList usur_sq_usuario = doc.GetElementsByTagName("usur_sq_usuario");
                XmlNodeList said_tx_nf = doc.GetElementsByTagName("said_tx_nf");


                DataSet dsUsuario = clsUsuario.SelecionarUsuario(Convert.ToInt32(usur_sq_usuario[0].InnerText));

                objSaid.Emen_sq_empresa = Convert.ToInt32(dsUsuario.Tables[0].Rows[0]["emen_cod_empr"].ToString());
                objSaid.Said_tx_placas = said_tx_placas[0].InnerText;
                objSaid.Said_dt_saida = Convert.ToDateTime(said_dt_saida[0].InnerText);
                objSaid.Usur_sq_usuario = Convert.ToInt32(usur_sq_usuario[0].InnerText);
                objSaid.Entr_sq_entrada = Convert.ToInt32(entr_sq_entrada[0].InnerText);
                objSaid.said_tx_nf = said_tx_nf[0].InnerText;

                clsSaid.CadastrarSaida(objSaid);

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<saida>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "</saida>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o código do usuário da qual necessitamos saber as atividades extras
        /// o mesmo irá retornar um xml com os dados das atividades extras
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarGrupoPorcodigoProduto(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsGrupoProdutos clsGrupoProdutos = new Classes.clsGrupoProdutos();

            //aqui o objeto da empresa
            Objetos.objGppm objGppm = new Objetos.objGppm();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XmlNodeList prma_cod_prma = doc.GetElementsByTagName("prma_cod_prma");


                DataSet dsGppm = new DataSet();
                dsGppm = clsGrupoProdutos.SelecionarGrupoPorCodigoProduto(Convert.ToInt32(prma_cod_prma[0].InnerText.ToString()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<Grupo_produto>";
                if (dsGppm.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    xmlRetorno = xmlRetorno + "<gppm>";
                    for (int i = 0; i <= dsGppm.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<grupo>";
                        xmlRetorno = xmlRetorno + "<gppm_cd_gppm>" + dsGppm.Tables[0].Rows[i]["gppm_cd_gppm"].ToString() + "</gppm_cd_gppm>";
                        xmlRetorno = xmlRetorno + "</grupo>";
                    }
                    xmlRetorno = xmlRetorno + "</gppm>";
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</Grupo_produto>";
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        public string SelecionarPrevisaoProducao(string xmlString)
        {


            //aqui instâncio a classe de empresas
            Classes.clsPrevisaoProducao clsPrevisaoProducao = new Classes.clsPrevisaoProducao();

            //aqui o objeto da empresa
            //Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {

                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                /*XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);

                string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");


                DataSet ds = new DataSet();
                ds = clsPrevisaoProducao.SelecionarPrevisaoProducaoEmpresa(Convert.ToInt32(emen_cod_empr[0].InnerText.Trim()));
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<previsaoProducao>";


                if (ds.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<previsao>";
                        xmlRetorno = xmlRetorno + "<id>" + ds.Tables[0].Rows[i]["id"].ToString() + "</id>";
                        xmlRetorno = xmlRetorno + "<cblp_dt_inicial>" + ds.Tables[0].Rows[i]["cblp_dt_inicial"].ToString() + "</cblp_dt_inicial>";
                        xmlRetorno = xmlRetorno + "<cblp_dt_final>" + ds.Tables[0].Rows[i]["cblp_dt_final"].ToString() + "</cblp_dt_final>";
                        xmlRetorno = xmlRetorno + "<cblp_int_versao>" + ds.Tables[0].Rows[i]["cblp_int_versao"].ToString() + "</cblp_int_versao>";
                        xmlRetorno = xmlRetorno + "</previsao>";
                        
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</previsaoProducao>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método recebera os dados do usuário para cadastro ou alteração
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarPlanoProducao(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsPrevisaoProducao clsPrevisaoProducao = new Classes.clsPrevisaoProducao();

            //aqui o objeto da empresa
            Objetos.objPrevisaoProducao objPlpv = new Objetos.objPrevisaoProducao();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

				
				XmlNodeList plpv_cod_plpv = doc.GetElementsByTagName("plpv_cod_plpv");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");
				XmlNodeList gppm_cd_gppm = doc.GetElementsByTagName("gppm_cd_gppm");
				XmlNodeList pvpl_num_semana_ano = doc.GetElementsByTagName("pvpl_num_semana_ano");

                XmlNodeList pvpl_dt_d0 = doc.GetElementsByTagName("pvpl_dt_d0");
                XmlNodeList pvpl_dt_d1 = doc.GetElementsByTagName("pvpl_dt_d1");
                XmlNodeList pvpl_dt_d2 = doc.GetElementsByTagName("pvpl_dt_d2");
                XmlNodeList pvpl_dt_d3 = doc.GetElementsByTagName("pvpl_dt_d3");
                XmlNodeList pvpl_dt_d4 = doc.GetElementsByTagName("pvpl_dt_d4");
                XmlNodeList pvpl_dt_d5 = doc.GetElementsByTagName("pvpl_dt_d5");
                XmlNodeList pvpl_dt_d6 = doc.GetElementsByTagName("pvpl_dt_d6");

                XmlNodeList pvpl_qtd_d0 = doc.GetElementsByTagName("pvpl_qtd_d0");
                XmlNodeList pvpl_qtd_d1 = doc.GetElementsByTagName("pvpl_qtd_d1");
                XmlNodeList pvpl_qtd_d2 = doc.GetElementsByTagName("pvpl_qtd_d2");
                XmlNodeList pvpl_qtd_d3 = doc.GetElementsByTagName("pvpl_qtd_d3");
                XmlNodeList pvpl_qtd_d4 = doc.GetElementsByTagName("pvpl_qtd_d4");
                XmlNodeList pvpl_qtd_d5 = doc.GetElementsByTagName("pvpl_qtd_d5");
                XmlNodeList pvpl_qtd_d6 = doc.GetElementsByTagName("pvpl_qtd_d6");

                XmlNodeList pvpl_qtd_semana1 = doc.GetElementsByTagName("pvpl_qtd_semana1");
                XmlNodeList pvpl_qtd_semana2 = doc.GetElementsByTagName("pvpl_qtd_semana2");
                XmlNodeList pvpl_qtd_semana3 = doc.GetElementsByTagName("pvpl_qtd_semana3");
                XmlNodeList pvpl_qtd_semana4 = doc.GetElementsByTagName("pvpl_qtd_semana4");
                XmlNodeList pvpl_num_versao = doc.GetElementsByTagName("pvpl_num_versao");
                XmlNodeList usur_cd_usur = doc.GetElementsByTagName("usur_cd_usur");


                if (plpv_cod_plpv[0].InnerText.ToString() != "")
                {
                    objPlpv.Pvpl_cod_pvpl = Convert.ToInt32(plpv_cod_plpv[0].InnerText.ToString());
                }
                objPlpv.Emen_cod_empr = Convert.ToInt32(emen_cod_empr[0].InnerText.ToString());
                objPlpv.Usur_cd_usur = Convert.ToInt32(usur_cd_usur[0].InnerText.ToString());
				objPlpv.Gppm_cd_gppm = gppm_cd_gppm[0].InnerText.ToString();

				objPlpv.Pvpl_num_semana_ano = Convert.ToInt32(pvpl_num_semana_ano[0].InnerText.ToString());

                objPlpv.Pvpl_dt_d0 = Convert.ToDateTime(pvpl_dt_d0[0].InnerText.ToString());
                objPlpv.Pvpl_dt_d1 = Convert.ToDateTime(pvpl_dt_d1[0].InnerText.ToString());
                objPlpv.Pvpl_dt_d2 = Convert.ToDateTime(pvpl_dt_d2[0].InnerText.ToString());
                objPlpv.Pvpl_dt_d3 = Convert.ToDateTime(pvpl_dt_d3[0].InnerText.ToString());
                objPlpv.Pvpl_dt_d4 = Convert.ToDateTime(pvpl_dt_d4[0].InnerText.ToString());
                objPlpv.Pvpl_dt_d5 = Convert.ToDateTime(pvpl_dt_d5[0].InnerText.ToString());
                objPlpv.Pvpl_dt_d6 = Convert.ToDateTime(pvpl_dt_d6[0].InnerText.ToString());

                objPlpv.Pvpl_qtd_d0 = Convert.ToInt32(pvpl_qtd_d0[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_d1 = Convert.ToInt32(pvpl_qtd_d1[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_d2 = Convert.ToInt32(pvpl_qtd_d2[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_d3 = Convert.ToInt32(pvpl_qtd_d3[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_d4 = Convert.ToInt32(pvpl_qtd_d4[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_d5 = Convert.ToInt32(pvpl_qtd_d5[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_d6 = Convert.ToInt32(pvpl_qtd_d6[0].InnerText.ToString());


                objPlpv.Pvpl_qtd_semana1 = Convert.ToInt32(pvpl_qtd_semana1[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_semana2 = Convert.ToInt32(pvpl_qtd_semana2[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_semana3 = Convert.ToInt32(pvpl_qtd_semana3[0].InnerText.ToString());
                objPlpv.Pvpl_qtd_semana4 = Convert.ToInt32(pvpl_qtd_semana4[0].InnerText.ToString());

                objPlpv.Pvpl_num_versao = Convert.ToInt32(pvpl_num_versao[0].InnerText.ToString());
                int plpv_cod_plpv_retorno = 0;

                if (objPlpv.Pvpl_cod_pvpl.ToString().Trim() == "0")
                {

                    plpv_cod_plpv_retorno = clsPrevisaoProducao.InserirPrevisaoProducao(objPlpv);

                }
                else
                {
                    objPlpv.Pvpl_cod_pvpl = Convert.ToInt32(plpv_cod_plpv[0].InnerText.ToString());
                    plpv_cod_plpv_retorno = objPlpv.Pvpl_cod_pvpl;
                    clsPrevisaoProducao.AlterarPrevisaoProducao(objPlpv);
                }

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<NivelEstoque>";
                xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                xmlRetorno = xmlRetorno + "<nves_cod_nves>" + plpv_cod_plpv_retorno + "</nves_cod_nves>";
                xmlRetorno = xmlRetorno + "</NivelEstoque>";

                return xmlRetorno;

            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este método retornará a lista de todos os motoristas cadastrados no sistema.
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarPlanoProducaoPorCodigo(string xmlString)
        {
            //aqui instâncio a classe de usuário
            Classes.clsPrevisaoProducao clsPrevisaoProducao = new Classes.clsPrevisaoProducao();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                XmlNodeList pvpl_cod_pvpl = doc.GetElementsByTagName("pvpl_cod_pvpl");


                DataSet dsPlanoProducao = new DataSet();
                dsPlanoProducao = clsPrevisaoProducao.SelecionarPlanoProducaoCodigo(Convert.ToInt32(pvpl_cod_pvpl[0].InnerText.Trim()));

                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                if (dsPlanoProducao.Tables[0].Rows.Count > 0)
                {

                    for (int i = 0; i <= dsPlanoProducao.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<PlanoProducao>";
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";


                        xmlRetorno = xmlRetorno + "<pvpl_num_semana_ano>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_num_semana_ano"].ToString().Trim() + "</pvpl_num_semana_ano>";
						xmlRetorno = xmlRetorno + "<gppm_cd_gppm>" + dsPlanoProducao.Tables[0].Rows[i]["gppm_cd_gppm"].ToString().Trim() + "</gppm_cd_gppm>";
						xmlRetorno = xmlRetorno + "<pvpl_dt_d0>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_dt_d0"].ToString().Trim() + "</pvpl_dt_d0>";
                        xmlRetorno = xmlRetorno + "<pvpl_dt_d1>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_dt_d1"].ToString().Trim() + "</pvpl_dt_d1>";
                        xmlRetorno = xmlRetorno + "<pvpl_dt_d2>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_dt_d2"].ToString().Trim() + "</pvpl_dt_d2>";
                        xmlRetorno = xmlRetorno + "<pvpl_dt_d3>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_dt_d3"].ToString().Trim() + "</pvpl_dt_d3>";
                        xmlRetorno = xmlRetorno + "<pvpl_dt_d4>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_dt_d4"].ToString().Trim() + "</pvpl_dt_d4>";
                        xmlRetorno = xmlRetorno + "<pvpl_dt_d5>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_dt_d5"].ToString().Trim() + "</pvpl_dt_d5>";
                        xmlRetorno = xmlRetorno + "<pvpl_dt_d6>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_dt_d6"].ToString().Trim() + "</pvpl_dt_d6>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_d0>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_d0"].ToString().Trim() + "</pvpl_qtd_d0>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_d1>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_d1"].ToString().Trim() + "</pvpl_qtd_d1>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_d2>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_d2"].ToString().Trim() + "</pvpl_qtd_d2>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_d3>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_d3"].ToString().Trim() + "</pvpl_qtd_d3>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_d4>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_d4"].ToString().Trim() + "</pvpl_qtd_d4>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_d5>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_d5"].ToString().Trim() + "</pvpl_qtd_d5>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_d6>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_d6"].ToString().Trim() + "</pvpl_qtd_d6>";

                        xmlRetorno = xmlRetorno + "<pvpl_qtd_semana1>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_semana1"].ToString().Trim() + "</pvpl_qtd_semana1>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_semana2>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_semana2"].ToString().Trim() + "</pvpl_qtd_semana2>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_semana3>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_semana3"].ToString().Trim() + "</pvpl_qtd_semana3>";
                        xmlRetorno = xmlRetorno + "<pvpl_qtd_semana4>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_qtd_semana4"].ToString().Trim() + "</pvpl_qtd_semana4>";
                        xmlRetorno = xmlRetorno + "<pvpl_num_versao>" + dsPlanoProducao.Tables[0].Rows[i]["pvpl_num_versao"].ToString().Trim() + "</pvpl_num_versao>";

                        xmlRetorno = xmlRetorno + "</PlanoProducao>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<NivelEstoque>";
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                    xmlRetorno = xmlRetorno + "</NivelEstoque>";
                }



                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }


        [WebMethod]
        ///<summary>
        /// Este método não apresentará parâmetros de entrada
        /// o mesmo irá retornar uma lista com todos os produtos/materiais, cadastrados na base de dados
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarProdutoMaterialTodosPorGrupo(string xmlString)
        {

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa
            Objetos.objProdutoMaterial objProdutoMaterial = new Objetos.objProdutoMaterial();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);
            XmlNodeList emen_cod_emen = doc.GetElementsByTagName("emen_cod_emen");

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                DataSet dsProdutoMaterial = new DataSet();
                dsProdutoMaterial = clsProdutoMaterial.SelecionarProdutoMaterialTodosPorGrupo(emen_cod_emen[0].InnerText);
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutoMaterial.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutoMaterial.Tables[0].Rows.Count - 1; i++)
                    {

                        xmlRetorno = xmlRetorno + "<produtomaterial>";
                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<unme_cod_unme>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_cod_unme"].ToString() + "</unme_cod_unme>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_fator_conservacao"].ToString().Replace(".", ",") + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_preco"].ToString().Replace(".", ",") + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "<famp_cd_famp>" + dsProdutoMaterial.Tables[0].Rows[i]["famp_cd_famp"].ToString() + "</famp_cd_famp>";
                        xmlRetorno = xmlRetorno + "<prma_dt_criacao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_dt_criacao"].ToString() + "</prma_dt_criacao>";
                        xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsProdutoMaterial.Tables[0].Rows[i]["usur_cd_usur"].ToString() + "</usur_cd_usur>";
                        xmlRetorno = xmlRetorno + "<prma_ativo>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_ativo"].ToString() + "</prma_ativo>";
                        xmlRetorno = xmlRetorno + "<unme_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_descricao"].ToString() + "</unme_descricao>";
                        xmlRetorno = xmlRetorno + "<unme_id_reg>" + dsProdutoMaterial.Tables[0].Rows[i]["unme_id_reg"].ToString() + "</unme_id_reg>";
                        xmlRetorno = xmlRetorno + "<tpma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["tpma_descricao"].ToString() + "</tpma_descricao>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutoMaterial.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<grem_nome>" + dsProdutoMaterial.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";


                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }
                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";

                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }

        }
        [WebMethod]
        ///<summary>
        /// Este método recebera os dados da diferença na conferencia para cadastro
        /// <param name="xmlString">deve conter o xml como string no formato pré-estabelecido</param>
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string CadastrarConferenciaInhouseMultiplas(string xmlString)
        {
            //Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

			//variavel para que possamos pegar a empresa de origem no começo e passar para todos os
			//métodos que utilizarem-na
			int Emen_cod_empr_origem = 0;

			//aqui instâncio a classe de empresas
			Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

            Classes.clsRecebimento clsRecebimento = new Classes.clsRecebimento();
            Classes.clsQrcod clsQrcod = new Classes.clsQrcod();
            Classes.clsMovimentoEstoque clsMovimentoEstoque = new Classes.clsMovimentoEstoque();
            Classes.clsNotaFiscal clsNotaFiscal = new Classes.clsNotaFiscal();

            Classes.clsArim clsArim = new Classes.clsArim();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();
            Classes.clsKits clsKits = new Classes.clsKits();
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa

            Objetos.objMovimentoEstoque objMovimentoEstoque = new Objetos.objMovimentoEstoque();
            Objetos.objMovimentoEstoque objMovimentoEstoqueFolhaKilo = new Objetos.objMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objArim objArim = new Objetos.objArim();
            Objetos.objMotorista objMotorista = new Objetos.objMotorista();
            Objetos.objVeiculos objVeiculos = new Objetos.objVeiculos();
            Objetos.objNotaFiscal objNotaFiscal = new Objetos.objNotaFiscal();
            List<Objetos.objNotaFiscal> lstNotas = new List<Objetos.objNotaFiscal>();
            Objetos.objItensNotaFiscal objItens = new Objetos.objItensNotaFiscal();

            //criando a lista para poder criar um array dos objetos

            List<Objetos.ObjDiferencaConferencia> lstObjDiferencaConferencia = new List<Objetos.ObjDiferencaConferencia>();
            List<Objetos.objMovimentoEstoque> lstobjMovimentoEstoque = new List<Objetos.objMovimentoEstoque>();
            List<Objetos.objSaldoEstoque> lstobjSaldoEstoque = new List<Objetos.objSaldoEstoque>();

            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";
            string gravou = "";
            int qtd_folhas_kit = -1;

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);

                string cod_prma_topo = "";
                string cod_prma_pallet = "";
                string cod_prma_folha = "";

                string qtd_folha_boa = "";
                string qtd_folha_ruim = "";

                XmlNodeList canf_numnt = doc.GetElementsByTagName("canf_numnt");
                XmlNodeList usur_cod_usur = doc.GetElementsByTagName("usur_cod_usur");
                XmlNodeList kit_bom_qtd = doc.GetElementsByTagName("kit_bom_qtd");
                XmlNodeList kit_ruin_qtd = doc.GetElementsByTagName("kit_ruin_qtd");
                XmlNodeList pallets_qtd = doc.GetElementsByTagName("pallets_qtd");
                XmlNodeList topos_qtd = doc.GetElementsByTagName("topos_qtd");
                XmlNodeList manterfoto = doc.GetElementsByTagName("manterfoto");
                XmlNodeList folhaskilo = doc.GetElementsByTagName("folhaskilo");
                XmlNodeList folhaskiloFixa = doc.GetElementsByTagName("folhaskilo");
				XmlNodeList emen_empr_origem = doc.GetElementsByTagName("emen_empr_origem");

				string totFolhasFixa = folhaskiloFixa[0].InnerText;

                string canf_cod_canf = "";

                string[] listaNotas = canf_numnt[0].InnerText.ToString().Split('|');

                string numNotas = "";
                int TotalNotasFolhas = 0;

                for (int i = 0; i <= listaNotas.Count() - 1; i++)
                {
                    if (i == 0)
                    {
                        numNotas = numNotas + "'" + listaNotas[i] + "'";
                    }
                    else
                    {
                        numNotas = numNotas + "," + "'" + listaNotas[i] + "'";
                    }
                }


                DataSet dsTotalNotasFolha = clsNotaFiscal.selecionarTotalNotasComFolhas(numNotas, usur_cod_usur[0].InnerText);
                TotalNotasFolhas = dsTotalNotasFolha.Tables[0].Rows.Count;


                DataSet ds = new DataSet();

                for (int i = 0; i < listaNotas.Count(); i++)
                {
                    ds = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestinoSumarizada(listaNotas[i].ToString(), Convert.ToInt32(usur_cod_usur[0].InnerText));

                    //aqui vou preencher o objeto  relativo a nota fiscal e seus itens

                    //primeiro prencherei o cabeçalho da nota
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objNotaFiscal.Canf_cod_canf = Convert.ToInt32(ds.Tables[0].Rows[0]["canf_cod_canf"].ToString());
                        objNotaFiscal.Canf_numnt = ds.Tables[0].Rows[0]["canf_numnt"].ToString();
                        objNotaFiscal.Emen_cd_empr_destino = Convert.ToInt32(ds.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());
                        objNotaFiscal.Emen_cod_empr_origem = Convert.ToInt32(ds.Tables[0].Rows[0]["Emen_cod_empr_origem"].ToString());
						Emen_cod_empr_origem = objNotaFiscal.Emen_cod_empr_origem;
						DataSet dsQuantidadeKit = clsEmpresa.SelecionarFolhasKitEmpresa(objNotaFiscal.Emen_cod_empr_origem);

                        if (Convert.ToDouble(dsQuantidadeKit.Tables[0].Rows[0]["emen_nr_folhas_kit"].ToString()) > 0)
                        {
                            qtd_folhas_kit = Convert.ToInt32(dsQuantidadeKit.Tables[0].Rows[0]["emen_nr_folhas_kit"].ToString());
                        }
                        else
                        {
                            qtd_folhas_kit = 794;
                        }


                        //aqui vou percorrer os os e preencher os itens da nf
                        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                        {
                            objItens.Prma_cod_prma = Convert.ToInt32(ds.Tables[0].Rows[j]["prma_cod_prma"].ToString());
                            objItens.Itnf_quantidade = Convert.ToDouble(ds.Tables[0].Rows[j]["itnf_quantidade"].ToString());

                            objNotaFiscal.LstItens.Add(objItens);
                            objItens = new Objetos.objItensNotaFiscal();

                        }
                        lstNotas.Add(objNotaFiscal);
                        objNotaFiscal = new Objetos.objNotaFiscal();

                        ds = new DataSet();

                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota fiscal " + canf_numnt[0].InnerText + "não encontrada </descerro></retornoErro>";
                    }
                }

                //aqui agora farei a busca para cada item, visando pegar a família do mesmo e assim verificar a quantidade informada
                DataSet dsProd = new DataSet();
                for (int l = 0; l < lstNotas.Count; l++)
                {
                    for (int k = 0; k < lstNotas[l].LstItens.Count; k++)
                    {
                        dsProd = new DataSet();
                        dsProd = clsProdutoMaterial.SelecionarUmProdutoMaterial(lstNotas[l].LstItens[k].Prma_cod_prma);

                        if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "3") //folha
                        {
                            if (lstNotas[l].LstItens[k].Itnf_quantidade == Convert.ToDouble(folhaskilo[0].InnerText.ToString()))
                            {
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = Convert.ToDouble(folhaskilo[0].InnerText.ToString());
                                folhaskilo[0].InnerText = "0";
                            }
                            else
                            {
                                folhaskilo[0].InnerText = Convert.ToString(Convert.ToDouble(folhaskilo[0].InnerText) - Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade));
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade);
                            }

                        }
                        else if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "2") //topo
                        {
                            if (lstNotas[l].LstItens[k].Itnf_quantidade == Convert.ToDouble(topos_qtd[0].InnerText.ToString()))
                            {
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = Convert.ToDouble(topos_qtd[0].InnerText.ToString());
                                topos_qtd[0].InnerText = "0";
                            }
                            else if (lstNotas[l].LstItens[k].Itnf_quantidade < Convert.ToDouble(topos_qtd[0].InnerText.ToString()))
                            {
                                topos_qtd[0].InnerText = Convert.ToString(Convert.ToDouble(topos_qtd[0].InnerText) - Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade));
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade);

                            }
                            else
                            {
                                topos_qtd[0].InnerText = Convert.ToString(Convert.ToDouble(topos_qtd[0].InnerText) - Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade));
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade);

                            }
                        }
                        else if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "1") //pallet
                        {
                            if (lstNotas[l].LstItens[k].Itnf_quantidade == Convert.ToDouble(pallets_qtd[0].InnerText.ToString()))
                            {
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = Convert.ToDouble(pallets_qtd[0].InnerText.ToString());
                                pallets_qtd[0].InnerText = "0";
                            }
                            else if (lstNotas[l].LstItens[k].Itnf_quantidade < Convert.ToDouble(pallets_qtd[0].InnerText.ToString()))
                            {
                                pallets_qtd[0].InnerText = Convert.ToString(Convert.ToDouble(pallets_qtd[0].InnerText) - Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade));
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = lstNotas[l].LstItens[k].Itnf_quantidade;
                            }
                            else
                            {
                                pallets_qtd[0].InnerText = Convert.ToString(Convert.ToDouble(lstNotas[l].LstItens[k].Itnf_quantidade) - Convert.ToDouble(pallets_qtd[0].InnerText));
                                lstNotas[l].LstItens[k].Quantidade_informada_conferencia = lstNotas[l].LstItens[k].Itnf_quantidade;

                            }
                        }
                    }
                }

                //agora que montei o objeto e as quantidades informadas, verei se temos diferenças e tendo acertarei
                //na primeira note na qual tivermos o produto, somando a quantidade informada a quantidade que sobrou.

                for (int i = 0; i < lstNotas.Count(); i++)
                {
                    for (int j = 0; j <= lstNotas[i].LstItens.Count() - 1; j++)
                    {
                        dsProd = clsProdutoMaterial.SelecionarUmProdutoMaterial(lstNotas[i].LstItens[j].Prma_cod_prma);

                        if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "3" && Convert.ToDouble(folhaskilo[0].InnerText) != 0) //folha
                        {
                            lstNotas[i].LstItens[j].Quantidade_informada_conferencia = lstNotas[i].LstItens[j].Quantidade_informada_conferencia - Convert.ToDouble(folhaskilo[0].InnerText);
                            folhaskilo[0].InnerText = "0";
                        }
                        /*else if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "2" && Convert.ToInt32(pallets_qtd[0].InnerText) != 0)//pallet
                        {
                            lstNotas[i].LstItens[j].Quantidade_informada_conferencia = lstNotas[i].LstItens[j].Quantidade_informada_conferencia - Convert.ToInt32(pallets_qtd[0].InnerText);
                            pallets_qtd[0].InnerText = "0";
                        }
                        else if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "1" && Convert.ToInt32(topos_qtd[0].InnerText) != 0) //topo
                        {
                            lstNotas[i].LstItens[j].Quantidade_informada_conferencia = lstNotas[i].LstItens[j].Quantidade_informada_conferencia - Convert.ToInt32(topos_qtd[0].InnerText);
                            topos_qtd[0].InnerText = "0";
                        }*/
                    }
                }




                DataSet dsnota = new DataSet();
                for (int i = 0; i < lstNotas.Count; i++)
                {
                    dsnota = clsNotaFiscal.SelecionarNotaFiscaempresaComMovimentacao(lstNotas[i].Canf_numnt, lstNotas[i].Emen_cod_empr_origem, 2);

                    if (dsnota.Tables[0].Rows.Count <= 0)
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Não foi realizado o recebimento para a nota fiscal " + lstNotas[i].Canf_numnt + " </descerro></retornoErro>";
                        return xmlRetorno;
                    }

                    dsnota = clsNotaFiscal.SelecionarNotaFiscaempresaComMovimentacao(lstNotas[i].Canf_numnt, lstNotas[i].Emen_cod_empr_origem, 3);
                    if (dsnota.Tables[0].Rows.Count > 0)
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal " + lstNotas[i].Canf_numnt + " já recebida</descerro></retornoErro>";
                        return xmlRetorno;
                    }

                }

                int empr_cod_empr_origem = 0;
                int empr_cd_empr_destino = 0;
                int grem_cd_grem_orig = 0;
                int grem_cd_grem_dest = 0;


                if (listaNotas.Count() > 0)
                {
                    for (int j = 0; j < lstNotas.Count(); j++)
                    {

                        dsnota = clsNotaFiscal.SelecionarNotaFiscaNumeroEmpresaDestino(lstNotas[j].Canf_numnt, Convert.ToInt32(usur_cod_usur[0].InnerText), Emen_cod_empr_origem.ToString());
                        canf_cod_canf = dsnota.Tables[0].Rows[0]["canf_cod_canf"].ToString();
                        //aqui vou ver qual o produto principal para pegar seu código e assim montar o kit

                        empr_cod_empr_origem = Convert.ToInt32(dsnota.Tables[0].Rows[0]["emen_cod_empr_origem"].ToString());
                        empr_cd_empr_destino = Convert.ToInt32(dsnota.Tables[0].Rows[0]["emen_cd_empr_destino"].ToString());
                        grem_cd_grem_orig = Convert.ToInt32(dsnota.Tables[0].Rows[0]["grem_cd_grem_orig"].ToString());
                        grem_cd_grem_dest = Convert.ToInt32(dsnota.Tables[0].Rows[0]["grem_cd_grem_dest"].ToString());

                        //aqui vou pegar os códigos e realizar algumas totalizações
                        for (int i = 0; i <= lstNotas[j].LstItens.Count - 1; i++)
                        {
                            bool achou = false;
                            dsProd = clsProdutoMaterial.SelecionarUmProdutoMaterial(lstNotas[j].LstItens[i].Prma_cod_prma);
                            if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "3" || dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "5") //folha
                            {
                                cod_prma_folha = lstNotas[j].LstItens[i].Prma_cod_prma.ToString();
                                int qtd_bom = 0;
                                int qtd_ruim = 0;

                                if (Convert.ToInt32(kit_bom_qtd[0].InnerText) > 0)
                                {
                                    if (j < lstNotas.Count - 1)
                                    {
                                        if (j > 0)
                                        {
                                            qtd_bom = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(kit_bom_qtd[0].InnerText) / (Convert.ToDouble(lstNotas.Count) - j)));
                                        }
                                        else
                                        {
                                            qtd_bom = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(kit_bom_qtd[0].InnerText) / Convert.ToDouble(lstNotas.Count)));
                                        }
                                    }
                                    else
                                    {
                                        qtd_bom = Convert.ToInt32(kit_bom_qtd[0].InnerText);
                                    }
                                }
                                if (Convert.ToInt32(kit_ruin_qtd[0].InnerText) > 0)
                                {
                                    if (j < lstNotas.Count - 1)
                                    {
                                        qtd_ruim = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(kit_ruin_qtd[0].InnerText) / TotalNotasFolhas));
                                    }
                                    else
                                    {
                                        qtd_ruim = Convert.ToInt32(kit_ruin_qtd[0].InnerText);
                                    }
                                }

                                kit_bom_qtd[0].InnerText = Convert.ToString(Convert.ToInt32(kit_bom_qtd[0].InnerText) - qtd_bom);
                                kit_ruin_qtd[0].InnerText = Convert.ToString(Convert.ToInt32(kit_ruin_qtd[0].InnerText) - qtd_ruim);

                                //aqu vou pegar a quantidade de folhas no kit da empresa



                                qtd_folha_boa = Convert.ToString(qtd_bom * qtd_folhas_kit);
                                qtd_folha_ruim = Convert.ToString(qtd_ruim * qtd_folhas_kit);
                            }
                            else if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "1") //pallet
                            {
                                cod_prma_pallet = dsProd.Tables[0].Rows[0]["prma_cod_prma"].ToString();

                            }
                            else if (dsProd.Tables[0].Rows[0]["famp_cd_famp"].ToString() == "2") //topo
                            {
                                cod_prma_topo = dsProd.Tables[0].Rows[0]["prma_cod_prma"].ToString();

                            }


                            //aqui vou montar o objeto de atualização do estoque
                            //aqui irei montar a quantidade a ser colocada em cada produto dependendo da família do mesmo:
                            //1 pallet, 2 top, 3 folhas
                            DataSet DsMapeamentoProdutos = new DataSet();
                            //Aqui farei o mapemanto do produto para poder ter o código dele como a fornecedora
                            if (cod_prma_folha != "")
                            {
                                DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoGrupos(Convert.ToInt32(cod_prma_folha), grem_cd_grem_dest, grem_cd_grem_orig);


                                objMovimentoEstoqueFolhaKilo.Canf_cod_canf = Convert.ToInt32(lstNotas[j].Canf_cod_canf);
                                objMovimentoEstoqueFolhaKilo.Moes_dt_criacao = DateTime.Now;
                                objMovimentoEstoqueFolhaKilo.Moes_quantidade = Convert.ToDouble(totFolhasFixa.Replace(".", ",")) / TotalNotasFolhas;
                                objMovimentoEstoqueFolhaKilo.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                                objMovimentoEstoqueFolhaKilo.Familia_produto = "Folha";
                                objMovimentoEstoqueFolhaKilo.Tpmo_cod_tpmo = 14;
                                objMovimentoEstoqueFolhaKilo.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);



                                if (objMovimentoEstoqueFolhaKilo.Prma_cod_prma != 0)
                                {
                                    lstobjMovimentoEstoque.Add(objMovimentoEstoqueFolhaKilo);
                                }
                                objMovimentoEstoqueFolhaKilo = new Objetos.objMovimentoEstoque();

                                //aqui vou verificar se a nf tem folhas

                                for (int k = 0; k < dsTotalNotasFolha.Tables[0].Rows.Count; k++)
                                {
                                    if (dsTotalNotasFolha.Tables[0].Rows[k]["canf_cod_canf"].ToString() == lstNotas[j].Canf_cod_canf.ToString())
                                    {
                                        achou = true;
                                    }
                                }


                                //aqui vou cadastrar as folhas boas

                                if (achou == true)
                                {
                                    if (Convert.ToInt32(qtd_folha_boa) > 0)
                                    {
                                        objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(lstNotas[j].Canf_cod_canf);
                                        objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                                        objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(qtd_folha_boa);
                                        objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                                        objMovimentoEstoque.Tpmo_cod_tpmo = 3;
                                        objMovimentoEstoque.Familia_produto = "Folha";
                                        objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                                        objMovimentoEstoque.Moes_folha_boa = true;

                                        if (objMovimentoEstoque.Prma_cod_prma != 0)
                                        {
                                            lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                                        }


                                        //aqui vou acertar o saldo de estoque das folhas boas
                                        objSaldoEstoque.Tpop_cod_tpop = 3;
                                        objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
                                        objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
                                        objSaldoEstoque.Familia_produto = "folha";
                                        objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
                                        objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;

                                        if (objSaldoEstoque.Prma_cod_prma != 0)
                                        {
                                            lstobjSaldoEstoque.Add(objSaldoEstoque);
                                        }

                                    }

                                    objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                                    objSaldoEstoque = new Objetos.objSaldoEstoque();

                                    //aqui vou acertar a movimentação de estoque de folhas ruins
                                    if (Convert.ToInt32(qtd_folha_ruim) > 0)
                                    {
                                        objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(lstNotas[j].Canf_cod_canf);
                                        objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                                        objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(qtd_folha_ruim);
                                        objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                                        objMovimentoEstoque.Tpmo_cod_tpmo = 3;
                                        objSaldoEstoque.Familia_produto = "folha";
                                        objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                                        objMovimentoEstoque.Moes_folha_boa = false;

                                        if (objMovimentoEstoque.Prma_cod_prma != 0)
                                        {
                                            lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                                        }


                                        objSaldoEstoque.Tpop_cod_tpop = 3;
                                        objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
                                        objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
                                        objSaldoEstoque.Familia_produto = "folha";
                                        objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
                                        objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;

                                        if (objSaldoEstoque.Prma_cod_prma != 0)
                                        {
                                            lstobjSaldoEstoque.Add(objSaldoEstoque);
                                        }

                                    }

                                    //aqui vou acertar o saldo de estoque das folhas ruim


                                    objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                                    objSaldoEstoque = new Objetos.objSaldoEstoque();
                                    cod_prma_folha = "";
                                }
                            }
                            //aqui farei a gravação para pallets
                            if (cod_prma_pallet != "")
                            {
                                DsMapeamentoProdutos = new DataSet();
                                DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoGrupos(Convert.ToInt32(cod_prma_pallet), grem_cd_grem_dest, grem_cd_grem_orig);

                                objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(lstNotas[j].Canf_cod_canf);
                                objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                                objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(lstNotas[j].LstItens[i].Quantidade_informada_conferencia);
                                objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                                objMovimentoEstoque.Tpmo_cod_tpmo = 3;
                                objMovimentoEstoque.Familia_produto = "pallet";
                                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);
                                if (objMovimentoEstoque.Prma_cod_prma != 0)
                                {
                                    lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                                }


                                //aqui acertarei o saldo do pallet
                                objSaldoEstoque.Tpop_cod_tpop = 3;
                                objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
                                objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
                                objSaldoEstoque.Familia_produto = "pallet";
                                objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
                                objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;

                                if (objSaldoEstoque.Prma_cod_prma != 0)
                                {
                                    lstobjSaldoEstoque.Add(objSaldoEstoque);
                                }


                                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                                objSaldoEstoque = new Objetos.objSaldoEstoque();
                                cod_prma_pallet = "";
                            }
                            //aqui vou realizr a gravação do topo
                            if (cod_prma_topo != "")
                            {
                                DsMapeamentoProdutos = new DataSet();
                                DsMapeamentoProdutos = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoGrupos(Convert.ToInt32(cod_prma_topo), grem_cd_grem_dest, grem_cd_grem_orig);

                                objMovimentoEstoque.Canf_cod_canf = Convert.ToInt32(lstNotas[j].Canf_cod_canf);
                                objMovimentoEstoque.Moes_dt_criacao = DateTime.Now;
                                objMovimentoEstoque.Moes_quantidade = Convert.ToInt32(lstNotas[j].LstItens[i].Quantidade_informada_conferencia);
                                objMovimentoEstoque.Prma_cod_prma = Convert.ToInt32(DsMapeamentoProdutos.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                                objMovimentoEstoque.Tpmo_cod_tpmo = 3;
                                objMovimentoEstoque.Familia_produto = "topo";
                                objMovimentoEstoque.Usur_cod_usur = Convert.ToInt32(usur_cod_usur[0].InnerText);

                                if (objMovimentoEstoque.Prma_cod_prma != 0)
                                {
                                    lstobjMovimentoEstoque.Add(objMovimentoEstoque);
                                }

                                //aqui vou gravar o saldo de estoque dos topos

                                objSaldoEstoque.Tpop_cod_tpop = 3;
                                objSaldoEstoque.Prma_cod_prma = objMovimentoEstoque.Prma_cod_prma;
                                objSaldoEstoque.Sles_quantidade = objMovimentoEstoque.Moes_quantidade;
                                objSaldoEstoque.Familia_produto = "topo";
                                objSaldoEstoque.Emen_cod_empresa = empr_cod_empr_origem;
                                objSaldoEstoque.Emen_cod_empresa_destino = empr_cd_empr_destino;

                                if (objSaldoEstoque.Prma_cod_prma != 0)
                                {
                                    lstobjSaldoEstoque.Add(objSaldoEstoque);
                                }


                                objMovimentoEstoque = new Objetos.objMovimentoEstoque();
                                objSaldoEstoque = new Objetos.objSaldoEstoque();
                                cod_prma_topo = "";

                            }
                        }
                    }

                    //aqui pego as diferenças e jogo no primeiro produto que encontrar de pallet e topo.

                    if (Convert.ToInt32(pallets_qtd[0].InnerText) > 0)
                    {
                        for (int i = 0; i <= lstobjMovimentoEstoque.Count() - 1; i++)
                        {
                            if (lstobjMovimentoEstoque[i].Familia_produto == "pallet")
                            {
                                lstobjMovimentoEstoque[i].Moes_quantidade = lstobjMovimentoEstoque[i].Moes_quantidade + Convert.ToInt32(pallets_qtd[0].InnerText);
                                break;
                            }
                        }

                        for (int i = 0; i <= lstobjSaldoEstoque.Count() - 1; i++)
                        {
                            if (lstobjSaldoEstoque[i].Familia_produto == "pallet")
                            {
                                lstobjSaldoEstoque[i].Sles_quantidade = lstobjSaldoEstoque[i].Sles_quantidade + Convert.ToInt32(pallets_qtd[0].InnerText);
                                break;
                            }
                        }



                    }

                    if (Convert.ToInt32(topos_qtd[0].InnerText) > 0)
                    {

                        for (int i = 0; i <= lstobjMovimentoEstoque.Count() - 1; i++)
                        {
                            if (lstobjMovimentoEstoque[i].Familia_produto == "topo")
                            {
                                lstobjMovimentoEstoque[i].Moes_quantidade = lstobjMovimentoEstoque[i].Moes_quantidade + Convert.ToInt32(topos_qtd[0].InnerText);
                                break;
                            }
                        }

                        for (int i = 0; i <= lstobjSaldoEstoque.Count() - 1; i++)
                        {
                            if (lstobjSaldoEstoque[i].Familia_produto == "topo")
                            {
                                lstobjSaldoEstoque[i].Sles_quantidade = lstobjSaldoEstoque[i].Sles_quantidade + Convert.ToInt32(pallets_qtd[0].InnerText);
                                break;
                            }
                        }
                    }


                    gravou = clsRecebimento.cadastraRecebimentoConferencia(lstobjMovimentoEstoque, lstobjSaldoEstoque, empr_cod_empr_origem, empr_cd_empr_destino, objMotorista.Moto_cod_moto, objVeiculos.Veic_cod_veic);
                    clsQrcod.CadastrarConferido(empr_cod_empr_origem.ToString(), Convert.ToInt32(kit_bom_qtd[0].InnerText), Convert.ToInt32(kit_ruin_qtd[0].InnerText));


                    if (gravou == "")
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                        xmlRetorno = xmlRetorno + "<conferencia>";
                        xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                        xmlRetorno = xmlRetorno + "</conferencia>";
                    }
                    else
                    {
                        xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + gravou + "</descerro></retornoErro>";
                    }
                    return xmlRetorno;
                }
                else
                {
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>Nota Fiscal não encontrada</descerro></retornoErro>";
                }
                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }
        }

        [WebMethod]
        ///<summary>
        /// Este metodo recebera um XML de onde saberá o codigo da família 
        /// o mesmo irá retornar um xml com os dados dos produtos pertencentes ao grupo
        /// <returns>Retorna uma string contendo um xml em formato pré-estabelecido</returns>
        ///</summary>
        public string SelecionarTodosProdutosPorFamiliaComSaldo(string xmlString)
        {
            Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

            //aqui instâncio a classe de empresas
            Classes.clsProdutoMaterial clsProdutoMaterial = new Classes.clsProdutoMaterial();

            //aqui o objeto da empresa


            //aqui variável para criação do xml de retorno
            string xmlRetorno = "";

            try
            {
                //aqui crio un xelemente a a partir do document criado
                XElement xmlElement = XElement.Parse(xmlString);

                //aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
                //com as tags contidas no mesmo de forma mais fácil e rápida
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlString);


                XDocument docValidar = new XDocument();
                docValidar.Add(xmlElement);



                XmlNodeList famp_cd_famp = doc.GetElementsByTagName("famp_cd_famp");
                XmlNodeList emen_cod_empr = doc.GetElementsByTagName("emen_cod_empr");



                DataSet dsProdutosMaterias = new DataSet();
                dsProdutosMaterias = clsProdutoMaterial.SelecionarTodosProdutosFamiliaComSaldo(famp_cd_famp[0].InnerText.ToString(), emen_cod_empr[0].InnerText.ToString());
                xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
                xmlRetorno = xmlRetorno + "<produtosmateriais>";
                if (dsProdutosMaterias.Tables[0].Rows.Count > 0)
                {
                    xmlRetorno = xmlRetorno + "<status>sucesso</status>";
                    for (int i = 0; i <= dsProdutosMaterias.Tables[0].Rows.Count - 1; i++)
                    {
                        xmlRetorno = xmlRetorno + "<produtomaterial>";

                        xmlRetorno = xmlRetorno + "<prma_cod_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_cod_prma"].ToString() + "</prma_cod_prma>";
                        xmlRetorno = xmlRetorno + "<prma_pn>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_pn"].ToString() + "</prma_pn>";
                        xmlRetorno = xmlRetorno + "<prma_confere_prma>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_confere_prma"].ToString() + "</prma_confere_prma>";
                        xmlRetorno = xmlRetorno + "<prma_descricao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_descricao"].ToString() + "</prma_descricao>";
                        xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsProdutosMaterias.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
                        xmlRetorno = xmlRetorno + "<tpma_cod_tipo>" + dsProdutosMaterias.Tables[0].Rows[i]["tpma_cod_tipo"].ToString() + "</tpma_cod_tipo>";
                        xmlRetorno = xmlRetorno + "<prma_fator_conservacao>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_fator_conservacao"].ToString() + "</prma_fator_conservacao>";
                        xmlRetorno = xmlRetorno + "<prma_preco>" + dsProdutosMaterias.Tables[0].Rows[i]["prma_preco"].ToString() + "</prma_preco>";
                        xmlRetorno = xmlRetorno + "</produtomaterial>";
                    }

                }
                else
                {
                    xmlRetorno = xmlRetorno + "<status>falha</status>";
                }
                xmlRetorno = xmlRetorno + "</produtosmateriais>";


                return xmlRetorno;
            }
            catch (Exception e)
            {
                xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
                return xmlRetorno;
            }

        }

		[WebMethod]
		public string SelecionarEmpresaPorCnpj(string xmlString)
		{

			//Aqui irei instânciar a classe que utilizarei para fazer uma validação do xml recebido.
			Funcionalidades.ValidacaoXML clsValidar = new Funcionalidades.ValidacaoXML();

			//aqui instâncio a classe de empresas
			Classes.cls_empresa clsEmpresa = new Classes.cls_empresa();

			//aqui o objeto da empresa
			Objetos.objEmpresa objEmpresa = new Objetos.objEmpresa();

			//aqui variável para criação do xml de retorno
			string xmlRetorno = "";

			try
			{

				//aqui crio un xelemente a a partir do document criado
				XElement xmlElement = XElement.Parse(xmlString);

				//aqui transformo a strind de entrada em um documento xml de forma a poder trabalhar
				//com as tags contidas no mesmo de forma mais fácil e rápida
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(xmlString);


				XDocument docValidar = new XDocument();
				docValidar.Add(xmlElement);

				/*string resultado = clsValidar.ValidarSelecionarEmpresa(docValidar);
                if (resultado.Trim() != "")
                {
                    //aqui crio o xml de retorno em caso de erro.  Assim o serviço ou programa que chamou este método
                    //terá informações sobre o erro, podendo corrigir o mesmo e então refazer a chamada com o xml de entrada correto.
                    xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + resultado + "</descerro></retornoErro>";
                    return xmlRetorno;
                }*/

				XmlNodeList emen_cnpj = doc.GetElementsByTagName("emen_cnpj");


				DataSet dsEmpresas = new DataSet();
				dsEmpresas = clsEmpresa.SelecionarEmpresaPorCNPJ(emen_cnpj[0].InnerText);
				xmlRetorno = "<?xml version='1.0' encoding='iso-8859-1'?>";
				xmlRetorno = xmlRetorno + "<empresa>";

				int i = 0;
				if (dsEmpresas.Tables[0].Rows.Count > 0)
				{
					xmlRetorno = xmlRetorno + "<status>sucesso</status>";
					xmlRetorno = xmlRetorno + "<emen_cod_empr>" + dsEmpresas.Tables[0].Rows[i]["emen_cod_empr"].ToString() + "</emen_cod_empr>";
					xmlRetorno = xmlRetorno + "<emen_cnpj>" + dsEmpresas.Tables[0].Rows[i]["emen_cnpj"].ToString() + "</emen_cnpj>";
					xmlRetorno = xmlRetorno + "<emen_nome>" + dsEmpresas.Tables[0].Rows[i]["emen_nome"].ToString() + "</emen_nome>";
					xmlRetorno = xmlRetorno + "<emen_nomered>" + dsEmpresas.Tables[0].Rows[i]["emen_nomered"].ToString() + "</emen_nomered>";
					xmlRetorno = xmlRetorno + "<emen_lograd>" + dsEmpresas.Tables[0].Rows[i]["emen_lograd"].ToString() + "</emen_lograd>";
					xmlRetorno = xmlRetorno + "<emen_numero>" + dsEmpresas.Tables[0].Rows[i]["emen_numero"].ToString() + "</emen_numero>";
					xmlRetorno = xmlRetorno + "<emen_comple>" + dsEmpresas.Tables[0].Rows[i]["emen_comple"].ToString() + "</emen_comple>";
					xmlRetorno = xmlRetorno + "<emen_bairro>" + dsEmpresas.Tables[0].Rows[i]["emen_bairro"].ToString() + "</emen_bairro>";
					xmlRetorno = xmlRetorno + "<emen_cep>" + dsEmpresas.Tables[0].Rows[i]["emen_cep"].ToString() + "</emen_cep>";
					xmlRetorno = xmlRetorno + "<emen_uf>" + dsEmpresas.Tables[0].Rows[i]["emen_uf"].ToString() + "</emen_uf>";
					xmlRetorno = xmlRetorno + "<emen_cidade>" + dsEmpresas.Tables[0].Rows[i]["emen_cidade"].ToString() + "</emen_cidade>";
					xmlRetorno = xmlRetorno + "<emen_ddd>" + dsEmpresas.Tables[0].Rows[i]["emen_ddd"].ToString() + "</emen_ddd>";
					xmlRetorno = xmlRetorno + "<emen_tel>" + dsEmpresas.Tables[0].Rows[i]["emen_tel"].ToString() + "</emen_tel>";
					xmlRetorno = xmlRetorno + "<emen_email>" + dsEmpresas.Tables[0].Rows[i]["emen_email"].ToString() + "</emen_email>";
					xmlRetorno = xmlRetorno + "<emen_dt_criacao>" + Convert.ToDateTime(dsEmpresas.Tables[0].Rows[i]["emen_dt_criacao"].ToString()).ToString("MM/dd/yyyy") + "</emen_dt_criacao>";
					xmlRetorno = xmlRetorno + "<usur_cd_usur>" + dsEmpresas.Tables[0].Rows[i]["usur_cod_usur"].ToString() + "</usur_cd_usur>";
					xmlRetorno = xmlRetorno + "<emen_ativo>" + dsEmpresas.Tables[0].Rows[i]["emen_ativo"].ToString() + "</emen_ativo>";
					xmlRetorno = xmlRetorno + "<grem_cd_grem>" + dsEmpresas.Tables[0].Rows[i]["grem_cd_grem"].ToString() + "</grem_cd_grem>";
					xmlRetorno = xmlRetorno + "<grem_nome>" + dsEmpresas.Tables[0].Rows[i]["grem_nome"].ToString() + "</grem_nome>";
					xmlRetorno = xmlRetorno + "<emen_forn_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_forn_emen"].ToString() + "</emen_forn_emen>";
					xmlRetorno = xmlRetorno + "<emen_carregamento_emen>" + dsEmpresas.Tables[0].Rows[i]["emen_carregamento_emen"].ToString() + "</emen_carregamento_emen>";
					xmlRetorno = xmlRetorno + "<emen_nr_folhas_kit>" + dsEmpresas.Tables[0].Rows[i]["emen_nr_folhas_kit"].ToString() + "</emen_nr_folhas_kit>";

				}
				else
				{
					xmlRetorno = xmlRetorno + "<status>falha</status>";
				}
				xmlRetorno = xmlRetorno + "</empresa>";


				return xmlRetorno;
			}
			catch (Exception e)
			{
				xmlRetorno = "<?xml version='1.0' encoding='Windows-1252'?><retornoErro><status>Erro</status><descerro>" + e.Message + "</descerro></retornoErro>";
				return xmlRetorno;
			}
		}


	}
}
