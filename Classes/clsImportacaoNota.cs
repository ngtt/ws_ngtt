﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsImportacaoNota
    {
        public DataSet SelecionarErroNotaFiscaNumero(string nfer_numnota_nfer)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "SELECT nfer_cod_nfer, nfer_numnota_nfer, nfer_erro_nfer, nfer_dt_nfer FROM public.tb_nfer where nfer_numnota_nfer = '" + nfer_numnota_nfer + "'";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        public DataSet SelecionarErroNotaFiscaTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "SELECT nfer_cod_nfer, nfer_numnota_nfer, nfer_erro_nfer, nfer_dt_nfer FROM public.tb_nfer";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public void DeletarErroNotaFiscaNumero(string nfer_numnota_nfer)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "DELETE FROM public.tb_nfer WHERE nfer_numnota_nfer = '" + nfer_numnota_nfer + "'";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }

        public void UpdateErroNotaFiscaNumero(Objetos.objImportacaoNota objImportacaoNota)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "UPDATE public.tb_nfer SET nfer_erro_nfer='" + objImportacaoNota.Nfer_erro_nfer + "', nfer_dt_nfer='" + DateTime.Now + "'" +
                      " WHERE nfer_numnota_nfer = '" + objImportacaoNota.Nfer_numnota_nfer + "'";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();


            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }

        public void CadastrarErroNotaFiscal(Objetos.objImportacaoNota objImportacaoNota)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "INSERT INTO public.tb_nfer(nfer_numnota_nfer, nfer_erro_nfer, nfer_dt_nfer) " +
                      "VALUES ('" + objImportacaoNota.Nfer_numnota_nfer + "','" + objImportacaoNota.Nfer_erro_nfer + "','" + DateTime.Now + "')";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();


            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }
    }
}