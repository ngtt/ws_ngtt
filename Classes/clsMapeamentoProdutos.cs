﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsMapeamentoProdutos
    {

        public DataSet SelecionarMapeamentoProdutoTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando =   "select tb_mama.*, " +
                        "tb_prma_int.grem_cd_grem as grem_cd_grem_interno, " +
                        "tb_prma_int.prma_cod_prma as prma_cod_prma_interno, " +
                        "tb_prma_int.prma_descricao as prma_descricao_interno, " +
                        "tb_prma_ext.grem_cd_grem as grem_cd_grem_externo, " +
                        "tb_prma_ext.prma_cod_prma as prma_cod_prma_externo, " +
                        "tb_prma_ext.prma_descricao as prma_descricao_externo, " +
                        "tb_grem_int.grem_nome as grem_nome_interno, " +
                        "tb_grem_ext.grem_nome as grem_nome_externo " +
                        "from tb_mama inner join  tb_prma as tb_prma_int on tb_mama.prma_cod_prma_interno = tb_prma_int.prma_cod_prma " +
                        "inner join  tb_prma as tb_prma_ext on tb_mama.prma_cod_prma_empr = tb_prma_ext.prma_cod_prma " +
                        "inner join tb_grem as tb_grem_int on tb_prma_int.grem_cd_grem = tb_grem_int.grem_cd_grem " +
                        "inner join tb_grem as tb_grem_ext on tb_prma_ext.grem_cd_grem = tb_grem_ext.grem_cd_grem where mama_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarMapeamentoProdutoPorCodigo(int mama_cd_mama)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_mama where mama_cod_mama = " + mama_cd_mama + " and mama_ativo = true";
                       


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public int cadastraMapeamentoProdutoMaterial(Objetos.objMapeamentoProdutosMaterias objMapeamentoProdutosMaterias)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object mama_cod_mama;


            comando = "INSERT INTO public.tb_mama(prma_cod_prma_empr, prma_cod_prma_interno, mama_ativo, " +
                       "mama_dt_criacao, usur_cd_usur, grem_cd_grem_interno, grem_cd_grem_externo) " +
                       "VALUES (" + objMapeamentoProdutosMaterias.Prma_cod_prma_empr +
                       ", " + objMapeamentoProdutosMaterias.Prma_cod_prma_interno +
                       ", " + objMapeamentoProdutosMaterias.Mama_ativo +
                       ", '" + objMapeamentoProdutosMaterias.Mama_dt_criacao +
                       "', " + objMapeamentoProdutosMaterias.Usur_cd_usur +
                       ", '" + objMapeamentoProdutosMaterias.Grem_cd_grem_interno +
                       "','" + objMapeamentoProdutosMaterias.Grem_cd_grem_externo + "') returning mama_cod_mama";
                       



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            mama_cod_mama = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(mama_cod_mama);
        }

        public void AlterarMapeamentoProdutoMaterial(Objetos.objMapeamentoProdutosMaterias objMapeamentoProdutosMaterias)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            


            comando = "UPDATE tb_mama set mama_ativo = '" + objMapeamentoProdutosMaterias.Mama_ativo + "' where mama_cod_mama =" + objMapeamentoProdutosMaterias.Mama_cod_mama;




            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
            
        }


        public DataSet SelecionarMapeamentoProdutocodProdutoExterno(int prma_cod_prma_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando =   "select tb_mama.* from tb_mama  where prma_cod_prma_interno = " +  prma_cod_prma_empr + " and mama_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarMapeamentoProdutocodProdutoExternoEmpresaInterna(int prma_cod_prma_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_mama.* from tb_mama  where prma_cod_prma_empr = " + prma_cod_prma_empr + " and mama_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarMapeamentoProdutocodProdutoExternoFornecedora(int prma_cod_prma_empr, int Emen_cod_empr_forn)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            //comando = "select tb_mama.* from tb_mama where prma_cod_prma_interno = " + prma_cod_prma_empr + " and emen_cod_empr_forn = " + Emen_cod_empr_forn;

            //comando = "select tb_mama.* from tb_mama  inner join tb_emen on tb_emen.grem_cd_grem = tb_mama.grem_cd_grem_interno where tb_emen.emen_cod_empr = 37 and prma_cod_prma_interno =" + prma_cod_prma_empr;

            comando = "select distinct tb_mama.* from 	tb_mama  inner join tb_emen on tb_emen.grem_cd_grem = tb_mama.grem_cd_grem_interno " +
                      "where prma_cod_prma_interno = " + prma_cod_prma_empr  +
                      " and	grem_cd_grem_externo = (select grem_cd_grem from tb_emen where tb_emen.emen_cod_empr = " + Emen_cod_empr_forn + ") and mama_ativo = true";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }


        public DataSet SelecionarMapeamentoProdutoCodigoGrupo(int prma_cod_prma, int grem_cd_grem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";



            comando = "select * from tb_mama where (prma_cod_prma_empr = " + prma_cod_prma + " or prma_cod_prma_interno = " + prma_cod_prma + ") and (grem_cd_grem_interno = " + grem_cd_grem + " or grem_cd_grem_externo = " + grem_cd_grem + " and mama_ativo = true)";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarMapeamentoProdutocodProdutoExternoGrupos(int prma_cod_prma_empr, int grem_cd_grem_externo, int grem_cd_grem_interno)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_mama.* from tb_mama where prma_cod_prma_interno = " + prma_cod_prma_empr + " and grem_cd_grem_externo =" + grem_cd_grem_externo + " and grem_cd_grem_interno = "+ grem_cd_grem_interno + " and mama_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarMapeamentoProdutocodEmpresaFornecedora(int prma_cod_prma_empr, int grem_cd_grem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_mama.* from tb_mama  where prma_cod_prma_interno = " + prma_cod_prma_empr + " and grem_cd_grem_externo= " + grem_cd_grem + " and mama_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }


    }


}