﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsGrupoProdutos
    {
              

        public DataSet SelecionarGrupoPorCodigoProduto(int prma_cod_prma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select gppm.gppm_cd_gppm from tb_gppm gppm inner join tb_prma prma " +
                      "on gppm.gppm_cd_gppm = prma.gppm_cd_gppm " +
                      "where prma.prma_cod_prma = " + prma_cod_prma;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarGruposProdutosTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_gppm order by gppm_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }


		public DataSet selecionaRProdutosPorCodigoEmpresa(int emen_cod_empr)
		{
			clsBanco banco = new clsBanco();
			string comando = "";

			comando = "select gppm_cd_gppm, gppm_descricao from tb_gppm " +
					  "inner join tb_emen on tb_gppm.grem_cd_grem = tb_emen.grem_cd_grem " +
					  "where emen_cod_empr = " + emen_cod_empr;


			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();
			return ds;

			
		}

        public DataSet SelecionarTodosGruposFornecedores()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select distinct tb_grem.* from tb_grem inner join  tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_forn_emen = true order by grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarTodosGruposClientes()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select distinct tb_grem.* from tb_grem inner join  tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_forn_emen = false order by grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }
    }



    
}