﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsKits
    {
        public DataSet SelecionarKitProduto(int prma_cod_prma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "SELECT kits_cod_kits, kits_descricao, kits_dt_criacao, usur_cd_usur, " +
                      "kits_quant_prod_prin, kits_quant_pallets, kits_quant_topo, kits_quant_folha, " +
                      "kits_quant_por_camada, prma_cod_prma " +
                      "FROM tb_kits where prma_cod_prma=" + prma_cod_prma;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarKitsTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "SELECT kits_cod_kits, kits_descricao, kits_dt_criacao, tb_kits.usur_cd_usur, " +
                      "kits_quant_prod_prin, kits_quant_pallets, kits_quant_topo, kits_quant_folha, " +
                      "kits_quant_por_camada, tb_kits.prma_cod_prma, prma_descricao " +
                      "FROM tb_kits inner join tb_prma on tb_kits.prma_cod_prma = tb_prma.prma_cod_prma order by kits_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public int Cadastrarkit(Objetos.objKits objKits)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int kits_cod_kits = 0;


            comando = "INSERT INTO public.tb_kits(kits_descricao, kits_dt_criacao, usur_cd_usur, kits_quant_prod_prin, " +
                      "kits_quant_pallets, kits_quant_topo, kits_quant_folha, kits_quant_por_camada, prma_cod_prma) " +
                      "VALUES ('" +objKits.Kits_descricao + "','" + objKits.Kits_dt_criacao + "'," + objKits.Usur_cd_usur +
                               "," + objKits.Kits_quant_prod_prin.ToString().Replace(",", ".") + "," + objKits.Kits_quant_pallets.ToString().Replace(",", ".") + "," + objKits.Kits_quant_topo.ToString().Replace(",", ".") +
                               "," + objKits.Kits_quant_folha.ToString().Replace(",", ".") + "," + objKits.Kits_quant_por_camada.ToString().Replace(",", ".") + "," + objKits.Prma_cod_prma + ") returning kits_cod_kits";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            kits_cod_kits = Convert.ToInt32(cmd.ExecuteScalar());
            connection.Close();

            return Convert.ToInt32(kits_cod_kits);
        }

        public void alterarKit(Objetos.objKits objKits)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE public.tb_kits " +
                      "SET kits_descricao='" + objKits.Kits_descricao + "', " +
                      "kits_dt_criacao='" + objKits.Kits_dt_criacao + "', " +
                      "usur_cd_usur=" + objKits.Usur_cd_usur + ", " + 
                      "kits_quant_prod_prin=" + objKits.Kits_quant_prod_prin.ToString().Replace(",",".") + ", " +
                      "kits_quant_pallets=" + objKits.Kits_quant_pallets.ToString().Replace(",",".") + ", " +
                      "kits_quant_topo=" + objKits.Kits_quant_topo.ToString().Replace(",",".") + ", " + 
                      "kits_quant_folha=" + objKits.Kits_quant_folha.ToString().Replace(",",".") + ", " +
                      "kits_quant_por_camada=" + objKits.Kits_quant_por_camada.ToString().Replace(",",".") + ", " +
                      "prma_cod_prma= " + objKits.Prma_cod_prma + " WHERE kits_cod_kits = " + objKits.Kits_cod_kits;



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet SelecionarKitsporCodigo(int kits_cod_kits)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "SELECT kits_cod_kits, kits_descricao, kits_dt_criacao, tb_kits.usur_cd_usur, " +
                      "kits_quant_prod_prin, kits_quant_pallets, kits_quant_topo, kits_quant_folha, " +
                      "kits_quant_por_camada, tb_kits.prma_cod_prma, prma_descricao " +
                      "FROM tb_kits inner join tb_prma on tb_kits.prma_cod_prma = tb_prma.prma_cod_prma " +
                      "where kits_cod_kits = " + kits_cod_kits + " order by kits_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }
    }
}