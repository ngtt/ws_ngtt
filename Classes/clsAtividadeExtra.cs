﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
namespace wsngtt.Classes
{
    public class clsAtividadeExtra
    {
        public void cadastrarAlterarAtividadesExtra(List<Objetos.objAtividadeExtra> objAtividadeExtra)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            NpgsqlCommand cmd = new NpgsqlCommand();

            comando = "delete from tb_usae where usur_cd_usur = " + objAtividadeExtra[0].Usur_cd_usur;
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();

            int usur_cd_usur = objAtividadeExtra[0].Usur_cd_usur;

            for (int i = 0;i <= objAtividadeExtra.Count - 1;i++)
            {
                comando = "insert into tb_usae(usur_cd_usur, ativ_cod_ativ) " +
                          "values(" + usur_cd_usur + "," + objAtividadeExtra[i].Ativ_cod_ativ + ")";

                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
                
            }
            connection.Close();
        }
    }
}