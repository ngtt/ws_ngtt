﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsFamiliaProdutos
    {
        public int cadastraFamiliaprodutos(Objetos.objFamiliaProdutos objFamiliaProdutos)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object famp_cd_famp;


            comando = "INSERT INTO public.tb_famp(famp_descricao, famp_dt_criacao, usur_cd_usur) " +
                      "VALUES ('" + objFamiliaProdutos.Famp_descricao + "','" + objFamiliaProdutos.Famp_dt_criaca + "'," + objFamiliaProdutos.Usur_cd_usur + ") returning famp_cd_famp";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            famp_cd_famp = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(famp_cd_famp);
        }

        public void alterarFamiliaProduto(Objetos.objFamiliaProdutos objFamiliaProdutos)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE public.tb_famp SET " +
                      "famp_descricao='" + objFamiliaProdutos.Famp_descricao + "', famp_dt_criacao='" + objFamiliaProdutos.Famp_dt_criaca + "', usur_cd_usur=" + objFamiliaProdutos.Usur_cd_usur +
                      "WHERE famp_cd_famp = " + objFamiliaProdutos.Famp_cd_famp;



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet SelecionarTodasFamiliaProdutos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_famp order by famp_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;
        }

        public DataSet SelecionarFamiliaPorCodigo(int famp_cd_famp)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_famp where famp_cd_famp = " + famp_cd_famp + " order by famp_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;
        }
    }
}