﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsSaldoEstoque
    {
        public void atualizarEstoque(Objetos.objSaldoEstoque objSaldoEstoque, Npgsql.NpgsqlConnection connection, int empr_cod_empr_origem, int empr_cd_empr_destino)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            NpgsqlCommand cmd = new NpgsqlCommand();
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter();

            DataSet ds = new DataSet();

            if (objSaldoEstoque.Tpop_cod_tpop == 1)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 1 and emen_cod_empr = " + empr_cod_empr_origem + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + empr_cod_empr_origem + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",1)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


                //aqui vou verificar se já existe para a empresa e para o produto em questão um registro com o código 3 (disponivel) caso tenha vou remover a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + empr_cod_empr_origem + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + empr_cod_empr_origem + "," + objSaldoEstoque.Prma_cod_prma + "," + (objSaldoEstoque.Sles_quantidade * -1).ToString().Replace(",", ".") + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();

                }
            }
            else if (objSaldoEstoque.Tpop_cod_tpop == 2)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 2 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + empr_cd_empr_destino + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",2)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


                //aqui vou verificar se já existe para a empresa e para o produto em questão um registro com o código 3 (disponivel) caso tenha vou remover a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 1 and emen_cod_empr = " + empr_cod_empr_origem + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
            }
            else if (objSaldoEstoque.Tpop_cod_tpop == 3)
            {
                
                //aqui vou verificar se tem o 2 (em conferencia) com o codigo da empresa de destino e se tiver vou tirar a quantidade informada do saldo
                comando = "select * from tb_sles where tpmo_cod_tpmo = 2 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

                //aqui vou vereificar se existe o codigo 3 (disponível) para a empresa de destino, se existir vou somar ao código, caso contrario vou inserir o novo ponto de estoque
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + empr_cd_empr_destino + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }
            //este método usa o codigo 33, pois, refere-se a entrada de inventário e o código deveria ser 3
            //porém, apenas para diferenciar que é de inventário e não normal, colocamos o 33, pois, existem diferenças
            //na entrada normal e na feita pelo inventário
            else if (objSaldoEstoque.Tpop_cod_tpop == 33)
            {

                //aqui vou vereificar se existe o codigo 3 (disponível) para a empresa de destino, se existir vou somar ao código, caso contrario vou inserir o novo ponto de estoque
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + empr_cd_empr_destino + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }
            else if (objSaldoEstoque.Tpop_cod_tpop == 4)
            {
                //caso tenha vindo o 4 ou seja o produto não foi enviado, então vou pegar a empresa origem o estatus 1 (em transito) e diminuir a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 1 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                //agora vou varificar se a empresa orgime tem o estoque do produto com codigo 3 e vou somar a quantidade ao mesmo, já que removi na hora que foi colocado no carregamento
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                //aqui vou vereificar se existe o codigo 3 para a empresa de destino, se existir vou somar ao código, caso contrario vou inserir o novo ponto de estoque
                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString().Replace(",", ".");
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + empr_cd_empr_destino + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

            }
            else if (objSaldoEstoque.Tpop_cod_tpop == 8 || objSaldoEstoque.Tpop_cod_tpop == 9)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }

            else if (objSaldoEstoque.Tpop_cod_tpop == 5)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                ds = new DataSet();
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + (Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) * -1) + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }




                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 5 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + (Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", "."))) + ",5)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

            }
            else if (objSaldoEstoque.Tpop_cod_tpop == 6)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                ds = new DataSet();
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + (Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) * -1) + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }




                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 6 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + (Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", "."))) + ",6)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

            }

            else if (objSaldoEstoque.Tpop_cod_tpop == 10)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {

                }


                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 6 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }



            }

            else if (objSaldoEstoque.Tpop_cod_tpop == 0)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) * -1 + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }
            else if (objSaldoEstoque.Tpop_cod_tpop == -1)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }

            else if (objSaldoEstoque.Tpop_cod_tpop == -2)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 6 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                {

                    comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                    cmd.CommandText = comando;
                    cmd.Connection = connection;
                    adap = new NpgsqlDataAdapter(cmd);
                    ds = new DataSet();
                    adap.Fill(ds);

                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) * -1 + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

                comando = "select * from tb_sles where tpmo_cod_tpmo = 1 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) + ",1)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }
            else if (objSaldoEstoque.Tpop_cod_tpop == -3)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 5 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                {

                    comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                    cmd.CommandText = comando;
                    cmd.Connection = connection;
                    adap = new NpgsqlDataAdapter(cmd);
                    ds = new DataSet();
                    adap.Fill(ds);

                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                             "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) * -1 + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

                comando = "select * from tb_sles where tpmo_cod_tpmo = 6 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) + ",6)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

            }
            else if (objSaldoEstoque.Tpop_cod_tpop == -4)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                             "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 5 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

            }
            else if (objSaldoEstoque.Tpop_cod_tpop == -5)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 3 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                ds = new DataSet();
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + Convert.ToInt32(objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".")) + ",3)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }
            else if (objSaldoEstoque.Tpop_cod_tpop == -6)
            {
                //aqui vou verificar se tem o 2 (em conferencia) com o codigo da empresa de destino e se tiver vou tirar a quantidade informada do saldo
                comando = "select * from tb_sles where tpmo_cod_tpmo = 2 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

            }



            else if (objSaldoEstoque.Tpop_cod_tpop == -7)
            {
                //aqui vou verificar se tem o 2 (em conferencia) com o codigo da empresa de destino e se tiver vou tirar a quantidade informada do saldo
                comando = "select * from tb_sles where tpmo_cod_tpmo = 2 and emen_cod_empr = " + empr_cd_empr_destino + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade -" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }

            }
            else if (objSaldoEstoque.Tpop_cod_tpop == 11 )
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 5 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",5)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }
            else if (objSaldoEstoque.Tpop_cod_tpop == 12)
            {
                //aqui vou verificar se já existe para a empresa de origem o registro e se tiver vou atualizar adicionando a quantidade
                comando = "select * from tb_sles where tpmo_cod_tpmo = 6 and emen_cod_empr = " + objSaldoEstoque.Emen_cod_empresa + " and prma_cod_prma =" + objSaldoEstoque.Prma_cod_prma;
                cmd.CommandText = comando;
                cmd.Connection = connection;
                adap = new NpgsqlDataAdapter(cmd);
                adap.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    comando = "update tb_sles set sles_quantidade = sles_quantidade +" + objSaldoEstoque.Sles_quantidade.ToString().Trim().Replace(",", ".") + " where sles_cod_sles = " + ds.Tables[0].Rows[0]["sles_cod_sles"].ToString();
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    comando = "INSERT INTO public.tb_sles(emen_cod_empr, prma_cod_prma, sles_quantidade,tpmo_cod_tpmo) " +
                              "VALUES (" + objSaldoEstoque.Emen_cod_empresa + "," + objSaldoEstoque.Prma_cod_prma + "," + objSaldoEstoque.Sles_quantidade.ToString().Replace(",", ".") + ",6)";
                    cmd.Connection = connection;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }


            }
        }


        public DataSet SelecionarEstoqueProdutoDisponivel(string emen_cod_empr, string prma_cod_prma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_sles where prma_cod_prma = " + prma_cod_prma + " and emen_cod_empr = " + emen_cod_empr +
                      "and (tpmo_cod_tpmo = 3) order by sles_cod_sles desc";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarEstoqueProduto(string emen_cod_empr, string prma_cod_prma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_sles where prma_cod_prma = " + prma_cod_prma + " and emen_cod_empr = " + emen_cod_empr +
                      "and (tpmo_cod_tpmo = 2 or tpmo_cod_tpmo = 3 or tpmo_cod_tpmo = 5 or tpmo_cod_tpmo = 6 or tpmo_cod_tpmo = 7)";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarEstoqueFechadoProduto(string emen_cod_empr, string prma_cod_prma, string data)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_fees where prma_cod_prma = " + prma_cod_prma + " and emen_cod_empr = " + emen_cod_empr +
                      "and (tpmo_cod_tpmo = 2 or tpmo_cod_tpmo = 3 or tpmo_cod_tpmo = 5 or tpmo_cod_tpmo = 6 or tpmo_cod_tpmo = 7) and fees_dt_fechamento ='" + data + "'";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarEstoqueConsolidadoProduto(string emen_cod_empr, string prma_cod_prma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_sles inner join tb_prma.prma_cod_prma = tb_sles.prma_cod_prma where prma_cod_prma = " + prma_cod_prma + " and emen_cod_empr = " + emen_cod_empr +
                      "and tb_prma.famp_cd_famp = 4  (tpmo_cod_tpmo = 2 or tpmo_cod_tpmo = 3 or tpmo_cod_tpmo = 5 or tpmo_cod_tpmo = 6 or tpmo_cod_tpmo = 7)";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public string FechamentoEstoque(string emen_cod_empr)
        {
            try
            {
                clsBanco banco = new clsBanco();
                string comando = "";

                comando = "insert into tb_fees (SELECT sles_cod_sles, emen_cod_empr, prma_cod_prma, sles_quantidade, tpmo_cod_tpmo, CURRENT_TIMESTAMP from tb_sles where emen_cod_empr = " + emen_cod_empr.Trim() + ")";



                NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
                connection.Open();
                DataSet ds = new DataSet();

                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

                adap.Fill(ds);
                connection.Close();

                return "";
            }
            catch (Exception e)
            {
                return e.Message;

            }

        }



        public DataSet SelecionarEstoqueFechamentoDatas()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select distinct fees_dt_fechamento from tb_fees";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }


    }
}