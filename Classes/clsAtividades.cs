﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsAtividades
    {
        public DataSet SelecionarAtividadesExtrasUsuarios(int usur_cd_usur)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select tb_ativ.* from tb_usae  inner join tb_ativ on tb_usae.ativ_cod_ativ = tb_ativ.ativ_cod_ativ where tb_usae.usur_cd_usur =" + usur_cd_usur + " order by tb_ativ.ativ_nm_ativ";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarAtividadesForaPerfil(int perf_cod_perf)
        {
            clsBanco banco = new clsBanco();
            string comando = "Select * from tb_ativ where ativ_cod_ativ not in(" +
                             "Select ativ_cod_ativ from tb_atpf where perf_cod_perf =" + perf_cod_perf + ") order by ativ_nm_ativ";

            

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarTodasPermissoesUsuario(int usur_cd_usur)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_ativ.* from tb_ativ inner join tb_atpf on tb_ativ.ativ_cod_ativ = tb_atpf.ativ_cod_ativ inner join tb_perf on tb_atpf.perf_cod_perf = tb_perf.perf_cod_perf inner join tb_usur on tb_usur.perf_cod_perfil = tb_perf.perf_cod_perf where usur_cd_usur = " + usur_cd_usur +
                      " union " +
                      "select tb_ativ.* from tb_ativ inner join tb_usae on tb_ativ.ativ_cod_ativ = tb_usae.ativ_cod_ativ where usur_cd_usur = " + usur_cd_usur + " order by ativ_nm_ativ";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarAtividadesTodas()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_ativ order by ativ_nm_ativ";
                      
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }


    }
}