﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsRecebimento
    {
        public string cadastraRecebimento(List<Objetos.objMovimentoEstoque> objMovimentoEstoque, List<Objetos.objSaldoEstoque> objSaldoEstoque, int empr_cod_empr_origem, int empr_cd_empr_destino, int moto_cod_moto, int veic_cod_veic, string canf_cod_canf, string tran_cod_tran, string veic_plac, string placa_velha, string cpf_motorista = null)
        {
            string erro = "";

            clsBanco banco = new clsBanco();
            

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            clsVeiculos clsVeiculo = new clsVeiculos(); 
            clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsNotaFiscal clsNota = new clsNotaFiscal();
            Objetos.objVeiculos objVeic = new Objetos.objVeiculos();
            clsNFVE clsNFVE = new clsNFVE();
            try
            {
                int veic_cod_veic_insert = 0;

                if (veic_cod_veic <= 0)
                {
                    objVeic.Tpve_cod_tpve = 10;
                    objVeic.Usur_cd_usur = 5;
                    objVeic.Usur_cd_usur1 = 5;
                    objVeic.Veic_descricao = "";
                    objVeic.Veic_dt_criacao = DateTime.Now;
                    objVeic.Veic_placa = veic_plac;
                    veic_cod_veic_insert = clsVeiculo.CadastrarVeiculo(objVeic);
                }
                else
                {
                    veic_cod_veic_insert = veic_cod_veic;
				



				}
				clsNFVE.atualizarVeicTrocarPlaca(canf_cod_canf, placa_velha , veic_plac);


				for (int i = 0; i <= objMovimentoEstoque.Count - 1; i++)
                {
                    clsNota.AlterarMotoristaVeiculoNF(connection, moto_cod_moto, veic_cod_veic_insert, objMovimentoEstoque[i].Canf_cod_canf.ToString(), cpf_motorista);
                    clsNota.AlterarTransportadoraNF(connection, Convert.ToInt32(tran_cod_tran), objMovimentoEstoque[i].Canf_cod_canf.ToString());
                    clsMovimentoEstoque.cadastramovimentoEstoque(objMovimentoEstoque[i], connection, empr_cd_empr_destino);
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque[i], connection, empr_cod_empr_origem, empr_cd_empr_destino);
                    clsNFVE.atualizarVeicPlacaRecebimento(objMovimentoEstoque[i].Canf_cod_canf.ToString(), veic_plac);
                }
                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                erro = ex.Message;
                return erro;
            }
        }

        public string cadastraRecebimentoConferencia(List<Objetos.objMovimentoEstoque> objMovimentoEstoque, List<Objetos.objSaldoEstoque> objSaldoEstoque, int empr_cod_empr_origem, int empr_cd_empr_destino, int moto_cod_moto, int veic_cod_veic)
        {
            string erro = "";

            clsBanco banco = new clsBanco();


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsNotaFiscal clsNota = new clsNotaFiscal();

            try
            {


                for (int i = 0; i <= objMovimentoEstoque.Count - 1; i++)
                {
                    clsMovimentoEstoque.cadastramovimentoEstoque(objMovimentoEstoque[i], connection, empr_cd_empr_destino);
                 

                }
                for (int i = 0; i <= objSaldoEstoque.Count - 1; i++)
                {
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque[i], connection, empr_cod_empr_origem, empr_cd_empr_destino);
                }

                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                erro = ex.Message;
                return erro;
            }
        }


        public string cadastraRecebimentoKitRuim(List<Objetos.objMovimentoEstoque> objMovimentoEstoque, List<Objetos.objSaldoEstoque> objSaldoEstoque, int empr_cod_empr_origem, int empr_cd_empr_destino, int moto_cod_moto, int veic_cod_veic, string canf_cod_canf)
        {
            string erro = "";

            clsBanco banco = new clsBanco();


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsNotaFiscal clsNota = new clsNotaFiscal();
            try
            {

                clsNota.AlterarMotoristaVeiculoNF(connection, moto_cod_moto, veic_cod_veic, canf_cod_canf);

                for (int i = 0; i <= objMovimentoEstoque.Count - 1; i++)
                {
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque[i], connection, empr_cod_empr_origem, empr_cd_empr_destino);

                }
                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                erro = ex.Message;
                return erro;
            }
        }
    }
}