﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsDiferencaConferencia
    {
        public string cadastraDiferencaConferencia(List<Objetos.ObjDiferencaConferencia> ObjDiferencaConferencia, List<Objetos.objMovimentoEstoque> objMovimentoEstoque, List<Objetos.objSaldoEstoque> objSaldoEstoque,  int empr_cod_empr_origem, int empr_cd_empr_destino)
        {
            string erro = "";

            clsBanco banco = new clsBanco();
            string comando = "";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            Classes.clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            Classes.clsMapeamentoProdutos clsMapeamentoProdutos = new clsMapeamentoProdutos();
            clsSaldoEstoque clsSaldoEstoque  = new clsSaldoEstoque();
            try
            {
               
                

                for (int i = 0; i <= ObjDiferencaConferencia.Count - 1; i++)
                {
                    if (ObjDiferencaConferencia[i].Dirc_qtde_nf != ObjDiferencaConferencia[i].Dirc_qtde_recbto)
                    {
                        if (ObjDiferencaConferencia[i].Dirc_qtde_nf * 1000 != ObjDiferencaConferencia[i].Dirc_qtde_recbto)
                        {
                            comando = "INSERT INTO public.tb_dirc(itnf_cod_itnf, dirc_qtde_recbto, dirc_dt_criacao, usur_cd_usur, dirc_qtde_nf, prma_cod_prma, canf_cod_canf) " +
                                      "VALUES (" + ObjDiferencaConferencia[i].Itnf_cod_itnf + ", " + ObjDiferencaConferencia[i].Dirc_qtde_recbto.ToString().Replace(",", ".") + ", '" + ObjDiferencaConferencia[i].Dirc_dt_criacao + "', " + ObjDiferencaConferencia[i].Usur_cd_usur + "," + ObjDiferencaConferencia[i].Dirc_qtde_nf.ToString().Replace(",", ".") + "," + ObjDiferencaConferencia[i].Prma_cod_prma + "," + ObjDiferencaConferencia[i].Canf_cod_canf + ") " +
                                      "returning dirc_cod_dirc ";

                            NpgsqlCommand cmd = new NpgsqlCommand();
                            cmd.CommandText = comando;
                            cmd.Connection = connection;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    DataSet mape = new DataSet();
                    for (int j =0;j<= objMovimentoEstoque.Count - 1;j++)
                    {
                        if (objMovimentoEstoque[j].Tpmo_cod_tpmo == 4)
                        {
                            mape = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoEmpresaInterna(Convert.ToInt32(objMovimentoEstoque[j].Prma_cod_prma));
                            if (mape.Tables[0].Rows.Count >0)
                            {
                                objMovimentoEstoque[j].Prma_cod_prma = Convert.ToInt32(mape.Tables[0].Rows[0]["prma_cod_prma_interno"].ToString());
                            }
                        }
                    }

                  
                    clsMovimentoEstoque.cadastramovimentoEstoque(objMovimentoEstoque[i], connection, empr_cd_empr_destino);
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque[i], connection, empr_cod_empr_origem, empr_cd_empr_destino);
                }
                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                erro = ex.Message;
                return erro;
            }
        }
    }



}