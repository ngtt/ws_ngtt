﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
using System.IO;

namespace wsngtt.Classes
{
    public class clsNivelEstoque
    {

        public int InserirNivelEstoque(Objetos.objNVES objNVES)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object nves_cod_nves;


            comando = "INSERT INTO public.tb_nves(prma_cod_prma, emen_cod_empr, nves_baixo, nves_regular, nves_alto, nves_risco, nves_dt_criacao, usur_cd_usur) " +
                      "VALUES ('" + objNVES.Prma_cod_prma + "','" + objNVES.Emen_cod_empr + "','" + objNVES.Nves_baixo + "'," +
                      "'" + objNVES.nves_regular + "','" + objNVES.nves_alto + "','" + objNVES.nves_risco + "','" + objNVES.nves_dt_criacao + "'," + objNVES.Usur_cd_usur + ") returning nves_cod_nves";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            nves_cod_nves = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(nves_cod_nves);
        }

        public DataSet SelecionarNivelEstoqueEmpresa(int emen_cod_empr)
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select emen.emen_nome,prma.prma_descricao,nves.nves_cod_nves,nves.prma_cod_prma,nves.emen_cod_empr,nves.nves_baixo, " +
		              "nves.nves_regular,nves.nves_alto,nves.nves_risco " +
                      "from " +
                      "tb_nves nves inner join tb_emen emen on emen.emen_cod_empr = nves.emen_cod_empr " +
                      "inner join tb_prma prma on prma.prma_cod_prma = nves.prma_cod_prma " +
                      "where " +
                      "emen.emen_cod_empr = " + emen_cod_empr;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }


        public void AlterarNivelEstoque(Objetos.objNVES objNVES)
        {
            clsBanco banco = new clsBanco();
            string comando = "UPDATE public.tb_nves SET " +
                             "prma_cod_prma='" + objNVES.Prma_cod_prma + "'," +
                             "emen_cod_empr='" + objNVES.Emen_cod_empr + "'," +
                             "nves_baixo='" + objNVES.Nves_baixo + "'," +
                             "nves_alto='" + objNVES.Nves_alto + "'," +
                             "nves_regular='" + objNVES.Nves_regular + "'," +
                             "nves_risco='" + objNVES.Nves_risco + "'," +
                             "nves_dt_criacao='" + objNVES.Nves_dt_criacao + "'," +
                             "usur_cd_usur=" + objNVES.Usur_cd_usur + " " +
                             "WHERE nves_cod_nves = " + objNVES.Nves_cod_nves;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }

        public DataSet SelecionarNivelPorCodigo(int nves_cod_nves)
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_nves where nves_cod_nves = " + nves_cod_nves;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }
    }

}
