﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
using System.IO;

namespace wsngtt.Classes
{
    public class clsPrevisaoProducao
    {

        public int InserirPrevisaoProducao(Objetos.objPrevisaoProducao objPlpv)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object plpv_cod_plpv;


            comando = "INSERT INTO public.tb_pvpl( " +
            "emen_cod_empr, pvpl_num_semana_ano, " +
            "pvpl_dt_d0, pvpl_dt_d1, pvpl_dt_d2, pvpl_dt_d3, pvpl_dt_d4, pvpl_dt_d5, " +
            "pvpl_dt_d6, pvpl_qtd_d0, pvpl_qtd_d1, pvpl_qtd_d2, pvpl_qtd_d3, " +
            "pvpl_qtd_d4, pvpl_qtd_d5, pvpl_qtd_d6, pvpl_qtd_semana1, pvpl_qtd_semana2, " +
			"pvpl_qtd_semana3, pvpl_qtd_semana4, pvpl_num_versao, gppm_cd_gppm, usur_cd_usur ) " +
            "VALUES( " + objPlpv.Emen_cod_empr + ", " +
            objPlpv.Pvpl_num_semana_ano + ", " +
            "'" + objPlpv.Pvpl_dt_d0 + "', " +
            "'" + objPlpv.Pvpl_dt_d1 + "', " +
            "'" + objPlpv.Pvpl_dt_d2 + "', " +
            "'" + objPlpv.Pvpl_dt_d3 + "', " +
            "'" + objPlpv.Pvpl_dt_d4 + "', " +
            "'" + objPlpv.Pvpl_dt_d5 + "', " +
            "'" + objPlpv.Pvpl_dt_d6 + "', " +
            objPlpv.Pvpl_qtd_d0 + ", " +
            objPlpv.Pvpl_qtd_d1 + ", " +
            objPlpv.Pvpl_qtd_d2 + ", " +
            objPlpv.Pvpl_qtd_d3 + ", " +
            objPlpv.Pvpl_qtd_d4 + ", " +
            objPlpv.Pvpl_qtd_d5 + ", " +
            objPlpv.Pvpl_qtd_d6 + ", " +
            objPlpv.Pvpl_qtd_semana1 + ", " +
            objPlpv.Pvpl_qtd_semana2 + ", " +
            objPlpv.Pvpl_qtd_semana3 + ", " +
            objPlpv.Pvpl_qtd_semana4 + ", " +
            objPlpv.Pvpl_num_versao + ", " +
			"'" + objPlpv.Gppm_cd_gppm + "', " +
			objPlpv.Usur_cd_usur + ") returning pvpl_cod_pvpl";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            plpv_cod_plpv = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(plpv_cod_plpv);
        }

        public DataSet SelecionarPrevisaoProducaoEmpresa(int emen_cod_empr)
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select  cblp_sq_cabecalho_linha_producao as id, cblp_dt_inicial, cblp_dt_final, cblp_int_versao from tb_cblp where emen_cod_empr = " + emen_cod_empr + " order by cblp_dt_final desc;";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }


        public void AlterarPrevisaoProducao(Objetos.objPrevisaoProducao objPlpv)
        {
            clsBanco banco = new clsBanco();
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            NpgsqlCommand cmd = new NpgsqlCommand();

            string comando = "insert into tb_pvpl_h " +
                             "select * from tb_pvpl where pvpl_cod_pvpl = " + objPlpv.Pvpl_cod_pvpl;

            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();

            comando = "UPDATE public.tb_pvpl " +
            "SET " +
            "pvpl_num_semana_ano = " + objPlpv.Pvpl_num_semana_ano + ", " +
            "pvpl_dt_d0 = '" + objPlpv.Pvpl_dt_d0 + "', " +
            "pvpl_dt_d1 = '" + objPlpv.Pvpl_dt_d1 + "', " +
            "pvpl_dt_d2 = '" + objPlpv.Pvpl_dt_d2 + "', " +
            "pvpl_dt_d3 = '" + objPlpv.Pvpl_dt_d3 + "', " +
            "pvpl_dt_d4 = '" + objPlpv.Pvpl_dt_d4 + "', " +
            "pvpl_dt_d5 = '" + objPlpv.Pvpl_dt_d5 + "', " +
            "pvpl_dt_d6 = '" + objPlpv.Pvpl_dt_d6 + "', " +
            "pvpl_qtd_d0 = " + objPlpv.Pvpl_qtd_d0 + ", " +
            "pvpl_qtd_d1 = " + objPlpv.Pvpl_qtd_d1 + ", " +
            "pvpl_qtd_d2 = " + objPlpv.Pvpl_qtd_d2 + ", " +
            "pvpl_qtd_d3 = " + objPlpv.Pvpl_qtd_d3 + ", " +
            "pvpl_qtd_d4 = " + objPlpv.Pvpl_qtd_d4 + ", " +
            "pvpl_qtd_d5 = " + objPlpv.Pvpl_qtd_d5 + ", " +
            "pvpl_qtd_d6 = " + objPlpv.Pvpl_qtd_d5 + ", " +
            "pvpl_qtd_semana1 = " + objPlpv.Pvpl_qtd_semana1 + ", " +
            "pvpl_qtd_semana2 = " + objPlpv.Pvpl_qtd_semana2 + ", " +
            "pvpl_qtd_semana3 = " + objPlpv.Pvpl_qtd_semana3 + ", " +
            "pvpl_qtd_semana4 = " + objPlpv.Pvpl_qtd_semana4 + ", " +
            "pvpl_num_versao = " + objPlpv.Pvpl_num_versao + ", " +
			"gppm_cd_gppm =  '" + objPlpv.Gppm_cd_gppm + "', " +
			"usur_cd_usur = " + objPlpv.Usur_cd_usur +
            "WHERE pvpl_cod_pvpl = " + objPlpv.Pvpl_cod_pvpl;

            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }

        public DataSet SelecionarPlanoProducaoCodigo(int pvpl_cod_pvpl)
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_pvpl where pvpl_cod_pvpl = " + pvpl_cod_pvpl;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }
    }

}
