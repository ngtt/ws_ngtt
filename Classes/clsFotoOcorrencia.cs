﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsFotoOcorrencia
    {

        public int cadastrarFotoOcorrencia(Objetos.objFotoOcorrencias objFotoOcorrencias)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object ftoc_cod_ftoc;


            comando = "INSERT INTO public.tb_ftoc(rnco_cod_rnco, ftoc_nomeimagem_ftoc) " +
                      "VALUES (" + objFotoOcorrencias.Rnco_cod_rnco + ",'" + objFotoOcorrencias.Ftoc_nomeimagem_ftoc + "')" + 
                      "returning ftoc_cod_ftoc";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            ftoc_cod_ftoc = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(ftoc_cod_ftoc);
        }
    }
}