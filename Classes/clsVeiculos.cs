﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsVeiculos
    {
        public int CadastrarVeiculo(Objetos.objVeiculos objVeiculos)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int veic_cod_veic = 0;


            comando = "INSERT INTO public.tb_veic(veic_placa, tpve_cod_tpve, veic_descricao, veic_ativo, " +
                      "veic_dt_criacao, usur_cd_usur) " +
                      "VALUES ('" + objVeiculos.Veic_placa + "'," + objVeiculos.Tpve_cod_tpve + ",'" + objVeiculos.Veic_descricao + "','" +
                                    objVeiculos.Veic_ativo + "','" + objVeiculos.Veic_dt_criacao + "'," + objVeiculos.Usur_cd_usur + ") returning veic_cod_veic";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            veic_cod_veic = Convert.ToInt32(cmd.ExecuteScalar());
            connection.Close();

            return Convert.ToInt32(veic_cod_veic);
        }

        public void alterarVeiculo(Objetos.objVeiculos objVeiculos)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE public.tb_veic SET " +
                      "veic_placa='" + objVeiculos.Veic_placa + "'," +
                      "tpve_cod_tpve= " + objVeiculos.Tpve_cod_tpve + "," +
                      "veic_descricao='" + objVeiculos.Veic_descricao + "'," +
                      "veic_ativo='" + objVeiculos.Veic_ativo + "'," +
                      "veic_dt_criacao='" + objVeiculos.Veic_dt_criacao + "'," +
                      "usur_cd_usur=" + objVeiculos.Usur_cd_usur +
                      "WHERE veic_cod_veic = " + objVeiculos.Veic_cod_veic;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet SelecionarTodosVeiculos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select tb_veic.*, tb_tpve.* from tb_veic inner join tb_tpve on tb_veic.tpve_cod_tpve = tb_tpve.tpve_cod_tpve order by veic_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarUmVeiculos(int veic_cod_veic)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_veic where veic_cod_veic = " + veic_cod_veic;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

		public DataSet SelecionarUmVeiculoPlaca(string veic_placa)
		{
			clsBanco banco = new clsBanco();
			string comando = "";

			comando = "Select * from tb_veic where veic_placa = '" + veic_placa + "'";


			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();
			return ds;
		}

	}
}