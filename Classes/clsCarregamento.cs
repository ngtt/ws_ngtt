﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;


namespace wsngtt.Classes
{
    public class clsCarregamento
    {
        public string cadastraCarregamento(List<Objetos.objMovimentoEstoque> objMovimentoEstoque, List<Objetos.objSaldoEstoque> objSaldoEstoque, int moto_cod_moto, int veic_cod_veic, int tran_cod_tran, string veic_plac, List<Objetos.objArim> objArim)
        {
            string erro = "";

            clsBanco banco = new clsBanco();


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            Classes.clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsNFVE clsNFVE = new clsNFVE();
            clsArim clsArim = new Classes.clsArim();
            clsNotaFiscal clsNota = new clsNotaFiscal();
            try
            {

                for (int i = 0; i <= objMovimentoEstoque.Count - 1; i++)
                {
                    
                    //aqui vou verificar se a nota já foi gravada na tabela que liga o veículo a nota
                    DataSet dsNFVE = new DataSet();
                    
                    dsNFVE = clsNFVE.SelecionarNFVEporCodigoCanfeVeicPlaca(objMovimentoEstoque[i].Canf_cod_canf.ToString(), veic_plac);

                    if (dsNFVE.Tables[0].Rows.Count <=0 )
                    {
                        if(i>0)
                        {
                            if (objMovimentoEstoque[i].Canf_cod_canf.ToString() != objMovimentoEstoque[i-1].Canf_cod_canf.ToString())
                            {
                                clsNFVE.CadastrarNFVE(objMovimentoEstoque[i].Canf_cod_canf.ToString(), veic_plac, connection);
                            }
                        }
                        else
                        {
                            clsNFVE.CadastrarNFVE(objMovimentoEstoque[i].Canf_cod_canf.ToString(), veic_plac, connection);
                        }
                        
                    }

                    clsNota.AlterarMotoristaVeiculoNF(connection, moto_cod_moto, veic_cod_veic, objMovimentoEstoque[i].Canf_cod_canf.ToString());
                    clsNota.AlterarTransportadoraNF(connection, tran_cod_tran, objMovimentoEstoque[i].Canf_cod_canf.ToString());

                    clsMovimentoEstoque.cadastramovimentoEstoque(objMovimentoEstoque[i], connection, objSaldoEstoque[i].Emen_cod_empresa);
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque[i], connection, objSaldoEstoque[i].Emen_cod_empresa, objSaldoEstoque[i].Emen_cod_empresa_destino);


                }

                for (int j = 0; j <= objArim.Count - 1; j++)
                {
                    clsArim.CadastrarImagem(objArim[j], connection);
                }
                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                connection.Close();
                erro = ex.Message;
                return erro;
            }
        }

        public DataSet SelecionarCarregamentoTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "select distinct tb_canf.canf_numnt, tb_canf.canf_cod_canf, tb_tran.tran_nomered, tb_moto.moto_nome, tb_moes.moes_dt_criacao,  " +
                             "tb_emen_orig.emen_nomered as emen_nome_orig, " +
                            "tb_emen_dest.emen_nomered as emen_nome_dest,  " +
                            "tb_veic.veic_placa  " +
                            "from  tb_canf  " +
                            "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
                            "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
                            "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
                            "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
                            "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto " +
                            "inner join tb_moes on tb_moes.canf_cod_canf = tb_canf.  " +
                            "where tb_moes.tpmo_cod_tpmo = 1";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.CommandTimeout = 100000000;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarCarregamentoPorcodifoNF(string canf_cod_canf)
        {
            clsBanco banco = new clsBanco();
            string comando = "select distinct tb_canf.canf_numnt, tb_tran.tran_nomered, tb_moto.moto_nome, tb_moes.moes_dt_criacao,  " +
                             "tb_emen_orig.emen_nomered as emen_nome_orig, " +
                             "tb_emen_dest.emen_nomered as emen_nome_dest,  " +
                             "tb_veic.veic_placa, tb_arim.arim_imagem " +
                             "from  " +
                             "tb_canf  " +
                             "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
                             "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
                             "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
                             "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
                             "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
                             "inner join tb_moes on tb_moes.canf_cod_canf = tb_canf.canf_cod_canf " +
                             "left join tb_arim on tb_canf.canf_cod_canf = tb_arim.canf_cod_canf " +
                             "where tb_moes.tpmo_cod_tpmo = 1 and tb_canf.canf_cod_canf = " + canf_cod_canf;
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarCarregamentoEmpresa(string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "select max(tb_moes.moes_dt_criacao) as moes_dt_criacao ,tb_canf.canf_cod_canf, tb_canf.canf_numnt, tb_tran.tran_nomered, tb_moto.moto_nome,   " +
                             "tb_emen_orig.emen_nomered as emen_nome_orig, " +
                            "tb_emen_dest.emen_nomered as emen_nome_dest,  " +
                            "tb_veic.veic_placa  " +
                            "from  tb_canf  " +
                            "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
                            "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
                            "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
                            "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
                            "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto " +
                            "inner join tb_moes on tb_moes.canf_cod_canf = tb_canf.canf_cod_canf " +
                            "where tb_moes.tpmo_cod_tpmo = 1 and tb_canf.emen_cod_empr_origem = " + emen_cod_empr + " group by tb_canf.canf_cod_canf, tb_canf.canf_numnt, tb_tran.tran_nomered, tb_moto.moto_nome,tb_emen_orig.emen_nomered ,tb_emen_dest.emen_nomered ,tb_veic.veic_placa order by tb_canf.canf_cod_canf desc";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.CommandTimeout = 100000000;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }



    }
}