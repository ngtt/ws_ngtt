﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsTipoProdutosMateriais
    {
        public int CadastrarTipoProdutosMateriais(Objetos.objTipoProdutosMateriais objTipoProdutosMateriais)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int tpma_cod_tipo = 0;


            comando = "INSERT INTO public.tb_tpma(tpma_descricao, tpma_data_criacao, usur_cd_usur) " +
                      "VALUES ('" + objTipoProdutosMateriais.Tpma_descricao + "','" + objTipoProdutosMateriais.Tpma_data_criacao + "'," + objTipoProdutosMateriais.User_cd_usur + ") returning tpma_cod_tipo";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            tpma_cod_tipo = Convert.ToInt32(cmd.ExecuteScalar());

            connection.Close();
            return Convert.ToInt32(tpma_cod_tipo);
        }

        public void AlterarTipoProdutosMateriais(Objetos.objTipoProdutosMateriais objTipoProdutosMateriais)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "UPDATE public.tb_tpma " +
                      "SET tpma_descricao='" + objTipoProdutosMateriais.Tpma_descricao + "', tpma_data_criacao='" + objTipoProdutosMateriais.Tpma_data_criacao + "', usur_cd_usur= " + objTipoProdutosMateriais.User_cd_usur +
                      "WHERE tpma_cod_tipo = " + objTipoProdutosMateriais.Tpma_cod_tipo;



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
            
        }

        public DataSet SelecionarTodosTipoProdutosMateriais()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tpma order by tpma_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }


        public DataSet SecionarUmTipoProdutosMateriais(int tpma_cod_tipo)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tpma where tpma_cod_tipo = " + tpma_cod_tipo;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }



    }
}