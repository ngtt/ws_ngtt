﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
using System.IO;

namespace wsngtt.Classes
{
    public class clsNFVE
    {

         public void CadastrarNFVE(string canf_cod_canf, string veic_placa_veic, Npgsql.NpgsqlConnection connection)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "INSERT INTO public.tb_nfve(canf_cod_canf, veic_placa_veic, nfve_recebido) " +
                      "VALUES ('" + canf_cod_canf + "','" + veic_placa_veic + "',false)";

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteScalar();
         }

        public DataSet SelecionarNFVEporCodigoCanfeVeicPlaca(string canf_cod_canf, string veic_placa_veic)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_nfve where canf_cod_canf = '" + canf_cod_canf + "' and veic_placa_veic = '" + veic_placa_veic + "' and nfve_recebido = false";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet atualizarVeicPlacaRecebimento(string canf_cod_canf, string veic_placa_veic)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = " update tb_nfve set nfve_recebido = true where canf_cod_canf = '" + canf_cod_canf + "' and veic_placa_veic = '" + veic_placa_veic + "'";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

		public DataSet atualizarVeicTrocarPlaca(string canf_cod_canf, string veic_placa_veic, string veic_placa_nova)
		{
			clsBanco banco = new clsBanco();
			string comando = "";

			comando = " update tb_nfve set veic_placa_veic = '" + veic_placa_nova + "' where canf_cod_canf = '" + canf_cod_canf + "' and veic_placa_veic = '" + veic_placa_veic + "'";

			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();
			return ds;
		}

	}
}