﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsTipoVeiculo
    {
        public int CadastrarTipoVeiculo(Objetos.objTipoVeiculo objTipoVeiculo)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int tpve_cod_tpve = 0;


            comando = "INSERT INTO public.tb_tpve(tpve_descricao, tpve_ativo, tpve_dt_criacao, usur_cd_usur) " +
                      "VALUES ('" + objTipoVeiculo.Tpve_descricao + "','" + objTipoVeiculo.Tpve_ativo + "','" + objTipoVeiculo.Tpve_dt_criacao +"'," + objTipoVeiculo.Usur_cd_usur +") returning tpve_cod_tpve";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            tpve_cod_tpve = Convert.ToInt32(cmd.ExecuteScalar());

            connection.Close();
            return Convert.ToInt32(tpve_cod_tpve);
        }

        public void alterarTipoVeiculo(Objetos.objTipoVeiculo objTipoVeiculo)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE public.tb_tpve  SET " +
                      "tpve_descricao='" + objTipoVeiculo.Tpve_descricao +
                      "',tpve_ativo='" + objTipoVeiculo.Tpve_ativo +
                      "',tpve_dt_criacao='" + objTipoVeiculo.Tpve_dt_criacao +
                      "', usur_cd_usur=" + objTipoVeiculo.Usur_cd_usur +
                      " WHERE tpve_cod_tpve = " + objTipoVeiculo.Tpve_cod_tpve;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet SelecionarTodosGrupos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_grem order by grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarTodosTiposVeiculos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tpve order by tpve_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarTiposVeiculosAtivos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tpve where tpve_ativo = 'True' order by tpve_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarUmTiposVeiculos(int tpve_cod_tpve)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tpve where tpve_cod_tpve = " + tpve_cod_tpve;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }
    }
}