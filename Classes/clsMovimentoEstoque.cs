﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsMovimentoEstoque
    {

        public bool cadastrarMovimentoEstoqueAvariadasFaltantes(Objetos.objMovimentoEstoque objMovimentoEstoque, string emen_cd_empr)
        {
            try
            {
                clsBanco banco = new clsBanco();
                string comando = "";
                comando = "INSERT INTO public.tb_moes(moes_dt_criacao, " +
							  "usur_cd_usur,canf_cod_canf, tpmo_cod_tpmo, moes_quantidade, emen_cod_empr, prma_cod_prma) " +
                              "VALUES ('" + objMovimentoEstoque.Moes_dt_criacao + "'," + objMovimentoEstoque.Usur_cod_usur +
                              "," + objMovimentoEstoque.Canf_cod_canf + "," + objMovimentoEstoque.Tpmo_cod_tpmo + "," + Math.Abs(objMovimentoEstoque.Moes_quantidade).ToString().Replace(",", ".") + "," + emen_cd_empr + "," + objMovimentoEstoque.Prma_cod_prma + ") returning moes_cod_moes";

                NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
                connection.Open();

                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
                connection.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public int cadastramovimentoEstoque(Objetos.objMovimentoEstoque objMovimentoEstoque, Npgsql.NpgsqlConnection connection, int emen_cd_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object moes_cod_moes;

            if (objMovimentoEstoque.Tpmo_cod_tpmo != -7)
            {
                if (objMovimentoEstoque.Canf_cod_canf != 0)
                {
                    comando = "INSERT INTO public.tb_moes(canf_cod_canf, prma_cod_prma, moes_dt_criacao, " +
                              "usur_cd_usur, tpmo_cod_tpmo, moes_quantidade, emen_cod_empr,moes_folha_boa) " +
                              "VALUES (" + objMovimentoEstoque.Canf_cod_canf + "," + objMovimentoEstoque.Prma_cod_prma +
                              ",'" + objMovimentoEstoque.Moes_dt_criacao + "'," + objMovimentoEstoque.Usur_cod_usur +
                              "," + objMovimentoEstoque.Tpmo_cod_tpmo + "," + Math.Abs(objMovimentoEstoque.Moes_quantidade).ToString().Replace(",", ".") + "," + emen_cd_empr + "," + objMovimentoEstoque.Moes_folha_boa + ") returning moes_cod_moes";
                }
                else
                {
                    comando = "INSERT INTO public.tb_moes(prma_cod_prma, moes_dt_criacao, " +
							  "usur_cd_usur, tpmo_cod_tpmo, moes_quantidade, emen_cod_empr, moes_folha_boa) " +
                              "VALUES (" + objMovimentoEstoque.Prma_cod_prma +
                              ",'" + objMovimentoEstoque.Moes_dt_criacao + "'," + objMovimentoEstoque.Usur_cod_usur +
                              "," + objMovimentoEstoque.Tpmo_cod_tpmo + "," + Math.Abs(objMovimentoEstoque.Moes_quantidade).ToString().Replace(",", ".") + "," + emen_cd_empr + "," + objMovimentoEstoque.Moes_folha_boa + ") returning moes_cod_moes";
                }


                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                moes_cod_moes = cmd.ExecuteScalar();
                return Convert.ToInt32(moes_cod_moes);
            }
            return Convert.ToInt32(0);
        }

        public void alterarEmpresa(Objetos.objEmpresa objempresa)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE tb_emen set " +
                      "emen_cnpj='" + objempresa.Emen_cnpj +
                      "', emen_nome='" + objempresa.Emen_nome +
                      "', emen_nomered='" + objempresa.Emen_nomered +
                      "', emen_lograd='" + objempresa.Emen_lograd +
                      "', emen_numero='" + objempresa.Emen_numero +
                      "', emen_uf='" + objempresa.Emen_uf +
                      "', emen_comple='" + objempresa.Emen_comple +
                      "', emen_bairro='" + objempresa.Emen_bairro +
                      "', emen_cep='" + objempresa.Emen_cep +
                      "',emen_ddd='" + objempresa.Emen_ddd +
                      "', emen_tel='" + objempresa.Emen_tel +
                      "', emen_email='" + objempresa.Emen_email +
                      "', emen_dt_criacao='" + objempresa.Emen_dt_criacao +
                      "', usur_cod_usur=" + objempresa.Usur_cd_usur +
                      ", emen_ativo= " + objempresa.Emen_ativo +
                      ", grem_cd_grem= " + objempresa.ObjGrupo.Grem_cd_grem +
                      " WHERE emen_cod_empr=" + objempresa.Emen_cod_empr;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }
    }
}