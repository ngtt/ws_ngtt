﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
using wsngtt.Funcionalidades;


namespace wsngtt.Classes
{
    public class clsProdutoMaterial
    {
        public int cadastraProdutoMaterial(Objetos.objProdutoMaterial objProdutoMaterial)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object prma_cd_prma;
            bool confere = false;


            if (objProdutoMaterial.Famp_cd_famp == 4)
            {
                confere = true;
            }
            comando = "INSERT INTO public.tb_prma(prma_pn, prma_descricao, tpma_cod_tipo, unme_cod_unme, " +
                      "prma_fator_conservacao, prma_preco, famp_cd_famp, prma_dt_criacao, " +
                      "usur_cd_usur, prma_ativo, grem_cd_grem, prma_confere_prma, gppm_cd_gppm) " +
                      "VALUES ('" + objProdutoMaterial.Prma_pn + "','" + objProdutoMaterial.Prma_descricao + "'," + objProdutoMaterial.Tpma_cod_tipo + "," + objProdutoMaterial.Unme_cod_unme +
                      "," + objProdutoMaterial.Prma_fator_conservacao.ToString().Replace(",", ".") + "," + objProdutoMaterial.Prma_preco.ToString().Replace(",", ".") + "," + objProdutoMaterial.Famp_cd_famp + ",'" + objProdutoMaterial.Prma_dt_criacao +
                      "'," + objProdutoMaterial.Usur_cd_usur + ",'" + objProdutoMaterial.Prma_ativo + "'," + objProdutoMaterial.Grem_cd_grem + "," + confere + ",'" + objProdutoMaterial.Gppm_cd_gppm + "') returning prma_cod_prma";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            prma_cd_prma = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(prma_cd_prma);
        }

        public void alterarProdutoMaterial(Objetos.objProdutoMaterial objProdutoMaterial)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE public.tb_prma SET " +
                      "prma_pn='" + objProdutoMaterial.Prma_pn + "', " +
                      "prma_descricao='" + objProdutoMaterial.Prma_descricao + "', " +
                      "tpma_cod_tipo= " + objProdutoMaterial.Tpma_cod_tipo + ", " +
                      "unme_cod_unme= " + objProdutoMaterial.Unme_cod_unme + ", " +
                      "prma_fator_conservacao= " + objProdutoMaterial.Prma_fator_conservacao.ToString().Replace(",", ".") + ", " +
                      "prma_preco= " + objProdutoMaterial.Prma_preco.ToString().Replace(",", ".") + ", " +
                      "famp_cd_famp= " + objProdutoMaterial.Famp_cd_famp + ", " +
                      "prma_dt_criacao='" + objProdutoMaterial.Prma_dt_criacao + "', " +
                      "usur_cd_usur= " + objProdutoMaterial.Usur_cd_usur + ", " +
                      "prma_ativo='" + objProdutoMaterial.Prma_ativo + "', " +
                      "grem_cd_grem=" + objProdutoMaterial.Grem_cd_grem + ", " +
                      "gppm_cd_gppm = '" + objProdutoMaterial.Gppm_cd_gppm + "' " +
                      " WHERE prma_cod_prma = " + objProdutoMaterial.Prma_cod_prma;



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }

        public DataSet SelecionarProdutoMaterialPorGrupo(string gppm_cd_gppm, string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select  tb_prma.*, tb_unme.*, tb_tpma.*, " +
                      "tb_grem.grem_nome from tb_prma inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
                      "inner join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo " +
                      "inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "inner join tb_emen on tb_emen.grem_cd_grem = tb_prma.grem_cd_grem " +
                      "where prma_ativo = true and gppm_cd_gppm = '" + gppm_cd_gppm + "' AND " +
                      "emen_cod_empr = " + emen_cod_empr + "order by tb_prma.prma_descricao, tb_grem.grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarProdutoMaterialTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select  tb_prma.*, tb_unme.*, tb_tpma.*, tb_grem.* " +
                      "from tb_prma " +
                      "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
                      "inner join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo " +
                      "inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "where prma_ativo = true order by prma_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }


        public DataSet SelecionarProdutoMaterialTodosPorGrupo(string emen_cod_emen)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select  tb_prma.*, tb_unme.*, tb_tpma.*, tb_grem.* " +
                      "from tb_prma " +
                      "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
                      "inner join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo " +
                      "inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "inner join tb_emen on tb_emen.grem_cd_grem = tb_prma.grem_cd_grem " +
                      "where prma_ativo = true and tb_emen.emen_cod_empr = " + emen_cod_emen + " order by prma_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }


        public DataSet SelecionarUmProdutoMaterial(int prma_cod_prma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select  tb_prma.*, tb_unme.*, tb_tpma.* from tb_prma inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme inner join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo where prma_cod_prma = " + prma_cod_prma + " and prma_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosGrupoConferencia(string grem_cd_grem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_prma.*, tb_tpma.*,	tb_unme.*,	tb_famp.*, tb_kits.* " +
                      "from tb_prma " +
                      "left join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo " +
                      "left join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
                      "left join tb_famp on tb_prma.famp_cd_famp = tb_famp.famp_cd_famp " +
                      "left join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma " +
                      "where " +
                      "tb_prma.prma_ativo = true and " +
                      "tb_prma.prma_confere_prma = true and " +
                      "tb_grem.grem_cd_grem = " + grem_cd_grem + " and prma_ativo = true order by tb_prma.prma_descricao";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }



        public DataSet SelecionarTodosProdutosGrupo(string grem_cd_grem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_prma.*, tb_kits.*, tb_prma.prma_pn || ' - ' || tb_prma.prma_descricao as produto " +
                      "from tb_prma left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma" +
                      " where " +
                      "tb_prma.grem_cd_grem = " + grem_cd_grem + " and prma_ativo = true order by prma_descricao";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosEmpresa(string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_prma " +
                      "inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "inner join tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "where tb_emen.emen_cod_empr =  " + emen_cod_empr + " and prma_ativo = true order by tb_prma.prma_descricao";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosFamilia(string famp_cd_famp, string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_prma " +
                      "inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "inner join tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "where famp_cd_famp = " + famp_cd_famp + " and tb_emen.emen_cod_empr = " + emen_cod_empr + " and prma_ativo = true  order by prma_descricao";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }


        public DataSet SelecionarUmProdutoMaterialPorPN(string prma_pn, string grem_cd_grem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            

            if (!Funcionalidades.clsUtil.contemLetras(prma_pn))
            {
                prma_pn = (Convert.ToInt32(prma_pn)).ToString();
            }

            comando = "select  tb_prma.*, tb_unme.*, tb_tpma.* from tb_prma inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme inner join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo where prma_pn = '" + prma_pn + "' and grem_cd_grem = " + grem_cd_grem + " and prma_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarUmProdutoMaterialPorPNStr(string prma_pn, string grem_cd_grem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select  tb_prma.*, tb_unme.*, tb_tpma.* from tb_prma inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme inner join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo where replace(replace(prma_pn,'/',''),'.','') like '%" + prma_pn + "%' and grem_cd_grem = '" + grem_cd_grem + "' and prma_ativo = true";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutoMaterialAtivoInativo()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select  tb_prma.*, tb_unme.*, tb_tpma.*, tb_grem.grem_nome from tb_prma inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme inner join tb_tpma on tb_prma.tpma_cod_tipo = tb_tpma.tpma_cod_tipo inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem  order by tb_prma.prma_descricao, tb_grem.grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosTipo(string tpma_cod_tipo, string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_prma " +
                      "inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "inner join tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "where tpma_cod_tipo = " + tpma_cod_tipo + " and tb_emen.emen_cod_empr = " + emen_cod_empr + " and prma_ativo = true  order by prma_descricao";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosFamiliaPA(string famp_cd_famp, string emen_cod_empr_origem, string emen_cod_empr_origem_destino)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            

            comando = "SELECT * FROM	tb_prma " +
                      "WHERE prma_cod_prma IN(SELECT prma_cod_prma_empr FROM tb_mama  WHERE   grem_cd_grem_interno = " +
                      "(SELECT grem_cd_grem FROM   tb_emen WHERE   emen_cod_empr = " + emen_cod_empr_origem_destino + ")) " +
                      "AND tb_prma.grem_cd_grem = (SELECT grem_cd_grem FROM tb_emen WHERE emen_cod_empr = " + emen_cod_empr_origem + ") " +
                      "and tb_prma.famp_cd_famp = " + famp_cd_famp;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosFamiliaME(string famp_cd_famp, string emen_cod_empr_origem, string emen_cod_empr_origem_destino)
        {
            clsBanco banco = new clsBanco();
            string comando = "select distinct tb_prma.* from tb_prma " +
                             "inner join tb_mama " +
                             "on tb_prma.grem_cd_grem = tb_mama.grem_cd_grem_externo " +
                             "inner join tb_emen emen on emen.grem_cd_grem = tb_prma.grem_cd_grem " +
                             "inner join tb_emen emen1 on emen.grem_cd_grem = tb_prma.grem_cd_grem " +
                             "where " +
                             "emen.emen_cod_empr = " + emen_cod_empr_origem + " and emen1.emen_cod_empr = " + emen_cod_empr_origem_destino + " and " +
                             "tb_prma.famp_cd_famp = " + famp_cd_famp;

            
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosFamiliaME2(string famp_cd_famp, string emen_cod_empr_origem, string emen_cod_empr_origem_destino)
        {
            clsBanco banco = new clsBanco();
            string comando = "select distinct tb_prma.* from tb_prma " +
                             "inner join tb_mama " +
                             "on tb_prma.grem_cd_grem = tb_mama.grem_cd_grem_interno " +
                             "inner join tb_emen emen on emen.grem_cd_grem = tb_prma.grem_cd_grem " +
                             "inner join tb_emen emen1 on emen.grem_cd_grem = tb_prma.grem_cd_grem " +
                             "where " +
                             "emen.emen_cod_empr = " + emen_cod_empr_origem + " and emen1.emen_cod_empr = " + emen_cod_empr_origem_destino + " and " +
                             "tb_prma.famp_cd_famp = " + famp_cd_famp;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarProdutosMEEmpresaUsuario(string usur_cd_usur)
        {
            clsBanco banco = new clsBanco();
            string comando = "select " +
                             "  tb_prma.prma_cod_prma, tb_prma.prma_descricao, tb_prma.famp_cd_famp " +
                             "from " +
                             "  tb_prma " +
                             "inner join " +
                             "  tb_grem " +
                             "on " +
                             "  tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                             "inner join " +
                             "  tb_emen " +
                             "on " +
                             "  tb_grem.grem_cd_grem = tb_emen.grem_cd_grem " +
                             "inner join " +
                             "  tb_usur " +
                             "on " +
                             "  tb_usur.emen_cod_empr = tb_emen.emen_cod_empr " +
                             "WHERE " +
                             "  tb_usur.usur_cd_usur = " + usur_cd_usur + " AND " +
                             "  tb_prma.famp_cd_famp in(1,2,3,5) " +
                             "ORDER BY " +
                             "  tb_prma.famp_cd_famp, tb_prma.prma_descricao ";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarTodosProdutosFamiliaComSaldo(string famp_cd_famp, string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select distinct tb_prma.prma_cod_prma, tb_prma.prma_descricao, tb_prma.prma_confere_prma, tb_prma.prma_pn, " +
                      "tb_grem.grem_cd_grem, tpma_cod_tipo,	tb_prma.prma_fator_conservacao,	tb_prma.prma_preco from tb_prma " +
                      "inner join tb_grem on tb_prma.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "inner join tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem " +
                      "inner join tb_sles on tb_sles.prma_cod_prma = tb_prma.prma_cod_prma and tb_sles.emen_cod_empr = tb_emen.emen_cod_empr " +
                      "where famp_cd_famp = " + famp_cd_famp + " and tb_emen.emen_cod_empr = " + emen_cod_empr + " and tb_sles.sles_quantidade > 0 AND tpmo_cod_tpmo = 3 and prma_ativo = true  order by prma_descricao";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }




    }
}