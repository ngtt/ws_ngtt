﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsTipoMovimentacao
    {
        public DataSet SecionarTodosTiposMovimentacao()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tpmo order by tpmo_tipo_movimento";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SecionarUmTiposMovimentacao(int tpmo_tipo_movimento)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tpmo where tpmo_cod_tpmo = " + tpmo_tipo_movimento;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }
    }
}