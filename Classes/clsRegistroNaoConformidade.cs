﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsRegistroNaoConformidade
    {
        public int cadastraRegistroNaoConformidade(Objetos.objRegistroNaoConformidade objRegistroNaoConformidade)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object rnco_cod_rnco;


            comando = "INSERT INTO public.tb_rnco(" +
                      "emen_cod_empr, canf_cod_canf, rnco_obs, rnco_dt_criacao, " +
                      "usur_cd_usur, mtoc_cod_mtoc, rnco_causador_rnco, arma_cod_arma, " +
                      "famp_cod_famp, unme_cod_unme, rnco_quant_rnco) " +
                      "VALUES (" + objRegistroNaoConformidade.Emen_cod_empr + "," + objRegistroNaoConformidade.Canf_cod_canf +
                      ",'" + objRegistroNaoConformidade.Rnco_obs + "','" + objRegistroNaoConformidade.Rnco_dt_criacao + "'" +
                      "," + objRegistroNaoConformidade.Usur_cd_usur + "," + objRegistroNaoConformidade.Mtoc_cod_mtoc + 
                      ",'" + objRegistroNaoConformidade.Rnco_causador_rnco + "'," + objRegistroNaoConformidade.Arma_cod_arma + 
                      "," + objRegistroNaoConformidade.Famp_cod_famp + "," + objRegistroNaoConformidade.Unme_cod_unm +
                      "," + objRegistroNaoConformidade.Rnco_quant_rnc + ") returning rnco_cod_rnco";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            rnco_cod_rnco = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(rnco_cod_rnco);
        }

        public DataSet SelecionarTodosRegistrosNaoConformidade()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_rnco.rnco_cod_rnco, tb_rnco.rnco_dt_criacao, tb_usur.usur_nome, tb_emen.emen_nomered, tb_canf.canf_numnt, tb_mtoc.mtoc_descricao " +
                      "from tb_rnco inner join tb_usur on tb_rnco.usur_cd_usur = tb_usur.usur_cd_usur " +
	                  "inner join tb_emen on tb_rnco.emen_cod_empr = tb_emen.emen_cod_empr " +
	                  "left join tb_canf on tb_rnco.canf_cod_canf = tb_canf.canf_cod_canf " +
	                  "inner join tb_mtoc on tb_rnco.mtoc_cod_mtoc = tb_mtoc.mtoc_cod_mtoc " +
	                  "order by rnco_dt_criacao desc";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.CommandTimeout = 10000000;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarTodosRegistrosNaoConformidadePorEmpresa(string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_rnco.rnco_cod_rnco, tb_rnco.rnco_dt_criacao, tb_usur.usur_nome, tb_emen.emen_nomered, tb_canf.canf_numnt, tb_mtoc.mtoc_descricao " +
                      "from tb_rnco inner join tb_usur on tb_rnco.usur_cd_usur = tb_usur.usur_cd_usur " +
                      "inner join tb_emen on tb_rnco.emen_cod_empr = tb_emen.emen_cod_empr " +
                      "left join tb_canf on tb_rnco.canf_cod_canf = tb_canf.canf_cod_canf " +
                      "inner join tb_mtoc on tb_rnco.mtoc_cod_mtoc = tb_mtoc.mtoc_cod_mtoc " +
                      "where tb_usur.emen_cod_empr = " + emen_cod_empr + " " +
                      "order by rnco_dt_criacao desc";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.CommandTimeout = 10000000;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarTodosRegistrosNaoConformidadePorCodigo(string rnco_cod_rnco)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_rnco.*, tb_usur.*, tb_emen.*, tb_canf.*, tb_mtoc.*, tb_arma.*, tb_unme.*, tb_ftoc.*, tb_famp.* " +
                      "from tb_rnco inner join tb_usur on tb_rnco.usur_cd_usur = tb_usur.usur_cd_usur " +
	                  "inner join tb_emen on tb_rnco.emen_cod_empr = tb_emen.emen_cod_empr " +
	                  "left join tb_canf on tb_rnco.canf_cod_canf = tb_canf.canf_cod_canf " +
	                  "inner join tb_mtoc on tb_rnco.mtoc_cod_mtoc = tb_mtoc.mtoc_cod_mtoc " +
	                  "left join tb_arma on tb_rnco.arma_cod_arma = tb_arma.arma_cod_arma " +
	                  "left join tb_unme on tb_rnco.unme_cod_unme = tb_unme.unme_cod_unme " +
	                  "left join tb_ftoc on tb_rnco.rnco_cod_rnco = tb_ftoc.rnco_cod_rnco " +
	                  "left join tb_famp on tb_rnco.famp_cod_famp = tb_famp.famp_cd_famp " +
                      "where tb_rnco.rnco_cod_rnco = " + rnco_cod_rnco +
                      "order by rnco_dt_criacao desc";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

    }


}