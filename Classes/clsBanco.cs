﻿ /* Desenvolvedor: Marco Sá
 * Data: 29/06/2016
 * Descrição: Conterá as funcionalidades conexão a base de dados e outras relativas a bd que venham a ser necessárias
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsBanco
    {
        public string StringConexaoLoginDB()
        {

            string constring = "";


            constring = ConfigurationManager.ConnectionStrings["NgttDB"].ToString();

            return constring; //"Server=localhost; UserId=root; Password=amais321; Database=riera;"; // Servidor Local
            //return "Server=dbmy0011.whservidor.com; UserId=amaisproje_2; Password=riera3; Database=amaisproje_2;"; // Servidor da UOL
            //return "Server=74.208.102.34; port=3307; UserId=root; Password=amais321; Database=riera;"; // Conexão remota 1and1 amais
            //return "Server=localhost; port=3307; UserId=root; Password=amais321; Database=riera;"; // Conexão remota 1and1 amais
        }

        public void fechaConexão(NpgsqlConnection conn)
        {
            conn.Close();
        }

    }
}