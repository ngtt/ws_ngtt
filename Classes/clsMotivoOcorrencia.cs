﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsMotivoOcorrencia
    {
        public DataSet SelecionarMotivoOcorrenciaTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_mtoc order by mtoc_descricao";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        public DataSet SelecionarMotivoOcorrenciaPorCodigo(int mtoc_cod_mtoc)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_mtoc where mtoc_cod_mtoc =" + mtoc_cod_mtoc + " order by mtoc_descricao ";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public int CadastrarMotivoOcorrencia(Objetos.objMotivoOcorrencia objMotivoOcorrencia)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int mtoc_cod_mtoc = 0;


            comando = "INSERT INTO public.tb_mtoc(mtoc_descricao, mtoc_dt_criacao, usur_cd_usur) " +
                      "VALUES ('" + objMotivoOcorrencia.Mtoc_descricao + "','" + objMotivoOcorrencia.Mtoc_dt_criacao + "'," + objMotivoOcorrencia.Usur_cd_usur + ") returning mtoc_cod_mtoc";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            mtoc_cod_mtoc = Convert.ToInt32(cmd.ExecuteScalar());
            connection.Close();

            return Convert.ToInt32(mtoc_cod_mtoc);
        }

        public void alterarMotivoOcorrencia(Objetos.objMotivoOcorrencia objMotivoOcorrencia)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE public.tb_mtoc SET mtoc_descricao='" + objMotivoOcorrencia.Mtoc_descricao + "'," +
                      "mtoc_dt_criacao='" + objMotivoOcorrencia.Mtoc_dt_criacao + "' " +
                      "WHERE mtoc_cod_mtoc = " + objMotivoOcorrencia.Mtoc_cod_mtoc;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }

    
}