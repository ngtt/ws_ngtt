﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsTipoOperacao
    {
        public DataSet SelecionaTipoOperacaoTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_tpop order by tpop_descricao";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }
    }
}