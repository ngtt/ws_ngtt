﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsTransportadora
    {

        public DataSet SelecionarTransportadoraCNPJ(string tran_cnpj)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tran where tran_cnpj = '" + tran_cnpj + "' and tran_ativo = true";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarTransportadoraTodas()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tran where tran_ativo = true order by tran_nomered";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarTransportadoraTodasPorCodigo(int tran_cod_tran)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_tran where tran_cod_tran =" + tran_cod_tran;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public int cadastraTransportadora(Objetos.objTransportadora objTransportadora)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object tran_cod_tran;


            comando = "INSERT INTO public.tb_tran(tran_cnpj, tran_nome, tran_nomered, " +
                      "tran_lograd, tran_num, tran_uf, tran_compl, tran_bairro, tran_cep, tran_ddd, " +
                      "tran_tel, tran_email, tran_dt_criacao, usur_cd_usur, tran_ativo)" +
                       "VALUES ('" + objTransportadora.Tran_cnpj + "','" + objTransportadora.Tran_nome + "','" + objTransportadora.Tran_nomered + "','" +
                       objTransportadora.Tran_lograd + "','" + objTransportadora.Tran_numero + "','" + objTransportadora.Tran_uf + "','" +
                       objTransportadora.Tran_comple + "','" + objTransportadora.Tran_bairro + "','" + objTransportadora.Tran_cep + "','" +
                       objTransportadora.Tran_ddd + "','" + objTransportadora.Tran_tel + "','" + objTransportadora.Tran_email + "','" +
                       objTransportadora.Tran_dt_criacao + "'," + objTransportadora.Usur_cd_usur + ",'" + objTransportadora.Tran_ativo + "') RETURNING tran_cod_tran";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            tran_cod_tran = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(tran_cod_tran);
        }

        public void alterarTransportadora(Objetos.objTransportadora objTransportadora)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE tb_tran set " +
                      "tran_cnpj='" + objTransportadora.Tran_cnpj +
                      "', tran_nome='" + objTransportadora.Tran_nome +
                      "', tran_nomered='" + objTransportadora.Tran_nomered +
                      "', tran_lograd='" + objTransportadora.Tran_lograd +
                      "', tran_num='" + objTransportadora.Tran_numero +
                      "', tran_uf='" + objTransportadora.Tran_uf +
                      "', tran_compl='" + objTransportadora.Tran_comple +
                      "', tran_bairro='" + objTransportadora.Tran_bairro +
                      "', tran_cep='" + objTransportadora.Tran_cep +
                      "',tran_ddd='" + objTransportadora.Tran_ddd +
                      "', tran_tel='" + objTransportadora.Tran_tel +
                      "', tran_email='" + objTransportadora.Tran_email +
                      "', tran_dt_criacao='" + objTransportadora.Tran_dt_criacao +
                      "', usur_cd_usur=" + objTransportadora.Usur_cd_usur +
                      ", tran_ativo= " + objTransportadora.Tran_ativo +
                      " WHERE tran_cod_tran=" + objTransportadora.Tran_cod_tran;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }
    }
}