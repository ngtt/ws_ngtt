﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
using System.IO;

namespace wsngtt.Classes
{
    public class clsEntr
    {

         public void CadastrarEntrada(Objetos.objEntr objentr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            comando = "INSERT INTO public.tb_entr(entr_tx_placas, usur_sq_usuario, " +
                      "entr_dt_entrada, emen_sq_empresa, entr_tx_nf) " +
                      "VALUES('" + objentr.Entr_tx_placas + "', " +
                      "'" + objentr.Usur_sq_usuario + "', " +
                      "'" + objentr.Entr_dt_entrada + "', " + 
                      "'" + objentr.Emen_sq_empresa + "'," +
                      "'" + objentr.Entr_tx_nf + "')";

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteScalar();
        }

        public DataSet SelecionarEntradas(string usur_sq_usuario)
        {
            clsBanco banco = new clsBanco();
            string comando = "select distinct * from tb_entr entr inner join tb_usur usur " +
                             "on entr.usur_sq_usuario = usur.usur_cd_usur " +
                             "where entr.emen_sq_empresa = usur.emen_cod_empr and " +
                             "entr.entr_sq_entrada not in (select entr_sq_entrada from tb_said) and " +
                             "usur.usur_cd_usur = " + usur_sq_usuario;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.CommandTimeout = 100000000;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }



    }
}