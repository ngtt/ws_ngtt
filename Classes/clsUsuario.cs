﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsUsuario
    {

        public DataSet Login(string idLogin, string usur_senha)
        {
            
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_usur.*, tb_emen.* from tb_usur inner join tb_emen on tb_usur.emen_cod_empr = tb_emen.emen_cod_empr where (usur_matricula = '" + idLogin + "' or usur_email = '" + idLogin + "')  and usur_senha = '" + usur_senha + "' and usur_ativo=true order by tb_usur.usur_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            
            return ds;
        }

        public DataSet SelecionarTodosUsuarios()
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_usur.*, tb_emen.* from tb_usur inner join tb_emen on tb_usur.emen_cod_empr = tb_emen.emen_cod_empr order by tb_usur.usur_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarUsuario(int usur_cd_usur)
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_usur.*, tb_emen.* from tb_usur inner join tb_emen on tb_usur.emen_cod_empr = tb_emen.emen_cod_empr where usur_cd_usur = " + usur_cd_usur;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public DataSet SelecionarUsuariosAtivos()
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_usur.*, tb_emen.* from tb_usur inner join tb_emen on tb_usur.emen_cod_empr = tb_emen.emen_cod_empr where usur_ativo = true order by tb_usur.usur_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public int InserirUsuario(Objetos.objUsuario objUsur)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object usur_cd_usur;


            comando = "INSERT INTO public.tb_usur(usur_matricula, usur_nome, usur_apelido, usur_email, " +
                                                 "usur_senha, emen_cod_empr, perf_cod_perfil, usur_dt_criacao, " +
                                                 "usur_ativo) " + 
                                                 "VALUES ( '" + objUsur.Usur_matricula + "','" + objUsur.Usur_nome + "','" + objUsur.Usur_apelido + 
                                                 "','" + objUsur.Usur_email + "','" + objUsur.Usur_senha + "'," + objUsur.Emen_cod_empresa + "," + objUsur.Perf_cod_perf +
                                                 ",'" + objUsur.Usur_dt_usur + "'," + objUsur.Usur_ativo + ") returning usur_cd_usur";
            
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            usur_cd_usur = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(usur_cd_usur);
        }


        public void AlterarUsuario(Objetos.objUsuario objUsur)
        {
            clsBanco banco = new clsBanco();
            string comando = "UPDATE public.tb_usur " +
                             "SET usur_matricula='" + objUsur.Usur_matricula + "'," +
                             "usur_nome='" + objUsur.Usur_nome + "'," +
                             "usur_apelido='" + objUsur.Usur_apelido + "'," + 
                             "usur_email='" + objUsur.Usur_email + "'," +
                             "emen_cod_empr=" + objUsur.Emen_cod_empresa + "," +
                             "perf_cod_perfil=" + objUsur.Perf_cod_perf + "," + 
                             "usur_dt_criacao=now()," +
                             "usur_ativo=" + objUsur.Usur_ativo + " " +
                             "WHERE usur_cd_usur = " + objUsur.Usur_cd_usur;

            
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
            
        }

        public void alterarSenha(int usur_cd_usur, string senha)
        {
            clsBanco banco = new clsBanco();
            string comando = "UPDATE public.tb_usur " +
                             "set usur_senha = '" + senha + "' " +
                             "where usur_cd_usur = " + usur_cd_usur; 


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}