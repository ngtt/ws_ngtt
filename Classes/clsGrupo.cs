﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsGrupo
    {
        public int CadastrarGrupo(Objetos.objGrupo objGrupo)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int grem_cod_grem = 0;


            comando = "INSERT INTO public.tb_grem(grem_nome, grem_descricao)" +
                      "VALUES ('" + objGrupo.Grem_nome + "','" + objGrupo.Grem_descricao + "') returning grem_cd_grem";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            grem_cod_grem = Convert.ToInt32(cmd.ExecuteScalar());
            connection.Close();

            return Convert.ToInt32(grem_cod_grem);
        }

        public void alterarGrupo(Objetos.objGrupo objGrupo)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            
            comando = "UPDATE public.tb_grem " +
                      "SET  grem_nome='" + objGrupo.Grem_nome + "', grem_descricao = '" + objGrupo.Grem_descricao + "' " +
                      "WHERE grem_cd_grem = " + objGrupo.Grem_cd_grem;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet SelecionarTodosGrupos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_grem order by grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarGrupoEmpresa(int emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select tb_emen.*, tb_grem.* from tb_emen inner join tb_grem on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_cod_empr = " + emen_cod_empr;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarGrupoPorCodigo(int grem_cd_grem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_grem where grem_cd_grem = " + grem_cd_grem;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }


        public DataSet SelecionarTodosGruposFornecedores()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select distinct tb_grem.* from tb_grem inner join  tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_forn_emen = true order by grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarTodosGruposClientes()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select distinct tb_grem.* from tb_grem inner join  tb_emen on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_forn_emen = false order by grem_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }
    }



    
}