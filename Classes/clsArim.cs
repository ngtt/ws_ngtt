﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
using System.IO;

namespace wsngtt.Classes
{
    public class clsArim
    {

         public void CadastrarImagem(Objetos.objArim objArim, Npgsql.NpgsqlConnection connection)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "INSERT INTO public.tb_arim(arim_imagem, canf_cod_canf, arim_dt_criacao, usur_cd_usur, tpmo_cod_tpmo, emen_cod_empr) " +
                      "VALUES ('" + objArim.Arim_imagem + "'," + objArim.Canf_cod_canf + ",'" + objArim.Arim_dt_criacao + "'," + objArim.Usur_cd_usur + "," + objArim.Tpmo_cod_tpmo + "," + objArim.Emen_cod_empr + ")";

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteScalar();
         }

         public void ApagarImangesImagem(int canf_cod_canf, int emen_cod_empr)
         {


            

             clsBanco banco = new clsBanco();
             DataSet dsArim = new DataSet();
             string comando = "Select * from tb_arim where canf_cod_canf = " + canf_cod_canf + " and emen_cod_empr = " + emen_cod_empr;
             NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
             connection.Open();
            

             NpgsqlCommand cmd = new NpgsqlCommand();
             cmd.CommandText = comando;
             cmd.Connection = connection;
             NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

             adap.Fill(dsArim);

             comando = "delete from tb_arim where canf_cod_canf = " + canf_cod_canf + " and emen_cod_empr = " + emen_cod_empr;
             cmd = new NpgsqlCommand();
             cmd.CommandText = comando;
             cmd.Connection = connection;
             cmd.ExecuteScalar();

             //aqui apago os arquivos da base

             for (int i = 0; i <= dsArim.Tables[0].Rows.Count - 1; i++)
             {
                 if (File.Exists(@"C:\Projetos\Publicacoes\Sgpo\services\XML\ngttimg\" + dsArim.Tables[0].Rows[i]["arim_imagem"].ToString().Trim()))
                 {
                     File.Delete(@"C:\Projetos\Publicacoes\Sgpo\services\XML\ngttimg\" + dsArim.Tables[0].Rows[i]["arim_imagem"].ToString().Trim());
                     
                 }
             }

         }

    }
}