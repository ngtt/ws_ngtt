﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsArmazem
    {

        public int cadastraArmazem(Objetos.objArma objArma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object arma_cd_arma;


            comando = "INSERT INTO public.tb_arma(arma_descricao, arma_ativo, arma_dt_criacao, usur_cd_usur) " +
                      "VALUES ('" + objArma.Arma_descricao + "','" + objArma.Arma_ativo + "','" + objArma.Arma_dt_criacao + "'," + objArma.Usur_cd_usur + ") returning arma_cod_arma";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            arma_cd_arma = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(arma_cd_arma);
        }

        public void AlterarArmazem(Objetos.objArma objArma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object arma_cd_arma;


            comando = "UPDATE public.tb_arma SET " +
                      "arma_descricao='" + objArma.Arma_descricao + "', arma_ativo='" + objArma.Arma_ativo + 
                      "', arma_dt_criacao='" + objArma.Arma_dt_criacao + "'," +
                      "usur_cd_usur= " + objArma.Usur_cd_usur + " WHERE arma_cod_arma = " + objArma.Arma_cod_arma;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            arma_cd_arma = cmd.ExecuteNonQuery();
            connection.Close();
            
        }

        public DataSet SelecionarArmazensTodos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_arma.*from tb_arma where arma_ativo = true order by arma_descricao";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarArmazenPorCodigo(int arma_cod_arma)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_arma.*from tb_arma where arma_cod_arma = " + arma_cod_arma;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

    }
}