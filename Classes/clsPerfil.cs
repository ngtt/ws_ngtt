﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsPerfil
    {
        public DataSet SelecionarPerfilAtividade(int perf_cod_perf)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_perf.*, tb_ativ.* from tb_perf inner join tb_atpf on tb_perf.perf_cod_perf = tb_atpf.perf_cod_perf inner join tb_ativ on tb_ativ.ativ_cod_ativ = tb_atpf.ativ_cod_ativ where tb_perf.perf_cod_perf = " + perf_cod_perf + " order by perf_nm_perfil";
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }


        public DataSet SelecionarTodosPerfil()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_perf order by perf_nm_perfil";
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

        public int CadastrarPerfil(Objetos.objPerfil objPerfil)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int perf_cod_perf = 0;


            comando = "INSERT INTO tb_perf(perf_nm_perfil) " +
                      "VALUES ('" + objPerfil.Perf_nm_perfil + "') returning perf_cod_perf";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            perf_cod_perf = Convert.ToInt32(cmd.ExecuteScalar());

            for (int i = 0; i <= objPerfil.ObjAtividade.Count - 1; i++)
            {
                cmd.CommandText = "INSERT INTO public.tb_atpf(perf_cod_perf, ativ_cod_ativ) " +
                                  "VALUES (" + perf_cod_perf +"," +objPerfil.ObjAtividade[i].Ativ_cod_ativ.ToString() +")";

                cmd.ExecuteNonQuery();
                
            }
            connection.Close();
            return Convert.ToInt32(perf_cod_perf);
        }


        public int alterarPerfil(Objetos.objPerfil objPerfil)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int perf_cod_perf = 0;


            comando = "UPDATE tb_perf " +
                      "SET perf_nm_perfil = '" + objPerfil.Perf_nm_perfil + "' WHERE perf_cod_perf = " + objPerfil.Perf_cod_perf;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();

            //aqui irei apagar todas as relações existente entre perfil e atividades e recriá-las
            cmd.CommandText = "delete from tb_atpf where perf_cod_perf = " + objPerfil.Perf_cod_perf;
            cmd.ExecuteNonQuery();

            for (int i = 0; i <= objPerfil.ObjAtividade.Count - 1; i++)
            {
                cmd.CommandText = "INSERT INTO tb_atpf(perf_cod_perf, ativ_cod_ativ) " +
                                  "VALUES (" + objPerfil.Perf_cod_perf + "," + objPerfil.ObjAtividade[i].Ativ_cod_ativ.ToString() + ")";

                cmd.ExecuteNonQuery();

            }
            connection.Close();
            return Convert.ToInt32(perf_cod_perf);
        }

    }
}