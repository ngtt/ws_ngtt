﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsNotaFiscal
    {

        public DataSet SelecionarNotaFiscaNumero(string canf_numnf)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_canf.*, tb_itnf.*, tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,  " +
                      "tb_emen_orig.emen_cod_empr as emen_cod_empr_orig,  " +
                      "tb_emen_orig.emen_cnpj as emen_cnpj_orig, " +
                      "tb_emen_orig.emen_nome as emen_nome_orig, " +
                      "tb_emen_orig.emen_nomered as emen_nomered_orig,  " +
                      "tb_emen_orig.emen_lograd as emen_lograd_orig,  " +
                      "tb_emen_orig.emen_numero as emen_numero_orig,  " +
                      "tb_emen_orig.emen_uf as emen_uf_orig,  " +
                      "tb_emen_orig.emen_cidade,  " +
                      "tb_emen_orig.emen_comple as emen_comple_orig,  " +
                      "tb_emen_orig.emen_bairro as emen_bairro_orig,  " +
                      "tb_emen_orig.emen_cep as emen_cep_orig,  " +
                      "tb_emen_orig.emen_ddd as emen_ddd_orig,  " +
                      "tb_emen_orig.emen_tel as emen_tel_orig,  " +
                      "tb_emen_orig.emen_email as emen_email_orig,  " +
                      "tb_emen_orig.grem_cd_grem as grem_cd_grem_orig,  " +
                      "tb_emen_dest.emen_cod_empr as emen_cod_empr_dest,  " +
                      "tb_emen_dest.emen_cnpj as emen_cnpj_dest,  " +
                      "tb_emen_dest.emen_nome as emen_nome_dest,  " +
                      "tb_emen_dest.emen_nomered as emen_nomered_dest,  " +
                      "tb_emen_dest.emen_lograd as emen_lograd_dest,  " +
                      "tb_emen_dest.emen_numero as emen_numero_dest,  " +
                      "tb_emen_dest.emen_uf as emen_uf_dest,  " +
                      "tb_emen_dest.emen_comple as emen_comple_dest,  " +
                      "tb_emen_dest.emen_bairro as emen_bairro_dest,  " +
                      "tb_emen_dest.emen_cep as emen_cep_dest,  " +
                      "tb_emen_dest.emen_ddd as emen_ddd_dest,  " +
                      "tb_emen_dest.emen_tel as emen_tel_dest,  " +
                      "tb_emen_dest.emen_email as emen_email_dest,  " +
                      "tb_emen_dest.grem_cd_grem as grem_cd_grem_dest,  " +
                      "tb_unme.*, tb_veic.*, tb_kits.*  " +
                      "from  " +
                      "tb_canf  " +
                      "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf  " +
                      "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma  " +
                      "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme  " +
                      "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
                      "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
                      "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
                      "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
                      "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma  " +
                      "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
                      "where  " +
                      "tb_canf.canf_numnt = '" + canf_numnf + "' and tb_canf.canf_cod_canf = (select max(canf_cod_canf) from tb_canf where canf_numnt = '" + canf_numnf + "') order by tb_prma.famp_cd_famp";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        //-----------------------
        public DataSet SelecionarNotaFiscaNumeroMultiplas(string canf_numnf)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            string[] Notasin = canf_numnf.Split(',');
            string NotasINmontado = "";

            for (int i = 0; i <= Notasin.Count() - 1; i++)
            {
                NotasINmontado = NotasINmontado + "'" + Notasin[i] + "'";

                if (i != Notasin.Count() - 1)
                {
                    NotasINmontado = NotasINmontado + ",";
                }
            }




            comando = "select distinct tb_canf.*, tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,  " +
                      "tb_emen_orig.emen_cod_empr as emen_cod_empr_orig,  " +
                      "tb_emen_orig.emen_cnpj as emen_cnpj_orig, " +
                      "tb_emen_orig.emen_nome as emen_nome_orig, " +
                      "tb_emen_orig.emen_nomered as emen_nomered_orig,  " +
                      "tb_emen_orig.emen_lograd as emen_lograd_orig,  " +
                      "tb_emen_orig.emen_numero as emen_numero_orig,  " +
                      "tb_emen_orig.emen_uf as emen_uf_orig,  " +
                      "tb_emen_orig.emen_cidade,  " +
                      "tb_emen_orig.emen_comple as emen_comple_orig,  " +
                      "tb_emen_orig.emen_bairro as emen_bairro_orig,  " +
                      "tb_emen_orig.emen_cep as emen_cep_orig,  " +
                      "tb_emen_orig.emen_ddd as emen_ddd_orig,  " +
                      "tb_emen_orig.emen_tel as emen_tel_orig,  " +
                      "tb_emen_orig.emen_email as emen_email_orig,  " +
                      "tb_emen_orig.grem_cd_grem as grem_cd_grem_orig,  " +
                      "tb_emen_dest.emen_cod_empr as emen_cod_empr_dest,  " +
                      "tb_emen_dest.emen_cnpj as emen_cnpj_dest,  " +
                      "tb_emen_dest.emen_nome as emen_nome_dest,  " +
                      "tb_emen_dest.emen_nomered as emen_nomered_dest,  " +
                      "tb_emen_dest.emen_lograd as emen_lograd_dest,  " +
                      "tb_emen_dest.emen_numero as emen_numero_dest,  " +
                      "tb_emen_dest.emen_uf as emen_uf_dest,  " +
                      "tb_emen_dest.emen_comple as emen_comple_dest,  " +
                      "tb_emen_dest.emen_bairro as emen_bairro_dest,  " +
                      "tb_emen_dest.emen_cep as emen_cep_dest,  " +
                      "tb_emen_dest.emen_ddd as emen_ddd_dest,  " +
                      "tb_emen_dest.emen_tel as emen_tel_dest,  " +
                      "tb_emen_dest.emen_email as emen_email_dest,  " +
                      "tb_emen_dest.grem_cd_grem as grem_cd_grem_dest,  " +
                      "tb_unme.*, tb_veic.*, tb_kits.*  " +
                      "from  " +
                      "tb_canf  " +
                      "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf  " +
                      "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma  " +
                      "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme  " +
                      "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
                      "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
                      "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
                      "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
                      "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma  " +
                      "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
                      "where  " +
                      "tb_canf.canf_numnt in (" + NotasINmontado + ")";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }
        //---
        public DataSet SelecionarNotaFiscaNumeroSumarizada(string canf_numnf, string emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

         

            comando =   "SELECT  " +
                        "sum(tb_itnf.itnf_quantidade) as itnf_quantidade,  " +
                        "max(tb_itnf.itnf_cod_itnf) as itnf_cod_itnf, " +
                        "tb_itnf.prma_cod_prma,  " +
                        "tb_canf.canf_chavenfe,  " +
                        "tb_canf.canf_cod_canf,  " +
                        "tb_canf.canf_dt_cancelamento,  " +
                        "tb_canf.canf_dt_criacao,  " +
                        "tb_canf.canf_dt_emissao,  " +
                        "tb_canf.canf_id_reg,  " +
                        "tb_canf.canf_manual,  " +
                        "tb_canf.canf_numnt,  " +
                        "tb_canf.canf_serienf,  " +
                        "tb_canf.canf_transito,  " +
                        "tb_canf.emen_cd_empr_destino,  " +
                        "tb_canf.emen_cod_empr_origem,  " +
                        "tb_canf.moto_cod_moto,  " +
                        "tb_canf.tran_cod_tran,  " +
                        "tb_canf.usur_cod_usur,  " +
                        "tb_canf.veic_cod_veic,  " +
                        "tb_itnf.itnf_cod_kit_lata,  " +
                        "tb_itnf.itnf_dt_criacao,  " +
                        "tb_itnf.itnf_id_reg,  " +
                        "tb_itnf.itnf_qtd_kit_lata,  " +
                        "tb_itnf.prma_cod_prma,  " +
                        "tb_prma.famp_cd_famp,  " +
                        "tb_prma.gppm_cd_gppm,  " +
                        "tb_prma.grem_cd_grem,  " +
                        "tb_prma.prma_ativo,  " +
                        "tb_prma.prma_confere_prma,  " +
                        "tb_prma.prma_descricao,  " +
                        "tb_prma.prma_dt_criacao,  " +
                        "tb_prma.prma_fator_conservacao,  " +
                        "tb_prma.prma_pn,  " +
                        "tb_prma.prma_preco,  " +
                        "tb_prma.tpma_cod_tipo,  " +
                        "tb_prma.unme_cod_unme,  " +
                        "tb_unme.unme_descricao,  " +
                        "tb_unme.unme_dt_criacao,  " +
                        "tb_unme.unme_id_reg,  " +
                        "tb_tran.tran_ativo,  " +
                        "tb_tran.tran_bairro,  " +
                        "tb_tran.tran_cep,  " +
                        "tb_tran.tran_cnpj,  " +
                        "tb_tran.tran_cod_tran,  " +
                        "tb_tran.tran_compl,  " +
                        "tb_tran.tran_ddd,  " +
                        "tb_tran.tran_dt_criacao,  " +
                        "tb_tran.tran_email,  " +
                        "tb_tran.tran_lograd,  " +
                        "tb_tran.tran_nome,  " +
                        "tb_tran.tran_nomered,  " +
                        "tb_tran.tran_num,  " +
                        "tb_tran.tran_tel,  " +
                        "tb_tran.tran_uf,  " +
                        "tb_moto.moto_bairro,  " +
                        "tb_moto.moto_cep,  " +
                        "tb_moto.moto_cnh,  " +
                        "tb_moto.moto_comple,  " +
                        "tb_moto.moto_cpf_moto,  " +
                        "tb_moto.moto_ddd,  " +
                        "tb_moto.moto_dt_criacao,  " +
                        "tb_moto.moto_email,  " +
                        "tb_moto.moto_lograd,  " +
                        "tb_moto.moto_nome,  " +
                        "tb_moto.moto_num,  " +
                        "tb_moto.moto_tel,  " +
                        "tb_moto.moto_uf,  " +
                        "tb_emen_orig.emen_cod_empr AS emen_cod_empr_orig,  " +
                        "tb_emen_orig.emen_cnpj AS emen_cnpj_orig,  " +
                        "tb_emen_orig.emen_nome AS emen_nome_orig,  " +
                        "tb_emen_orig.emen_nomered AS emen_nomered_orig,  " +
                        "tb_emen_orig.emen_lograd AS emen_lograd_orig,  " +
                        "tb_emen_orig.emen_numero AS emen_numero_orig,  " +
                        "tb_emen_orig.emen_uf AS emen_uf_orig,  " +
                        "tb_emen_orig.emen_cidade,  " +
                        "tb_emen_orig.emen_comple AS emen_comple_orig,  " +
                        "tb_emen_orig.emen_bairro AS emen_bairro_orig,  " +
                        "tb_emen_orig.emen_cep AS emen_cep_orig,  " +
                        "tb_emen_orig.emen_ddd AS emen_ddd_orig,  " +
                        "tb_emen_orig.emen_tel AS emen_tel_orig,  " +
                        "tb_emen_orig.emen_email AS emen_email_orig,  " +
                        "tb_emen_orig.grem_cd_grem AS grem_cd_grem_orig, " +
                        "tb_emen_dest.emen_cod_empr AS emen_cod_empr_dest, " +
                        "tb_emen_dest.emen_cnpj AS emen_cnpj_dest, " +
                        "tb_emen_dest.emen_nome AS emen_nome_dest, " +
                        "tb_emen_dest.emen_nomered AS emen_nomered_dest, " +
                        "tb_emen_dest.emen_lograd AS emen_lograd_dest, " +
                        "tb_emen_dest.emen_numero AS emen_numero_dest, " +
                        "tb_emen_dest.emen_uf AS emen_uf_dest, " +
                        "tb_emen_dest.emen_comple AS emen_comple_dest, " +
                        "tb_emen_dest.emen_bairro AS emen_bairro_dest, " +
                        "tb_emen_dest.emen_cep AS emen_cep_dest, " +
                        "tb_emen_dest.emen_ddd AS emen_ddd_dest, " +
                        "tb_emen_dest.emen_tel AS emen_tel_dest, " +
                        "tb_emen_dest.emen_email AS emen_email_dest, " +
                        "tb_emen_dest.grem_cd_grem AS grem_cd_grem_dest, " +
                        "tb_unme.unme_descricao, " +
                        "tb_unme.unme_dt_criacao, " +
                        "tb_unme.unme_id_reg, " +
                        "tb_veic.tpve_cod_tpve, " +
                        "tb_veic.veic_ativo, " +
                        "tb_veic.veic_cod_veic, " +
                        "tb_veic.veic_descricao, " +
                        "tb_veic.veic_placa, " +
                        "tb_kits.kits_cod_kits, " +
                        "tb_kits.kits_descricao, " +
                        "tb_kits.kits_dt_criacao, " +
                        "tb_kits.kits_quant_folha, " +
                        "tb_kits.kits_quant_pallets, " +
                        "tb_kits.kits_quant_pallets, " +
                        "tb_kits.kits_quant_por_camada, " +
                        "tb_kits.kits_quant_prod_prin, " +
                        "tb_kits.kits_quant_topo " +
                    "FROM " +
                    "    tb_canf " +
                    "INNER JOIN tb_itnf ON tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf " +
                    "INNER JOIN tb_prma ON tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma " +
                    "INNER JOIN tb_unme ON tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
                    "INNER JOIN tb_emen tb_emen_orig ON tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr " +
                    "INNER JOIN tb_emen tb_emen_dest ON tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr " +
                    "LEFT JOIN tb_tran ON tb_canf.tran_cod_tran = tb_tran.tran_cod_tran " +
                    "LEFT JOIN tb_veic ON tb_canf.veic_cod_veic = tb_veic.veic_cod_veic " +
                    "LEFT JOIN tb_kits ON tb_prma.prma_cod_prma = tb_kits.prma_cod_prma " +
                    "LEFT JOIN tb_moto ON tb_canf.moto_cod_moto = tb_moto.moto_cod_moto " +
                    "WHERE " +
                    "    tb_canf.canf_numnt in (" + canf_numnf + ")" + "and tb_emen_dest.emen_cod_empr = '" + emen_cod_empr + "'" +
                    "GROUP BY " +
                    "tb_canf.canf_chavenfe, tb_canf.canf_cod_canf, tb_canf.canf_dt_cancelamento, " +
                    "tb_canf.canf_dt_criacao,tb_canf.canf_dt_emissao,tb_canf.canf_id_reg, " +
                    "tb_canf.canf_manual,tb_canf.canf_numnt,tb_canf.canf_serienf,tb_canf.canf_transito, " +
                    "tb_canf.emen_cd_empr_destino, tb_canf.emen_cod_empr_origem, " +
                    "tb_canf.moto_cod_moto,tb_canf.tran_cod_tran,tb_canf.usur_cod_usur, " +
                    "tb_canf.veic_cod_veic, tb_itnf.prma_cod_prma, tb_itnf.itnf_cod_kit_lata, " +
                    "tb_itnf.itnf_dt_criacao, tb_itnf.itnf_id_reg, tb_itnf.itnf_qtd_kit_lata, " +
                    "tb_prma.famp_cd_famp, tb_prma.gppm_cd_gppm, tb_prma.grem_cd_grem, " +
                    "tb_prma.prma_ativo, tb_prma.prma_confere_prma, tb_prma.prma_descricao, " +
                    "tb_prma.prma_dt_criacao, tb_prma.prma_fator_conservacao, " +
                    "tb_prma.prma_pn, tb_prma.prma_preco, tb_prma.tpma_cod_tipo, tb_prma.unme_cod_unme, " +
                    "tb_unme.unme_descricao, tb_unme.unme_dt_criacao, tb_unme.unme_id_reg, tb_tran.tran_ativo, " +
                    "tb_tran.tran_bairro,tb_tran.tran_cep,tb_tran.tran_cnpj,tb_tran.tran_cod_tran, " +
                    "tb_tran.tran_compl, " +
                    "tb_tran.tran_ddd, " +
                    "tb_tran.tran_dt_criacao, " +
                    "tb_tran.tran_email, " +
                    "tb_tran.tran_lograd, " +
                    "tb_tran.tran_nome, " +
                    "tb_tran.tran_nomered, " +
                    "tb_tran.tran_num, " +
                    "tb_tran.tran_tel, " +
                    "tb_tran.tran_uf, " +
                    "tb_moto.moto_bairro, " +
                    "tb_moto.moto_cep, " +
                    "tb_moto.moto_cnh, " +
                    "tb_moto.moto_comple, " +
                    "tb_moto.moto_cpf_moto, " +
                    "tb_moto.moto_ddd, " +
                    "tb_moto.moto_dt_criacao, " +
                    "tb_moto.moto_email, " +
                    "tb_moto.moto_lograd, " +
                    "tb_moto.moto_nome, " +
                    "tb_moto.moto_num, " +
                    "tb_moto.moto_tel, " +
                    "tb_moto.moto_uf, " +
                    "emen_cod_empr_orig, " +
                    "emen_cnpj_orig, " +
                    "emen_nome_orig, " +
                    "emen_nomered_orig, " +
                    "emen_lograd_orig, " +
                    "emen_numero_orig, " +
                    "emen_uf_orig, " +
                    "tb_emen_orig.emen_cidade, " +
                    "emen_comple_orig, " +
                    "emen_bairro_orig, " +
                    "emen_cep_orig, " +
                    "emen_ddd_orig, " +
                    "emen_tel_orig, " +
                    "emen_email_orig, " +
                    "grem_cd_grem_orig, " +
                    "emen_cod_empr_dest, " +
                    "emen_cnpj_dest, " +
                    "emen_nome_dest, " +
                    "emen_nomered_dest, " +
                    "emen_lograd_dest, " +
                    "emen_numero_dest, " +
                    "emen_uf_dest, " +
                    "emen_comple_dest, " +
                    "emen_bairro_dest, " +
                    "emen_cep_dest, " +
                    "emen_ddd_dest, " +
                    "emen_tel_dest, " +
                    "emen_email_dest, " +
                    "grem_cd_grem_dest, " +
                    "tb_unme.unme_descricao, " +
                    "tb_unme.unme_dt_criacao, " +
                    "tb_unme.unme_id_reg, " +
                    "tb_veic.tpve_cod_tpve, " +
                    "tb_veic.veic_ativo, " +
                    "tb_veic.veic_cod_veic, " +
                    "tb_veic.veic_descricao, " +
                    "tb_veic.veic_placa, " +
                    "tb_kits.kits_cod_kits, " +
                    "tb_kits.kits_descricao, " +
                    "tb_kits.kits_dt_criacao, " +
                    "tb_kits.kits_quant_folha, " +
                    "tb_kits.kits_quant_pallets, " +
                    "tb_kits.kits_quant_pallets, " +
                    "tb_kits.kits_quant_por_camada, " +
                    "tb_kits.kits_quant_prod_prin, " +
                    "tb_kits.kits_quant_topo " +
                    "ORDER BY " +
                    "tb_canf.canf_cod_canf";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public void AlterarMotoristaVeiculoNF(Npgsql.NpgsqlConnection connection, int moto_cod_moto, int veic_cod_veic, string canf_cod_canf, string cpf_motorista = null)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            
            if (moto_cod_moto != 0)
            {
                comando = " UPDATE public.tb_canf  " +
                       "SET moto_cod_moto= " + moto_cod_moto + ", " +
                       "moto_cpf_moto = '" + cpf_motorista + "' " +
                        " WHERE canf_cod_canf = ' " + canf_cod_canf + "'";

                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }





            if (veic_cod_veic != 0)
            {
                comando = " UPDATE public.tb_canf  " +
                     "set veic_cod_veic=  " + veic_cod_veic +
                     " WHERE canf_cod_canf = ' " + canf_cod_canf + "'";

                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();

            }
        }

        public void AlterarTransportadoraNF(Npgsql.NpgsqlConnection connection, int tran_cod_tran, string canf_cod_canf)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            if (tran_cod_tran != 0)
            {
                comando = " UPDATE public.tb_canf  " +
                       "SET tran_cod_tran= " + tran_cod_tran +
                        " WHERE canf_cod_canf = ' " + canf_cod_canf + "'";

                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }
        }

        public DataSet SelecionarNotaFiscaempresaOrigemDestino(int emen_cod_empr, string tiponota)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            if (tiponota.ToUpper().Trim() == "ORIGEM")
            {

                comando = "select distinct tb_canf.*, tb_moes.moes_dt_criacao as dt_carregamento , Cast(current_date + (current_time + interval '1 hour')  - tb_moes.moes_dt_criacao::timestamp as char(15)) as tempo_decorrido from tb_canf inner join tb_moes on tb_moes.canf_cod_canf = tb_canf.canf_cod_canf  where emen_cod_empr_origem =  " + emen_cod_empr + " and tb_canf.canf_cod_canf not in (select canf_cod_canf from tb_moes where tpmo_cod_tpmo =2 or tpmo_cod_tpmo =4 or tpmo_cod_tpmo =7) order by tb_moes.moes_dt_criacao desc";
            }
            else
            {
                comando = "select distinct tb_canf.*,tb_moes.moes_dt_criacao as dt_carregamento ,  Cast(current_date + (current_time + interval '1 hour')  - tb_moes.moes_dt_criacao::timestamp as char(15)) as tempo_decorrido from tb_canf inner join tb_moes on tb_moes.canf_cod_canf = tb_canf.canf_cod_canf  where emen_cd_empr_destino =  " + emen_cod_empr + " and tb_canf.canf_cod_canf not in (select canf_cod_canf from tb_moes where tpmo_cod_tpmo =2 or tpmo_cod_tpmo =4 or tpmo_cod_tpmo =7) order by tb_moes.moes_dt_criacao desc";
            }




            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarNotaFiscalNumeroEmpresaOrigem(string emen_cnpj, string canf_numnt)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "select tb_canf.*, tb_itnf.*, tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,  " +
                      "tb_emen_orig.emen_cod_empr as emen_cod_empr_orig,  " +
                      "tb_emen_orig.emen_cnpj as emen_cnpj_orig, " +
                      "tb_emen_orig.emen_nome as emen_nome_orig, " +
                      "tb_emen_orig.emen_nomered as emen_nomered_orig,  " +
                      "tb_emen_orig.emen_lograd as emen_lograd_orig,  " +
                      "tb_emen_orig.emen_numero as emen_numero_orig,  " +
                      "tb_emen_orig.emen_uf as emen_uf_orig,  " +
                      "tb_emen_orig.emen_comple as emen_comple_orig,  " +
                      "tb_emen_orig.emen_bairro as emen_bairro_orig,  " +
                      "tb_emen_orig.emen_cep as emen_cep_orig,  " +
                      "tb_emen_orig.emen_ddd as emen_ddd_orig,  " +
                      "tb_emen_orig.emen_tel as emen_tel_orig,  " +
                      "tb_emen_orig.emen_email as emen_email_orig,  " +
                      "tb_emen_orig.grem_cd_grem as grem_cd_grem_orig,  " +
                      "tb_emen_dest.emen_cod_empr as emen_cod_empr_dest,  " +
                      "tb_emen_dest.emen_cnpj as emen_cnpj_dest,  " +
                      "tb_emen_dest.emen_nome as emen_nome_dest,  " +
                      "tb_emen_dest.emen_nomered as emen_nomered_dest,  " +
                      "tb_emen_dest.emen_lograd as emen_lograd_dest,  " +
                      "tb_emen_dest.emen_numero as emen_numero_dest,  " +
                      "tb_emen_dest.emen_uf as emen_uf_dest,  " +
                      "tb_emen_dest.emen_comple as emen_comple_dest,  " +
                      "tb_emen_dest.emen_bairro as emen_bairro_dest,  " +
                      "tb_emen_dest.emen_cep as emen_cep_dest,  " +
                      "tb_emen_dest.emen_ddd as emen_ddd_dest,  " +
                      "tb_emen_dest.emen_tel as emen_tel_dest,  " +
                      "tb_emen_dest.emen_email as emen_email_dest,  " +
                      "tb_emen_dest.grem_cd_grem as grem_cd_grem_dest,  " +
                      "tb_unme.*, tb_veic.*, tb_kits.*  " +
                      "from  " +
                      "tb_canf  " +
                      "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf  " +
                      "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma  " +
                      "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme  " +
                      "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
                      "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
                      "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
                      "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
                      "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma  " +
                      "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
                      "where  " +
                      "tb_canf.canf_numnt = '" + canf_numnt + "' and tb_emen_orig.emen_cnpj = '" + emen_cnpj + "' order by tb_prma.famp_cd_famp";
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        public string cadastraNotaFiscal(Objetos.objNotaFiscal objNotaFiscal, List<Objetos.objItensNotaFiscal> objItensNotaFiscal, Objetos.objXmlmo objXmlmo)
        {
            string erro = "";

            clsBanco banco = new clsBanco();
            object canf_cod_canf;
            string comando = "";

            

            int cod_transportadora = objNotaFiscal.Tran_cod_tran;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            Classes.clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsArim clsArim = new Classes.clsArim();
            clsNotaFiscal clsNota = new clsNotaFiscal();
            try
            {
                if (cod_transportadora != 0)
                {
                    comando = "INSERT INTO public.tb_canf(canf_numnt, canf_serienf, canf_chavenfe,  " +
                              "canf_dt_emissao, emen_cod_empr_origem, emen_cd_empr_destino,  tran_cod_tran,  " +
                              "canf_dt_criacao, usur_cod_usur, canf_manual, moto_cod_moto, veic_cod_veic )  " +
                              "VALUES ('" + objNotaFiscal.Canf_numnt + "','" + objNotaFiscal.Canf_serienf + "', '" + objNotaFiscal.Canf_chavenfe +
                              "','" + objNotaFiscal.Canf_dt_emissao + "'," + objNotaFiscal.Emen_cod_empr_origem + "," + objNotaFiscal.Emen_cd_empr_destino +
                              "," + objNotaFiscal.Tran_cod_tran + ",'" + objNotaFiscal.Canf_dt_criacao + "'," + objNotaFiscal.Usur_cd_usur + "," + objNotaFiscal.Canf_manual + ",5169,1323) RETURNING canf_cod_canf";
                }
                else
                {
                    comando = "INSERT INTO public.tb_canf(canf_numnt, canf_serienf, canf_chavenfe, " +
                              "canf_dt_emissao, emen_cod_empr_origem, emen_cd_empr_destino, " +
                              "canf_dt_criacao, usur_cod_usur, canf_manual, moto_cod_moto, veic_cod_veic) " +
                              "VALUES ('" + objNotaFiscal.Canf_numnt + "','" + objNotaFiscal.Canf_serienf + "', '" + objNotaFiscal.Canf_chavenfe +
                              "','" + objNotaFiscal.Canf_dt_emissao + "'," + objNotaFiscal.Emen_cod_empr_origem + "," + objNotaFiscal.Emen_cd_empr_destino +
                              ",'" + objNotaFiscal.Canf_dt_criacao + "'," + objNotaFiscal.Usur_cd_usur + "," + objNotaFiscal.Canf_manual + ",5169,1323) RETURNING canf_cod_canf";
                }
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                canf_cod_canf = cmd.ExecuteScalar();

                int cod_canf = Convert.ToInt32(canf_cod_canf);

                for (int i = 0; i <= objItensNotaFiscal.Count - 1; i++)
                {
                    comando = "INSERT INTO public.tb_itnf(canf_cod_canf, prma_cod_prma, itnf_quantidade, itnf_dt_criacao, usur_cd_usur)  " +
                              "VALUES (" + cod_canf + ", " + objItensNotaFiscal[i].Prma_cod_prma + ", " + objItensNotaFiscal[i].Itnf_quantidade.ToString().Replace(",", ".") +
                              ",'" + objItensNotaFiscal[i].Itnf_dt_criacao + "', " + objItensNotaFiscal[i].Usur_cd_usur + ")";

                    cmd = new NpgsqlCommand();
                    cmd.CommandText = comando;
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                }

                if (objNotaFiscal.Canf_manual != true)
                {
                    comando = "INSERT INTO public.tb_xlmo(canf_cod_canf, xmlo_dt_emissao, xlmo_dt_criacao, usur_cd_usur, xlmo_xmlnf_xlmo)  " +
                               "VALUES ( " + cod_canf + ",' " + objXmlmo.Xmlo_dt_emissao + "',' " + objXmlmo.Xlmo_dt_criacao +
                               "', " + objXmlmo.Usur_cd_usur + ",' " + objXmlmo.Xlmo_xmlnf_xlmo.Replace("'", "") + "')";

                    cmd = new NpgsqlCommand();
                    cmd.CommandText = comando;
                    cmd.Connection = connection;

                    cmd.ExecuteNonQuery();
                }

                

                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                connection.Close();
                erro = ex.Message;
                return erro;
            }
        }

		public void cadastrarItemNF(int cod_canf, Objetos.objItensNotaFiscal objItensNotaFiscal)
		{
			string erro = "";

			clsBanco banco = new clsBanco();
			object canf_cod_canf;
			string comando = "";

			comando = "INSERT INTO public.tb_itnf(canf_cod_canf, prma_cod_prma, itnf_quantidade, itnf_dt_criacao, usur_cd_usur)  " +
				  "VALUES (" + cod_canf + ", " + objItensNotaFiscal.Prma_cod_prma + ", 0 " +
				  ",'" + objItensNotaFiscal.Itnf_dt_criacao + "', " + objItensNotaFiscal.Usur_cd_usur + ")";

			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			cmd.ExecuteNonQuery();


		}

		public DataSet SelecionarNotaFiscaempresaComMovimentacaoPorCodigo(string canf_cod_canf, int tpmo_cod_tpmo)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_canf inner join tb_moes on tb_canf.canf_cod_canf = tb_moes.canf_cod_canf where tb_canf.canf_cod_canf = '" + canf_cod_canf + "' and tpmo_cod_tpmo =  " + tpmo_cod_tpmo;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        public DataSet SelecionarNotaFiscaempresaComMovimentacao(string numeronota, int emen_cod_empr, int tpmo_cod_tpmo)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_canf inner join tb_moes on tb_canf.canf_cod_canf = tb_moes.canf_cod_canf where canf_numnt = '" + numeronota + "' and tb_canf.emen_cod_empr_origem =  " + emen_cod_empr + " and tpmo_cod_tpmo =  " + tpmo_cod_tpmo;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarNotasFiscaisDigitadasEmpresa(int emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "select distinct tb_canf.* from tb_canf  where emen_cod_empr_origem =  " + emen_cod_empr + "  and canf_manual = true order by canf_dt_emissao desc";




            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarUsuarioResponsavelCarregamento(int emen_cod_empr, string canf_numnt)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "select distinct tb_usur.usur_nome from tb_usur  " +
                      "inner join tb_moes  " +
                      "on tb_usur.usur_cd_usur = tb_moes.usur_cd_usur  " +
                      "inner join tb_canf on tb_moes.canf_cod_canf = tb_canf.canf_cod_canf  " +
                      "WHERE  " +
                      "tb_canf.canf_numnt = '" + canf_numnt + "' AND  " +
                      "tb_moes.emen_cod_empr =  " + emen_cod_empr + " and  " +
                      "tb_moes.tpmo_cod_tpmo = 1 ";





            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

		public DataSet SelecionarNotaFiscaNumeroEmpresaOrigem(string canf_numnf, int usur_cd_usur)
		{
			clsBanco banco = new clsBanco();
			string comando = "";

			comando = "select tb_canf.*, tb_itnf.*, tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,  " +
					  "tb_emen_orig.emen_cod_empr as emen_cod_empr_orig,  " +
					  "tb_emen_orig.emen_cnpj as emen_cnpj_orig, " +
					  "tb_emen_orig.emen_nome as emen_nome_orig, " +
					  "tb_emen_orig.emen_nomered as emen_nomered_orig,  " +
					  "tb_emen_orig.emen_lograd as emen_lograd_orig,  " +
					  "tb_emen_orig.emen_numero as emen_numero_orig,  " +
					  "tb_emen_orig.emen_uf as emen_uf_orig,  " +
					  "tb_emen_orig.emen_cidade,  " +
					  "tb_emen_orig.emen_comple as emen_comple_orig,  " +
					  "tb_emen_orig.emen_bairro as emen_bairro_orig,  " +
					  "tb_emen_orig.emen_cep as emen_cep_orig,  " +
					  "tb_emen_orig.emen_ddd as emen_ddd_orig,  " +
					  "tb_emen_orig.emen_tel as emen_tel_orig,  " +
					  "tb_emen_orig.emen_email as emen_email_orig,  " +
					  "tb_emen_orig.grem_cd_grem as grem_cd_grem_orig,  " +
					  "tb_emen_dest.emen_cod_empr as emen_cod_empr_dest,  " +
					  "tb_emen_dest.emen_cnpj as emen_cnpj_dest,  " +
					  "tb_emen_dest.emen_nome as emen_nome_dest,  " +
					  "tb_emen_dest.emen_nomered as emen_nomered_dest,  " +
					  "tb_emen_dest.emen_lograd as emen_lograd_dest,  " +
					  "tb_emen_dest.emen_numero as emen_numero_dest,  " +
					  "tb_emen_dest.emen_uf as emen_uf_dest,  " +
					  "tb_emen_dest.emen_comple as emen_comple_dest,  " +
					  "tb_emen_dest.emen_bairro as emen_bairro_dest,  " +
					  "tb_emen_dest.emen_cep as emen_cep_dest,  " +
					  "tb_emen_dest.emen_ddd as emen_ddd_dest,  " +
					  "tb_emen_dest.emen_tel as emen_tel_dest,  " +
					  "tb_emen_dest.emen_email as emen_email_dest,  " +
					  "tb_emen_dest.grem_cd_grem as grem_cd_grem_dest,  " +
					  "tb_unme.*, tb_veic.*, tb_kits.*  " +
					  "from  " +
					  "tb_canf  " +
					  "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf  " +
					  "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma  " +
					  "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme  " +
					  "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
					  "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
					  "INNER JOIN tb_usur on tb_usur.emen_cod_empr = tb_canf.emen_cod_empr_origem " +
					  "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
					  "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
					  "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma  " +
					  "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
					  "where  " +
					  "tb_canf.canf_numnt = '" + canf_numnf + "' " +
					  "and tb_usur.usur_cd_usur = " + usur_cd_usur + " " +
					  "and tb_canf.canf_cod_canf = (select max(canf_cod_canf) from tb_canf where canf_numnt = '" + canf_numnf + "') order by tb_prma.famp_cd_famp";

			
			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();


			return ds;
		}

		public DataSet SelecionarNotaFiscaNumeroEmpresaDestino(string canf_numnf, int usur_cd_usur, string emen_empr_origem = null)
		{
			clsBanco banco = new clsBanco();
			string comando = "";




			comando = "SELECT tb_canf.*, tb_itnf.*,	tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,	tb_emen_orig.emen_cod_empr AS emen_cod_empr_orig, " +
					  "tb_emen_orig.emen_cnpj AS emen_cnpj_orig, tb_emen_orig.emen_nome AS emen_nome_orig, " +
					  "tb_emen_orig.emen_nomered AS emen_nomered_orig, " +
					  "tb_emen_orig.emen_lograd AS emen_lograd_orig, " +
					  "tb_emen_orig.emen_numero AS emen_numero_orig, " +
					  "tb_emen_orig.emen_uf AS emen_uf_orig, tb_emen_orig.emen_cidade, tb_emen_orig.emen_comple AS emen_comple_orig, " +
					  "tb_emen_orig.emen_bairro AS emen_bairro_orig, tb_emen_orig.emen_cep AS emen_cep_orig, tb_emen_orig.emen_ddd AS emen_ddd_orig, " +
					  "tb_emen_orig.emen_tel AS emen_tel_orig, tb_emen_orig.emen_email AS emen_email_orig, tb_emen_orig.grem_cd_grem AS grem_cd_grem_orig, " +
					  "tb_emen_dest.emen_cod_empr AS emen_cod_empr_dest, tb_emen_dest.emen_cnpj AS emen_cnpj_dest, tb_emen_dest.emen_nome AS emen_nome_dest, " +
					  "tb_emen_dest.emen_nomered AS emen_nomered_dest, tb_emen_dest.emen_lograd AS emen_lograd_dest, tb_emen_dest.emen_numero AS emen_numero_dest, " +
					  "tb_emen_dest.emen_uf AS emen_uf_dest, tb_emen_dest.emen_comple AS emen_comple_dest, tb_emen_dest.emen_bairro AS emen_bairro_dest, " +
					  "tb_emen_dest.emen_cep AS emen_cep_dest, tb_emen_dest.emen_ddd AS emen_ddd_dest, tb_emen_dest.emen_tel AS emen_tel_dest, " +
					  "tb_emen_dest.emen_email AS emen_email_dest, tb_emen_dest.grem_cd_grem AS grem_cd_grem_dest, tb_unme.*, " +
					  "tb_veic.*, tb_kits.* " +
					  "FROM " +
					  "tb_canf " +
					  "INNER JOIN tb_itnf ON tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf " +
					  "INNER JOIN tb_prma ON tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma " +
					  "INNER JOIN tb_unme ON tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
					  "INNER JOIN tb_emen tb_emen_orig ON tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr " +
					  "INNER JOIN tb_emen tb_emen_dest ON tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr " +
					  "INNER JOIN tb_usur ON tb_usur.emen_cod_empr = tb_canf.emen_cd_empr_destino " +
					  "LEFT JOIN tb_tran ON tb_canf.tran_cod_tran = tb_tran.tran_cod_tran " +
					  "LEFT JOIN tb_veic ON tb_canf.veic_cod_veic = tb_veic.veic_cod_veic " +
					  "LEFT JOIN tb_kits ON tb_prma.prma_cod_prma = tb_kits.prma_cod_prma " +
					  "LEFT JOIN tb_moto ON tb_canf.moto_cod_moto = tb_moto.moto_cod_moto " +
					  "WHERE " +
					  "tb_canf.canf_numnt = '" + canf_numnf + "' " +
					  "AND tb_usur.usur_cd_usur = " + usur_cd_usur + " AND tb_canf.emen_cd_empr_destino = ( " +
					  "SELECT emen_cod_empr FROM tb_usur WHERE usur_cd_usur = " + usur_cd_usur + ") " +
					  "AND tb_canf.canf_cod_canf = " +
					  "( SELECT canf_cod_canf FROM tb_canf WHERE canf_numnt = '" + canf_numnf + "' AND " +
					  "emen_cd_empr_destino = (SELECT emen_cod_empr FROM tb_usur WHERE usur_cd_usur = " + usur_cd_usur + " AND emen_cod_empr_origem = " + emen_empr_origem + " ))  ";
					  

			if (emen_empr_origem != null)
			{
				comando = comando + " AND emen_cod_empr_origem = " + emen_empr_origem; 
			}
		    comando = comando + " ORDER BY tb_prma.famp_cd_famp ";


			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();


			return ds;
		}



        public DataSet SelecionarNotaFiscaNumeroEmpresaDestinoInHouse(string canf_numnf, int usur_cd_usur, string emen_empr_origem)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

			comando = "select tb_canf.*, tb_itnf.*, tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,  " +
					  "tb_emen_orig.emen_cod_empr as emen_cod_empr_orig,  " +
					  "tb_emen_orig.emen_cnpj as emen_cnpj_orig, " +
					  "tb_emen_orig.emen_nome as emen_nome_orig, " +
					  "tb_emen_orig.emen_nomered as emen_nomered_orig,  " +
					  "tb_emen_orig.emen_lograd as emen_lograd_orig,  " +
					  "tb_emen_orig.emen_numero as emen_numero_orig,  " +
					  "tb_emen_orig.emen_uf as emen_uf_orig,  " +
					  "tb_emen_orig.emen_cidade,  " +
					  "tb_emen_orig.emen_comple as emen_comple_orig,  " +
					  "tb_emen_orig.emen_bairro as emen_bairro_orig,  " +
					  "tb_emen_orig.emen_cep as emen_cep_orig,  " +
					  "tb_emen_orig.emen_ddd as emen_ddd_orig,  " +
					  "tb_emen_orig.emen_tel as emen_tel_orig,  " +
					  "tb_emen_orig.emen_email as emen_email_orig,  " +
					  "tb_emen_orig.grem_cd_grem as grem_cd_grem_orig,  " +
					  "tb_emen_dest.emen_cod_empr as emen_cod_empr_dest,  " +
					  "tb_emen_dest.emen_cnpj as emen_cnpj_dest,  " +
					  "tb_emen_dest.emen_nome as emen_nome_dest,  " +
					  "tb_emen_dest.emen_nomered as emen_nomered_dest,  " +
					  "tb_emen_dest.emen_lograd as emen_lograd_dest,  " +
					  "tb_emen_dest.emen_numero as emen_numero_dest,  " +
					  "tb_emen_dest.emen_uf as emen_uf_dest,  " +
					  "tb_emen_dest.emen_comple as emen_comple_dest,  " +
					  "tb_emen_dest.emen_bairro as emen_bairro_dest,  " +
					  "tb_emen_dest.emen_cep as emen_cep_dest,  " +
					  "tb_emen_dest.emen_ddd as emen_ddd_dest,  " +
					  "tb_emen_dest.emen_tel as emen_tel_dest,  " +
					  "tb_emen_dest.emen_email as emen_email_dest,  " +
					  "tb_emen_dest.grem_cd_grem as grem_cd_grem_dest,  " +
					  "tb_unme.*, tb_veic.*, tb_kits.*  " +
					  "from  " +
					  "tb_canf  " +
					  "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf  " +
					  "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma  " +
					  "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme  " +
					  "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
					  "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
					  "INNER JOIN tb_usur on tb_usur.emen_cod_empr = tb_canf.emen_cd_empr_destino " +
					  "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
					  "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
					  "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma  " +
					  "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
					  "where  " +
					  "tb_canf.canf_numnt = '" + canf_numnf + "' " +
					  "AND tb_canf.emen_cd_empr_destino = ( SELECT emen_cod_empr FROM tb_usur WHERE usur_cd_usur = " + usur_cd_usur + " ) " +
					  "and tb_usur.usur_cd_usur = " + usur_cd_usur + " " +
					  "and tb_canf.canf_cod_canf in (select canf_cod_canf from tb_canf where canf_numnt = '" + canf_numnf + "' and emen_cd_empr_destino = ( SELECT emen_cod_empr FROM tb_usur WHERE usur_cd_usur = " + usur_cd_usur + " ) ) ";
			if (emen_empr_origem != null)
			{
				comando = comando + "and tb_emen_orig.emen_cod_empr = " + emen_empr_origem + " ";
			}
			comando = comando + "order by tb_prma.famp_cd_famp";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarNotaFiscaNumeroSemMovimentacao(string canf_numnf)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_canf.*, tb_itnf.*, tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,  " +
                      "tb_emen_orig.emen_cod_empr as emen_cod_empr_orig,  " +
                      "tb_emen_orig.emen_cnpj as emen_cnpj_orig, " +
                      "tb_emen_orig.emen_nome as emen_nome_orig, " +
                      "tb_emen_orig.emen_nomered as emen_nomered_orig,  " +
                      "tb_emen_orig.emen_lograd as emen_lograd_orig,  " +
                      "tb_emen_orig.emen_numero as emen_numero_orig,  " +
                      "tb_emen_orig.emen_uf as emen_uf_orig,  " +
                      "tb_emen_orig.emen_cidade,  " +
                      "tb_emen_orig.emen_comple as emen_comple_orig,  " +
                      "tb_emen_orig.emen_bairro as emen_bairro_orig,  " +
                      "tb_emen_orig.emen_cep as emen_cep_orig,  " +
                      "tb_emen_orig.emen_ddd as emen_ddd_orig,  " +
                      "tb_emen_orig.emen_tel as emen_tel_orig,  " +
                      "tb_emen_orig.emen_email as emen_email_orig,  " +
                      "tb_emen_orig.grem_cd_grem as grem_cd_grem_orig,  " +
                      "tb_emen_dest.emen_cod_empr as emen_cod_empr_dest,  " +
                      "tb_emen_dest.emen_cnpj as emen_cnpj_dest,  " +
                      "tb_emen_dest.emen_nome as emen_nome_dest,  " +
                      "tb_emen_dest.emen_nomered as emen_nomered_dest,  " +
                      "tb_emen_dest.emen_lograd as emen_lograd_dest,  " +
                      "tb_emen_dest.emen_numero as emen_numero_dest,  " +
                      "tb_emen_dest.emen_uf as emen_uf_dest,  " +
                      "tb_emen_dest.emen_comple as emen_comple_dest,  " +
                      "tb_emen_dest.emen_bairro as emen_bairro_dest,  " +
                      "tb_emen_dest.emen_cep as emen_cep_dest,  " +
                      "tb_emen_dest.emen_ddd as emen_ddd_dest,  " +
                      "tb_emen_dest.emen_tel as emen_tel_dest,  " +
                      "tb_emen_dest.emen_email as emen_email_dest,  " +
                      "tb_emen_dest.grem_cd_grem as grem_cd_grem_dest,  " +
                      "tb_unme.*, tb_veic.*, tb_kits.*  " +
                      "from  " +
                      "tb_canf  " +
                      "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf  " +
                      "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma  " +
                      "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme  " +
                      "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
                      "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
                      "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
                      "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
                      "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma  " +
                      "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
                      "where  " +
                      "tb_canf.canf_numnt = '" + canf_numnf + "' and tb_canf.canf_cod_canf not in (select canf_cod_canf from tb_moes where canf_cod_canf is not null) order by tb_prma.famp_cd_famp";

         


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarNotaFiscaNumeroSemMovimentacaoEmpresaOrigem(string canf_numnf, string usur_cd_usur)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "SELECT DISTINCT " +
					  "tb_canf.*, tb_itnf.*, tb_prma.*,	tb_unme.*,	tb_tran.*, tb_moto.*, " +
					  "tb_emen_orig.emen_cod_empr AS emen_cod_empr_orig, " +
					  "tb_emen_orig.emen_cnpj AS emen_cnpj_orig, tb_emen_orig.emen_nome AS emen_nome_orig, " +
					  "tb_emen_orig.emen_nomered AS emen_nomered_orig, tb_emen_orig.emen_lograd AS emen_lograd_orig, " +
					  "tb_emen_orig.emen_numero AS emen_numero_orig, tb_emen_orig.emen_uf AS emen_uf_orig, " +
					  "tb_emen_orig.emen_cidade, tb_emen_orig.emen_comple AS emen_comple_orig, tb_emen_orig.emen_bairro AS emen_bairro_orig, " +
					  "tb_emen_orig.emen_cep AS emen_cep_orig, tb_emen_orig.emen_ddd AS emen_ddd_orig, " +
					  "tb_emen_orig.emen_tel AS emen_tel_orig,	tb_emen_orig.emen_email AS emen_email_orig, " +
					  "tb_emen_orig.grem_cd_grem AS grem_cd_grem_orig, tb_emen_dest.emen_cod_empr AS emen_cod_empr_dest, " +
				      "tb_emen_dest.emen_cnpj AS emen_cnpj_dest, tb_emen_dest.emen_nome AS emen_nome_dest, " +
					  "tb_emen_dest.emen_nomered AS emen_nomered_dest,	tb_emen_dest.emen_lograd AS emen_lograd_dest, " +
					  "tb_emen_dest.emen_numero AS emen_numero_dest, " +
					  "tb_emen_dest.emen_uf AS emen_uf_dest, tb_emen_dest.emen_comple AS emen_comple_dest, " +
				      "tb_emen_dest.emen_bairro AS emen_bairro_dest, tb_emen_dest.emen_cep AS emen_cep_dest, " +
					  "tb_emen_dest.emen_ddd AS emen_ddd_dest, tb_emen_dest.emen_tel AS emen_tel_dest, " +
					  "tb_emen_dest.emen_email AS emen_email_dest, tb_emen_dest.grem_cd_grem AS grem_cd_grem_dest, " +
					  "tb_unme.*, tb_veic.*, tb_kits.* " +
					  "FROM tb_canf " +
					  "INNER JOIN tb_itnf ON tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf " +
					  "INNER JOIN tb_prma ON tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma " +
					  "INNER JOIN tb_unme ON tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
					  "INNER JOIN tb_emen tb_emen_orig ON tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr " +
					  "INNER JOIN tb_emen tb_emen_dest ON tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr " +
					  "LEFT JOIN tb_tran ON tb_canf.tran_cod_tran = tb_tran.tran_cod_tran " +
					  "LEFT JOIN tb_veic ON tb_canf.veic_cod_veic = tb_veic.veic_cod_veic " +
					  "LEFT JOIN tb_kits ON tb_prma.prma_cod_prma = tb_kits.prma_cod_prma " +
					  "LEFT JOIN tb_moto ON tb_canf.moto_cod_moto = tb_moto.moto_cod_moto " +
					  "INNER JOIN tb_usur ON tb_usur.emen_cod_empr = tb_emen_orig.emen_cod_empr " +
				      "WHERE tb_canf.canf_numnt = '" + canf_numnf + "' AND " +
					  "tb_canf.canf_cod_canf NOT IN( " +
					  "SELECT DISTINCT " +
					  "canf_cod_canf " +
					  "FROM	tb_moes	WHERE " +
					  "canf_cod_canf = ( " +
					  "SELECT DISTINCT 	canf_cod_canf " +
					  "FROM tb_canf	INNER JOIN tb_emen tb_emen_orig ON tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr " +
					  "INNER JOIN tb_usur ON tb_usur.emen_cod_empr = tb_canf.emen_cod_empr_origem " +
					  "WHERE canf_numnt = '" + canf_numnf + "' " +
					  "and tb_usur.usur_cd_usur = " + usur_cd_usur + " )	AND tpmo_cod_tpmo = 1 ) " +
					  "AND tb_usur.emen_cod_empr = tb_emen_orig.emen_cod_empr " +
					  "AND tb_usur.usur_cd_usur = " + usur_cd_usur + " " +
					  "ORDER BY tb_prma.famp_cd_famp";




            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);

			if (ds.Tables[0].Rows.Count <= 0)
			{
				comando = "SELECT DISTINCT " +
					  "tb_canf.*, tb_itnf.*, tb_prma.*,	tb_unme.*,	tb_tran.*, tb_moto.*, " +
					  "tb_emen_orig.emen_cod_empr AS emen_cod_empr_orig, " +
					  "tb_emen_orig.emen_cnpj AS emen_cnpj_orig, tb_emen_orig.emen_nome AS emen_nome_orig, " +
					  "tb_emen_orig.emen_nomered AS emen_nomered_orig, tb_emen_orig.emen_lograd AS emen_lograd_orig, " +
					  "tb_emen_orig.emen_numero AS emen_numero_orig, tb_emen_orig.emen_uf AS emen_uf_orig, " +
					  "tb_emen_orig.emen_cidade, tb_emen_orig.emen_comple AS emen_comple_orig, tb_emen_orig.emen_bairro AS emen_bairro_orig, " +
					  "tb_emen_orig.emen_cep AS emen_cep_orig, tb_emen_orig.emen_ddd AS emen_ddd_orig, " +
					  "tb_emen_orig.emen_tel AS emen_tel_orig,	tb_emen_orig.emen_email AS emen_email_orig, " +
					  "tb_emen_orig.grem_cd_grem AS grem_cd_grem_orig, tb_emen_dest.emen_cod_empr AS emen_cod_empr_dest, " +
					  "tb_emen_dest.emen_cnpj AS emen_cnpj_dest, tb_emen_dest.emen_nome AS emen_nome_dest, " +
					  "tb_emen_dest.emen_nomered AS emen_nomered_dest,	tb_emen_dest.emen_lograd AS emen_lograd_dest, " +
					  "tb_emen_dest.emen_numero AS emen_numero_dest, " +
					  "tb_emen_dest.emen_uf AS emen_uf_dest, tb_emen_dest.emen_comple AS emen_comple_dest, " +
					  "tb_emen_dest.emen_bairro AS emen_bairro_dest, tb_emen_dest.emen_cep AS emen_cep_dest, " +
					  "tb_emen_dest.emen_ddd AS emen_ddd_dest, tb_emen_dest.emen_tel AS emen_tel_dest, " +
					  "tb_emen_dest.emen_email AS emen_email_dest, tb_emen_dest.grem_cd_grem AS grem_cd_grem_dest, " +
					  "tb_unme.*, tb_veic.*, tb_kits.* " +
					  "FROM tb_canf " +
					  "INNER JOIN tb_itnf ON tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf " +
					  "INNER JOIN tb_prma ON tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma " +
					  "INNER JOIN tb_unme ON tb_prma.unme_cod_unme = tb_unme.unme_cod_unme " +
					  "INNER JOIN tb_emen tb_emen_orig ON tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr " +
					  "INNER JOIN tb_emen tb_emen_dest ON tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr " +
					  "LEFT JOIN tb_tran ON tb_canf.tran_cod_tran = tb_tran.tran_cod_tran " +
					  "LEFT JOIN tb_veic ON tb_canf.veic_cod_veic = tb_veic.veic_cod_veic " +
					  "LEFT JOIN tb_kits ON tb_prma.prma_cod_prma = tb_kits.prma_cod_prma " +
					  "LEFT JOIN tb_moto ON tb_canf.moto_cod_moto = tb_moto.moto_cod_moto " +
					  //"INNER JOIN tb_usur ON tb_usur.emen_cod_empr = tb_emen_orig.emen_cod_empr " +
					  "WHERE tb_canf.canf_numnt = '" + canf_numnf + "' AND " +
					  "tb_canf.canf_cod_canf NOT IN( " +
					  "SELECT DISTINCT " +
					  "canf_cod_canf " +
					  "FROM	tb_moes	WHERE " +
					  "canf_cod_canf = ( " +
					  "SELECT DISTINCT 	canf_cod_canf " +
					  "FROM tb_canf	INNER JOIN tb_emen tb_emen_orig ON tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr " +
					  "INNER JOIN tb_usur ON tb_usur.emen_cod_empr = tb_canf.emen_cod_empr_origem " +
					  "WHERE canf_numnt = '" + canf_numnf + "' " +
					  " )	AND tpmo_cod_tpmo = 1 ) " +
					  //"AND tb_usur.emen_cod_empr = tb_emen_orig.emen_cod_empr " +
					  //"AND tb_usur.usur_cd_usur = " + usur_cd_usur + " " +
					  "ORDER BY tb_prma.famp_cd_famp";

				ds = new DataSet();

				cmd = new NpgsqlCommand();
				cmd.CommandText = comando;
				cmd.Connection = connection;
				adap = new NpgsqlDataAdapter(cmd);
				adap.Fill(ds);
			}

            
            connection.Close();


            return ds;
        }

        public DataSet SelecionarNotaFiscaNumeroEmpresaDestinoSumarizada(string canf_numnf, int usur_cd_usur)
        {
            clsBanco banco = new clsBanco();
            string comando = "";




            comando = "SELECT tb_canf.canf_cod_canf, "+
                              "tb_canf.canf_numnt, tb_canf.emen_cd_empr_destino, tb_canf.Emen_cod_empr_origem, " +
                              "tb_prma.prma_cod_prma, tb_prma.famp_cd_famp, sum(tb_itnf.itnf_quantidade) as itnf_quantidade " +
                      "FROM " +
                      "tb_canf " +
                      "INNER JOIN tb_itnf ON tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf " +
                      "INNER JOIN tb_prma ON tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma " +
                      "INNER JOIN tb_emen tb_emen_orig ON tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr " +
                      "INNER JOIN tb_emen tb_emen_dest ON tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr " +
                      "INNER JOIN tb_usur ON tb_usur.emen_cod_empr = tb_canf.emen_cd_empr_destino " +
                      "WHERE " +
                      "tb_canf.canf_numnt = '" + canf_numnf + "' " +
                      "AND tb_usur.usur_cd_usur = " + usur_cd_usur +
                      "AND tb_canf.emen_cd_empr_destino = (SELECT emen_cod_empr FROM tb_usur WHERE usur_cd_usur = " + usur_cd_usur + ") " +
                      "AND tb_canf.canf_cod_canf = (SELECT MAX(canf_cod_canf) FROM tb_canf WHERE canf_numnt = '" + canf_numnf + "' AND emen_cd_empr_destino = (SELECT emen_cod_empr FROM tb_usur WHERE usur_cd_usur = " + usur_cd_usur + " ) ) " +
                      "GROUP BY tb_canf.canf_cod_canf, tb_canf.canf_numnt,	tb_canf.emen_cd_empr_destino,	tb_canf.Emen_cod_empr_origem, " +
				      "tb_prma.prma_cod_prma, tb_prma.famp_cd_famp " +
                      "ORDER BY  tb_prma.famp_cd_famp;";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet selecionarTotalNotasComFolhas(string canf_numnf, string usur_cd_usur)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select count(*) as total, canf_numnt from " +
                      "(SELECT count(tb_canf.canf_cod_canf), tb_prma.famp_cd_famp, tb_canf.canf_numnt " +
                      "FROM tb_canf " +
                      "INNER JOIN tb_itnf ON tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf " +
                      "INNER JOIN tb_prma ON tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma " +
                      "WHERE " +
                      "tb_canf.canf_numnt in (" + canf_numnf + ") and(tb_prma.famp_cd_famp = 3 or tb_prma.famp_cd_famp = 5) " +
                      "group by tb_prma.famp_cd_famp, tb_canf.canf_numnt) as a " +
                      "group by canf_numnt";

            comando = "select canf_numnt, max(canf.canf_cod_canf) as canf_cod_canf from tb_canf as canf " +
                      "inner join tb_itnf itnf " +
                      "on canf.canf_cod_canf = itnf.canf_cod_canf " +
                      "inner join tb_prma prma " +
                      "on prma.prma_cod_prma = itnf.prma_cod_prma " +
                      "where canf_numnt in(" +canf_numnf +")  and " +
                      "(prma.famp_cd_famp = 3 or prma.famp_cd_famp = 5) and " +
                      "itnf.itnf_quantidade > 0 " +
                      "AND canf.emen_cd_empr_destino = (SELECT emen_cod_empr FROM tb_usur WHERE usur_cd_usur = " + usur_cd_usur + ") " +
                      "group by canf_numnt ";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

		public DataSet SelecionarNotaFiscalNumeroEmpresaOrigemDestino(string emen_cnpj, string emen_cnpj1,  string canf_numnt)
		{
			clsBanco banco = new clsBanco();
			string comando = "";


			comando = "select tb_canf.*, tb_itnf.*, tb_prma.*, tb_unme.*, tb_tran.*, tb_moto.*,  " +
					  "tb_emen_orig.emen_cod_empr as emen_cod_empr_orig,  " +
					  "tb_emen_orig.emen_cnpj as emen_cnpj_orig, " +
					  "tb_emen_orig.emen_nome as emen_nome_orig, " +
					  "tb_emen_orig.emen_nomered as emen_nomered_orig,  " +
					  "tb_emen_orig.emen_lograd as emen_lograd_orig,  " +
					  "tb_emen_orig.emen_numero as emen_numero_orig,  " +
					  "tb_emen_orig.emen_uf as emen_uf_orig,  " +
					  "tb_emen_orig.emen_comple as emen_comple_orig,  " +
					  "tb_emen_orig.emen_bairro as emen_bairro_orig,  " +
					  "tb_emen_orig.emen_cep as emen_cep_orig,  " +
					  "tb_emen_orig.emen_ddd as emen_ddd_orig,  " +
					  "tb_emen_orig.emen_tel as emen_tel_orig,  " +
					  "tb_emen_orig.emen_email as emen_email_orig,  " +
					  "tb_emen_orig.grem_cd_grem as grem_cd_grem_orig,  " +
					  "tb_emen_dest.emen_cod_empr as emen_cod_empr_dest,  " +
					  "tb_emen_dest.emen_cnpj as emen_cnpj_dest,  " +
					  "tb_emen_dest.emen_nome as emen_nome_dest,  " +
					  "tb_emen_dest.emen_nomered as emen_nomered_dest,  " +
					  "tb_emen_dest.emen_lograd as emen_lograd_dest,  " +
					  "tb_emen_dest.emen_numero as emen_numero_dest,  " +
					  "tb_emen_dest.emen_uf as emen_uf_dest,  " +
					  "tb_emen_dest.emen_comple as emen_comple_dest,  " +
					  "tb_emen_dest.emen_bairro as emen_bairro_dest,  " +
					  "tb_emen_dest.emen_cep as emen_cep_dest,  " +
					  "tb_emen_dest.emen_ddd as emen_ddd_dest,  " +
					  "tb_emen_dest.emen_tel as emen_tel_dest,  " +
					  "tb_emen_dest.emen_email as emen_email_dest,  " +
					  "tb_emen_dest.grem_cd_grem as grem_cd_grem_dest,  " +
					  "tb_unme.*, tb_veic.*, tb_kits.*  " +
					  "from  " +
					  "tb_canf  " +
					  "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf  " +
					  "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma  " +
					  "inner join tb_unme on tb_prma.unme_cod_unme = tb_unme.unme_cod_unme  " +
					  "inner join tb_emen tb_emen_orig on tb_canf.emen_cod_empr_origem = tb_emen_orig.emen_cod_empr  " +
					  "inner join tb_emen tb_emen_dest on tb_canf.emen_cd_empr_destino = tb_emen_dest.emen_cod_empr  " +
					  "left join tb_tran on tb_canf.tran_cod_tran = tb_tran.tran_cod_tran  " +
					  "left join tb_veic on tb_canf.veic_cod_veic = tb_veic.veic_cod_veic  " +
					  "left join tb_kits on tb_prma.prma_cod_prma = tb_kits.prma_cod_prma  " +
					  "left join tb_moto on tb_canf.moto_cod_moto = tb_moto.moto_cod_moto  " +
					  "where  " +
					  "tb_canf.canf_numnt = '" + canf_numnt + "' and tb_emen_orig.emen_cnpj = '" + emen_cnpj + "' order by tb_prma.famp_cd_famp";
			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();


			return ds;
		}


		//alterado marco

		public DataSet SelecionarProdutoFamiliaNF(string canf_numnt, string emen_cod_empr_origem, string emen_cd_empr_destino, string famp_cd_famp)
		{
			clsBanco banco = new clsBanco();
			string comando = "";


			comando = "select * from tb_canf " +
					  "inner join tb_itnf on tb_canf.canf_cod_canf = tb_itnf.canf_cod_canf " +
					  "inner join tb_prma on tb_itnf.prma_cod_prma = tb_prma.prma_cod_prma " +
					  "inner join tb_famp on tb_famp.famp_cd_famp = tb_prma.famp_cd_famp " +
					   "where canf_numnt = '" + canf_numnt + "'" +
					   "and tb_prma.famp_cd_famp =  " + famp_cd_famp + " " +
					   "and emen_cod_empr_origem =  " + emen_cod_empr_origem + " " +
					   "and emen_cd_empr_destino =  " + emen_cd_empr_destino + " "; ;

			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();


			return ds;
		}



	}
}

    



