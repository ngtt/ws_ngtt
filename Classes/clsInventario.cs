﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;


namespace wsngtt.Classes
{
    public class clsInventario
    {
        public DataSet SelecionarInventario(int emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_inve.*,tb_prma.prma_descricao, tb_tpmo.tpmo_descricao " +
                      "from tb_inve " +
                      "inner join tb_emen on tb_inve.emen_cod_empr = tb_emen.emen_cod_empr " +
                      "inner join tb_prma on tb_inve.prma_cod_prma = tb_prma.prma_cod_prma " +
                      "inner join tb_tpmo on tb_inve.tpmo_cod_tpmo = tb_tpmo.tpmo_cod_tpmo " +
                      "where tb_inve.emen_cod_empr = " + emen_cod_empr + " and tpma_cod_tipo = 1 order by inve_cod_inve desc";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public string CadastrarInventario(Objetos.objInventario objInve, double quantidadeProduto, double QuantidadeDivisores, bool qtd = true)
        {
            
            string erro = "";
            clsBanco banco = new clsBanco();
            string comando = "";
            

            clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();
            clsMovimentoEstoque clsMoes = new Classes.clsMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objMovimentoEstoque objMoes = new Objetos.objMovimentoEstoque();

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();
            try
            {
                //aqui irei fazer o insert da tabela inventário
                comando = "INSERT INTO public.tb_inve(prma_cod_prma, emen_cod_empr, inve_quantidade, " +
                           "inve_dt_criacao, usur_cd_usur, inve_operacao, prma_cod_prma_pallet, " +
                           "prma_cod_prma_topo, prma_cod_prma_divisor, emen_cod_empr_forn, tpmo_cod_tpmo) " +
                           "VALUES (" + objInve.Prma_cod_prma + "," + objInve.Emen_cod_empr + "," + objInve.Inve_quantidade +
                           ",'" + objInve.Inve_dt_criacao + "'," + objInve.Usur_cd_usur + ",'" + objInve.Inve_operacao + "'," + objInve.Prma_cod_prma_pallet +
                           "," + objInve.Prma_cod_prma_topo + "," + objInve.Prma_cod_prma_divisor + "," + objInve.Emen_cod_empr_forn + "," + objInve.Tpmo_cod_tpmo + ")";

                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();

                if (objInve.Inve_operacao == "Contagem Estoque" && qtd == true)
                {
                    transacao.Commit();
                    connection.Close();
                    return "";
                    
                }

                //aqui vou inserir ou atualizar a tabela de saldos de acordo com a operação que iremos receber
                if (objInve.Inve_operacao == "Saída" || objInve.Inve_operacao == "Utilizado produção" || objInve.Inve_operacao == "Contagem Estoque")
                {
                    objInve.Inve_quantidade = objInve.Inve_quantidade * -1;
                    QuantidadeDivisores = QuantidadeDivisores * -1;
                    quantidadeProduto = quantidadeProduto * -1;

                }

                //pegar o mapeamento do produto de acordo com a empresa fornecedora
                //Objetos.objMapeamentoProdutos objMapeamentoProdutos = new Objetos.objMapeamentoProdutos();
                //clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();

                cls_empresa clsempresa = new cls_empresa();

                DataSet dsEmpresa = clsempresa.SelecionarEmpresa(objInve.Emen_cod_empr);

                int prma_cod_prma_aux;
                prma_cod_prma_aux = objInve.Prma_cod_prma;
                

                objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                objSaldoEstoque.Sles_quantidade = quantidadeProduto;
                objSaldoEstoque.Prma_cod_prma = prma_cod_prma_aux;
                objSaldoEstoque.Tpop_cod_tpop = objInve.Tpmo_cod_tpmo;

                //aqui vou gravar o saldo em estoque para o produto principal do inventário
                clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);

                //linha nova
                prma_cod_prma_aux = objInve.Prma_cod_prma_pallet;

                if (objInve.Inve_operacao == "Utilizado produção" || objInve.Inve_operacao == "Contagem Estoque")
                {
                    
                    objMoes.Moes_quantidade = quantidadeProduto;
                    objMoes.Prma_cod_prma = objInve.Prma_cod_prma;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 9;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);
                    
                    //movido para cá
                    objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                    objSaldoEstoque.Sles_quantidade = Math.Abs(objInve.Inve_quantidade);
                    objSaldoEstoque.Prma_cod_prma = prma_cod_prma_aux;
                    objSaldoEstoque.Tpop_cod_tpop = 6;

                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);

                    objMoes.Moes_quantidade = objInve.Inve_quantidade; ;
                    objMoes.Prma_cod_prma = objInve.Prma_cod_prma_pallet;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 9;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);

                    prma_cod_prma_aux = objInve.Prma_cod_prma_topo;

                    //aqui vou atualizar o estoque para os topos
                    objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                    objSaldoEstoque.Sles_quantidade = Math.Abs(objInve.Inve_quantidade);
                    objSaldoEstoque.Prma_cod_prma = prma_cod_prma_aux;
                    objSaldoEstoque.Tpop_cod_tpop = 6;

                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);

                    objMoes.Moes_quantidade = objInve.Inve_quantidade; ;
                    objMoes.Prma_cod_prma = objInve.Prma_cod_prma_topo;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 9;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);

                    prma_cod_prma_aux = objInve.Prma_cod_prma_divisor;

                    objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                    objSaldoEstoque.Sles_quantidade = Math.Abs(QuantidadeDivisores);
                    objSaldoEstoque.Prma_cod_prma = prma_cod_prma_aux;
                    objSaldoEstoque.Tpop_cod_tpop = 5;
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);

                    objMoes.Moes_quantidade = objSaldoEstoque.Sles_quantidade; 
                    objMoes.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 9;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);
                }
                else
                {
                    objMoes.Moes_quantidade = quantidadeProduto;
                    objMoes.Prma_cod_prma = objInve.Prma_cod_prma;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 8;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);

                    //movido para cá
                    objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                    objSaldoEstoque.Sles_quantidade = objInve.Inve_quantidade;
                    objSaldoEstoque.Prma_cod_prma = prma_cod_prma_aux;
                    objSaldoEstoque.Tpop_cod_tpop = 33;

                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);

                    objMoes.Moes_quantidade = objInve.Inve_quantidade; ;
                    objMoes.Prma_cod_prma = objInve.Prma_cod_prma_pallet;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 8;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);

                    prma_cod_prma_aux = objInve.Prma_cod_prma_topo;

                    //aqui vou atualizar o estoque para os topos
                    objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                    objSaldoEstoque.Sles_quantidade = objInve.Inve_quantidade;
                    objSaldoEstoque.Prma_cod_prma = prma_cod_prma_aux;
                    objSaldoEstoque.Tpop_cod_tpop = 33;

                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);

                    objMoes.Moes_quantidade = objInve.Inve_quantidade; ;
                    objMoes.Prma_cod_prma = objInve.Prma_cod_prma_topo;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 8;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);

                    prma_cod_prma_aux = objInve.Prma_cod_prma_divisor;

                    objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                    objSaldoEstoque.Sles_quantidade = QuantidadeDivisores;
                    objSaldoEstoque.Prma_cod_prma = prma_cod_prma_aux;
                    objSaldoEstoque.Tpop_cod_tpop = 33;
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);

                    objMoes.Moes_quantidade = objInve.Inve_quantidade; ;
                    objMoes.Prma_cod_prma = objSaldoEstoque.Prma_cod_prma;
                    objMoes.Moes_dt_criacao = DateTime.Now;
                    objMoes.Tpmo_cod_tpmo = 8;
                    objMoes.Usur_cod_usur = objInve.Usur_cd_usur;
                    clsMoes.cadastramovimentoEstoque(objMoes, connection, objInve.Emen_cod_empr);
                }
                
                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch(Exception error)
            {
                transacao.Rollback();
                connection.Close();
                return error.Message;

            }
			
        }

		//aqui cadastro inventário entrada de linha apenas
		public string CadastrarInventarioEntradaLinha(Objetos.objInventario objInve)
		{

			string erro = "";
			clsBanco banco = new clsBanco();
			string comando = "";

		
			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			NpgsqlTransaction transacao;
			connection.Open();
			transacao = (NpgsqlTransaction)connection.BeginTransaction();
			try
			{
				//aqui irei fazer o insert da tabela inventário
				comando = "INSERT INTO public.tb_inve(prma_cod_prma, emen_cod_empr, inve_quantidade, " +
						   "inve_dt_criacao, usur_cd_usur, inve_operacao, prma_cod_prma_pallet, " +
						   "prma_cod_prma_topo, prma_cod_prma_divisor, emen_cod_empr_forn, tpmo_cod_tpmo) " +
						   "VALUES (" + objInve.Prma_cod_prma + "," + objInve.Emen_cod_empr + "," + objInve.Inve_quantidade +
						   ",'" + objInve.Inve_dt_criacao + "'," + objInve.Usur_cd_usur + ",'" + objInve.Inve_operacao + "'," + objInve.Prma_cod_prma_pallet +
						   "," + objInve.Prma_cod_prma_topo + "," + objInve.Prma_cod_prma_divisor + "," + objInve.Emen_cod_empr_forn + "," + objInve.Tpmo_cod_tpmo + ")";

				NpgsqlCommand cmd = new NpgsqlCommand();
				cmd.CommandText = comando;
				cmd.Connection = connection;
				cmd.ExecuteNonQuery();
				transacao.Commit();
				connection.Close();
				return erro;
			}
			catch (Exception error)
			{
				transacao.Rollback();
				connection.Close();
				return error.Message;

			}

		}

		public DataSet SelecionarInventarioPorCodigo(int inve_cod_inve)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_inve.*,tb_prma.prma_descricao " +
                      "from tb_inve " +
                      "inner join tb_emen on tb_inve.emen_cod_empr = tb_emen.emen_cod_empr " +
                      "inner join tb_prma on tb_inve.prma_cod_prma = tb_prma.prma_cod_prma " +
                      "where tb_inve.inve_cod_inve = " + inve_cod_inve;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarInventarioME(int emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_inve.*,tb_prma.prma_descricao, tb_tpmo.tpmo_descricao " +
                      "from tb_inve " +
                      "inner join tb_emen on tb_inve.emen_cod_empr = tb_emen.emen_cod_empr " +
                      "inner join tb_prma on tb_inve.prma_cod_prma = tb_prma.prma_cod_prma " +
                      "inner join tb_tpmo on tb_inve.tpmo_cod_tpmo = tb_tpmo.tpmo_cod_tpmo " +
                      "where tb_inve.emen_cod_empr = " + emen_cod_empr + " and tpma_cod_tipo = 2";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public string CadastrarInventarioME(Objetos.objInventario objInve)
        {

            string erro = "";
            clsBanco banco = new clsBanco();
            string comando = "";

            clsSaldoEstoque clsSaldoEstoque = new Classes.clsSaldoEstoque();
            clsMovimentoEstoque clsMoes = new Classes.clsMovimentoEstoque();
            Objetos.objSaldoEstoque objSaldoEstoque = new Objetos.objSaldoEstoque();
            Objetos.objMovimentoEstoque objMoes = new Objetos.objMovimentoEstoque();

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();
            try
            {
                //aqui irei fazer o insert da tabela inventário
                comando = "INSERT INTO public.tb_inve(prma_cod_prma, emen_cod_empr, inve_quantidade, " +
                           "inve_dt_criacao, usur_cd_usur, inve_operacao,  " +
                           "emen_cod_empr_forn, tpmo_cod_tpmo) " +
                           "VALUES (" + objInve.Prma_cod_prma + "," + objInve.Emen_cod_empr + "," + objInve.Inve_quantidade +
                           ",'" + objInve.Inve_dt_criacao + "'," + objInve.Usur_cd_usur + ",'" + objInve.Inve_operacao + "'" +
                           "," + objInve.Emen_cod_empr_forn + ", " + objInve.Tpmo_cod_tpmo + ")";


                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();

                //aqui vou inserir ou atualizar a tabela de saldos de acordo com a operação que iremos receber
                if (objInve.Inve_operacao == "Saída" || objInve.Inve_operacao == "Utilizado produção")
                {
                    objInve.Inve_quantidade = objInve.Inve_quantidade * -1;
                }

                //pegar o mapeamento do produto de acordo com a empresa fornecedora
                Objetos.objMapeamentoProdutos objMapeamentoProdutos = new Objetos.objMapeamentoProdutos();
                clsMapeamentoProdutos clsMapeamentoProdutos = new Classes.clsMapeamentoProdutos();

                cls_empresa clsempresa = new cls_empresa();

                DataSet dsEmpresa = clsempresa.SelecionarEmpresa(objInve.Emen_cod_empr);

                int prma_cod_prma_aux;

                /*if (Convert.ToBoolean(dsEmpresa.Tables[0].Rows[0]["emen_forn_emen"].ToString()) == true)
                {
                    prma_cod_prma_aux = objInve.Prma_cod_prma;
                }
                else
                {
                    DataSet mapeamento = clsMapeamentoProdutos.SelecionarMapeamentoProdutocodProdutoExternoFornecedora(objInve.Prma_cod_prma, objInve.Emen_cod_empr_forn);
                    if (mapeamento.Tables[0].Rows.Count == 0)
                    {
                        transacao.Rollback();
                        connection.Close();
                        return "Mapeamendo não encontrado para o produto";
                    }
                    else
                    {
                        prma_cod_prma_aux = Convert.ToInt32(mapeamento.Tables[0].Rows[0]["prma_cod_prma_empr"].ToString());
                    }
                }*/


                objSaldoEstoque.Emen_cod_empresa = objInve.Emen_cod_empr;
                objSaldoEstoque.Sles_quantidade = objInve.Inve_quantidade;
                objSaldoEstoque.Prma_cod_prma = objInve.Prma_cod_prma;
                objSaldoEstoque.Tpop_cod_tpop = objInve.Tpmo_cod_tpmo;

                //aqui vou gravar o saldo em estoque para o produto principal do inventário
                clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objInve.Emen_cod_empr, objInve.Emen_cod_empr);
                
               
                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception error)
            {
                transacao.Rollback();
                connection.Close();
                return error.Message;

            }
            //aqui irei cadastrar o inventario


        }
    }
}