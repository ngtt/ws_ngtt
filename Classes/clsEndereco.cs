﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsEndereco
    {
        public int Cadastrarendereco(Objetos.objEndereco objEndereco)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            int ende_cd_ende = 0;


            comando = "INSERT INTO public.tb_ende(ende_descricao, ende_ativo, ende_dt_criacao, usur_cd_usur) " +
                                                 "VALUES ('" + objEndereco.Ende_descricao + "','" + objEndereco.Ende_ativo + "','" +
                                                             objEndereco.Ende_dt_criacao + "'," + objEndereco.Usur_cd_usur + ") ruturning ende_cod_ende";



            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            ende_cd_ende = Convert.ToInt32(cmd.ExecuteScalar());

            connection.Close();
            return Convert.ToInt32(ende_cd_ende);
        }

        public void alterarEndereco(Objetos.objEndereco objEndereco)
        {
            clsBanco banco = new clsBanco();
            string comando = "";


            comando = "UPDATE public.tb_ende SET " +
                      "ende_descricao='" + objEndereco.Ende_descricao + "', ende_ativo='" + objEndereco.Ende_ativo +
                      "', ende_dt_criacao='" + objEndereco.Ende_dt_criacao + "', usur_cd_usur=" + objEndereco.Usur_cd_usur +
                      "WHERE ende_cod_ende = " + objEndereco.Ende_cod_ende;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet SelecionarTodosEnderecos()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_ende order by ende_descricao";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;
        }

        public DataSet SelecionarUmEndereco(int ende_cod_ende)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_ende where ende_cod_ende = " + ende_cod_ende;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;
        }
    }
}