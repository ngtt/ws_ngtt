﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;


namespace wsngtt.Classes
{
    public class clsMotorista
    {
        public int InserirMotorista(Objetos.objMotorista objMotorista)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object moto_cd_moto;


            comando = "INSERT INTO public.tb_moto(moto_cpf_moto, moto_cnh, moto_nome, moto_lograd, " +
                      "moto_num, moto_uf, moto_comple, moto_bairro, moto_cep, moto_ddd, moto_tel, " +
                      "moto_email, moto_dt_criacao, usur_cd_usur) " +
                      "VALUES ('" + objMotorista.Moto_cpf_moto + "','" + objMotorista.Moto_cnh + "','" + objMotorista.Moto_nome + "'," +
                      "'" + objMotorista.Moto_lograd + "','" + objMotorista.Moto_num + "','" + objMotorista.Moto_uf + "','" + objMotorista.Moto_comple + "'," +
                      "'" + objMotorista.Moto_bairro + "','" + objMotorista.Moto_cep + "','" + objMotorista.Moto_ddd + "','" + objMotorista.Moto_tel + "'," +
                      "'" + objMotorista.Moto_email + "','" + objMotorista.Moto_dt_criacao + "'," + objMotorista.Usur_cd_usur + ") returning moto_cod_moto";
                       

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            moto_cd_moto = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(moto_cd_moto);
        }

        public void AlterarMotorista(Objetos.objMotorista objMotorista)
        {
            clsBanco banco = new clsBanco();
            string comando = "UPDATE public.tb_moto SET " +
                             "moto_cpf_moto='" + objMotorista.Moto_cpf_moto + "'," +
                             "moto_cnh='" + objMotorista.Moto_cnh + "'," +
                             "moto_nome='" + objMotorista.Moto_nome + "'," +
                             "moto_lograd='" + objMotorista.Moto_lograd + "'," +
                             "moto_num='" + objMotorista.Moto_num + "'," +
                             "moto_uf='" + objMotorista.Moto_uf + "',"+
                             "moto_comple='" + objMotorista.Moto_comple + "'," +
                             "moto_bairro='" + objMotorista.Moto_bairro + "',"+
                             "moto_cep='" + objMotorista.Moto_cep + "'," +
                             "moto_ddd='" + objMotorista.Moto_ddd + "'," +
                             "moto_tel='" + objMotorista.Moto_tel + "'," +
                             "moto_email='" + objMotorista.Moto_email + "'," +
                             "moto_dt_criacao='" + objMotorista.Moto_dt_criacao + "'," +
                             "usur_cd_usur=" + objMotorista.Usur_cd_usur + " " +
                             "WHERE moto_cod_moto = " + objMotorista.Moto_cod_moto;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();

        }

        public DataSet SelecionarMotoristasTotos()
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_moto order by moto_nome";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }

		public DataSet SelecionarMotoristaPorNome(string Nome_moto)
		{

			clsBanco banco = new clsBanco();
			string comando = "";

			comando = "select * from tb_moto where moto_nome = '" + Nome_moto + "'";


			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();

			return ds;

		}

		public DataSet SelecionarMotoristaPorCodigo(int moto_cod_moto)
        {

            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_moto where moto_cod_moto = " + moto_cod_moto;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();

            return ds;

        }
    }
}