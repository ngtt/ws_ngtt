﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace wsngtt.Classes
{
    public class clsEntradaLinha
    {
        public string cadastraEntradaLinha(List<Objetos.objMovimentoEstoque> objMovimentoEstoque, List<Objetos.objSaldoEstoque> objSaldoEstoque, int emen_cod_empr)
        {
            string erro = "";

            clsBanco banco = new clsBanco();


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            Classes.clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsArim clsArim = new Classes.clsArim();
            clsNotaFiscal clsNota = new clsNotaFiscal();
            try
            {

                

                for (int i = 0; i <= objMovimentoEstoque.Count - 1; i++)
                {
                    
                    clsMovimentoEstoque.cadastramovimentoEstoque(objMovimentoEstoque[i], connection, emen_cod_empr);
                    
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque[i], connection, emen_cod_empr, emen_cod_empr);


                }

               
                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                connection.Close();
                erro = ex.Message;
                return erro;
            }
        }

        public string cadastraRetornoLinha(List<Objetos.objMovimentoEstoque> objMovimentoEstoque, List<Objetos.objSaldoEstoque> objSaldoEstoque, int emen_cod_empr)
        {
            string erro = "";

            clsBanco banco = new clsBanco();


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

            Classes.clsMovimentoEstoque clsMovimentoEstoque = new clsMovimentoEstoque();
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsArim clsArim = new Classes.clsArim();
            clsNotaFiscal clsNota = new clsNotaFiscal();
            try
            {



                for (int i = 0; i <= objMovimentoEstoque.Count - 1; i++)
                {
                    if (objMovimentoEstoque[i].Prma_cod_prma != 0)
                    {
                        clsMovimentoEstoque.cadastramovimentoEstoque(objMovimentoEstoque[i], connection, emen_cod_empr);
                    }
                    clsSaldoEstoque.atualizarEstoque(objSaldoEstoque[i], connection, emen_cod_empr, emen_cod_empr);


                }


                transacao.Commit();
                connection.Close();
                return erro;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                connection.Close();
                erro = ex.Message;
                return erro;
            }
        }
    }
}