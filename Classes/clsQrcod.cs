﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
namespace wsngtt.Classes
{
    public class clsQrcod
    {
        public string CadastrarKitsBonsRuins(Objetos.objQrcode objQrcode, Objetos.objSaldoEstoque objSaldoEstoque)
        {
            
            clsBanco banco = new clsBanco();
            

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            NpgsqlTransaction transacao;
            NpgsqlCommand cmd = new NpgsqlCommand();
            connection.Open();
            transacao = (NpgsqlTransaction)connection.BeginTransaction();

           
            clsSaldoEstoque clsSaldoEstoque = new clsSaldoEstoque();
            clsArim clsArim = new Classes.clsArim();
            clsNotaFiscal clsNota = new clsNotaFiscal();

            string comando = "";

            try
            {
                comando = "INSERT INTO public.tb_qrcd(qrcd_txqrcd, qrcd_dt_criacao, emen_cod_empr, usur_cod_usur, " +
                                                      "qrcd_qtd_qrcd, qrcd_bol_conj_bom, qrcd_cod_pallet, qrcd_cod_topo, qrcd_cod_folha) " +
                            "VALUES ('" + objQrcode.Qrcd_txqrcd + "','" + objQrcode.Qrcd_dt_criacao + "'," + objQrcode.Emen_cod_empr +
                            "," + objQrcode.Usur_cod_usur + "," + objQrcode.Qrcd_qtd_qrcd + "," + objQrcode.Qrcd_bol_conj_bom + "," + objQrcode.Qrcd_cod_pallet +"," + objQrcode.Qrcd_cod_topo + "," + objQrcode.Qrcd_cod_folha + ") returning qrcd_cod_qrcd";

                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();

                clsSaldoEstoque.atualizarEstoque(objSaldoEstoque, connection, objSaldoEstoque.Emen_cod_empresa, objSaldoEstoque.Emen_cod_empresa);

                transacao.Commit();
                connection.Close();
                return "";

                
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                connection.Close();
                return ex.Message;
            }
        }

     
        public DataSet SelecionarQrcode(string qrcd_txqrcd)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select tb_qrcd.*, tb_emen.*, tb_usur.* from tb_qrcd inner join tb_emen on tb_qrcd.emen_cod_empr = tb_emen.emen_cod_empr inner join tb_usur on tb_qrcd.usur_cod_usur = tb_usur.usur_cd_usur  where qrcd_txqrcd = '" + qrcd_txqrcd + "'";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public void CadastrarConferido(string emen_cod_empr, int qtd_bons, int qtd_ruins)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select * from tb_qrcd where qrcd_bol_conj_bom = true and qrcd_conferencia_qrcd = false and emen_cod_empr = " + emen_cod_empr + "  limit " + qtd_bons;
            
            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                comando = "update tb_qrcd set qrcd_conferencia_qrcd=true where qrcd_cod_qrcd =" + ds.Tables[0].Rows[i]["qrcd_cod_qrcd"].ToString();

                cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }

            comando = "select * from tb_qrcd where qrcd_bol_conj_bom = false and qrcd_conferencia_qrcd = false and emen_cod_empr = " + emen_cod_empr + "  limit " + qtd_ruins;
            ds = new DataSet();

            cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                comando = "update tb_qrcd set qrcd_conferencia_qrcd=true where qrcd_cod_qrcd =" + ds.Tables[0].Rows[i]["qrcd_cod_qrcd"].ToString();

                cmd = new NpgsqlCommand();
                cmd.CommandText = comando;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
            }

            connection.Close();
        }

    }
}