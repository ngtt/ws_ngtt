﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
using System.IO;

namespace wsngtt.Classes
{
    public class clsSaid
    {

         public void CadastrarSaida(Objetos.objSaid objSaid)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            comando = "INSERT INTO public.tb_said(said_tx_placas, usur_sq_usuario, entr_sq_entrada, " +
                      "said_dt_saida, emen_sq_empresa, said_tx_nf) " +
                      "VALUES( " +
                      "'" + objSaid.Said_tx_placas + "', " +
                      "'" + objSaid.Usur_sq_usuario + "', " +
                      "'" + objSaid.Entr_sq_entrada + "', " +
                      "'" + objSaid.Said_dt_saida + "', " +
                      "'" + objSaid.Emen_sq_empresa + "'," +
                      "'" + objSaid.said_tx_nf + "')";

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteScalar();
        }
    }
}