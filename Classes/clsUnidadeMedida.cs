﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;
namespace wsngtt.Classes
{
    public class clsUnidadeMedida
    {
        public int cadastrarUnidadeMedida(Objetos.objUnidadeMedida objUnidadeMedida)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object unme_cod_unme;


            comando = "INSERT INTO public.tb_unme(unme_descricao, unme_id_reg, unme_dt_criacao, usur_cd_usur) " +
                      " VALUES ('" + objUnidadeMedida.Unme_descricao + "','" + objUnidadeMedida.Unme_id_reg + "','" + objUnidadeMedida.Unme_dt_criacao + "'," + objUnidadeMedida.Usur_cd_usur + ") returning unme_cod_unme";
           
;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            unme_cod_unme = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(unme_cod_unme);
        }


        public void AlterarUnidadeMedida(Objetos.objUnidadeMedida objUnidadeMedida)
        {
            clsBanco banco = new clsBanco();
            string comando = "";




            comando = "UPDATE public.tb_unme " +
                      "SET unme_descricao='" + objUnidadeMedida.Unme_descricao + "', unme_id_reg='" + objUnidadeMedida.Unme_id_reg + "', unme_dt_criacao='" + objUnidadeMedida.Unme_dt_criacao + "', usur_cd_usur=" + objUnidadeMedida.Usur_cd_usur +
                      " WHERE unme_cod_unme = " + objUnidadeMedida.Unme_cod_unme;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
            
        }

        public DataSet SelecionarTodasUnidadesMedida()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_unme order by unme_id_reg";


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();
            return ds;
        }

        public DataSet SelecionarUmaUnidadeMedida(int unme_cod_unme)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "Select * from tb_unme where unme_cod_unme = " + unme_cod_unme;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);
            connection.Close();
            adap.Fill(ds);

            return ds;
        }
    }
}