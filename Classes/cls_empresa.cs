﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;


namespace wsngtt.Classes
{
    public class cls_empresa
    {



        public DataSet SelecionarTodasEmpresas()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_emen.*, tb_grem.* from tb_emen inner join tb_grem on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_ativo = true order by emen_nomered" ;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;

        }

        public DataSet SelecionarEmpresasAtivas()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_emen.*, tb_grem.* from tb_emen inner join tb_grem on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_ativo=true order by emen_nomered";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        public DataSet SelecionarFolhasKitEmpresa(int emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select emen_nr_folhas_kit from tb_emen where emen_cod_empr = " + emen_cod_empr;

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        public DataSet SelecionarEmpresa(int emen_cod_empr)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_emen.*, tb_grem.* from tb_emen inner join tb_grem on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_cod_empr=" + emen_cod_empr + " and emen_ativo = true";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


        public int cadastraEmpresa(Objetos.objEmpresa objempresa)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            object emen_cd_emen;


            comando = "INSERT INTO public.tb_emen(emen_cnpj, emen_nome, emen_nomered, " +
                      "emen_lograd, emen_numero, emen_uf, emen_comple, emen_bairro, emen_cep, emen_ddd, " +
                      "emen_tel, emen_email, emen_dt_criacao, usur_cod_usur, emen_ativo, grem_cd_grem, emen_forn_emen, emen_carregamento_emen, emen_cidade, emen_nr_folhas_kit)" +
                       "VALUES ('" + objempresa.Emen_cnpj + "','" + objempresa.Emen_nome + "','" + objempresa.Emen_nomered + "','" +
                       objempresa.Emen_lograd + "','" + objempresa.Emen_numero + "','" + objempresa.Emen_uf + "','" +
                       objempresa.Emen_comple + "','" + objempresa.Emen_bairro + "','" + objempresa.Emen_cep + "','" +
                       objempresa.Emen_ddd + "','" + objempresa.Emen_tel + "','" + objempresa.Emen_email + "','" +
                       objempresa.Emen_dt_criacao + "'," + objempresa.Usur_cd_usur + ",'" + objempresa.Emen_ativo + "'," + objempresa.ObjGrupo.Grem_cd_grem + "," + objempresa.Emen_forn_emen +"," + objempresa.Emen_carregamento_emen + ",'" + objempresa.Emen_cidade + "'," + objempresa.Emen_nr_folhas_kit +  ") RETURNING emen_cod_empr";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            emen_cd_emen = cmd.ExecuteScalar();
            connection.Close();
            return Convert.ToInt32(emen_cd_emen);
        }

        public void alterarEmpresa(Objetos.objEmpresa objempresa)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            
            comando = "UPDATE tb_emen set " +
                      "emen_cnpj='" + objempresa.Emen_cnpj + 
                      "', emen_nome='" + objempresa.Emen_nome + 
                      "', emen_nomered='" + objempresa.Emen_nomered + 
                      "', emen_lograd='" + objempresa.Emen_lograd + 
                      "', emen_numero='" + objempresa.Emen_numero +
                      "', emen_uf='" + objempresa.Emen_uf + 
                      "', emen_comple='" + objempresa.Emen_comple + 
                      "', emen_bairro='" + objempresa.Emen_bairro + 
                      "', emen_cep='" + objempresa.Emen_cep + 
                      "',emen_ddd='" + objempresa.Emen_ddd + 
                      "', emen_tel='" + objempresa.Emen_tel + 
                      "', emen_email='" + objempresa.Emen_email + 
                      "', emen_dt_criacao='" + objempresa.Emen_dt_criacao + 
                      "', usur_cod_usur=" + objempresa.Usur_cd_usur + 
                      ", emen_ativo= " + objempresa.Emen_ativo +
                      ", grem_cd_grem= " + objempresa.ObjGrupo.Grem_cd_grem +
                      ", emen_forn_emen = " + objempresa.Emen_forn_emen +
                      ", emen_carregamento_emen = " + objempresa.Emen_carregamento_emen +
                      ", emen_cidade = '" + objempresa.Emen_cidade + "'" +
                      ", emen_nr_folhas_kit = " + objempresa.Emen_nr_folhas_kit + " " +
                      " WHERE emen_cod_empr=" + objempresa.Emen_cod_empr;


            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
            
        }

        public DataSet SelecionarEmpresasFornecedoras()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_emen.*, tb_grem.* from tb_emen inner join tb_grem on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_ativo=true and emen_forn_emen=true and emen_ativo = true order by emen_nomered";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarEmpresasRecebedoras()
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_emen.*, tb_grem.* from tb_emen inner join tb_grem on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_ativo=true and emen_forn_emen=false and emen_ativo = true order by emen_nomered";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }

        public DataSet SelecionarEmpresaPorCNPJ(string emen_cnpj)
        {
            clsBanco banco = new clsBanco();
            string comando = "";

            comando = "select tb_emen.*, tb_grem.* from tb_emen inner join tb_grem on tb_emen.grem_cd_grem = tb_grem.grem_cd_grem where emen_cnpj='" + emen_cnpj + "'";

            NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
            connection.Open();
            DataSet ds = new DataSet();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = comando;
            cmd.Connection = connection;
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

            adap.Fill(ds);
            connection.Close();


            return ds;
        }


		public DataSet SelecionarEmpresasIH()
		{
			clsBanco banco = new clsBanco();
			string comando = "";

			comando = "Select emen_cod_empr, emen_nomered from tb_emen where emen_forn_emen = true order by emen_nomered";

			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();


			return ds;
		}

		public DataSet SelecionarEmpresasOS()
		{
			clsBanco banco = new clsBanco();
			string comando = "";

			comando = "Select emen_cod_empr, emen_nomered from tb_emen where emen_forn_emen = false order by emen_nomered";

			NpgsqlConnection connection = new NpgsqlConnection(banco.StringConexaoLoginDB());
			connection.Open();
			DataSet ds = new DataSet();

			NpgsqlCommand cmd = new NpgsqlCommand();
			cmd.CommandText = comando;
			cmd.Connection = connection;
			NpgsqlDataAdapter adap = new NpgsqlDataAdapter(cmd);

			adap.Fill(ds);
			connection.Close();


			return ds;
		}


	}
}