﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using System.Xml.Serialization;
  

namespace wsngtt.Funcionalidades
{
    public class ValidacaoXML
    {
        public string ValidarLoginXml(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "LoginXMLEntrada.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarCadastroAlteracaoEmpresaXML(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "EmpresaCadastroalteracaoXmlenvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarSelecionarEmpresa(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmaEmpresa.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarSelecionarUmUsuario(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmUsuario.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarAtividadesUsuario(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarAtividadesUsuario.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarCadastroAlteracaoUsuarioXML(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "UsuarioCadastroAlteracaoEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarPerfilAtividade(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarPerfilAtividadesEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarPerfilCadastroAlteracao(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "PerfilCadastroAlteracao.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarCadastroAlteracaoGrupoXML(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarAlterarGrupoEmpresas.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarSelecionarGrupoempresa(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarGrupoempresaEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarAtividadesExtrasUsuarios(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarAtividadesExtraUsuarios.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastrarAlterarAtividadeExtraUsuario(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarAlterarAtividadesExtrasUsuario.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarAtivdadesForaPerfil(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarAtivdadesForaPerfil.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarCadastroAlteracaoArmazem(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastroAlteracaoArmazemEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionaArmazensEmpresa(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionaArmazensEmpresa.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastroAlteracaoFamiliaProdutos(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastroAlteracaoFamiliaProdutos.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastrarTipoProdutosMateriais(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastroAlteracaoTipoProdutosMateriais.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmTipoProdutosMateriais(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmTipoProdutosMateriais.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmTipoMovimentacao(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmTipoMovimentacao.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastrarAlterarUnidadesMedida(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarAlterarUnidadesMedidaEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmaUnidadeMedida(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmaUnidadeMedida.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastroAlteracaoProdutoMaterial(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastroAlteracaoProdutoMaterial.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmProdutoMaterial(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmProdutoMaterial.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastrarAlterarEnderecoEnvio(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarAlterarEnderecoEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmEndereco(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmEndereco.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastrarAlterarMapeamentoProdMatEnvio(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarAlterarMapeamentoProdMatEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarTodosMapeamentosProdutosMateriaisEmpresaEnvio(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarTodosMapeamentosProdutosMateriaisEmpresaEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmMapeamentoProdutoMaterialEmpresaEnvio(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmMapeamentoProdutoMaterialEmpresaEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastrarAlterarTipoVeiculoEnvio(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarTipoVeiculoEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmTipodeVeiculoEnvio(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmTipodeVeiculoEnvio.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string CadastrarAlterarVeiculo(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarAlterarVeiculo.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string SelecionarUmVeiculo(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarUmVeiculo.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarCadastroAlteracaoMotorista(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "CadastrarAlterarMotorista.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        public string ValidarSelecionarFamiliaProdutoPorCodigo(XDocument Anexo)
        {
            string resultado = "";
            XmlSchemaSet schemas = new XmlSchemaSet();
            //aqui pego pelo caminho do servidor o xsd a ser utilizado na validação.  Assim quando for para o servidor remoto
            //não teremos problemas do arquivo não ser encontrado
            schemas.Add("", @System.Web.HttpContext.Current.Server.MapPath("/xsds/") + "SelecionarFamiliaProdutoporCodigo.xsd");
            Anexo.Validate(schemas, (o, erro) => { resultado = erro.Message; });
            return resultado;
        }

        
        
    }
}