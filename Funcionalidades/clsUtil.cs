﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace wsngtt.Funcionalidades
{
    public static class clsUtil
    {
        public static bool contemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
                return true;
            else
                return false;
        }

        public static bool contemNumeros(string texto)
        {
            if (texto.Where(c => char.IsNumber(c)).Count() > 0)
                return true;
            else
                return false;
        }

        public static bool IsNumber(string valor)
        {
            //valor = "123456789";
            return Regex.IsMatch(valor, @"^\d{9}$");
        }
    }
}