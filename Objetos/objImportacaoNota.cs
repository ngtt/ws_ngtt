﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objImportacaoNota
    {
        private int nfer_cod_nfer;

        public int Nfer_cod_nfer
        {
            get { return nfer_cod_nfer; }
            set { nfer_cod_nfer = value; }
        }

        private string nfer_numnota_nfer;

        public string Nfer_numnota_nfer
        {
            get { return nfer_numnota_nfer; }
            set { nfer_numnota_nfer = value; }
        }

        private string nfer_erro_nfer;

        public string Nfer_erro_nfer
        {
            get { return nfer_erro_nfer; }
            set { nfer_erro_nfer = value; }
        }

        private DateTime nfer_dt_nfer;

        public DateTime Nfer_dt_nfer
        {
            get { return nfer_dt_nfer; }
            set { nfer_dt_nfer = value; }
        }
    }
}