﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objXmlmo
    {
        private int xlmo_cod_xlmo;

        public int Xlmo_cod_xlmo
        {
            get { return xlmo_cod_xlmo; }
            set { xlmo_cod_xlmo = value; }
        }

        private int canf_cod_canf;

        public int Canf_cod_canf
        {
            get { return canf_cod_canf; }
            set { canf_cod_canf = value; }
        }

        private DateTime xmlo_dt_emissao;

        public DateTime Xmlo_dt_emissao
        {
            get { return xmlo_dt_emissao; }
            set { xmlo_dt_emissao = value; }
        }

        private DateTime xlmo_dt_cancelament;

        public DateTime Xlmo_dt_cancelament
        {
            get { return xlmo_dt_cancelament; }
            set { xlmo_dt_cancelament = value; }
        }

        private DateTime xlmo_dt_criacao;

        public DateTime Xlmo_dt_criacao
        {
            get { return xlmo_dt_criacao; }
            set { xlmo_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private string xlmo_xmlnf_xlmo;

        public string Xlmo_xmlnf_xlmo
        {
            get { return xlmo_xmlnf_xlmo; }
            set { xlmo_xmlnf_xlmo = value; }
        }
    }
}