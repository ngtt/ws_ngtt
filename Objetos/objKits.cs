﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objKits
    {
        private int kits_cod_kits;

        public int Kits_cod_kits
        {
            get { return kits_cod_kits; }
            set { kits_cod_kits = value; }
        }

        private string kits_descricao;

        public string Kits_descricao
        {
            get { return kits_descricao; }
            set { kits_descricao = value; }
        }

        private DateTime kits_dt_criacao;

        public DateTime Kits_dt_criacao
        {
            get { return kits_dt_criacao; }
            set { kits_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private double kits_quant_prod_prin;

        public double Kits_quant_prod_prin
        {
            get { return kits_quant_prod_prin; }
            set { kits_quant_prod_prin = value; }
        }

        private double kits_quant_pallets;

        public double Kits_quant_pallets
        {
            get { return kits_quant_pallets; }
            set { kits_quant_pallets = value; }
        }

        private double kits_quant_topo;

        public double Kits_quant_topo
        {
            get { return kits_quant_topo; }
            set { kits_quant_topo = value; }
        }

        private double kits_quant_folha;

        public double Kits_quant_folha
        {
            get { return kits_quant_folha; }
            set { kits_quant_folha = value; }
        }


        private double kits_quant_por_camada;

        public double Kits_quant_por_camada
        {
            get { return kits_quant_por_camada; }
            set { kits_quant_por_camada = value; }
        }

        private int grem_cd_grem;

        public int Grem_cd_grem
        {
            get { return grem_cd_grem; }
            set { grem_cd_grem = value; }
        }

        private int prma_cod_prma;

        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }

        private double quantidade_nf;

        public double Quantidade_nf
        {
            get { return quantidade_nf; }
            set { quantidade_nf = value; }
        }

        private double fator_conservacao;

        public double Fator_conservacao
        {
            get { return fator_conservacao; }
            set { fator_conservacao = value; }
        }

        private int famp_cd_famp;

        public int Famp_cd_famp
        {
            get { return famp_cd_famp; }
            set { famp_cd_famp = value; }
        }
    }
}