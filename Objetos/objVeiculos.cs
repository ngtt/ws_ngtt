﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objVeiculos
    {
        private int tpve_cod_tpve;

        public int Tpve_cod_tpve
        {
            get { return tpve_cod_tpve; }
            set { tpve_cod_tpve = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur1
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
        private bool veic_ativo;

        public bool Veic_ativo
        {
            get { return veic_ativo; }
            set { veic_ativo = value; }
        }
        private int veic_cod_veic;

        public int Veic_cod_veic
        {
            get { return veic_cod_veic; }
            set { veic_cod_veic = value; }
        }
        private string veic_descricao;

        public string Veic_descricao
        {
            get { return veic_descricao; }
            set { veic_descricao = value; }
        }
        private DateTime veic_dt_criacao;

        public DateTime Veic_dt_criacao
        {
            get { return veic_dt_criacao; }
            set { veic_dt_criacao = value; }
        }
        private string veic_placa;

        public string Veic_placa
        {
            get { return veic_placa; }
            set { veic_placa = value; }
        }
    }
}