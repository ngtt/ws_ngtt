﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objFotoOcorrencias
    {
        int ftoc_cod_ftoc;

        public int Ftoc_cod_ftoc
        {
            get { return ftoc_cod_ftoc; }
            set { ftoc_cod_ftoc = value; }
        }

        string ftoc_nomeimagem_ftoc;

        public string Ftoc_nomeimagem_ftoc
        {
            get { return ftoc_nomeimagem_ftoc; }
            set { ftoc_nomeimagem_ftoc = value; }
        }


        int rnco_cod_rnco;

        public int Rnco_cod_rnco
        {
            get { return rnco_cod_rnco; }
            set { rnco_cod_rnco = value; }
        }
    }
}