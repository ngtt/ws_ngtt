﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objFamiliaProdutos
    {
        private int famp_cd_famp;

        public int Famp_cd_famp
        {
            get { return famp_cd_famp; }
            set { famp_cd_famp = value; }
        }

       
        private string famp_descricao;

        public string Famp_descricao
        {
            get { return famp_descricao; }
            set { famp_descricao = value; }
        }

        private DateTime famp_dt_criaca;

        public DateTime Famp_dt_criaca
        {
            get { return famp_dt_criaca; }
            set { famp_dt_criaca = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
    }
}