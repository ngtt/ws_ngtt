﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objPrevisaoProducao
    {
        private int pvpl_cod_pvpl;
        private int prma_cod_prma;
        private int pvpl_num_semana_ano;
        private DateTime pvpl_dt_d0;
        private DateTime pvpl_dt_d1;
        private DateTime pvpl_dt_d2;
        private DateTime pvpl_dt_d3;
        private DateTime pvpl_dt_d4;
        private DateTime pvpl_dt_d5;
        private DateTime pvpl_dt_d6;
        private int pvpl_qtd_d0;
        private int pvpl_qtd_d1;
        private int pvpl_qtd_d2;
        private int pvpl_qtd_d3;
        private int pvpl_qtd_d4;
        private int pvpl_qtd_d5;
        private int pvpl_qtd_d6;
        private int pvpl_qtd_semana1;
        private int pvpl_qtd_semana2;
        private int pvpl_qtd_semana3;
        private int pvpl_qtd_semana4;
        private int pvpl_num_versao;
        private int usur_cd_usur;
        private int emen_cod_empr;
		private string gppm_cd_gppm;


		public int Pvpl_cod_pvpl { get => pvpl_cod_pvpl; set => pvpl_cod_pvpl = value; }
        public int Prma_cod_prma { get => prma_cod_prma; set => prma_cod_prma = value; }
        public int Pvpl_num_semana_ano { get => pvpl_num_semana_ano; set => pvpl_num_semana_ano = value; }
        public DateTime Pvpl_dt_d0 { get => pvpl_dt_d0; set => pvpl_dt_d0 = value; }
        public DateTime Pvpl_dt_d1 { get => pvpl_dt_d1; set => pvpl_dt_d1 = value; }
        public DateTime Pvpl_dt_d2 { get => pvpl_dt_d2; set => pvpl_dt_d2 = value; }
        public DateTime Pvpl_dt_d3 { get => pvpl_dt_d3; set => pvpl_dt_d3 = value; }
        public DateTime Pvpl_dt_d4 { get => pvpl_dt_d4; set => pvpl_dt_d4 = value; }
        public DateTime Pvpl_dt_d5 { get => pvpl_dt_d5; set => pvpl_dt_d5 = value; }
        public DateTime Pvpl_dt_d6 { get => pvpl_dt_d6; set => pvpl_dt_d6 = value; }
        public int Pvpl_qtd_d0 { get => pvpl_qtd_d0; set => pvpl_qtd_d0 = value; }
        public int Pvpl_qtd_d1 { get => pvpl_qtd_d1; set => pvpl_qtd_d1 = value; }
        public int Pvpl_qtd_d2 { get => pvpl_qtd_d2; set => pvpl_qtd_d2 = value; }
        public int Pvpl_qtd_d3 { get => pvpl_qtd_d3; set => pvpl_qtd_d3 = value; }
        public int Pvpl_qtd_d4 { get => pvpl_qtd_d4; set => pvpl_qtd_d4 = value; }
        public int Pvpl_qtd_d5 { get => pvpl_qtd_d5; set => pvpl_qtd_d5 = value; }
        public int Pvpl_qtd_d6 { get => pvpl_qtd_d6; set => pvpl_qtd_d6 = value; }
        public int Pvpl_qtd_semana1 { get => pvpl_qtd_semana1; set => pvpl_qtd_semana1 = value; }
        public int Pvpl_qtd_semana2 { get => pvpl_qtd_semana2; set => pvpl_qtd_semana2 = value; }
        public int Pvpl_qtd_semana3 { get => pvpl_qtd_semana3; set => pvpl_qtd_semana3 = value; }
        public int Pvpl_qtd_semana4 { get => pvpl_qtd_semana4; set => pvpl_qtd_semana4 = value; }
        public int Pvpl_num_versao { get => pvpl_num_versao; set => pvpl_num_versao = value; }
        public int Usur_cd_usur { get => usur_cd_usur; set => usur_cd_usur = value; }
        public int Emen_cod_empr { get => emen_cod_empr; set => emen_cod_empr = value; }
		public string Gppm_cd_gppm { get => gppm_cd_gppm; set => gppm_cd_gppm = value; }
	}
}