﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objTipoMovimentacao
    {
        private int tpmo_cod_tpmo;

        public int Tpmo_cod_tpmo
        {
            get { return tpmo_cod_tpmo; }
            set { tpmo_cod_tpmo = value; }
        }

        private string tpmo_descricao;

        public string Tpmo_descricao
        {
            get { return tpmo_descricao; }
            set { tpmo_descricao = value; }
        }

        private DateTime tpmo_dt_criacao;

        public DateTime Tpmo_dt_criacao
        {
            get { return tpmo_dt_criacao; }
            set { tpmo_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private string tpmo_tipo_movimento;

        public string Tpmo_tipo_movimento
        {
            get { return tpmo_tipo_movimento; }
            set { tpmo_tipo_movimento = value; }
        }
    }
}