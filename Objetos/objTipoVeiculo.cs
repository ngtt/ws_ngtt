﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objTipoVeiculo
    {
        private bool tpve_ativo;

        public bool Tpve_ativo
        {
            get { return tpve_ativo; }
            set { tpve_ativo = value; }
        }
        private int tpve_cod_tpve;

        public int Tpve_cod_tpve
        {
            get { return tpve_cod_tpve; }
            set { tpve_cod_tpve = value; }
        }
        private string tpve_descricao;

        public string Tpve_descricao
        {
            get { return tpve_descricao; }
            set { tpve_descricao = value; }
        }
        private DateTime tpve_dt_criacao;

        public DateTime Tpve_dt_criacao
        {
            get { return tpve_dt_criacao; }
            set { tpve_dt_criacao = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
    }
}