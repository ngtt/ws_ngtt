﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objNotaFiscal
    {
        private int canf_cod_canf;

        public int Canf_cod_canf
        {
            get { return canf_cod_canf; }
            set { canf_cod_canf = value; }
        }

        private string canf_numnt;

        public string Canf_numnt
        {
            get { return canf_numnt; }
            set { canf_numnt = value; }
        }

        private string canf_serienf;

        public string Canf_serienf
        {
            get { return canf_serienf; }
            set { canf_serienf = value; }
        }

        private string canf_chavenfe;

        public string Canf_chavenfe
        {
            get { return canf_chavenfe; }
            set { canf_chavenfe = value; }
        }

        private DateTime canf_dt_emissao;

        public DateTime Canf_dt_emissao
        {
            get { return canf_dt_emissao; }
            set { canf_dt_emissao = value; }
        }

        private int emen_cod_empr_origem;

        public int Emen_cod_empr_origem
        {
            get { return emen_cod_empr_origem; }
            set { emen_cod_empr_origem = value; }
        }

        private int emen_cd_empr_destino;

        public int Emen_cd_empr_destino
        {
            get { return emen_cd_empr_destino; }
            set { emen_cd_empr_destino = value; }
        }



        private bool canf_manual;

        public bool Canf_manual
        {
            get { return canf_manual; }
            set { canf_manual = value; }
        }

        private DateTime canf_dt_cancelamento;


        public DateTime Canf_dt_cancelamento
        {
            get { return canf_dt_cancelamento; }
            set { canf_dt_cancelamento = value; }
        }

        private string canf_transito;

        public string Canf_transito
        {
            get { return canf_transito; }
            set { canf_transito = value; }
        }

        private int tran_cod_tran;

        public int Tran_cod_tran
        {
            get { return tran_cod_tran; }
            set { tran_cod_tran = value; }
        }

        private int moto_cod_moto;

        public int Moto_cod_moto
        {
            get { return moto_cod_moto; }
            set { moto_cod_moto = value; }
        }

        private int veic_cod_veic;

        public int Veic_cod_veic
        {
            get { return veic_cod_veic; }
            set { veic_cod_veic = value; }
        }

        private string canf_id_reg;

        public string Canf_id_reg
        {
            get { return canf_id_reg; }
            set { canf_id_reg = value; }
        }

        private DateTime canf_dt_criacao;

        public DateTime Canf_dt_criacao
        {
            get { return canf_dt_criacao; }
            set { canf_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        

        private List<objItensNotaFiscal> lstItens = new List<objItensNotaFiscal>();
        public List<objItensNotaFiscal> LstItens
        {
            get { return lstItens; }
            set { lstItens = value; }
        }

      
    }
}