﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class ObjDiferencaConferencia
    {
        int dirc_cod_dirc;

        public int Dirc_cod_dirc
        {
            get { return dirc_cod_dirc; }
            set { dirc_cod_dirc = value; }
        }

        private int canf_cod_canf;

        public int Canf_cod_canf
        {
            get { return canf_cod_canf; }
            set { canf_cod_canf = value; }
        }

        private int itnf_cod_itnf;
        
        public int Itnf_cod_itnf
        {
            get { return itnf_cod_itnf; }
            set { itnf_cod_itnf = value; }
        }

        private double dirc_qtde_recbto;

        public double Dirc_qtde_recbto
        {
            get { return dirc_qtde_recbto; }
            set { dirc_qtde_recbto = value; }
        }

        private double dirc_qtde_nf;

        public double Dirc_qtde_nf
        {
            get { return dirc_qtde_nf; }
            set { dirc_qtde_nf = value; }
        }

        private DateTime dirc_dt_criacao;

        public DateTime Dirc_dt_criacao
        {
            get { return dirc_dt_criacao; }
            set { dirc_dt_criacao = value; }
        }

        private int prma_cod_prma;

        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }

        int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
    }
}