﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objTransportadora
    {
        private int tran_cod_tran;
        private string tran_cnpj;
        private string tran_nome;
        private string tran_nomered;
        private string tran_lograd;
        private string tran_numero;
        private string tran_comple;
        private string tran_bairro;
        private string tran_cep;
        private string tran_uf;
        private string tran_ddd;
        private string tran_tel;
        private string tran_email;
        private DateTime tran_dt_criacao;
        private int usur_cd_usur;
        private bool tran_ativo;
       



 

        public int Tran_cod_tran
        {
            get { return tran_cod_tran; }
            set { tran_cod_tran = value; }
        }

        public string Tran_cnpj
        {
            get { return tran_cnpj; }
            set { tran_cnpj = value; }
        }

        public string Tran_nome
        {
            get { return tran_nome; }
            set { tran_nome = value; }
        }

        public string Tran_nomered
        {
            get { return tran_nomered; }
            set { tran_nomered = value; }
        }

        public string Tran_lograd
        {
            get { return tran_lograd; }
            set { tran_lograd = value; }
        }

        public string Tran_numero
        {
            get { return tran_numero; }
            set { tran_numero = value; }
        }

        public string Tran_comple
        {
            get { return tran_comple; }
            set { tran_comple = value; }
        }

        public string Tran_bairro
        {
            get { return tran_bairro; }
            set { tran_bairro = value; }
        }

        public string Tran_cep
        {
            get { return tran_cep; }
            set { tran_cep = value; }
        }

        public string Tran_uf
        {
            get { return tran_uf; }
            set { tran_uf = value; }
        }

        public string Tran_ddd
        {
            get { return tran_ddd; }
            set { tran_ddd = value; }
        }

        public string Tran_tel
        {
            get { return tran_tel; }
            set { tran_tel = value; }
        }

        public string Tran_email
        {
            get { return tran_email; }
            set { tran_email = value; }
        }

        public DateTime Tran_dt_criacao
        {
            get { return tran_dt_criacao; }
            set { tran_dt_criacao = value; }
        }

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        public bool Tran_ativo
        {
            get { return tran_ativo; }
            set { tran_ativo = value; }
        }

       



    }
}