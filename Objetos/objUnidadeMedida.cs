﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objUnidadeMedida
    {
        private int unme_cod_unme;

        public int Unme_cod_unme
        {
            get { return unme_cod_unme; }
            set { unme_cod_unme = value; }
        }

        private string unme_descricao;

        public string Unme_descricao
        {
            get { return unme_descricao; }
            set { unme_descricao = value; }
        }

        private string unme_id_reg;

        public string Unme_id_reg
        {
            get { return unme_id_reg; }
            set { unme_id_reg = value; }
        }

        private DateTime unme_dt_criacao;

        public DateTime Unme_dt_criacao
        {
            get { return unme_dt_criacao; }
            set { unme_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
    }
}