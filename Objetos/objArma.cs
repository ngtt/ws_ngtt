﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objArma
    {
        private int arma_cod_arma;

        public int Arma_cod_arma
        {
            get { return arma_cod_arma; }
            set { arma_cod_arma = value; }
        }
        private string arma_descricao;

        public string Arma_descricao
        {
            get { return arma_descricao; }
            set { arma_descricao = value; }
        }
        private bool arma_ativo;

        public bool Arma_ativo
        {
            get { return arma_ativo; }
            set { arma_ativo = value; }
        }
        private DateTime arma_dt_criacao;

        public DateTime Arma_dt_criacao
        {
            get { return arma_dt_criacao; }
            set { arma_dt_criacao = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }




    }
}