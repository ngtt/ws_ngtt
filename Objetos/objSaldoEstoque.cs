﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objSaldoEstoque
    {
        private int sles_cod_sles;
        public int Sles_cod_sles
        {
            get { return sles_cod_sles; }
            set { sles_cod_sles = value; }
        }

        public string familia_produto;

        public string Familia_produto
        {
            get { return familia_produto; }
            set { familia_produto = value; }
        }

        private int emen_cod_empresa;

        public int Emen_cod_empresa
        {
            get { return emen_cod_empresa; }
            set { emen_cod_empresa = value; }
        }

        private int emen_cod_empresa_destino;
        public int Emen_cod_empresa_destino
        {
            get { return emen_cod_empresa_destino; }
            set { emen_cod_empresa_destino = value; }
        }

        private int prma_cod_prma;

        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }

        private double sles_quantidade;

        public double Sles_quantidade
        {
            get { return sles_quantidade; }
            set { sles_quantidade = value; }
        }

        private int tpop_cod_tpop;

        public int Tpop_cod_tpop
        {
            get { return tpop_cod_tpop; }
            set { tpop_cod_tpop = value; }
        }
    }
}