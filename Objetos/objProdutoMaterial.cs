﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objProdutoMaterial
    {
        private int prma_cod_prma;
        
        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }
        private string prma_pn;

        public string Prma_pn
        {
            get { return prma_pn; }
            set { prma_pn = value; }
        }
        private string prma_descricao;

        public string Prma_descricao
        {
            get { return prma_descricao; }
            set { prma_descricao = value; }
        }
        private int tpma_cod_tipo;

        public int Tpma_cod_tipo
        {
            get { return tpma_cod_tipo; }
            set { tpma_cod_tipo = value; }
        }
        private int unme_cod_unme;

        public int Unme_cod_unme
        {
            get { return unme_cod_unme; }
            set { unme_cod_unme = value; }
        }
        private decimal prma_fator_conservacao;

        public decimal Prma_fator_conservacao
        {
            get { return prma_fator_conservacao; }
            set { prma_fator_conservacao = value; }
        }
        private decimal prma_preco;

        public decimal Prma_preco
        {
            get { return prma_preco; }
            set { prma_preco = value; }
        }
        private int famp_cd_famp;

        public int Famp_cd_famp
        {
            get { return famp_cd_famp; }
            set { famp_cd_famp = value; }
        }
        private DateTime prma_dt_criacao;

        public DateTime Prma_dt_criacao
        {
            get { return prma_dt_criacao; }
            set { prma_dt_criacao = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
        private bool prma_ativo;

        public bool Prma_ativo
        {
            get { return prma_ativo; }
            set { prma_ativo = value; }
        }

        private int grem_cd_grem;

        public int Grem_cd_grem
        {
            get { return grem_cd_grem; }
            set { grem_cd_grem = value; }
        }

        private string gppm_cd_gppm;

        public string Gppm_cd_gppm
        {
            get { return gppm_cd_gppm; }
            set { gppm_cd_gppm = value; }
        }
    }
}