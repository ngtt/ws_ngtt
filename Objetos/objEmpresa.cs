﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objEmpresa
    {
        private int emen_cod_empr;
        private string emen_cnpj;
        private string emen_nome;
        private string emen_nomered;
        private string emen_lograd;
        private string emen_numero;
        private string emen_comple;
        private string emen_bairro;
        private string emen_cep;
        private string emen_uf;
        private string emen_cidade;
        private string emen_ddd;
        private string emen_tel;
        private string emen_email;
        private DateTime emen_dt_criacao;
        private int usur_cd_usur;
        private bool emen_ativo;
        private bool emen_forn_emen;
        private bool emen_carregamento_emen;
        private Objetos.objGrupo objGrupo = new objGrupo();
        private int emen_nr_folhas_kit;





        public Objetos.objGrupo ObjGrupo
        {
            get { return objGrupo; }
            set { objGrupo = value; }
        }

        public int Emen_cod_empr
        {
            get { return emen_cod_empr; }
            set { emen_cod_empr = value; }
        }

        public string Emen_cnpj
        {
            get { return emen_cnpj; }
            set { emen_cnpj = value; }
        }

        public string Emen_nome
        {
            get { return emen_nome; }
            set { emen_nome = value; }
        }

        public string Emen_nomered
        {
            get { return emen_nomered; }
            set { emen_nomered = value; }
        }
        
        public string Emen_lograd
        {
            get { return emen_lograd; }
            set { emen_lograd = value; }
        }

        public string Emen_numero
        {
            get { return emen_numero; }
            set { emen_numero = value; }
        }
        
        public string Emen_comple
        {
            get { return emen_comple; }
            set { emen_comple = value; }
        }

        public string Emen_bairro
        {
            get { return emen_bairro; }
            set { emen_bairro = value; }
        }
        
        public string Emen_cep
        {
            get { return emen_cep; }
            set { emen_cep = value; }
        }
        
        public string Emen_uf
        {
            get { return emen_uf; }
            set { emen_uf = value; }
        }

        public string Emen_ddd
        {
            get { return emen_ddd; }
            set { emen_ddd = value; }
        }
        
        public string Emen_tel
        {
            get { return emen_tel; }
            set { emen_tel = value; }
        }

        public string Emen_email
        {
            get { return emen_email; }
            set { emen_email = value; }
        }

        public DateTime Emen_dt_criacao
        {
            get { return emen_dt_criacao; }
            set { emen_dt_criacao = value; }
        }
        
        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
        
        public bool Emen_ativo
        {
            get { return emen_ativo; }
            set { emen_ativo = value; }
        }

        public bool Emen_forn_emen
        {
            get { return emen_forn_emen; }
            set { emen_forn_emen = value; }
        }

        public bool Emen_carregamento_emen
        {
            get { return emen_carregamento_emen; }
            set { emen_carregamento_emen = value; }
        }

        public string Emen_cidade
        {
            get
            {
                return emen_cidade;
            }

            set
            {
                emen_cidade = value;
            }
        }

        public int Emen_nr_folhas_kit { get => emen_nr_folhas_kit; set => emen_nr_folhas_kit = value; }
    }
}