﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objMotivoOcorrencia
    {
        private int mtoc_cod_mtoc;

        public int Mtoc_cod_mtoc
        {
            get { return mtoc_cod_mtoc; }
            set { mtoc_cod_mtoc = value; }
        }

        private string mtoc_descricao;

        public string Mtoc_descricao
        {
            get { return mtoc_descricao; }
            set { mtoc_descricao = value; }
        }

        private DateTime mtoc_dt_criacao;

        public DateTime Mtoc_dt_criacao
        {
            get { return mtoc_dt_criacao; }
            set { mtoc_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }



    }


}