﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objPerfil
    {
        private int perf_cod_perf;
        private string perf_nm_perfil;
        private List<objAtividade> objAtividade = new List<objAtividade>();

        public int Perf_cod_perf
        {
            get { return perf_cod_perf; }
            set { perf_cod_perf = value; }
        }

        public string Perf_nm_perfil
        {
            get { return perf_nm_perfil; }
            set { perf_nm_perfil = value; }
        }

        public List<objAtividade> ObjAtividade
        {
            get { return objAtividade; }
            set { objAtividade = value; }
        }


    }
}