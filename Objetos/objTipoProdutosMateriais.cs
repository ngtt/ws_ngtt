﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objTipoProdutosMateriais
    {
        private int tpma_cod_tipo;

        public int Tpma_cod_tipo
        {
            get { return tpma_cod_tipo; }
            set { tpma_cod_tipo = value; }
        }

        private string tpma_descricao;

        public string Tpma_descricao
        {
            get { return tpma_descricao; }
            set { tpma_descricao = value; }
        }

        private DateTime tpma_data_criacao;

        public DateTime Tpma_data_criacao
        {
            get { return tpma_data_criacao; }
            set { tpma_data_criacao = value; }
        }

        private int user_cd_usur;

        public int User_cd_usur
        {
            get { return user_cd_usur; }
            set { user_cd_usur = value; }
        }
    }
}