﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objArim
    {
        private int arim_cod_arim;

        public int Arim_cod_arim
        {
            get { return arim_cod_arim; }
            set { arim_cod_arim = value; }
        }

        private string arim_imagem;

        public string Arim_imagem
        {
            get { return arim_imagem; }
            set { arim_imagem = value; }
        }

     
        private int canf_cod_canf;

        public int Canf_cod_canf
        {
            get { return canf_cod_canf; }
            set { canf_cod_canf = value; }
        }

        private DateTime arim_dt_criacao;

        public DateTime Arim_dt_criacao
        {
            get { return arim_dt_criacao; }
            set { arim_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private int tpmo_cod_tpmo;

        public int Tpmo_cod_tpmo
        {
            get { return tpmo_cod_tpmo; }
            set { tpmo_cod_tpmo = value; }
        }

        private int emen_cod_empr;

        public int Emen_cod_empr
        {
            get { return emen_cod_empr; }
            set { emen_cod_empr = value; }
        }
    }
}