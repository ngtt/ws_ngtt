﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objRegistroNaoConformidade
    {
        private int rnco_cod_rnco;

        public int Rnco_cod_rnco
        {
            get { return rnco_cod_rnco; }
            set { rnco_cod_rnco = value; }
        }

       
        private int emen_cod_empr;

        public int Emen_cod_empr
        {
            get { return emen_cod_empr; }
            set { emen_cod_empr = value; }
        }

        private int canf_cod_canf;

        public int Canf_cod_canf
        {
            get { return canf_cod_canf; }
            set { canf_cod_canf = value; }
        }

       private string rnco_obs;

        public string Rnco_obs
        {
            get { return rnco_obs; }
            set { rnco_obs = value; }
        }

        private DateTime rnco_dt_criacao;

        public DateTime Rnco_dt_criacao
        {
            get { return rnco_dt_criacao; }
            set { rnco_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private int mtoc_cod_mtoc;

        public int Mtoc_cod_mtoc
        {
            get { return mtoc_cod_mtoc; }
            set { mtoc_cod_mtoc = value; }
        }


        private string rnco_causador_rnco;

        public string Rnco_causador_rnco
        {
            get { return rnco_causador_rnco; }
            set { rnco_causador_rnco = value; }
        }

        private int arma_cod_arma;

        public int Arma_cod_arma
        {
            get { return arma_cod_arma; }
            set { arma_cod_arma = value; }
        }

        private int famp_cod_famp;

        public int Famp_cod_famp
        {
            get { return famp_cod_famp; }
            set { famp_cod_famp = value; }
        }


        private int unme_cod_unm;

        public int Unme_cod_unm
        {
            get { return unme_cod_unm; }
            set { unme_cod_unm = value; }
        }

        private double rnco_quant_rnc;
        public double Rnco_quant_rnc
        {
            get { return rnco_quant_rnc; }
            set { rnco_quant_rnc = value; }
        }


    }
}