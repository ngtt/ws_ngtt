﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objQrcode
    {
        private int qrcd_cod_qrcd;

        public int Qrcd_cod_qrcd
        {
            get { return qrcd_cod_qrcd; }
            set { qrcd_cod_qrcd = value; }
        }

        private string qrcd_txqrcd;

        public string Qrcd_txqrcd
        {
            get { return qrcd_txqrcd; }
            set { qrcd_txqrcd = value; }
        }

        private DateTime qrcd_dt_criacao;

        public DateTime Qrcd_dt_criacao
        {
            get { return qrcd_dt_criacao; }
            set { qrcd_dt_criacao = value; }
        }

        private int emen_cod_empr;

        public int Emen_cod_empr
        {
            get { return emen_cod_empr; }
            set { emen_cod_empr = value; }
        }

        private int usur_cod_usur;

        public int Usur_cod_usur
        {
            get { return usur_cod_usur; }
            set { usur_cod_usur = value; }
        }

        private int qrcd_qtd_qrcd;

        public int Qrcd_qtd_qrcd
        {
            get { return qrcd_qtd_qrcd; }
            set { qrcd_qtd_qrcd = value; }
        }

        private bool qrcd_conferencia_qrc;

        public bool Qrcd_conferencia_qrc
        {
            get { return qrcd_conferencia_qrc; }
            set { qrcd_conferencia_qrc = value; }
        }

        private bool qrcd_entregue_qrc;

        public bool Qrcd_entregue_qrc
        {
            get { return qrcd_entregue_qrc; }
            set { qrcd_entregue_qrc = value; }
        }

        private bool qrcd_entregue_qrcd;

        public bool Qrcd_entregue_qrcd
        {
            get { return qrcd_entregue_qrcd; }
            set { qrcd_entregue_qrcd = value; }
        }

        private bool qrcd_bol_conj_bom;

        public bool Qrcd_bol_conj_bom
        {
            get { return qrcd_bol_conj_bom; }
            set { qrcd_bol_conj_bom = value; }
        }

        private int qrcd_cod_pallet;

        public int Qrcd_cod_pallet
        {
            get { return qrcd_cod_pallet; }
            set { qrcd_cod_pallet = value; }
        }

        private int qrcd_cod_topo;

        public int Qrcd_cod_topo
        {
            get { return qrcd_cod_topo; }
            set { qrcd_cod_topo = value; }
        }

        private int qrcd_cod_folha;

        public int Qrcd_cod_folha
        {
            get { return qrcd_cod_folha; }
            set { qrcd_cod_folha = value; }
        }


    }
}