﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objItensNotaFiscal
    {
        private int itnf_cod_itnf;

        public int Itnf_cod_itnf
        {
            get { return itnf_cod_itnf; }
            set { itnf_cod_itnf = value; }
        }

        private int canf_cod_canf;

        public int Canf_cod_canf
        {
            get { return canf_cod_canf; }
            set { canf_cod_canf = value; }
        }

        private int prma_cod_prma;

        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }

        private double itnf_quantidade;

        public double Itnf_quantidade
        {
            get { return itnf_quantidade; }
            set { itnf_quantidade = value; }
        }

        private DateTime itnf_dt_criacao;

        public DateTime Itnf_dt_criacao
        {
            get { return itnf_dt_criacao; }
            set { itnf_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private double quantidade_informada_conferencia;

        public double Quantidade_informada_conferencia
        {
            get { return quantidade_informada_conferencia; }
            set { quantidade_informada_conferencia = value; }
        }

    }
}