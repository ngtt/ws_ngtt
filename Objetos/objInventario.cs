﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objInventario
    {
        private int inve_cod_inve;

        public int Inve_cod_inve
        {
            get { return inve_cod_inve; }
            set { inve_cod_inve = value; }
        }

        private int prma_cod_prma;

        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }

        private int emen_cod_empr;

        public int Emen_cod_empr
        {
            get { return emen_cod_empr; }
            set { emen_cod_empr = value; }
        }

        private double inve_quantidade;

        public double Inve_quantidade
        {
            get { return inve_quantidade; }
            set { inve_quantidade = value; }
        }

        private DateTime inve_dt_criacao;

        public DateTime Inve_dt_criacao
        {
            get { return inve_dt_criacao; }
            set { inve_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private string inve_operacao;
        public string Inve_operacao
        {
            get { return inve_operacao; }
            set { inve_operacao = value; }
        }

        private int prma_cod_prma_pallet;

        public int Prma_cod_prma_pallet
        {
            get { return prma_cod_prma_pallet; }
            set { prma_cod_prma_pallet = value; }
        }

        private int prma_cod_prma_topo;

        public int Prma_cod_prma_topo
        {
            get { return prma_cod_prma_topo; }
            set { prma_cod_prma_topo = value; }
        }

        private int prma_cod_prma_divisor;

        public int Prma_cod_prma_divisor
        {
            get { return prma_cod_prma_divisor; }
            set { prma_cod_prma_divisor = value; }
        }

        private int emen_cod_empr_forn;

        public int Emen_cod_empr_forn
        {
            get { return emen_cod_empr_forn; }
            set { emen_cod_empr_forn = value; }
        }

        private int tpmo_cod_tpmo;

        public int Tpmo_cod_tpmo
        {
            get { return tpmo_cod_tpmo; }
            set { tpmo_cod_tpmo = value; }
        }
    }
}