﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objTipoOperacao
    {
        private int tpop_cod_tpo;

        public int Tpop_cod_tpop
        {
            get { return tpop_cod_tpo; }
            set { tpop_cod_tpo = value; }
        }

        private string tpop_descricao;

        public string Tpop_descricao
        {
            get { return tpop_descricao; }
            set { tpop_descricao = value; }
        }

        private DateTime tpop_dt_criacao;

        public DateTime Tpop_dt_criacao
        {
            get { return tpop_dt_criacao; }
            set { tpop_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

    }
}