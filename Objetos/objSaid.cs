﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objSaid
    {

        private int said_sq_saida;

        public int Said_sq_saida
        {
            get { return said_sq_saida; }
            set { said_sq_saida = value; }
        }

        private int entr_sq_entrada;

        public int Entr_sq_entrada
        {
            get { return entr_sq_entrada; }
            set { entr_sq_entrada = value; }
        }

        private string Said_tx_nf;

        private int emen_sq_empresa;

        public int Emen_sq_empresa { get => emen_sq_empresa; set => emen_sq_empresa = value; }


        private string said_tx_placas;

        public string Said_tx_placas
        {
            get { return said_tx_placas; }
            set { said_tx_placas = value; }
        }

     
        private DateTime said_dt_saida;

        public DateTime Said_dt_saida
        {
            get { return said_dt_saida; }
            set { said_dt_saida = value; }
        }

        private int usur_sq_usuario;

        public int Usur_sq_usuario
        {
            get { return usur_sq_usuario; }
            set { usur_sq_usuario = value; }
        }

        public string said_tx_nf { get => Said_tx_nf; set => Said_tx_nf = value; }
    }
}