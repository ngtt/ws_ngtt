﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objAtividadeExtra
    {
        int usae_cod_usae;
        int usur_cd_usur;
        int ativ_cod_ativ;

        public int Usae_cod_usae
        {
            get { return usae_cod_usae; }
            set { usae_cod_usae = value; }
        }

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        public int Ativ_cod_ativ
        {
            get { return ativ_cod_ativ; }
            set { ativ_cod_ativ = value; }
        }

    }
}