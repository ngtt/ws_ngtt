﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objAtividade
    {
        private int ativ_cod_ativ;
        private string ativ_nm_ativ;

        public int Ativ_cod_ativ
        {
            get { return ativ_cod_ativ; }
            set { ativ_cod_ativ = value; }
        }

        public string Ativ_nm_ativ
        {
            get { return ativ_nm_ativ; }
            set { ativ_nm_ativ = value; }
        }
    }
}