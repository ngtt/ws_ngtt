﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objGppm
    {
        private string gppm_cd_gppm;

        public string Gppm_cd_gppm
        {
            get { return gppm_cd_gppm; }
            set { gppm_cd_gppm = value; }
        }
        private string gppm_descricao;

        public string Gppm_descricao
        {
            get { return gppm_descricao; }
            set { gppm_descricao = value; }
        }
      
     
        private DateTime gppm_dt_criacao;

        public DateTime Gppm_dt_criacao
        {
            get { return gppm_dt_criacao; }
            set { gppm_dt_criacao = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }




    }
}