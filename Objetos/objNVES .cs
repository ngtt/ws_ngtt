﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objNVES
    {
        private int nves_cod_nves;

        public int Nves_cod_nves
        {
            get { return nves_cod_nves; }
            set { nves_cod_nves = value; }
        }

        private int prma_cod_prma;

        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }

        private int emen_cod_empr;

        public int Emen_cod_empr
        {
            get { return emen_cod_empr; }
            set { emen_cod_empr = value; }
        }

        public double nves_baixo;

        public double Nves_baixo
        {
            get { return nves_baixo; }
            set { nves_baixo = value; }
        }

        public double nves_regular;

        public double Nves_regular
        {
            get { return nves_regular; }
            set { nves_regular = value; }
        }

        public double nves_alto;

        public double Nves_alto
        {
            get { return nves_alto; }
            set { nves_alto = value; }
        }

        public double nves_risco;

        public double Nves_risco
        {
            get { return nves_risco; }
            set { nves_risco = value; }
        }

        public DateTime nves_dt_criacao;

        public DateTime Nves_dt_criacao
        {
            get { return nves_dt_criacao; }
            set { nves_dt_criacao = value; }
        }

        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

    }
}