﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace wsngtt.Objetos
{
    public class objUsuario
    {
        private int usur_cd_usur;
        private string usur_matricula;
        private string usur_nome;
        private string usur_apelido;
        private string usur_email;
        private string usur_senha;
        private int emen_cod_empresa;
        private int perf_cod_perf;
        private DateTime usur_dt_usur;
        private bool usur_ativo;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        public string Usur_matricula
        {
            get { return usur_matricula; }
            set { usur_matricula = value; }
        }

        public string Usur_nome
        {
            get { return usur_nome; }
            set { usur_nome = value; }
        }

        public string Usur_apelido
        {
            get { return usur_apelido; }
            set { usur_apelido = value; }
        }

        public string Usur_email
        {
            get { return usur_email; }
            set { usur_email = value; }
        }

        public string Usur_senha
        {
            get { return usur_senha; }
            set { usur_senha = value; }
        }

        public int Emen_cod_empresa
        {
            get { return emen_cod_empresa; }
            set { emen_cod_empresa = value; }
        }

        public bool Usur_ativo
        {
            get { return usur_ativo; }
            set { usur_ativo = value; }
        }

        public int Perf_cod_perf
        {
            get { return perf_cod_perf; }
            set { perf_cod_perf = value; }
        }

        public DateTime Usur_dt_usur
        {
            get { return usur_dt_usur; }
            set { usur_dt_usur = value; }
        }
        

       



    }
}