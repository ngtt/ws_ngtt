﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objMovimentoEstoque
    {
        private int canf_cod_canf;

        public int Canf_cod_canf
        {
            get { return canf_cod_canf; }
            set { canf_cod_canf = value; }
        }

        private int prma_cod_prma;

        public int Prma_cod_prma
        {
            get { return prma_cod_prma; }
            set { prma_cod_prma = value; }
        }


        private int tpmo_cod_tpmo;

        public int Tpmo_cod_tpmo
        {
            get { return tpmo_cod_tpmo; }
            set { tpmo_cod_tpmo = value; }
        }

        private double moes_quantidade;

        public double Moes_quantidade
        {
            get { return moes_quantidade; }
            set { moes_quantidade = value; }
        }

        private double moes_quantidade_folha_kilos;
        public double Moes_quantidade_folha_kilos
        {
            get { return moes_quantidade_folha_kilos; }
            set { moes_quantidade_folha_kilos = value; }
        }
        
        private DateTime moes_dt_criacao;

        public DateTime Moes_dt_criacao
        {
            get { return moes_dt_criacao; }
            set { moes_dt_criacao = value; }
        }

        private int usur_cod_usur;

        public int Usur_cod_usur
        {
            get { return usur_cod_usur; }
            set { usur_cod_usur = value; }
        }

		private bool moes_folha_boa;

		public bool Moes_folha_boa
		{
			get { return moes_folha_boa; }
			set { moes_folha_boa = value; }
		}

        private string familia_produto;

        public string Familia_produto
        {
            get { return familia_produto; }
            set { familia_produto = value; }
        }

    }
}