﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objMapeamentoProdutos
    {
        private int mama_cod_mama;

        public int Mama_cod_mama
        {
            get { return mama_cod_mama; }
            set { mama_cod_mama = value; }
        }
        private int prma_cod_prma_empr;

        public int Prma_cod_prma_empr
        {
            get { return prma_cod_prma_empr; }
            set { prma_cod_prma_empr = value; }
        }

        private int prma_cod_prma_interno;

        public int Prma_cod_prma_interno
        {
            get { return prma_cod_prma_interno; }
            set { prma_cod_prma_interno = value; }
        }

        private bool mama_ativo;

        public bool Mama_ativo
        {
            get { return mama_ativo; }
            set { mama_ativo = value; }
        }
        private DateTime mama_dt_criacao;

        public DateTime Mama_dt_criacao
        {
            get { return mama_dt_criacao; }
            set { mama_dt_criacao = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }

        private int grem_cd_grem_interno;

        public int Grem_cd_grem_interno
        {
            get { return grem_cd_grem_interno; }
            set { grem_cd_grem_interno = value; }
        }
        private int grem_cd_grem_externo;

        public int Grem_cd_grem_externo
        {
            get { return grem_cd_grem_externo; }
            set { grem_cd_grem_externo = value; }
        }
    }
}