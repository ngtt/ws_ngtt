﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objGrupo
    {
        int grem_cd_grem;
        string grem_nome;
        string grem_descricao;

        public int Grem_cd_grem
        {
            get { return grem_cd_grem; }
            set { grem_cd_grem = value; }
        }

        public string Grem_nome
        {
            get { return grem_nome; }
            set { grem_nome = value; }
        }

        public string Grem_descricao
        {
            get { return grem_descricao; }
            set { grem_descricao = value; }
        }

        
        

    }
}