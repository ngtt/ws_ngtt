﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objEndereco
    {

        private int ende_cod_ende;

        public int Ende_cod_ende
        {
            get { return ende_cod_ende; }
            set { ende_cod_ende = value; }
        }
        private string ende_descricao;

        public string Ende_descricao
        {
            get { return ende_descricao; }
            set { ende_descricao = value; }
        }
        private bool ende_ativo;

        public bool Ende_ativo
        {
            get { return ende_ativo; }
            set { ende_ativo = value; }
        }
        private DateTime ende_dt_criacao;

        public DateTime Ende_dt_criacao
        {
            get { return ende_dt_criacao; }
            set { ende_dt_criacao = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }
    }
}