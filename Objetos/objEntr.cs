﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objEntr
    {
        private int entr_sq_entrada;

        public int Entr_sq_entrada
        {
            get { return entr_sq_entrada; }
            set { entr_sq_entrada = value; }
        }

        private int emen_sq_empresa;

        public int Emen_sq_empresa { get => emen_sq_empresa; set => emen_sq_empresa = value; }


        private string entr_tx_placas;

        public string Entr_tx_placas
        {
            get { return entr_tx_placas; }
            set { entr_tx_placas = value; }
        }

        private string entr_tx_nf;



        private DateTime entr_dt_entrada;

        public DateTime Entr_dt_entrada
        {
            get { return entr_dt_entrada; }
            set { entr_dt_entrada = value; }
        }

        private int usur_sq_usuario;

        public int Usur_sq_usuario
        {
            get { return usur_sq_usuario; }
            set { usur_sq_usuario = value; }
        }

        public string Entr_tx_nf { get => entr_tx_nf; set => entr_tx_nf = value; }
    }
}