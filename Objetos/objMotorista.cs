﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsngtt.Objetos
{
    public class objMotorista
    {
        private int moto_cod_moto;

        public int Moto_cod_moto
        {
            get { return moto_cod_moto; }
            set { moto_cod_moto = value; }
        }
        private string moto_cpf_moto;

        public string Moto_cpf_moto
        {
            get { return moto_cpf_moto; }
            set { moto_cpf_moto = value; }
        }
        private string moto_cnh;

        public string Moto_cnh
        {
            get { return moto_cnh; }
            set { moto_cnh = value; }
        }
        private string moto_nome;

        public string Moto_nome
        {
            get { return moto_nome; }
            set { moto_nome = value; }
        }
        private string moto_lograd;

        public string Moto_lograd
        {
            get { return moto_lograd; }
            set { moto_lograd = value; }
        }
        private string moto_num;

        public string Moto_num
        {
            get { return moto_num; }
            set { moto_num = value; }
        }
        private string moto_uf;

        public string Moto_uf
        {
            get { return moto_uf; }
            set { moto_uf = value; }
        }
        private string moto_comple;

        public string Moto_comple
        {
            get { return moto_comple; }
            set { moto_comple = value; }
        }
        private string moto_bairro;

        public string Moto_bairro
        {
            get { return moto_bairro; }
            set { moto_bairro = value; }
        }
        private string moto_cep;

        public string Moto_cep
        {
            get { return moto_cep; }
            set { moto_cep = value; }
        }
        private string moto_ddd;

        public string Moto_ddd
        {
            get { return moto_ddd; }
            set { moto_ddd = value; }
        }
        private string moto_tel;

        public string Moto_tel
        {
            get { return moto_tel; }
            set { moto_tel = value; }
        }
        private string moto_email;

        public string Moto_email
        {
            get { return moto_email; }
            set { moto_email = value; }
        }
        private DateTime moto_dt_criacao;

        public DateTime Moto_dt_criacao
        {
            get { return moto_dt_criacao; }
            set { moto_dt_criacao = value; }
        }
        private int usur_cd_usur;

        public int Usur_cd_usur
        {
            get { return usur_cd_usur; }
            set { usur_cd_usur = value; }
        }






    }
}